-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th10 17, 2020 lúc 07:58 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `st_chef`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'anh dai dien',
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten dang nhap/email',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mat khau',
  `fullname` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten hien thi',
  `phone` int(11) DEFAULT NULL COMMENT 'so dien thoai',
  `role` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: user, 1: admin',
  `valid` tinyint(1) DEFAULT 1 COMMENT 'khoa/mo khoa',
  `no_edit` tinyint(1) DEFAULT 0 COMMENT 'duoc phep edit',
  `is_root` tinyint(1) DEFAULT 0 COMMENT 'tai khoan root'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `group_id`, `img`, `username`, `password`, `fullname`, `phone`, `role`, `valid`, `no_edit`, `is_root`) VALUES
(1, NULL, '', 'admin', '96683bb9ddf6841f487bdb1ab42fcface7c82233', 'Chef Studio', NULL, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin_group`
--

CREATE TABLE `admin_group` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin_group`
--

INSERT INTO `admin_group` (`id`, `name`, `valid`) VALUES
(1, 'Quản trị hệ thống', 1),
(2, 'Quản lý nội dung', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin_log`
--

CREATE TABLE `admin_log` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'category permission',
  `table` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'table focus',
  `row_id` int(11) DEFAULT NULL COMMENT 'id focus',
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'du lieu raw',
  `content_new` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'du lieu update',
  `create_time` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin_log`
--

INSERT INTO `admin_log` (`id`, `admin_id`, `action`, `category`, `table`, `row_id`, `content`, `content_new`, `create_time`) VALUES
(9423, 1, 'DELETE', 'Slider', 'slider', NULL, '', '', '2020-10-17 05:47:21'),
(9424, 1, 'DELETE', 'Slider', 'slider', NULL, '', '', '2020-10-17 05:47:27'),
(9425, 1, 'DELETE', 'Slider', 'slider', NULL, '', '', '2020-10-17 05:47:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL,
  `menuhead_id` int(11) DEFAULT NULL,
  `menugroup_id` int(11) DEFAULT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'fa fa-circle-o text-red' COMMENT 'fa fa-icon',
  `url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` tinyint(1) DEFAULT 1,
  `valid` tinyint(1) DEFAULT 1,
  `is_group` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0 COMMENT 'khong co menu nay'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `menuhead_id`, `menugroup_id`, `category`, `name`, `icon`, `url`, `position`, `valid`, `is_group`, `deleted`) VALUES
(13, NULL, 18, 'admin', 'tài khoản quản trị', 'fa fa-circle-o text-red', 'quan-tri/tai-khoan-quan-tri.html', 1, 1, NULL, 0),
(14, 2, NULL, 'page', 'về chúng tôi', 'fa fa-info', 'quan-tri/ve-chung-toi.html', 1, 1, NULL, 0),
(16, 2, NULL, 'faq', 'FAQ', 'fa fa-question', 'quan-tri/faq.html', 1, 1, NULL, 0),
(17, NULL, 26, 'news', 'nội dung', 'fa fa-circle-o text-red', 'quan-tri/noi-dung.html', 6, 1, NULL, 0),
(18, 1, NULL, NULL, 'Quản trị', 'fa  fa-user-circle-o', NULL, 1, 1, 1, 0),
(20, 1, NULL, NULL, 'Under more', 'fa fa-circle-o text-red', NULL, 1, 0, 1, 0),
(21, NULL, 18, 'admin_group', 'nhóm quản trị', 'fa fa-circle-o text-red', 'quan-tri/nhom-quan-tri.html', 1, 1, NULL, 0),
(22, 3, NULL, 'site_setting', 'thông tin website', 'fa fa-cog', 'quan-tri/thong-tin-website.html', 9, 1, NULL, 0),
(23, 2, NULL, 'contact', 'Liên hệ', 'fa fa-address-card-o', 'quan-tri/lien-he.html', 99, 1, NULL, 1),
(24, 3, NULL, 'customer_comment', 'cảm nhận khách hàng', 'fa fa-comment', 'quan-tri/cam-nhan-khach-hang', 1, 0, NULL, 1),
(25, NULL, 26, 'news_category', 'phân loại', 'fa fa-circle-o text-red', 'quan-tri/phan-loai.html', 5, 1, NULL, 0),
(26, 2, NULL, NULL, 'Tin tức', 'fa fa-file-text-o', NULL, 1, 1, 1, 0),
(27, NULL, NULL, 'profile', 'profile', 'fa fa-circle-o text-red', 'quan-tri/profile.html', 1, 1, NULL, 0),
(29, 4, NULL, NULL, 'Sản phẩm', 'fa fa-clone', NULL, 1, 1, 1, 0),
(30, NULL, 29, 'product', 'sản phẩm', 'fa fa-circle-o text-red', 'quan-tri/san-pham.html', 2, 1, NULL, 0),
(31, NULL, 29, 'product_type', 'Loại sản phẩm', 'fa fa-circle-o text-red', 'quan-tri/loai-san-pham', 1, 0, NULL, 0),
(32, NULL, 18, 'admin_function', 'chức năng quản trị', 'fa fa-circle-o text-red', 'quan-tri/chuc-nang-quan-tri.html', 1, 1, NULL, 0),
(33, 5, NULL, 'slider', 'Slider', 'fa fa-circle-o text-red', 'quan-tri/slider.html', 1, 1, NULL, 0),
(34, 5, NULL, 'banner', 'Banner', 'fa fa-circle-o text-red', 'quan-tri/banner.html', 1, 1, NULL, 0),
(35, 5, NULL, 'menu', 'Menu ngang', 'fa fa-circle-o text-red', 'quan-tri/menu-ngang', 1, 0, NULL, 0),
(36, 5, NULL, 'menu_vertical', 'Menu dọc', 'fa fa-circle-o text-red', 'quan-tri/menu-doc', 1, 0, NULL, 0),
(37, 5, NULL, 'menu_banner', 'Menu_banner', 'fa fa-circle-o text-red', 'quan-tri/menu_banner', 1, 0, NULL, 0),
(38, NULL, 29, 'product_category', 'phân loại sản phẩm', 'fa fa-circle-o text-red', 'quan-tri/phan-loai-san-pham.html', 1, 1, NULL, 0),
(39, NULL, 29, 'brand', 'Dòng', 'fa fa-circle-o text-red', 'quan-tri/dong.html', 4, 1, NULL, 0),
(40, NULL, 42, 'agency', 'Điểm bán', 'fa fa-circle-o text-red', 'quan-tri/diem-ban.html', 2, 1, NULL, 0),
(41, NULL, 42, 'agency_category', 'phân loại điểm bán', 'fa fa-circle-o text-red', 'quan-tri/phan-loai-diem-ban.html', 1, 1, NULL, 0),
(42, 2, NULL, NULL, 'Điểm bán', 'fa fa-shopping-bag', NULL, 3, 1, 1, 0),
(43, 4, NULL, 'order', 'Đơn hàng', 'fa fa-files-o', 'quan-tri/don-hang.html', 1, 1, NULL, 0),
(45, 2, NULL, 'comment', 'Bình luận', 'fa fa-circle-o text-red', 'quan-tri/binh-luan.html', 1, 1, NULL, 0),
(47, 2, NULL, 'popup', 'Popup', 'fa fa-circle-o text-red', 'quan-tri/popup.html', 6, 1, NULL, 0),
(48, NULL, 29, 'price', 'Khoảng giá tiền sản phẩm', 'fa fa-circle-o text-red', 'quan-tri/khoang-gia-tien-san-pham.html', 3, 1, NULL, 0),
(49, 2, NULL, 'trademark', 'Thương hiệu', 'fa fa-circle-o text-red', 'quan-tri/thuong-hieu.html', 1, 1, NULL, 0),
(50, NULL, 26, 'news_discount', 'Khuyến mại', 'fa fa-circle-o text-red', 'quan-tri/khuyen-mai.html', 8, 1, NULL, 0),
(51, 2, NULL, 'bank', 'Tài khoản ngân hàng', 'fa fa-circle-o text-red', 'quan-tri/tai-khoan-ngan-hang.html', 5, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin_menuhead`
--

CREATE TABLE `admin_menuhead` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `position` tinyint(1) DEFAULT 1,
  `valid` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `admin_menuhead`
--

INSERT INTO `admin_menuhead` (`id`, `name`, `position`, `valid`) VALUES
(1, 'Hệ thống', 1, 1),
(2, 'Nội dung', 2, 1),
(3, 'Cấu hình', 99, 1),
(4, 'Sản phẩm', 1, 1),
(5, 'THEME', 4, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin_permission`
--

CREATE TABLE `admin_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `category` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `category_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin_permission`
--

INSERT INTO `admin_permission` (`id`, `name`, `key`, `category`, `category_name`, `valid`) VALUES
(3619, 'Xem', 'read', 'admin', 'tài khoản quản trị', 1),
(3620, 'Sửa', 'edit', 'admin', 'tài khoản quản trị', 1),
(3621, 'Xóa', 'delete', 'admin', 'tài khoản quản trị', 1),
(3622, 'Xem', 'read', 'page', 'về chúng tôi', 1),
(3623, 'Sửa', 'edit', 'page', 'về chúng tôi', 1),
(3624, 'Xóa', 'delete', 'page', 'về chúng tôi', 1),
(3952, 'Xem', 'read', 'faq', 'FAQ', 0),
(3953, 'Sửa', 'edit', 'faq', 'FAQ', 0),
(3954, 'Xóa', 'delete', 'faq', 'FAQ', 0),
(4132, 'Xem', 'read', 'news', 'nội dung', 1),
(4133, 'Sửa', 'edit', 'news', 'nội dung', 1),
(4134, 'Xóa', 'delete', 'news', 'nội dung', 1),
(4420, 'Xem', 'read', 'admin_group', 'nhóm quản trị', 1),
(4421, 'Sửa', 'edit', 'admin_group', 'nhóm quản trị', 1),
(4422, 'Xóa', 'delete', 'admin_group', 'nhóm quản trị', 1),
(4678, 'Xem', 'read', 'site_setting', 'thông tin website', 1),
(4679, 'Sửa', 'edit', 'site_setting', 'thông tin website', 1),
(4680, 'Xóa', 'delete', 'site_setting', 'thông tin website', 1),
(4726, 'Xem', 'read', 'contact', 'Liên hệ', 0),
(4727, 'Sửa', 'edit', 'contact', 'Liên hệ', 0),
(4728, 'Xóa', 'delete', 'contact', 'Liên hệ', 0),
(6466, 'Xem', 'read', 'customer_comment', 'cảm nhận khách hàng', 0),
(6467, 'Sửa', 'edit', 'customer_comment', 'cảm nhận khách hàng', 0),
(6468, 'Xóa', 'delete', 'customer_comment', 'cảm nhận khách hàng', 0),
(6709, 'Xem', 'read', 'news_category', 'phân loại', 1),
(6710, 'Sửa', 'edit', 'news_category', 'phân loại', 1),
(6711, 'Xóa', 'delete', 'news_category', 'phân loại', 1),
(6808, 'Xem', 'read', 'profile', 'profile', 1),
(6809, 'Sửa', 'edit', 'profile', 'profile', 1),
(6810, 'Xóa', 'delete', 'profile', 'profile', 1),
(10069, 'Xem', 'read', 'product', 'sản phẩm', 1),
(10070, 'Sửa', 'edit', 'product', 'sản phẩm', 1),
(10071, 'Xóa', 'delete', 'product', 'sản phẩm', 1),
(10075, 'Xem', 'read', 'product_type', 'Loại sản phẩm', 1),
(10076, 'Sửa', 'edit', 'product_type', 'Loại sản phẩm', 1),
(10077, 'Xóa', 'delete', 'product_type', 'Loại sản phẩm', 1),
(10138, 'Xem', 'read', 'admin_function', 'chức năng quản trị', 1),
(10139, 'Sửa', 'edit', 'admin_function', 'chức năng quản trị', 1),
(10140, 'Xóa', 'delete', 'admin_function', 'chức năng quản trị', 1),
(10141, 'Xem', 'read', 'slider', 'Slider', 1),
(10142, 'Sửa', 'edit', 'slider', 'Slider', 1),
(10143, 'Xóa', 'delete', 'slider', 'Slider', 1),
(10270, 'Xem', 'read', 'banner', 'Banner', 1),
(10271, 'Sửa', 'edit', 'banner', 'Banner', 1),
(10272, 'Xóa', 'delete', 'banner', 'Banner', 1),
(10339, 'Xem', 'read', 'menu', 'Menu ngang', 0),
(10340, 'Sửa', 'edit', 'menu', 'Menu ngang', 0),
(10341, 'Xóa', 'delete', 'menu', 'Menu ngang', 0),
(10555, 'Xem', 'read', 'menu_vertical', 'Menu dọc', 0),
(10556, 'Sửa', 'edit', 'menu_vertical', 'Menu dọc', 0),
(10557, 'Xóa', 'delete', 'menu_vertical', 'Menu dọc', 0),
(10720, 'Xem', 'read', 'menu_banner', 'Menu_banner', 0),
(10721, 'Sửa', 'edit', 'menu_banner', 'Menu_banner', 0),
(10722, 'Xóa', 'delete', 'menu_banner', 'Menu_banner', 0),
(10891, 'Xem', 'read', 'product_category', 'phân loại sản phẩm', 1),
(10892, 'Sửa', 'edit', 'product_category', 'phân loại sản phẩm', 1),
(10893, 'Xóa', 'delete', 'product_category', 'phân loại sản phẩm', 1),
(11632, 'Xem', 'read', 'brand', 'Dòng', 1),
(11633, 'Sửa', 'edit', 'brand', 'Dòng', 1),
(11634, 'Xóa', 'delete', 'brand', 'Dòng', 1),
(13546, 'Xem', 'read', 'agency', 'Điểm bán', 1),
(13547, 'Sửa', 'edit', 'agency', 'Điểm bán', 1),
(13548, 'Xóa', 'delete', 'agency', 'Điểm bán', 1),
(13549, 'Xem', 'read', 'agency_category', 'phân loại điểm bán', 1),
(13550, 'Sửa', 'edit', 'agency_category', 'phân loại điểm bán', 1),
(13551, 'Xóa', 'delete', 'agency_category', 'phân loại điểm bán', 1),
(13834, 'Xem', 'read', 'order', 'Đơn hàng', 1),
(13835, 'Sửa', 'edit', 'order', 'Đơn hàng', 1),
(13836, 'Xóa', 'delete', 'order', 'Đơn hàng', 1),
(13858, 'Xem', 'read', 'comment', 'Bình luận', 1),
(13859, 'Sửa', 'edit', 'comment', 'Bình luận', 1),
(13860, 'Xóa', 'delete', 'comment', 'Bình luận', 1),
(16309, 'Xem', 'read', 'popup', 'Popup', 1),
(16310, 'Sửa', 'edit', 'popup', 'Popup', 1),
(16311, 'Xóa', 'delete', 'popup', 'Popup', 1),
(16705, 'Xem', 'read', 'price', 'Khoảng giá tiền sản phẩm', 1),
(16706, 'Sửa', 'edit', 'price', 'Khoảng giá tiền sản phẩm', 1),
(16707, 'Xóa', 'delete', 'price', 'Khoảng giá tiền sản phẩm', 1),
(27880, 'Xem', 'read', 'trademark', 'Thương hiệu', 1),
(27881, 'Sửa', 'edit', 'trademark', 'Thương hiệu', 1),
(27882, 'Xóa', 'delete', 'trademark', 'Thương hiệu', 1),
(75597, 'Xem', 'read', 'news_discount', 'Khuyến mại', 1),
(75598, 'Sửa', 'edit', 'news_discount', 'Khuyến mại', 1),
(75599, 'Xóa', 'delete', 'news_discount', 'Khuyến mại', 1),
(75600, 'Xem', 'read', 'bank', 'Tài khoản ngân hàng', 1),
(75601, 'Sửa', 'edit', 'bank', 'Tài khoản ngân hàng', 1),
(75602, 'Xóa', 'delete', 'bank', 'Tài khoản ngân hàng', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin_permission_group`
--

CREATE TABLE `admin_permission_group` (
  `group_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `admin_permission_group`
--

INSERT INTO `admin_permission_group` (`group_id`, `permission_id`) VALUES
(2, 4132),
(2, 4133),
(2, 4134),
(2, 3622),
(2, 3623),
(2, 3624),
(2, 10069),
(2, 10070),
(2, 10071),
(2, 10075),
(2, 10076),
(2, 10077),
(1, 3622),
(1, 3623),
(1, 3624),
(1, 4420),
(1, 4421),
(1, 4422),
(1, 4132),
(1, 4133),
(1, 4134),
(1, 6709),
(1, 6710),
(1, 6711),
(1, 10075),
(1, 10076),
(1, 10077),
(1, 6808),
(1, 6809),
(1, 6810),
(1, 10069),
(1, 10070),
(1, 10071),
(1, 3619),
(1, 3620),
(1, 3621),
(1, 4678),
(1, 4679),
(1, 4680);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` tinyint(2) NOT NULL DEFAULT 99,
  `valid` tinyint(1) NOT NULL DEFAULT 1,
  `is_hot` tinyint(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `agency`
--

INSERT INTO `agency` (`id`, `category_id`, `name`, `address`, `email`, `phone`, `url`, `note`, `image`, `position`, `valid`, `is_hot`) VALUES
(1, 1, 'The Butcher Shop by GOFOOD - 161 Trung Kính', '161 Trung Kính, Cầu Giấy, Hà Nội', 'contact@chefstudio.vn', '0898 582 828 - 0898 550 000', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.4288618909914!2d105.79236791490453!3d21.015519486005548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab583283394f%3A0xbe9617fb035672e6!2sThe+Butcher+Shop+by+GOFOOD!5e0!3m2!1sen!2s!4v1541342482727', 'Mở cửa từ 8h30-12h15 và 13h30-19h15 hằng ngày', '/uploads/r/gofood-161-trung-kinh.jpg', 2, 0, 1),
(2, 1, 'Chef Studio', 'D04-41 An Phú Shop Villa - KDT Dương Nội, Hà Đông, Hà Nội', 'contact@chefstudio.vn', '084 888 18 00', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7450.4859808949095!2d105.7566768!3d20.9828946!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313453cc17ee3283%3A0x23a9b76da646d14d!2sAn%20Ph%C3%BA%20Shop%20Villa!5e0!3m2!1svi!2s!4v1601866538425!5m2!1svi!2s', '8:30 sáng - 19h15 tất cả các ngày trong tuần', '/uploads/r/diem-ban/chef-studio-sai-gon.jpg', 1, 1, 1),
(3, 1, 'Thực phẩm cao cấp Gofood - 413 Thuỵ Khuê', '413 Thuỵ Khuê, Tây Hồ, Hà Nội', 'contact@gofood.vn', '0898583838', 'https://www.google.com/maps/place/Th%E1%BB%B1c+Ph%E1%BA%A9m+Cao+C%E1%BA%A5p+GOFOOD/@21.1693067,105.7645006,12.74z/data=!4m5!3m4!1s0x0:0x84238da1b6c6e6f6!8m2!3d21.0473593!4d105.8108614?hl=vi-VN', 'Mở cửa từ 8h30-12h15 và 13h30-19h15 hằng ngày', '', 2, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `agency_category`
--

CREATE TABLE `agency_category` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'duong danh anh dai dien',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten danh muc',
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mo ta danh muc',
  `is_hot` tinyint(1) NOT NULL DEFAULT 0,
  `position` tinyint(1) DEFAULT 99 COMMENT 'vi tri hien thi',
  `meta_title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'meta title',
  `meta_desc` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'meta desc',
  `valid` tinyint(1) DEFAULT 1 COMMENT 'hien thi/khong hien thi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `agency_category`
--

INSERT INTO `agency_category` (`id`, `image`, `name`, `slug`, `desc`, `is_hot`, `position`, `meta_title`, `meta_desc`, `valid`) VALUES
(1, '', 'Chef Studio Hà Nội', '', NULL, 0, 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_card` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valid` tinyint(4) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_hot` tinyint(1) NOT NULL DEFAULT 0,
  `position` tinyint(2) NOT NULL DEFAULT 99,
  `valid` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banner`
--

INSERT INTO `banner` (`id`, `name`, `url`, `image`, `image_2`, `is_hot`, `position`, `valid`) VALUES
(1, 'Sản phẩm nổi bật', 'https://chefstudio.vn/noi-gang-phu-gom/brands/staub', '/uploads/r/banner/898x360/san-pham-noi-bat-3.jpg', '/uploads/r/banner/400x352/noi-gang-staub-1.jpg', 0, 6, 1),
(4, 'Thiết bị gia dụng', '/thiet-bi-gia-dung', '/uploads/r/banner/898x360/1601896069_thiet-bi-gia-dung-3.jpg', '/uploads/r/banner/400x352/thiet-bi-gia-dung-4.jpg', 1, 2, 1),
(5, 'Dao, thìa & dĩa', '/dao', '/uploads/r/banner/898x360/dao-thia-dia.jpg', '/uploads/r/banner/898x360/dao-thia-dia-1.jpg', 0, 5, 1),
(7, 'Nồi', '/noi', '/uploads/r/banner/898x360/noi.jpg', '/uploads/r/banner/400x352/noi1.jpg', 1, 3, 1),
(8, 'Khay và bếp nướng', '/khay-va-bep-nuong', '/uploads/r/banner/898x360/khay-va-bep-nuong.jpg', '/uploads/r/banner/400x352/khay-va-bep-nuong-01.jpg', 1, 5, 1),
(9, 'Dao và Kéo', '/dao-va-keo', '/uploads/r/1566402241_bo-dao-lam-bep-6-mon-354-2015-11-0-0-product.jpg', '/uploads/r/img5.png', 1, 6, 1),
(11, 'Chảo', '/chao', '/uploads/r/banner/898x360/chao.jpg', '/uploads/r/banner/400x352/chao1.jpg', 1, 4, 1),
(12, 'Bếp ', '/bep', '/uploads/r/banner/898x360/bep-1.jpg', '/uploads/r/banner/400x352/bep.jpg', 1, 1, 1),
(13, 'Robot hút bụi', '/robot-hut-bui', '/uploads/r/banner/898x360/1601895244_robot-hut-bui.jpg', '/uploads/r/banner/400x352/robot-hut-bui-1.jpg', 1, 7, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` tinyint(2) NOT NULL DEFAULT 99,
  `valid` tinyint(1) NOT NULL DEFAULT 1,
  `trademark_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `brand`
--

INSERT INTO `brand` (`id`, `name`, `image`, `position`, `valid`, `trademark_id`) VALUES
(2, ' Shun Classic', '/uploads/r/dong-san-pham/shun-classic.jpg', 1, 1, 10),
(3, 'Shun Premier', '/uploads/r/dong-san-pham/shun-premier.jpg', 2, 1, 10),
(4, 'Shun Kanso', '/uploads/r/dong-san-pham/shun-kanso.jpg', 3, 1, 10),
(5, ' Wasabi Black', '/uploads/r/dong-san-pham/wasabi-black.jpg', 4, 1, 10),
(6, 'Bologna Granitium', '/uploads/r/dong-san-pham/dong-bologna-granitium-ballarini.jpg', 1, 1, 11),
(7, 'Bologna Keravis', '/uploads/r/dong-san-pham/dong-bologna-keravis-ballarini.jpg', 2, 1, 11),
(8, 'Positano', '/uploads/r/dong-san-pham/dong-positano-ballarini.jpg', 3, 1, 11),
(9, 'Siena', '/uploads/r/dong-san-pham/dong-siena-ballarini.jpg', 4, 1, 11);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `product` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `note` int(11) NOT NULL,
  `created_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `cart`
--

INSERT INTO `cart` (`id`, `product`, `name`, `phone`, `email`, `address`, `note`, `created_time`) VALUES
(1, '{\"1\":\"1\",\"2\":\"3\"}', 0, 974500326, 0, 0, 0, '2018-07-25 11:50:18'),
(2, '{\"1\":\"2\",\"2\":\"2\"}', 0, 974500326, 0, 0, 0, '2018-07-25 12:06:33'),
(3, '{\"1\":\"1\",\"2\":\"2\"}', 0, 974500326, 0, 0, 0, '2018-07-25 12:07:19'),
(4, '{\"1\":\"3\"}', 0, 974500326, 0, 0, 0, '2018-07-25 12:07:57'),
(5, '{\"2\":\"1\"}', 0, 974500326, 0, 0, 0, '2018-07-25 12:08:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten nguoi lien he',
  `email` varchar(125) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email nguoi lien he',
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'so dien thoai',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'dia chi',
  `content` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'noi dung lien he',
  `valid` tinyint(1) DEFAULT 1,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `phone`, `address`, `content`, `valid`, `create_time`) VALUES
(1, 'A Nguyen Van', 'anguyenvan@yopmail.com', '0974555326', 'Hà Nội', 'Hà Nội Việt Nam', 0, '2017-01-20 07:09:53'),
(3, 'test', '', '09767779787', NULL, '', 1, '2018-10-31 07:50:26'),
(4, 'test', '', '09767779787', NULL, '', 1, '2018-10-31 07:51:53'),
(5, 'test', '', '09767779787', NULL, '', 1, '2018-10-31 07:53:39'),
(6, 'test', '', '09767779787', NULL, '', 1, '2018-10-31 07:54:52'),
(7, 'test', '', '+84 24 3754 782', NULL, '', 1, '2018-10-31 08:00:23'),
(8, 'Tẽdt', 'test@gmail.com', '09876665555', NULL, '', 1, '2018-11-10 08:49:46'),
(9, 'Đỗ Văn Tư', 'tubean1504@gmail.com', '09846133933', NULL, 'fsdfs', 1, '2018-12-02 05:44:37'),
(10, 'Đỗ Văn Tư', 'tubean1504@gmail.com', '09846133933', NULL, 'fsdfs', 1, '2018-12-02 05:44:38');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer_comment`
--

CREATE TABLE `customer_comment` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'anh dai dien',
  `user_id` int(11) DEFAULT NULL,
  `fullname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten khach hang',
  `parent_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT 'news/product',
  `star` tinyint(2) DEFAULT 80 COMMENT 'danh gia sao',
  `career` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'chuc nghiep',
  `topic` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'noi dung cam nhan',
  `role` tinyint(1) DEFAULT 1 COMMENT '0: admin, 1: user',
  `position` tinyint(4) DEFAULT 99 COMMENT 'vi tri (asc)',
  `is_deleted` tinyint(1) DEFAULT 0,
  `valid` tinyint(1) DEFAULT 1 COMMENT 'public/unpublic',
  `is_useful` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `email` varchar(65) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer_comment`
--

INSERT INTO `customer_comment` (`id`, `image`, `user_id`, `fullname`, `parent_id`, `category_id`, `type`, `star`, `career`, `topic`, `desc`, `role`, `position`, `is_deleted`, `valid`, `is_useful`, `create_time`, `sex`, `email`) VALUES
(1, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 80, NULL, 'test 1', '2121', 1, 99, 1, 1, NULL, '2019-08-06 15:03:22', NULL, NULL),
(2, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 80, NULL, 'têt', 'ưee', 1, 99, 1, 0, NULL, '2019-08-06 15:10:18', NULL, NULL),
(3, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 80, NULL, 'test', 'test', 1, 99, 1, 0, NULL, '2019-08-06 15:11:54', NULL, NULL),
(4, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 80, NULL, 'test112121', '312313', 1, 99, 1, 0, NULL, '2019-08-06 15:13:36', NULL, NULL),
(5, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 80, NULL, 'test 5555', '64eeee', 1, 99, 1, 0, NULL, '2019-08-06 15:14:43', NULL, NULL),
(6, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 80, NULL, 'tesst 4311', '31313 131231', 1, 99, 1, 0, NULL, '2019-08-06 15:14:51', NULL, NULL),
(7, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 80, NULL, 'tesst 4311', '31313 131231', 1, 99, 1, 0, NULL, '2019-08-06 15:14:51', NULL, NULL),
(8, NULL, 3, 'Đỗ Văn Tư', NULL, 12, 2, 80, NULL, 'test', '212', 1, 99, 1, 0, NULL, '2019-08-06 15:39:24', NULL, NULL),
(9, NULL, 3, 'Đỗ Văn Tư', NULL, 12, 2, 80, NULL, 'test star', 'test', 1, 99, 1, 1, NULL, '2019-08-06 16:07:03', NULL, NULL),
(10, NULL, 3, 'Đỗ Văn Tư', NULL, 12, 2, 60, NULL, 'test', 'q', 1, 99, 1, 1, NULL, '2019-08-06 16:07:13', NULL, NULL),
(11, NULL, 3, 'Đỗ Văn Tư', NULL, 12, 2, 60, NULL, 'test', 'q', 1, 99, 1, 1, NULL, '2019-08-06 16:07:13', NULL, NULL),
(12, NULL, 3, 'Đỗ Văn Tư', NULL, 12, 2, 100, NULL, 'test', 'tessssss', 1, 99, 1, 1, NULL, '2019-08-06 16:08:24', NULL, NULL),
(13, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 40, NULL, 'test 2 sao', 'tôi đánh giá rất cao chất lượng sản phẩm', 1, 99, 1, 0, NULL, '2019-08-06 17:00:54', NULL, NULL),
(14, NULL, 3, 'Đỗ Văn Tư', NULL, 6, 1, NULL, NULL, NULL, 'test comment', 1, 99, 1, 0, NULL, '2019-08-06 17:01:12', NULL, NULL),
(15, NULL, 3, 'Đỗ Văn Tư', 14, 6, 1, NULL, NULL, NULL, 'test 1111', 1, 99, 0, 0, NULL, '2019-08-06 17:01:20', NULL, NULL),
(16, NULL, 3, 'Đỗ Văn Tư', NULL, 13, 2, 40, NULL, 'test', 'san pham tot', 1, 99, 1, 0, NULL, '2019-08-08 09:34:08', NULL, NULL),
(17, NULL, 3, 'Đỗ Văn Tư', NULL, 7, 1, NULL, NULL, NULL, 'tesst', 1, 99, 1, 0, NULL, '2019-08-08 10:10:35', NULL, NULL),
(18, NULL, 3, 'Đỗ Văn Tư', 17, 7, 1, NULL, NULL, NULL, 'test', 1, 99, 1, 0, NULL, '2019-08-08 10:10:41', NULL, NULL),
(19, NULL, 3, 'Đỗ Văn Tư', 17, 7, 1, NULL, NULL, NULL, 'testt', 1, 99, 0, 0, NULL, '2019-08-08 10:11:06', NULL, NULL),
(20, NULL, 3, 'Đỗ Văn Tư', NULL, 5, 1, NULL, NULL, NULL, 'test', 1, 99, 1, 0, NULL, '2019-08-12 16:45:24', NULL, NULL),
(22, NULL, 3, 'Đỗ Văn Tư', 20, 5, 1, NULL, NULL, NULL, 'test', 1, 99, 0, 0, NULL, '2019-08-12 16:47:45', NULL, NULL),
(23, NULL, 3, 'Đỗ Văn Tư', 20, 5, 1, NULL, NULL, NULL, 'test', 1, 99, 0, 1, NULL, '2019-08-12 16:53:54', NULL, NULL),
(24, NULL, 3, 'Đỗ Văn Tư', 20, 5, 1, NULL, NULL, NULL, 'test 123', 1, 99, 0, 1, NULL, '2019-08-12 16:54:11', NULL, NULL),
(25, NULL, 3, 'Đỗ Văn Tư', 20, 5, 1, NULL, NULL, NULL, 'testt 1123231', 1, 99, 0, 1, NULL, '2019-08-12 16:56:22', NULL, NULL),
(26, NULL, 3, 'Đỗ Văn Tư', 20, 5, 1, NULL, NULL, NULL, 'test 123123', 1, 99, 0, 1, NULL, '2019-08-12 16:58:35', NULL, NULL),
(27, NULL, 3, 'Đỗ Văn Tư', 20, 5, 1, NULL, NULL, NULL, 'tesstttt', 1, 99, 0, 1, NULL, '2019-08-12 16:59:53', NULL, NULL),
(28, NULL, 3, 'Đỗ Văn Tư', 20, 5, 1, NULL, NULL, NULL, 'testttt', 1, 99, 0, 1, NULL, '2019-08-12 17:00:09', NULL, NULL),
(29, NULL, 3, 'Đỗ Văn Tư', 20, 5, 1, NULL, NULL, NULL, 'test', 1, 99, 0, 1, NULL, '2019-08-12 17:00:44', NULL, NULL),
(30, NULL, 1, 'Vũ Thị Lan', 20, 5, 1, 80, NULL, NULL, 'testt 2121', 0, 99, 0, 1, NULL, '2019-08-14 22:43:04', NULL, NULL),
(31, NULL, 7, 'Lan Vũ', NULL, 13, 2, NULL, NULL, NULL, 'tôi muốn mua chiếc chảo của bạn', 1, 99, 1, 0, NULL, '2019-09-04 11:07:01', NULL, NULL),
(32, NULL, NULL, 'tu', NULL, 7, 2, NULL, NULL, NULL, 'test', 1, 99, 1, 0, NULL, '2019-09-11 03:12:17', NULL, 'test@gmail.com'),
(33, NULL, NULL, 'Tu', NULL, 10, 2, NULL, NULL, NULL, 'Test', 1, 99, 1, 0, NULL, '2019-09-11 15:44:19', NULL, 'tutest@gmail.com'),
(34, NULL, NULL, 'Lan Vũ', NULL, 19, 1, NULL, NULL, NULL, 'Chảo gang có nấu được trên bếp từ không ạ?', 1, 99, 1, 0, NULL, '2019-09-16 10:22:57', NULL, 'vuthilan97vc@gmail.com'),
(35, 'uploads/image_comment/Screen Shot 2019-09-23 at 5.47.12 PM.png', NULL, 'Test', NULL, 13, 1, NULL, NULL, NULL, 'anh tét', 1, 99, 1, 0, NULL, '2019-09-23 10:55:12', NULL, 'test@gmail.co'),
(36, 'uploads/image_comment/Screen Shot 2019-09-23 at 5.47.12 PM.png', NULL, 'Test', NULL, 13, 1, NULL, NULL, NULL, 'bcc', 1, 99, 1, 0, NULL, '2019-09-23 10:55:38', NULL, 'test@gmail.co'),
(37, 'uploads/image_comment/FB_IMG_1566603407462.jpg', NULL, 'Lan ', NULL, 14, 2, NULL, NULL, NULL, 'Shop có chảo này ko ạ', 1, 99, 1, 0, NULL, '2019-09-23 11:23:40', NULL, 'vuthilan97vc@gmail.com'),
(38, '', NULL, 'Quang Anh', NULL, 12, 2, NULL, NULL, NULL, 'Chảo này rán đậu ngon lắm đấy nhỉ.', 1, 99, 1, 0, NULL, '2019-09-24 17:04:47', NULL, 'trungta.sth@gmail.com'),
(39, 'uploads/image_comment/chao-4.jpg', NULL, 'Vũ Thị Lan', NULL, 15, 2, NULL, NULL, NULL, 'chảo', 1, 99, 1, 0, NULL, '2019-09-25 09:13:14', NULL, 'vuthilan97vc@gmail.com'),
(40, 'uploads/image_comment/chao-3.jpg', NULL, 'Vũ Thị Lan', NULL, 11, 1, NULL, NULL, NULL, 'chảo này còn ko ạ', 1, 99, 1, 0, NULL, '2019-09-25 09:19:10', NULL, 'vuthilan97vc@gmail.com'),
(41, NULL, 1, 'Chef Studio', 40, 11, 1, 80, NULL, NULL, 'Dạ chảo này Chef còn ạ', 0, 99, 0, 1, NULL, '2019-09-25 10:26:26', NULL, NULL),
(42, 'uploads/image_comment/crs8-chao-thep-carbon-lodge-20-3-cm.jpg', NULL, 'test', NULL, 16, 2, NULL, NULL, NULL, 'test', 1, 99, 1, 0, NULL, '2019-09-30 04:19:55', NULL, 'test123@gmail.com'),
(43, 'uploads/image_comment/crs10-chao-thep-carbon-lodge-25-4-cm-1.jpg', NULL, 'test', 42, 16, 2, NULL, NULL, NULL, 'rttttt', 1, 99, 0, 0, NULL, '2019-09-30 04:20:08', NULL, 'test123@gmail.com'),
(44, 'uploads/image_comment/crs10-chao-thep-carbon-lodge-25-4-cm-4.jpg', NULL, 'test', 42, 16, 2, NULL, NULL, NULL, 'rwwwww', 1, 99, 0, 0, NULL, '2019-09-30 04:20:16', NULL, 'test123@gmail.com'),
(45, 'uploads/image_comment/khuyen-mai-tang-tay-cam-lodge.jpg', NULL, 'Vũ Thị Lan', NULL, 17, 2, NULL, NULL, NULL, 'Tôi muốn đặt chảo này, sdt 0964873904', 1, 99, 1, 0, NULL, '2019-09-30 06:37:38', NULL, 'vuthilan97vc@gmail.com'),
(46, '', NULL, 'Vũ Thị Lan', NULL, 17, 2, NULL, NULL, NULL, 'tôi muốn đặt chảo 20.3 cm 0964873904', 1, 99, 1, 0, NULL, '2019-09-30 06:38:23', NULL, 'vuthilan97vc@gmail.com'),
(47, 'uploads/image_comment/chao-gang-lodge-09.jpg', NULL, 'Vũ Thị Lan', NULL, 29, 2, NULL, NULL, NULL, 'Dạ chảo này giá bao nhiêu ạ', 1, 99, 1, 1, NULL, '2019-10-08 04:55:34', NULL, 'vuthilan97vc@gmail.com'),
(48, '', NULL, 'Vũ Thị Lan', 47, 29, 2, NULL, NULL, NULL, '.', 1, 99, 1, 1, NULL, '2019-10-08 04:56:29', NULL, 'vuthilan97vc@gmail.com'),
(49, '', NULL, 'Nguyễn Thị Thuận', NULL, 27, 2, NULL, NULL, NULL, 'tôi muốn bạn giới thiệu các loại chảo để mua. tên Thuận, điện thoại 0915932888', 1, 99, 1, 1, NULL, '2020-02-20 16:14:45', NULL, 'thuannt2@yahoo.com'),
(50, NULL, 1, 'Chef Studio', 49, 27, 2, 80, NULL, NULL, 'Dạ Chef xin chào chị ạ, Chefstudio sẽ liên hệ lại với chị sớm vào ngày mai ạ', 0, 99, 0, 1, NULL, '2020-02-27 15:16:25', NULL, NULL),
(51, '', NULL, 'Lê văn Uyên ', NULL, 473, 2, NULL, NULL, NULL, 'Có hàng giao ngay không ạ ?', 1, 99, 1, 1, NULL, '2020-03-01 14:20:29', NULL, 'Lvuyen1952@gmail.com'),
(52, '', NULL, 'Nguyên Xuân Sơn', NULL, 169, 2, NULL, NULL, NULL, 'Chảo gang nướng Staub 26 cm - Màu xám\n        Ưu điểm của nó là gì mà đắt thế  gấp  15 lân chảo gang Vạn lợi VN', 1, 99, 1, 1, NULL, '2020-07-03 06:56:55', NULL, 'sonnguyen08@gmailo.com'),
(53, '', NULL, 'Lisa Hirate', NULL, 188, 2, NULL, NULL, NULL, 'Hello. Can you send to Japan?', 1, 99, 1, 1, NULL, '2020-07-21 02:48:39', NULL, 'kahuna.5@i.softbank.jp'),
(54, NULL, 1, 'Chef Studio', 53, 188, 2, 80, NULL, NULL, 'Hi you, Now, Chef Studio can send to Viet Nam, thanks', 0, 99, 0, 1, NULL, '2020-07-29 09:03:26', NULL, NULL),
(57, NULL, 1, 'Chef Studio', 52, 169, 2, 80, NULL, NULL, 'Dạ chảo gang Staub được sản xuất bán thủ công, nhập khẩu trực tiếp từ Pháp, bên cạnh vẻ đẹp sang trọng; Chất liệu gang cao cấp, giữ nhiệt cho các món áp chảo, nướng, bỏ lò tuyệt ngon…; Khả năng chống dính tự nhiên an toàn cho sức khỏe; Lớp tráng men bền màu với 3 lớp cao cấp; Gang nguyên khối chắc chắn ạ,... ', 0, 99, 0, 1, NULL, '2020-07-29 09:14:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_related` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT 99,
  `valid` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0-ngang 1-doc',
  `position` tinyint(1) NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `type`, `position`, `valid`) VALUES
(1, 'Trang chủ', '/', 0, 1, 1),
(2, 'Tin tức', '/tin-tuc', 0, 3, 1),
(3, 'Giới thiệu', 'https://hpk.vn/gioi-thieu-1.html', 0, 2, 1),
(4, 'Kỹ thuật', '/ky-thuat', 0, 4, 1),
(5, 'Khuyến mại', '/khuyen-mai', 0, 5, 1),
(6, 'Hướng dẫn', '/huong-dan', 0, 7, 0),
(7, 'Liên hệ', '/lien-he', 0, 6, 1),
(8, 'Nước làm mát', '/nuoc-lam-mat', 1, 1, 1),
(9, 'Nước rửa kính', '#', 1, 2, 1),
(10, 'Dầu phanh', '#', 1, 3, 1),
(11, 'Động cơ', '#', 1, 4, 1),
(12, 'Hướng dẫn kỹ thuật', '#', 1, 5, 1),
(13, 'Liên hệ', '#', 1, 6, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_banner`
--

CREATE TABLE `menu_banner` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` tinyint(2) NOT NULL DEFAULT 99,
  `valid` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu_banner`
--

INSERT INTO `menu_banner` (`id`, `name`, `url`, `image`, `position`, `valid`) VALUES
(1, 'Nước làm mát2', '#', '/uploads/r/menu-banner/subbanner-01.jpg', 1, 1),
(2, 'Nước rửa kính', '#nuoc-rua-kin', '/uploads/r/menu-banner/subbanner-02.jpg', 2, 1),
(3, 'Dầu phanh', '#dau-phanh', '/uploads/r/menu-banner/subbanner-01.jpg', 3, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT 'id chuyen muc',
  `image` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'anh dai dien',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tieu de tin',
  `type` enum('CONTENT','VIDEO','MANUAL','DISCOUNT') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'CONTENT',
  `video_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mo ta tin',
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'noi dung tin',
  `products_related` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'san pham lien quan',
  `is_hot` tinyint(1) DEFAULT 0 COMMENT 'la tin noi bat',
  `is_mega_menu` tinyint(1) NOT NULL DEFAULT 0,
  `position` tinyint(1) DEFAULT 99,
  `valid` tinyint(1) DEFAULT 1,
  `meta_title` varchar(170) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seo meta title',
  `meta_desc` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seo meta desc',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `category_id`, `image`, `name`, `type`, `video_link`, `slug`, `desc`, `content`, `products_related`, `is_hot`, `is_mega_menu`, `position`, `valid`, `meta_title`, `meta_desc`, `create_time`, `update_time`) VALUES
(24, 1, '/uploads/r/tin-tuc/thong-tin-san-pham/chao-gang-da-ngoai.jpg', 'LODGE CAST IRON - Đồ bếp lý tưởng cho những chuyến dã ngoại', 'CONTENT', NULL, '', 'Những dụng cụ nấu nướng của Lodge Cast Iron được yêu thích nhất cho chuyến dã ngoại, cắm trại cùng gia đình. Chảo gang bền bỉ, nấu ăn ngon và tỏa nhiệt đều', '<p>Theo bạn thì một chuyến dã ngoại với gia đình, bạn bè như thế nào thì tuyệt vời nhất?</p>\n<p>Riêng đối với Chefstudio, một chuyến đi dã ngoại được xem là tuyệt vời khi cả gia đình hay cả nhóm bạn bè cùng nhau vui chơi, tham quan, hoạt động ngoài trời và đặc biệt là được cùng nấu nướng cũng như ăn uống cùng nhau.</p>\n<p>Đa số mọi người thường chuẩn bị sơ chế sẵn tất cả các thức ăn ở nhà và mang chúng theo, vừa tiết kiệm thời gian vừa “cứu nguy” những lúc đói sau khi đã vui chơi thấm mệt. Có một điều đặc biệt là gần 70% các nhóm, các gia đình ở châu Âu khi đi dã ngoại đều chọn những dụng cụ nấu nướng của <strong><span style=\"color: #0000ff;\"><a style=\"color: #0000ff;\" title=\"Lodge Cast Iron\" href=\"../../chao-gang/brands/lodge\">Lodge Cast Iron</a></span>.</strong></p>\n<h2><span style=\"color: #800000;\"><strong>Văn hóa dã ngoại và những ưu điểm khiến “phương Tây” ưa chuộng chảo gang Lodge</strong></span></h2>\n<p>Có thể bạn đã từng thấy rất nhiều hình ảnh những đứa trẻ từ nhỏ đã được đi chơi dã ngoại cùng với trường, lớp hoặc bố mẹ chúng. Đây là thói quen cực tốt bởi chúng sẽ giúp những đứa trẻ có thêm những kiến thức sống, những kĩ năng sinh tồn trong rừng, núi,... Chúng sẽ được học cách vui chơi lành mạnh, khỏe khoắn và được gần gũi với mọi người. </p>\n<p style=\"text-align: center;\"> <img src=\"../../uploads/r/tin-tuc/thong-tin-san-pham/cam-trai-voi-chao-gang.jpg\" alt=\"Chảo gang Lodge lý tưởng cho những chuyến dã ngoại\" width=\"800\" height=\"448\" /></p>\n<p style=\"text-align: center;\"><em>Chảo gang Lodge đơn giản, mộc mạc nhưng vô cùng bền bỉ và ăn ngon</em></p>\n<p style=\"text-align: center;\"><em><iframe width=\"425\" height=\"350\" src=\"//www.youtube.com/embed/gs6B88w1Mk8\" allowfullscreen=\"allowfullscreen\"></iframe></em></p>\n<p style=\"text-align: center;\"><em>Lodge Cast Iron - Trải nghiệm nấu ăn tuyệt vời với nồi chảo gang Mỹ</em></p>\n<p>Những ưu điểm trên của dã ngoại vốn dĩ rất cần thiết trong cuộc sống nay còn quan trọng hơn khi thời đại 4.0, con người dần có khoảng cách với nhau do ảnh hưởng của công nghệ thì những hoạt động ngoài trời bên nhau sẽ có nhiệm vụ kết nối họ lại với nhau.</p>\n<h2><span style=\"color: #800000;\"><strong>Vì sao Lodge Cast Iron lại là đồ bếp lý tưởng cho những chuyến dã ngoại?</strong></span></h2>\n<h3><strong><em>Độ bền cao</em></strong></h3>\n<p><span style=\"color: #0000ff;\"><strong><a style=\"color: #0000ff;\" title=\"Lodge\" href=\"../../brands/lodge\">Lodge</a></strong></span> là thương hiệu dụng cụ bếp thuộc top thế giới, có độ bền cực cao và đã được kiểm chứng hơn 120 năm trên toàn thế giới. Những dụng cụ mang thương hiệu Lodge thường có độ bền ít nhất là 5 năm và không có giới hạn cao nhất vì nếu biết cách sử dụng, bảo quản cũng như tôi dầu, chúng có thể sử dụng được trên 100 năm.</p>\n<p style=\"text-align: center;\"><img src=\"../../uploads/r/tin-tuc/thong-tin-san-pham/cam-trai-voi-lodge-cast-iron.jpg\" alt=\"Lò nướng than Lodge ngoài trời\" width=\"750\" height=\"750\" /></p>\n<p style=\"text-align: center;\"><strong><em><a title=\"Lò nướng than Lodge\" href=\"../../khay-va-bep-nuong/lo-nuong-bbq-lodge-sportsmans-grill-49.html\">Lò nướng than Lodge</a> ngoài trời</em></strong></p>\n<p>Những cuộc khảo sát sản phẩm diễn ra ở Mỹ cho thấy rằng, có rất nhiều hộ gia đình đã sử dụng những chiếc nồi gang, chảo gang có từ thời ông bà của họ và đến giờ con họ đã gần 50 tuổi và những dụng cụ ấy vẫn dùng tốt. Đó là minh chứng rõ ràng nhất cho độ bền của những dụng cụ mang tên Lodge.</p>\n<h3><strong><em>Về chất liệu</em></strong></h3>\n<p>Vì đã là thương hiệu có uy tín và gần như là biểu tượng của nước Mỹ trong phân khúc dụng cụ gia đình cao cấp nên bất kì một sản phẩm nào của Lodge Cast Iron cũng được kiểm tra nghiêm ngặt về mọi thứ trước khi đến được tay của người tiêu dùng. </p>\n<p>Những chiếc nồi gang, chảo gang của Lodge luôn làm từ 98% sắt và 2% carbon. Điều này giúp chúng có độ bền, độ an toàn và thời gian sử dụng cực cao. </p>\n<p style=\"text-align: center;\"><img src=\"../../uploads/r/tin-tuc/thong-tin-san-pham/chao-gang-ldoge-dong-hanh-cung-chuyen-da-ngoai.jpg\" alt=\"Lodge cast iron cho chuyến dã ngoại\" width=\"800\" height=\"534\" /></p>\n<p style=\"text-align: center;\"><em>Làm bằng gang nguyên khối 100%, chảo gang Lodge lý tưởng cho chuyến dã ngoại</em></p>\n<p>Khi đi dã ngoại, thứ mà các gia đình hay các nhóm quan tâm đó chính là dụng cụ nấu nướng có dễ dùng, có tốt và có nhiều tính năng hay không. Rất hiểu thị trường, Lodge đã thiết kế những sản phẩm của mình rất phù hợp, vừa có thể sang trọng trang trí trong gian bếp hiện đại, vừa tiện lợi khi mang đi dã ngoại. Ví dụ: những chiếc chảo gang có thể dùng để nướng, chiên, xào những thức ăn đã sơ chế sẵn như rau củ, xúc xích,... và cả beef steak - món yêu thích của người Âu - Mỹ.</p>\n<h3><strong><em>Về ngoại hình</em></strong></h3>\n<p>Những<strong> dụng cụ bếp của Lodge</strong> có rất nhiều kiểu dáng, kích cỡ, màu sắc để đáp ứng hầu hết tất cả những nhu cầu của khách hàng trong rất nhiều mục đích sử dụng của họ.</p>\n<p>Khi đi dã ngoại, bạn có thể chuẩn bị 2 sản phẩm đơn giản đó chính là <strong>chảo gang sâu lòng</strong> và<strong> chiếc chảo gang nướng</strong> chuyên dụng cho các món nướng, áp chảo. Chỉ như vậy đã có thể chế biến đầy đủ các món ăn, từ món soup cho trẻ nhỏ đến những món nướng barbecue thơm ngon cho cả gia đình mà không gặp chút khó khăn nào.</p>\n<p style=\"text-align: center;\"><img src=\"../../uploads/r/tin-tuc/thong-tin-san-pham/chao-gang-ranh-nuong.jpg\" alt=\"Chảo gang nướng rãnh\" width=\"800\" height=\"533\" /></p>\n<p style=\"text-align: center;\"><em><strong><span style=\"color: #0000ff;\"><a style=\"color: #0000ff;\" title=\"Chảo gang Lodge rãnh vuông\" href=\"../../chao-gang/chao-gang-nuong-lodge-ranh-vuong-305-cm-37.html\">Chảo gang Lodge rãnh vuông</a></span></strong> nướng cá hồi siêu ngon</em></p>\n<p>Ngoài ra, chảo hay nồi của Lodge thường rất dễ vệ sinh sau khi chế biến xong. Nếu như không có nước sạch nhiều, bạn có thể dùng khăn lau sơ sau đó có thể cất chúng và mang về rửa sau. Những vết khét cũng không phải là vấn đề đối với những sản phẩm của Lodge, bạn có thể tham khảo cách bảo quản những dụng cụ của Lodge tại <em>Chefstudio.vn.</em> </p>\n<h2><strong><span style=\"color: #800000;\">Đi dã ngoại, mua dụng cụ nấu của Lodge tại đâu?</span> </strong></h2>\n<p><strong>Uy tín, chất lượng, nhập khẩu chính hãng</strong> chính là những tiêu chí và sứ mệnh của<em><span style=\"color: #800000;\"><a style=\"color: #800000;\" title=\" Chefstudio.vn\" href=\"../../\"> Chef Studio</a></span></em> khi trở thành một trong những nhà cung cấp sản phẩm của <strong>Lodge Cast Iron</strong> tại thị trường Việt Nam.</p>\n<p>Để chọn mua những sản phẩm chính hãng, không sợ hàng giả, vui lòng truy cập website của <strong>Chef Studio</strong> và đặt hàng trực tiếp. Bạn sẽ được tư vấn miễn phí, cung cấp thông tin chính xác và giao hàng tận nơi trên toàn quốc! Hãy sắm ngay cho gia đình mình một chiếc chảo gang Lodge nhé!</p>\n<p> </p>\n<p><img src=\"../../uploads/r/thuong-hieu/chefstudio-viet-nam.jpg\" alt=\"Chefstudio Việt Nam\" width=\"210\" height=\"79\" /></p>\n<p><strong>CHEF STUDIO</strong></p>\n<p><strong>Hotline:</strong><strong> 084 888 18 00</strong></p>\n<p>Email: <a title=\"contact@chefstudio.vn\" href=\"mailto:contact@chefstudio.vn\">contact@chefstudio.vn</a></p>', '21,29,32,', 1, 1, 1, 1, 'Lodge Cast Iron - Đồ bếp lý tưởng cho những chuyến dã ngoại', 'Có một điều đặc biệt là gần 70% các nhóm, các gia đình ở châu Âu khi đi dã ngoại đều chọn những dụng cụ nấu nướng của Lodge Cast Iron, tại sao lại như vậy?', '2019-12-09 17:20:23', '2020-07-02 17:46:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news_category`
--

CREATE TABLE `news_category` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'duong danh anh dai dien',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten danh muc',
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mo ta danh muc',
  `product_id` int(11) DEFAULT NULL COMMENT 'san pham lien quan',
  `is_hot` tinyint(1) NOT NULL DEFAULT 0,
  `position` tinyint(1) DEFAULT 99 COMMENT 'vi tri hien thi',
  `meta_title` varchar(170) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'meta title',
  `meta_desc` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'meta desc',
  `valid` tinyint(1) DEFAULT 1 COMMENT 'hien thi/khong hien thi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news_category`
--

INSERT INTO `news_category` (`id`, `image`, `name`, `slug`, `desc`, `product_id`, `is_hot`, `position`, `meta_title`, `meta_desc`, `valid`) VALUES
(1, '/uploads/r/tin-tuc/chuyen-muc-tin/thong-tin-san-pham-chef.jpg', 'Thông tin sản phẩm', 'tin-tuc-san-pham', NULL, NULL, 1, 1, 'Thông tin sản phẩm', 'Thông tin chi tiết về các sản phẩm đồ bếp chuyên dụng tại Chef Studio', 1),
(2, '/uploads/r/tin-tuc/chuyen-muc-tin/cong-thuc-nau-an.jpg', 'Công thức nấu ăn', 'cong-thuc-nau-an', NULL, NULL, 1, 3, 'Công thức nấu ăn', 'Tổng hợp những công thức nấu ăn ngon và đơn giản từ Chef Studio. Cùng chef trở thành bếp trưởng tại căn bếp nhà bạn nhé', 1),
(3, '/uploads/r/tin-tuc/chuyen-muc-tin/meo-su-dung-va-bao-quan.jpg', 'Mẹo sử dụng và bảo quản', 'cham-soc-va-su-dung', NULL, NULL, 1, 2, 'Mẹo sử dụng và bảo quản', 'Chef Studio đồng hành cùng căn bếp nhà bạn với những mẹo vặt, tip hướng dẫn có ích giúp bạn sử dụng, bảo quản nhà bếp dễ dàng hơn', 1),
(4, '/uploads/r/tin-tuc/chuyen-muc-tin/video-chef.jpg', 'Video', 'video', NULL, NULL, 1, 4, 'Video Chef Studio', 'Kênh tổng hợp những video hướng dẫn, nấu ăn, giới thiệu sản phẩm thực tế tại Chef Studio, chúng tôi muốn mang lại trải nghiệm nấu nướng hoàn hảo nhất', 1),
(5, '/uploads/r/tin-tuc/chuyen-muc-tin/su-kien.jpg', 'Sự kiện', 'su-kien', NULL, NULL, 1, 5, 'Sự kiện Chef Studio', 'Thông báo mới nhất về những sự kiện của Chef Studio, gồm những sự kiện chuyên sâu về đồ bếp, ẩm thực, hãng nồi, chảo,...nổi tiếng trên thế giới', 1),
(6, '/uploads/r/tin-tuc/chuyen-muc-tin/phan-hoi-khach-hang.jpg', 'Phản hồi khách hàng', 'phan-hoi-khach-hang', NULL, NULL, 1, 6, 'Phản hồi từ khách hàng Chef Studio', 'Tổng hợp những feedback, phản hồi về sản phẩm của khách hàng Chefstudio.vn', 1),
(7, '/uploads/r/tin-tuc/chuyen-muc-tin/khuyen-mai-chef.jpg', 'Khuyến Mại', 'khuyen-mai', NULL, NULL, 0, 7, 'Khuyến Mại Chef Studio', 'Khuyến Mại Chef Studio - Liên tục cập nhật những thông tin mới nhất về chương trình khuyến mại tại Chef Studio, nhằm cung cấp cho khách hàng sản phẩm chất lượng', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news_tag`
--

CREATE TABLE `news_tag` (
  `news_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'nguoi nhan',
  `phone` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'dien thoai nguoi nhan',
  `email` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email nguoi nhan',
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'dia chi nhan hang',
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ghi chu',
  `product` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'san pham trong don hang',
  `total_price` int(6) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL COMMENT 'thoi gian dat hang',
  `status` tinyint(1) DEFAULT 0,
  `is_finished` varchar(45) COLLATE utf8_unicode_ci DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `other_address` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipment_other` tinyint(1) DEFAULT 0,
  `is_call` tinyint(1) DEFAULT 0,
  `is_invoice` tinyint(1) DEFAULT 0,
  `payment_method` tinyint(1) DEFAULT 1,
  `delivery` tinyint(1) DEFAULT 1,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_tax_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_detail`
--

CREATE TABLE `order_detail` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `number` int(4) NOT NULL,
  `price_per_once` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tieu de bai viet',
  `sub_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid` tinyint(1) DEFAULT 1,
  `static_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tên bài viết',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'hinh anh',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mo ta',
  `position` tinyint(4) DEFAULT 1 COMMENT 'desc',
  `menu_id` int(11) DEFAULT NULL,
  `meta_title` varchar(170) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seo meta title',
  `meta_desc` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seo meta desc',
  `is_menu` tinyint(1) DEFAULT 0,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('VE CHUNG TOI','THANH TOAN','CHINH SACH','CAU HOI THUONG GAP') COLLATE utf8_unicode_ci DEFAULT 'VE CHUNG TOI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `page`
--

INSERT INTO `page` (`id`, `name`, `sub_title`, `desc`, `content`, `valid`, `static_name`, `image`, `description`, `position`, `menu_id`, `meta_title`, `meta_desc`, `is_menu`, `slug`, `type`) VALUES
(1, 'Về chúng tôi', NULL, 'Giới thiệu về AIndustry', '<div class=\"_2cuy _3dgx _2vxa\">Chef Studio Việt Nam được th&agrave;nh lập với mong muốn h&igrave;nh th&agrave;nh một hệ sinh th&aacute;i chuy&ecirc;n biệt, d&agrave;nh ri&ecirc;ng cho những người đam m&ecirc; nấu nướng. Ch&uacute;ng t&ocirc;i chuy&ecirc;n cung cấp đồ d&ugrave;ng nh&agrave; bếp, đồ gia dụng cao cấp, nhập khẩu ch&iacute;nh h&atilde;ng từ những thương hiệu nổi tiếng h&agrave;ng đầu tr&ecirc;n thế giới.</div>\n<div class=\"_2cuy _3dgx _2vxa\">&nbsp;</div>\n<div class=\"_2cuy _3dgx _2vxa\">B&ecirc;n cạnh việc đảm bảo chất lượng sản phẩm, Chef Studio c&ograve;n muốn đem tới cho kh&aacute;ch h&agrave;ng những m&oacute;n đồ c&oacute; t&iacute;nh thẩm mỹ cao, mang lại sự trải nghiệm tuyệt vời khi sử dụng.</div>\n<div class=\"_2cuy _3dgx _2vxa\">&nbsp;</div>\n<div class=\"_2cuy _3dgx _2vxa\">Th&ocirc;ng qua những sản phẩm m&agrave; Chef Studio mang lại, ch&uacute;ng t&ocirc;i muốn truyền tải th&ocirc;ng điệp: mỗi người l&agrave;m bếp đều xứng đ&aacute;ng l&agrave; một bếp trưởng.</div>\n<div class=\"_2cuy _3dgx _2vxa\">&nbsp;</div>\n<div class=\"_2cuy _3dgx _2vxa\">\n<p><img src=\"../../uploads/r/thuong-hieu/chefstudio-viet-nam.jpg\" alt=\"Chefstudio Việt Nam\" width=\"210\" height=\"79\" /></p>\n<p><strong>CHEFSTUDIO VIỆT NAM</strong></p>\n<p><strong>Hotline:</strong><strong>&nbsp;084 888 1800</strong></p>\n<p>Email:&nbsp;<a title=\"contact@chefstudio.vn\" href=\"mailto:contact@chefstudio.vn\">contact@chefstudio.vn</a></p>\n</div>', 1, NULL, '/uploads/r/loi-ich-uu-viet-khi-su-dung-chao-gang-lodge.jpg', NULL, 1, NULL, 'về Chefstudio.vn', 'Chef Studio Việt Nam được thành lập với mong muốn hình thành một hệ sinh thái chuyên biệt, dành riêng cho những người đam mê nấu nướng', 1, 'gioi-thieu-ve-chung-toi.html', 'VE CHUNG TOI'),
(2, 'Phương thức thanh toán', NULL, 'Quý khách có thể thanh toán qua hệ thống tài khoản ngân hàng của GOFOOD', '<p><span style=\"color: #000000;\"><strong>Qu&yacute; kh&aacute;ch c&oacute; thể thanh to&aacute;n qua hệ thống t&agrave;i khoản ng&acirc;n h&agrave;ng của Chef Studio:</strong></span></p>\n<p><span style=\"color: #000000;\"><strong>1. Chủ t&agrave;i khoản: <span style=\"color: #0000ff;\">Tạ Quang Trung</span></strong></span></p>\n<p><span style=\"color: #000000;\"><strong>Số t&agrave;i khoản: <span style=\"color: #0000ff;\">0261003495137</span></strong></span></p>\n<p><span style=\"color: #0000ff;\"><strong>Ng&acirc;n H&agrave;ng Vietcombank CN Thủ Thi&ecirc;m, Quận 2, TP HCM<br /><br /></strong></span></p>\n<p><span style=\"color: #000000;\"><strong>2. Chủ t&agrave;i khoản: <span style=\"color: #0000ff;\">Tạ Quang Trung</span></strong></span></p>\n<p><span style=\"color: #000000;\"><strong>Số t&agrave;i khoản: <span style=\"color: #0000ff;\">19028068315010</span></strong></span></p>\n<p><span style=\"color: #0000ff;\"><strong>Ng&acirc;n h&agrave;ng Techcombank CN T&acirc;y Hồ, H&agrave; Nội<br /><br /></strong></span></p>\n<p><span style=\"color: #000000;\"><strong>3. Chủ t&agrave;i khoản: <span style=\"color: #0000ff;\">Tạ Quang Trung</span></strong></span></p>\n<p><span style=\"color: #000000;\"><strong>Số t&agrave;i khoản: <span style=\"color: #0000ff;\">901777888</span></strong></span></p>\n<p><span style=\"color: #0000ff;\"><strong>Ng&acirc;n h&agrave;ng ACB - PGD Hiệp B&igrave;nh, Thủ Đức, TPHCM<br /><br /></strong></span></p>\n<p><span style=\"color: #000000;\"><strong>4. Chủ t&agrave;i khoản: <span style=\"color: #0000ff;\">C&ocirc;ng Ty TNHH FBC S&agrave;i G&ograve;n</span></strong></span></p>\n<p><span style=\"color: #000000;\"><strong>Số t&agrave;i khoản: <span style=\"color: #0000ff;\">9092 666888</span></strong></span></p>\n<p><span style=\"color: #0000ff;\"><strong>Ng&acirc;n h&agrave;ng ACB - Chi nh&aacute;nh Nguyễn Văn Trỗi, Ph&uacute; Nhuận, TPHCM</strong></span></p>\n<p></p>\n<p><img src=\"../../uploads/r/thuong-hieu/chefstudio-viet-nam.jpg\" alt=\"Chefstudio Việt Nam\" width=\"210\" height=\"79\" /></p>\n<p><strong>CHEF STUDIO</strong></p>\n<p><strong>Hotline:</strong><strong>&nbsp;084 888 18 00</strong></p>', 1, NULL, '/uploads/r/loi-ich-uu-viet-khi-su-dung-chao-gang-lodge.jpg', NULL, 2, NULL, 'Phương thức thanh toán', 'Các chủ tài khoản thanh toán của Chef Studio', 1, 'phuong-thuc-thanh-toan.html', 'THANH TOAN');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `popup`
--

CREATE TABLE `popup` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `popup`
--

INSERT INTO `popup` (`id`, `name`, `link`, `image`, `valid`) VALUES
(1, 'Shop Now', 'https://chefstudio.vn/tin-tuc-san-pham/uu-dai-cuc-lon-danh-tang-khach-hang-nhan-dip-cuoi-nam-31.html', '/uploads/r/popup/khuyen-mai-thuong-hieu-lodge-uu-dai.jpg', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `price_product`
--

CREATE TABLE `price_product` (
  `id` int(11) NOT NULL,
  `price_1` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_2` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `price_product`
--

INSERT INTO `price_product` (`id`, `price_1`, `price_2`, `valid`) VALUES
(1, '0', '600000', 1),
(2, '600000', '1500000', 1),
(3, '1500000', '3000000', 1),
(4, '3000000', '6000000', 1),
(5, '6000000', '12000000', 1),
(6, '12000000', '16000000', 1),
(7, '16000000', '20000000', 1),
(8, '20000000', '', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `category_slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_type` int(11) DEFAULT NULL COMMENT 'loai san pham',
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten san pham',
  `image` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'json anh san pham',
  `code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ma san pham',
  `style` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'quy cach',
  `height` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depth` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `star` tinyint(1) DEFAULT 5 COMMENT 'danh gia 1->5 sao',
  `from` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'xuat xu',
  `manufacturer` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'hang san xuat',
  `trademark` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'thuong hieu',
  `size` tinyint(1) UNSIGNED ZEROFILL DEFAULT NULL COMMENT 'size san pham',
  `desc` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mo ta san pham',
  `guide` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'noi dung san pham',
  `content1` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_hot` tinyint(1) DEFAULT 1 COMMENT 'noi bat',
  `is_empty` tinyint(1) DEFAULT 0 COMMENT '1(het han) 0(con hang)',
  `is_new` tinyint(1) DEFAULT 0 COMMENT '1: san pham moi',
  `is_popular` tinyint(1) DEFAULT 0 COMMENT '1: san pham pho bien',
  `percent_discount` int(11) DEFAULT NULL COMMENT 'phan tram giam gia',
  `is_discount` tinyint(1) DEFAULT 0 COMMENT '1: san pham giam gia',
  `is_best_seller` tinyint(1) DEFAULT 0 COMMENT 'san pham ban chay',
  `is_mega_menu` tinyint(1) DEFAULT 0,
  `price_1` int(9) DEFAULT NULL COMMENT 'gia niem yet',
  `price_2` int(9) DEFAULT NULL COMMENT 'gia khuyen mai',
  `weight` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'trong luong',
  `pice` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Chiếc' COMMENT 'don vi tinh',
  `position` tinyint(2) NOT NULL DEFAULT 99,
  `total_vote` int(6) DEFAULT 0 COMMENT 'total vote',
  `total_views` tinyint(4) DEFAULT 0 COMMENT 'luot xem',
  `update_time` datetime DEFAULT NULL COMMENT 'ngay cap nhat',
  `valid` tinyint(1) DEFAULT 1 COMMENT 'hien thi',
  `meta_title` varchar(170) COLLATE utf8_unicode_ci NOT NULL COMMENT 'seo title',
  `meta_desc` varchar(320) COLLATE utf8_unicode_ci NOT NULL COMMENT 'seo desc',
  `meta_keyword` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'seo keyword',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ngay tao',
  `video` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_related` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gift` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'duong danh anh dai dien',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten danh muc',
  `parent_id` int(11) DEFAULT 0 COMMENT 'danh muc cha',
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `desc` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mo ta danh muc',
  `is_menu` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0: off, 1: on',
  `is_hot` tinyint(1) NOT NULL DEFAULT 0,
  `position` tinyint(1) DEFAULT 99 COMMENT 'vi tri hien thi',
  `meta_title` varchar(170) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'meta title',
  `meta_desc` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'meta desc',
  `valid` tinyint(1) DEFAULT 1 COMMENT 'hien thi/khong hien thi',
  `brand_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_category`
--

INSERT INTO `product_category` (`id`, `image`, `name`, `parent_id`, `slug`, `desc`, `is_menu`, `is_hot`, `position`, `meta_title`, `meta_desc`, `valid`, `brand_id`) VALUES
(6, '/uploads/r/img6.png', 'Bát đĩa', 0, 'bat-dia', '', 0, 0, 1, '', '', 0, NULL),
(7, '/uploads/r/dao-thia-dia.jpg', 'Dao, thìa & dĩa', 0, 'dao-thia-dia', '', 0, 1, 1, '', '', 1, NULL),
(8, '/uploads/r/san-pham/chuyen-muc/noi.jpg', 'Nồi', 0, 'noi', '', 1, 0, 4, 'Nồi', 'Các dụng cụ nồi của Chefstudio.vn', 1, '[\"4\",\"9\"]'),
(9, '/uploads/r/san-pham/chuyen-muc/chao.jpg', 'Chảo', 0, 'chao', '', 1, 0, 5, 'Chảo', 'Tổng hợp chảo chuyên dụng từ các hãng đồ bếp nổi tiếng trên thế giới như chảo Lodge, chảo Staub, chảo Zwilling... mang lại vẻ sang trong cho căn bếp của bạn....', 1, '[\"4\",\"9\",\"11\",\"12\"]'),
(10, '/uploads/r/post1.png', 'Khay và bếp nướng', 0, 'khay-va-bep-nuong', '<p>Khay gang nướng Lodge chuy&ecirc;n d&ugrave;ng cho c&aacute;c m&oacute;n nướng, r&aacute;n, đặc biệt th&iacute;ch hợp cho c&aacute;c cuộc d&atilde; ngoại ngo&agrave;i trời v&agrave; tiệc nướng tại nh&agrave;. Khay nướng nhỏ ph&ugrave; hợp cho c&aacute;c m&oacute;n khai vị n&oacute;ng, lạnh, nước sốt, b&aacute;nh nướng v&agrave; m&oacute;n tr&aacute;ng miệng nhờ khả năng giữ nhiệt l&acirc;u, tỏa nhiệt đều v&agrave; c&oacute; thể sử dụng tốt trong l&ograve; nướng.</p>', 1, 0, 6, '', '', 1, '[\"4\",\"9\",\"20\"]');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_type`
--

CREATE TABLE `product_type` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'danh muc cha',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'anh dai dien',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ten loai san pham',
  `desc` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mo ta loai san pham',
  `level` tinyint(1) DEFAULT 1 COMMENT 'cap danh muc',
  `position` tinyint(1) DEFAULT 99,
  `valid` tinyint(1) DEFAULT 1,
  `meta_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_desc` varchar(170) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `site_setting`
--

CREATE TABLE `site_setting` (
  `id` int(11) NOT NULL,
  `logo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slogan` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_desc` varchar(320) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_email` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `style` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_app_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_fanpage` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pinterest` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `gg_plus_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `gg_analytics_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gg_tag_manager_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cache_on` tinyint(1) DEFAULT 1,
  `company_name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_phone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_email` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_sky` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_info` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_time` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_address2` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'van phong',
  `google_map` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `skype` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `support_email` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email nhan thong bao',
  `linkedin` text COLLATE utf8_unicode_ci NOT NULL,
  `company_phone_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_phone_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_phone_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_tax_code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_bct` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `site_setting`
--

INSERT INTO `site_setting` (`id`, `logo`, `logo_2`, `favicon`, `name`, `url`, `slogan`, `meta_keyword`, `meta_title`, `meta_desc`, `admin_email`, `style`, `fb_app_id`, `fb_fanpage`, `twitter`, `instagram`, `pinterest`, `gg_plus_id`, `gg_analytics_id`, `gg_tag_manager_id`, `cache_on`, `company_name`, `company_address`, `company_phone`, `company_email`, `company_sky`, `company_info`, `company_time`, `youtube`, `company_address2`, `google_map`, `skype`, `support_email`, `linkedin`, `company_phone_1`, `company_phone_2`, `company_phone_3`, `company_tax_code`, `url_bct`) VALUES
(153, '/uploads/r/logo_1.png', NULL, NULL, 'Chef Studio Vietnam', NULL, NULL, 'Chefstudio siêu thị đồ bếp cao cấp', 'chefstudio.vn | Siêu thị đồ bếp,', 'Chefstudio, đồ dùng nhà bếp nhập khẩu ', NULL, NULL, '', 'https://www.facebook.com/ChefStudio.vn', '', 'https://www.instagram.com/chefstudiovietnam/', '', '', '', '', 1, 'CÔNG TY TNHH FBC SÀI GÒN', 'Số 111 Đường B,', '084 888 1800', 'contact@chefstudio.vn', NULL, 'Đừng ngần ngại hãy liên hệ ngay với chúng tôi bất cứ lúc nào thông qua Chat trực tuyến hoặc số điện thoại công ty, chúng tôi luôn hoan nghênh và sẵn sàng tư vấn, hỗ trợ bạn.', '8h30 - 19h15 hằng ngày', 'https://www.youtube.com/channel/UChMAvoRyPY8-G5SCEiN1YDw', NULL, NULL, '', 'test@chef.com.vn', '', '084 888 5999 (8h30 - 19h15)', '096 934 555 (8h30 - 19h15)', '084 888 5999 (8h30 - 19h15)', '0316016493', 'http://online.gov.vn/Home/WebDetails/64061');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `name` varchar(125) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `button` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` tinyint(1) NOT NULL DEFAULT 99,
  `valid` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slug`
--

CREATE TABLE `slug` (
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slug`
--

INSERT INTO `slug` (`name`, `route`, `type`) VALUES
('tin-tuc', 'public/news/index', 'news'),
('gioi-thieu.html', 'public/page/content/1', 'page'),
('thanh-toan.html', 'public/page/content/2', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('gioi-thieu.html', 'public/page/content/1', 'page'),
('gioi-thieu.html', 'public/page/content/1', 'page'),
('gioi-thieu.html', 'public/page/content/1', 'page'),
('gioi-thieu.html', 'public/page/content/1', 'page'),
('gioi-thieu.html', 'public/page/content/1', 'page'),
('gioi-thieu.html', 'public/page/content/1', 'page'),
('giai-phap-ky-thuat', 'public/news/index/4', 'news'),
('bang-gia-thang-cap-ma-kem-nhung-nong-nam-2018.html', 'public/download/content/1', 'download'),
('download', 'public/download/index', 'download'),
('bang-gia-thang-cap-ma-kem-nhung-nong-nam-2018.html', 'public/download/content/1', 'download'),
('bang-gia-thang-cap-ma-kem-nhung-nong-nam-2018.html', 'public/download/content/1', 'download'),
('bang-gia-thang-cap-ma-kem-nhung-nong-nam-2018.html', 'public/download/content/1', 'download'),
('gioi-thieu.html', 'public/page/index', 'page'),
('gioi-thieu-ve-chung-toi', 'public/page/content/1', 'page'),
('gioi-thieu-ve-chung-toi', 'public/page/content/1', 'page'),
('bang-gia.html', 'public/download/content/2', 'download'),
('gioi-thieu-ve-chung-toi', 'public/page/content/1', 'page'),
('thanh-toan.html', 'public/page/content/2', 'page'),
('gioi-thieu-ve-chung-toi', 'public/page/content/1', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('gioi-thieu-ve-chung-toi.html', 'public/page/content/1', 'page'),
('san-pham-co-khi', 'public/product/index/6', 'product'),
('tu-crack-6-u', 'public/product/index/7', 'product'),
('tu-crack-6-u', 'public/product/index/7', 'product'),
('tu-crack-10-u', 'public/product/index/8', 'product'),
('tu-crack-12-u', 'public/product/index/9', 'product'),
('shelter-hub', 'public/product/index/10', 'product'),
('thiet-bi-phu-tro-bts-shelter', 'public/product/index/11', 'product'),
('thiet-bi-phu-tro-bts-shelter', 'public/product/index/11', 'product'),
('cua-khu-de-rac', 'public/product/index/13', 'product'),
('tu-dien-ha-the', 'public/product/index/14', 'product'),
('tu-dien-trung-the', 'public/product/index/15', 'product'),
('thang-cap', 'public/product/index/16', 'product'),
('mang-cap', 'public/product/index/17', 'product'),
('bo-gia-antena', 'public/product/index/20', 'product'),
('panel-chong-chay', 'public/product/index/21', 'product'),
('san-pham-co-khi', 'public/product/index/6', 'product'),
('tu-crack-6-u', 'public/product/index/7', 'product'),
('tu-crack-10-u', 'public/product/index/8', 'product'),
('tu-crack-12-u', 'public/product/index/9', 'product'),
('tu-crack-10-u', 'public/product/index/8', 'product'),
('san-pham-co-khi', 'public/product/index/6', 'product'),
('crack-cabinet', 'public/product/index/7', 'product'),
('open-crack', 'public/product/index/8', 'product'),
('open-rack', 'public/product/index/8', 'product'),
('out-door-rack', 'public/product/index/9', 'product'),
('phu-kien-tu-rack', 'public/product/index/22', 'product'),
('crack-cabinet', 'public/product/index/7', 'product'),
('bat-di-ban-an', 'public/product/index/6', 'product'),
('dao-thia-dia', 'public/product/index/7', 'product'),
('noi', 'public/product/index/8', 'product'),
('chao', 'public/product/index/9', 'product'),
('khay-va-bep-nuong', 'public/product/index/10', 'product'),
('dao', 'public/product/index/11', 'product'),
('bep', 'public/product/index/13', 'product'),
('bep', 'public/product/index/13', 'product'),
('phu-kien', 'public/product/index/14', 'product'),
('chao-gang-1', 'public/product/index/15', 'product'),
('chao-da', 'public/product/index/16', 'product'),
('chao-da', 'public/product/index/16', 'product'),
('chao-inox', 'public/product/index/17', 'product'),
('chao-gang-lodge', 'public/product/index/15', 'product'),
('chaochao-gang-lodge', 'public/product/index/15', 'product'),
('chao-gang-lodge', 'public/product/index/15', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao', 'public/product/index/9', 'product'),
('gioi-thieu-ve-chung-toi.html', 'public/page/content/1', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('cach-thanh-toan.html', 'public/page/content/2', 'page'),
('khay-va-bep-nuong', 'public/product/index/10', 'product'),
('quy-doi-tra-san-pham', 'public/page/content/4', 'page'),
('quy-doi-tra-san-pham.html', 'public/page/content/4', 'page'),
('khay-va-bep-nuong', 'public/product/index/10', 'product'),
('noi', 'public/product/index/8', 'product'),
('chao-gang-lodge', 'public/product/index/15', 'product'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('quy-che-hoat-dong.html', 'public/page/content/6', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-tra-hang-va-hoan-tien.html', 'public/page/content/8', 'page'),
('chao-gang-lodge', 'public/product/index/15', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao', 'public/product/index/9', 'product'),
('quy-dinh-doi-tra-san-pham.html', 'public/page/content/4', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('gioi-thieu-ve-chung-toi.html', 'public/page/content/1', 'page'),
('bat-dia', 'public/product/index/6', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('gioi-thieu-ve-chung-toi.html', 'public/page/content/1', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('quy-dinh-doi-tra-san-pham.html', 'public/page/content/4', 'page'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('quy-che-hoat-dong.html', 'public/page/content/6', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-tra-hang-va-hoan-tien.html', 'public/page/content/8', 'page'),
('tin-tuc-san-pham', 'public/news/index/1', 'news'),
('cham-soc-va-su-dung', 'public/news/index/3', 'news'),
('cong-thuc-nau-an', 'public/news/index/2', 'news'),
('noi-gang', 'public/product/index/20', 'product'),
('noi-gang', 'public/product/index/20', 'product'),
('chao-thep-carbon', 'public/product/index/21', 'product'),
('noi-gang-phu-gom', 'public/product/index/22', 'product'),
('phu-kien-lodge', 'public/product/index/23', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('phu-kien-staub', 'public/product/index/24', 'product'),
('noi-gang', 'public/product/index/20', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('chao-gang', 'public/product/index/15', 'product'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('tin-tuc-san-pham', 'public/news/index/1', 'news'),
('cham-soc-va-su-dung', 'public/news/index/3', 'news'),
('cong-thuc-nau-an', 'public/news/index/2', 'news'),
('phu-kien-kai', 'public/product/index/25', 'product'),
('phu-kien-kai', 'public/product/index/25', 'product'),
('brands/lodge', 'public/trademark/index/4', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('chao-nhom', 'public/product/index/26', 'product'),
('chao-nhom', 'public/product/index/26', 'product'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('brands/lodge', 'public/trademark/index/4', 'trademark'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('chao-inox', 'public/product/index/17', 'product'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('gioi-thieu-ve-chung-toi.html', 'public/page/content/1', 'page'),
('tin-tuc-san-pham', 'public/news/index/1', 'news'),
('cham-soc-va-su-dung', 'public/news/index/3', 'news'),
('tin-tuc-san-pham', 'public/news/index/1', 'news'),
('brands/lodge', 'public/trademark/index/4', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('chao-gang', 'public/product/index/15', 'product'),
('noi', 'public/product/index/8', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao-thep-carbon', 'public/product/index/21', 'product'),
('phu-kien-staub', 'public/product/index/24', 'product'),
('phu-kien-lodge', 'public/product/index/23', 'product'),
('noi-gang', 'public/product/index/20', 'product'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/lodge', 'public/trademark/index/4', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('phu-kien-zwilling', 'public/product/index/27', 'product'),
('phu-kien-zwilling', 'public/product/index/27', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao', 'public/product/index/9', 'product'),
('chao', 'public/product/index/9', 'product'),
('noi', 'public/product/index/8', 'product'),
('noi-inox', 'public/product/index/28', 'product'),
('brands/elo', 'public/trademark/index/13', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/elo', 'public/trademark/index/13', 'trademark'),
('brands/elo', 'public/trademark/index/13', 'trademark'),
('tin-tuc-san-pham', 'public/news/index/1', 'news'),
('gioi-thieu-ve-chung-toi.html', 'public/page/content/1', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('gioi-thieu-ve-chung-toi.html', 'public/page/content/1', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('goi-qua-mien-phi', 'public/page/content/12', 'page'),
('goi-qua-mien-phi', 'public/page/content/12', 'page'),
('chinh-sach-bao-hanh.html', 'public/page/content/13', 'page'),
('goi-qua-mien-phi.html', 'public/page/content/12', 'page'),
('cau-hoi-thuong-gap.html', 'public/page/content/14', 'page'),
('cau-hoi-thuong-gap.html', 'public/page/content/14', 'page'),
('cau-hoi-thuong-gap.html', 'public/page/content/14', 'page'),
('chinh-sach-bao-hanh.html', 'public/page/content/13', 'page'),
('cau-hoi-thuong-gap.html', 'public/page/content/14', 'page'),
('dao-thia-dia', 'public/product/index/7', 'product'),
('dao-thia-dia', 'public/product/index/7', 'product'),
('tin-tuc-san-pham', 'public/news/index/1', 'news'),
('cham-soc-va-su-dung', 'public/news/index/3', 'news'),
('cong-thuc-nau-an', 'public/news/index/2', 'news'),
('cham-soc-va-su-dung', 'public/news/index/3', 'news'),
('video', 'public/news/index/4', 'news'),
('su-kien', 'public/news/index/5', 'news'),
('tin-tuc-san-pham', 'public/news/index/1', 'news'),
('cham-soc-va-su-dung', 'public/news/index/3', 'news'),
('cau-hoi-thuong-gap.html', 'public/page/content/14', 'page'),
('cau-hoi-thuong-gap.html', 'public/page/content/14', 'page'),
('cau-hoi-thuong-gap.html', 'public/page/content/14', 'page'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('dao-va-keo', 'public/product/index/11', 'product'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('dao-nhap-khau', 'public/product/index/30', 'product'),
('keo', 'public/product/index/31', 'product'),
('dao-kai', 'public/product/index/30', 'product'),
('dao-kai', 'public/product/index/30', 'product'),
('dao-nhap-khau', 'public/product/index/30', 'product'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('phan-hoi-khach-hang', 'public/news/index/6', 'news'),
('dao-nhat-ban-nhap-khau', 'public/product/index/30', 'product'),
('dao-nhap-khau', 'public/product/index/30', 'product'),
('dao-nhap-khau', 'public/product/index/30', 'product'),
('dao-bo', 'public/product/index/32', 'product'),
('dao-le', 'public/product/index/33', 'product'),
('dao-le', 'public/product/index/33', 'product'),
('noi-gang', 'public/product/index/20', 'product'),
('brands/bosch', 'public/trademark/index/14', 'trademark'),
('brands/bosch', 'public/trademark/index/14', 'trademark'),
('thiet-bi-bep', 'public/product/index/13', 'product'),
('thiet-bi-gia-dung', 'public/product/index/34', 'product'),
('phu-kien', 'public/product/index/14', 'product'),
('bep', 'public/product/index/13', 'product'),
('bep', 'public/product/index/13', 'product'),
('may-say-quan-ao', 'public/product/index/35', 'product'),
('may-giat-va-may-say', 'public/product/index/36', 'product'),
('may-say-quan-ao', 'public/product/index/35', 'product'),
('may-giat-ket-hop-say', 'public/product/index/37', 'product'),
('may-say', 'public/product/index/35', 'product'),
('may-giat-va-may-say', 'public/product/index/36', 'product'),
('may-giat', 'public/product/index/37', 'product'),
('may-giat', 'public/product/index/37', 'product'),
('may-say-va-may-giat', 'public/product/index/35', 'product'),
('may-giat-va-may-say', 'public/product/index/36', 'product'),
('may-say', 'public/product/index/35', 'product'),
('may-say', 'public/product/index/35', 'product'),
('may-say', 'public/product/index/35', 'product'),
('may-giat', 'public/product/index/37', 'product'),
('bep-tu', 'public/product/index/38', 'product'),
('bep-tu', 'public/product/index/38', 'product'),
('bep-tu', 'public/product/index/38', 'product'),
('may-rua-bat', 'public/product/index/39', 'product'),
('may-hut-mui', 'public/product/index/40', 'product'),
('may-hut-mui', 'public/product/index/40', 'product'),
('brands/demo', 'public/trademark/index/15', 'trademark'),
('brands/demo-1', 'public/trademark/index/16', 'trademark'),
('lo-nuong', 'public/product/index/41', 'product'),
('lo-nuong-lo-vi-song', 'public/product/index/42', 'product'),
('lo-vi-song', 'public/product/index/43', 'product'),
('lo-nuong', 'public/product/index/41', 'product'),
('lo-nuong-ket-hop', 'public/product/index/44', 'product'),
('tu-lanh', 'public/product/index/45', 'product'),
('brands/dexter-russell', 'public/trademark/index/17', 'trademark'),
('may-hut-mui', 'public/product/index/40', 'product'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/elo', 'public/trademark/index/13', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/dexter-russell', 'public/trademark/index/17', 'trademark'),
('brands/elo', 'public/trademark/index/13', 'trademark'),
('brands/elo', 'public/trademark/index/13', 'trademark'),
('brands/bosch', 'public/trademark/index/14', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub-cast-iron', 'public/trademark/index/9', 'trademark'),
('brands/lodge-cast-iron', 'public/trademark/index/4', 'trademark'),
('brands/staub-cast-iron', 'public/trademark/index/9', 'trademark'),
('brands/staub-cast-iron', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('bat-dia', 'public/product/index/6', 'product'),
('bat-dia', 'public/product/index/6', 'product'),
('gioi-thieu-ve-chung-toi.html', 'public/page/content/1', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('goi-qua-mien-phi.html', 'public/page/content/12', 'page'),
('chinh-sach-bao-hanh.html', 'public/page/content/13', 'page'),
('cau-hoi-thuong-gap.html', 'public/page/content/14', 'page'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('noi', 'public/product/index/8', 'product'),
('khuyen-mai', 'public/news/index/7', 'news'),
('chao-gang', 'public/product/index/15', 'product'),
('noi', 'public/product/index/8', 'product'),
('chao', 'public/product/index/9', 'product'),
('dao-nhap-khau', 'public/product/index/30', 'product'),
('may-giat', 'public/product/index/37', 'product'),
('noi-gang-phu-gom', 'public/product/index/22', 'product'),
('thiet-bi-gia-dung', 'public/product/index/34', 'product'),
('dao-va-keo', 'public/product/index/11', 'product'),
('bep', 'public/product/index/13', 'product'),
('khay-va-bep-nuong', 'public/product/index/10', 'product'),
('noi', 'public/product/index/8', 'product'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/elo', 'public/trademark/index/13', 'trademark'),
('brands/bosch', 'public/trademark/index/14', 'trademark'),
('brands/dexter-russell', 'public/trademark/index/17', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('brands/lodgecastiron', 'public/trademark/index/4', 'trademark'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('chinh-sach-bao-hanh.html', 'public/page/content/13', 'page'),
('chinh-sach-bao-hanh.html', 'public/page/content/13', 'page'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('robot-hut-bui', 'public/product/index/46', 'product'),
('noi-chien-khong-dau', 'public/product/index/47', 'product'),
('noi-chien-khong-dau', 'public/product/index/47', 'product'),
('brands/medion', 'public/trademark/index/18', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('may-rua-bat', 'public/product/index/39', 'product'),
('lo-nuong-ket-hop', 'public/product/index/44', 'product'),
('lo-nuong-ket-hop', 'public/product/index/44', 'product'),
('bep-nuong', 'public/product/index/48', 'product'),
('khay-nuong', 'public/product/index/49', 'product'),
('lo-nuong-ket-hop-vi-song', 'public/product/index/44', 'product'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('chinh-sach-doi-hang', 'public/page/content/4', 'page'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('chinh-sach-bao-mat.html', 'public/page/content/5', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('goi-qua-mien-phi.html', 'public/page/content/12', 'page'),
('goi-qua-mien-phi.html', 'public/page/content/12', 'page'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-bao-hanh.html', 'public/page/content/13', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach.html', 'public/page/content/3', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-mua-hang', 'public/page/content/6', 'page'),
('chinh-sach-van-chuyen.html', 'public/page/content/7', 'page'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('chinh-sach-bao-hanh.html', 'public/page/content/13', 'page'),
('brands/philips', 'public/trademark/index/19', 'trademark'),
('brands/vietcamp', 'public/trademark/index/20', 'trademark'),
('brands/kai', 'public/trademark/index/10', 'trademark'),
('brands/ballarini', 'public/trademark/index/11', 'trademark'),
('brands/zwilling-j-a-henckels', 'public/trademark/index/12', 'trademark'),
('brands/elo', 'public/trademark/index/13', 'trademark'),
('brands/bosch', 'public/trademark/index/14', 'trademark'),
('brands/dexter-russell', 'public/trademark/index/17', 'trademark'),
('phu-kien-may-rua-bat', 'public/product/index/50', 'product'),
('brands/finish', 'public/trademark/index/21', 'trademark'),
('phu-kien-may-rua-bat', 'public/product/index/50', 'product'),
('chinh-sach-va-quy-dinh-chung', 'public/page/content/8', 'page'),
('robot-hut-bui', 'public/product/index/46', 'product'),
('phu-kien-may-rua-bat', 'public/product/index/50', 'product'),
('khay-va-bep-nuong', 'public/product/index/10', 'product'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('phuong-thuc-thanh-toan.html', 'public/page/content/2', 'page'),
('tin-tuc-san-pham', 'public/news/index/1', 'news'),
('cham-soc-va-su-dung', 'public/news/index/3', 'news'),
('cong-thuc-nau-an', 'public/news/index/2', 'news'),
('video', 'public/news/index/4', 'news'),
('su-kien', 'public/news/index/5', 'news'),
('phan-hoi-khach-hang', 'public/news/index/6', 'news'),
('khuyen-mai', 'public/news/index/7', 'news'),
('phu-kien-may-rua-bat', 'public/product/index/50', 'product'),
('brands/neato', 'public/trademark/index/22', 'trademark'),
('bep-ga', 'public/product/index/51', 'product'),
('bep-gas', 'public/product/index/51', 'product'),
('brands/neato', 'public/trademark/index/22', 'trademark'),
('brands/staub', 'public/trademark/index/9', 'trademark'),
('brands/neato', 'public/trademark/index/22', 'trademark'),
('brands/neato', 'public/trademark/index/22', 'trademark'),
('brands/neato', 'public/trademark/index/22', 'trademark'),
('brands/ecovacs', 'public/trademark/index/23', 'trademark'),
('brands/ecovacs', 'public/trademark/index/23', 'trademark'),
('brands/ecovacs', 'public/trademark/index/23', 'trademark'),
('brands/ecovacs', 'public/trademark/index/23', 'trademark'),
('brands/ecovacs', 'public/trademark/index/23', 'trademark'),
('brands/ecovacs', 'public/trademark/index/23', 'trademark'),
('apply-to-be-a-supplier', 'public/page/content/15', 'page'),
('apply-to-be-a-supplier.html', 'public/page/content/15', 'page'),
('apply-to-be-a-supplier.html', 'public/page/content/15', 'page'),
('apply-to-be-a-supplier.html', 'public/page/content/15', 'page'),
('apply-to-be-a-supplier.html', 'public/page/content/15', 'page'),
('apply-to-be-a-supplier.html', 'public/page/content/15', 'page');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `subscribe`
--

CREATE TABLE `subscribe` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `subscribe`
--

INSERT INTO `subscribe` (`id`, `email`, `status`, `created_time`) VALUES
(1, 're@gmail.com', 1, '2018-10-17 17:03:40'),
(2, 'ss@gmail.com', 1, '2018-10-17 17:04:16'),
(3, 'test@gmail.com', 1, '2018-11-12 11:16:28'),
(4, 'tubean1504@gmail.com', 1, '2018-12-02 08:06:52'),
(5, 'tubean1504@gmail.com', 1, '2018-12-02 08:06:57'),
(6, 'tubean1504@gmail.com', 1, '2018-12-02 08:07:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `position` tinyint(1) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `trademark`
--

CREATE TABLE `trademark` (
  `id` int(11) NOT NULL,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_hot` tinyint(1) DEFAULT 0 COMMENT '0: off, 1: on',
  `position` tinyint(1) DEFAULT 99,
  `valid` tinyint(1) DEFAULT 1 COMMENT '0: off, 1 on',
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(170) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_desc` varchar(2320) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `trademark`
--

INSERT INTO `trademark` (`id`, `image`, `name`, `is_hot`, `position`, `valid`, `desc`, `meta_title`, `meta_desc`, `slug`, `video_link`) VALUES
(4, '/uploads/r/thuong-hieu/lodge.jpg', 'Lodge Cast Iron', 0, 1, 1, '<p xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\">Ra đời vào năm 1896 tại một xưởng đúc nhỏ</span> ở thị trấn South Pittsburg</span>, Tennessee,</span> Hoa Kỳ, vợ chồng ông bà Joseph Lodge đã cho ra lò những sản phẩm bằng gang đầu tiên - Đánh dấu sự ra đời của thương hiệu <span xss=\"removed\" style=\"color: #993300;\"><strong>Lodge Cast Iron</strong></span> mà sau này nổi tiếng khắp Hoa Kỳ và nhiều nơi trên thế giới. Giờ đây, gần như không một gia đình người Mỹ nào lại không biết đến thương hiệu Lodge Cast Iron và trong nhà luôn có ít nhất một sản phẩm của hãng Lodge.<span xss=\"removed\">  </span></p>\n<p xss=\"removed\" style=\"text-align: center;\"><span xss=\"removed\"><img src=\"../../uploads/r/thuong-hieu/bo-suu-tap-do-gang-hang-lodge-cast-iron.jpg\" alt=\"Bộ sưu tập nồi chảo gang Lodge\" width=\"800\" height=\"533\" /></span></p>\n<p xss=\"removed\" style=\"text-align: center;\"><em>Bộ sưu tập nồi chảo gang Lodge</em></p>\n<p>Trải qua hơn một thế kỷ, với kinh nghiệm dày dặn về kỹ thuật đúc cùng công nghệ sản xuất tiên tiến, các sản phẩm của Lodge ngày càng đa dạng về mẫu mã, chất lượng hàng đầu được công nhận về gang đúc. </p>\n<h2><span xss=\"removed\" style=\"color: #993300;\">Vẻ đẹp mộc mạc, giản dị với nồi, chảo gang Lodge Cast Iron</span></h2>\n<p>Nhìn qua các sản phẩm<span xss=\"removed\"> <a href=\"../../chao-gang/brands/lodgecastiron\" title=\"gang thô \" xss=\"removed\"><strong>gang thô</strong> </a></span>của Lodge, bạn sẽ thấy khá giống với những chiếc <span xss=\"removed\">nồi gang cổ truyền</span> Việt mà ta vẫn thường thấy trong thế kỷ trước - <span xss=\"removed\">Một vẻ đẹp mộc mạc,</span> đơn sơ nhưng bền bỉ, mạnh mẽ, làm gợi lại những món ăn đậm đà hương vị của bà, của mẹ vẫn thường nấu khi bé. </p>\n<p xss=\"removed\" style=\"text-align: center;\"> <img src=\"../../uploads/r/thuong-hieu/noi-gang-tho-lodge.jpg\" alt=\"Nồi gang thô của Lodge với vẻ đẹp mộc mạc\" width=\"800\" height=\"533\" /></p>\n<p style=\"text-align: center;\"><em>Nồi gang thô của Lodge với vẻ đẹp mộc mạc</em></p>\n<p>Đối với dòng <span style=\"color: #0000ff;\"><a title=\"Nồi gang phủ gốm Lodge\" href=\"../../noi-gang-phu-gom/brands/lodgecastiron\" style=\"color: #0000ff;\"><strong>gang tráng men</strong></a> </span>của Lodge lại mang vẻ đẹp hiện đại, tinh tế khi được khoác lên mình lớp gốm sứ cao cấp, sang trọng không kém phần bắt mắt.</p>\n<p xss=\"removed\" style=\"text-align: center;\"> <img src=\"../../uploads/r/thuong-hieu/noi-gang-trang-men-lodge.jpg\" alt=\"Nồi gang tráng men Lodge\" width=\"800\" height=\"667\" /></p>\n<p style=\"text-align: center;\"><em>Nồi gang tráng men Lodge mang vẻ đẹp hiện đại, tinh tế với lớp phủ gốm cao cấp</em></p>\n<p> </p>\n<h2><span xss=\"removed\" style=\"color: #993300;\">Điều gì khiến cho Lodge Cast Iron được yêu thích như vậy?</span></h2>\n<p> </p>\n<p>Các sản phẩm đồ bếp của Lodge chủ yếu được làm từ gang nguyên khối, mang lại những dụng cụ nhà bếp vô cùng bền bỉ, dày dặn, bạn chỉ cần sử dụng đúng cách, những chiếc nồi,<strong> <a title=\"chảo gang Lodge\" href=\"../../chao-gang/brands/lodgecastiron\">chảo gang</a></strong> này có truyền từ thế hệ này qua thế hệ khác. </p>\n<p> </p>\n<p>Không chỉ vậy, các sản phẩm của Lodge còn có rất nhiều ưu điểm nổi bật, khiến nó được yêu thích tại nhiều nơi trên thế giới:</p>\n<p> </p>\n<p>- Khả năng chống dính tự nhiên, an toàn cho sức khỏe và có thể củng cố lại sau mỗi lần tôi dầu;</p>\n<p>- Bổ sung lượng sắt an toàn cho sức khỏe mỗi khi nấu trên đồ gang Lodge;</p>\n<p>- Giữ nhiệt lâu, tỏa nhiệt đều khắp bề mặt, giúp cho thực phẩm chín đều, có lớp vỏ hấp dẫn;</p>\n<p>- Hoạt động tốt trên tất cả các bề mặt bếp.</p>\n<p> </p>\n<p><strong>Xem thêm:</strong></p>\n<p><span style=\"color: #0000ff;\"><em><a title=\"Cách tôi dầu cho chảo gang Lodge\" href=\"../../tin-tuc-san-pham/khi-nao-can-toi-dau-cho-chao-gang-va-cach-toi-dau-bang-bep-39.html\" style=\"color: #0000ff;\">Khi nào cần tôi dầu cho chảo gang Lodge và cách tôi dầu bằng bếp</a></em></span></p>\n<p><span style=\"color: #0000ff;\"><em><a title=\"Mẹo làm sạch chảo gang Lodge\" href=\"../../cham-soc-va-su-dung/meo-lam-sach-chao-gang-lodge-sau-moi-lan-su-dung-25.html\" style=\"color: #0000ff;\">Mẹo làm sạch chảo gang Lodge</a></em></span></p>\n<p> </p>\n<h2><span xss=\"removed\" style=\"color: #993300;\">Các sản phẩm đồ bếp chuyên dụng của Lodge</span></h2>\n<p>Với mong muốn đáp ứng tối đa nhu cầu khách hàng và phát triển thương hiệu, Lodge Cast Iron luôn luôn lắng nghe và phát triển, cho ra mắt thị trường các dòng sản phẩm mới. Các sản phẩm của Lodge luôn đa dạng về mẫu mã và kích thước, đáp ứng các yêu cầu nấu nướng khách hàng một cách tốt nhất. </p>\n<p style=\"text-align: center;\">  <img src=\"../../uploads/r/thuong-hieu/noi-chao-gang-lodge.jpg\" alt=\"Các sản phẩm hãng Lodge đa dạng\" width=\"574\" height=\"574\" /></p>\n<p style=\"text-align: center;\"><em>Các sản phẩm hãng Lodge đa dạng về mẫu mã, kích thước</em></p>\n<p>Một vài dòng sản phẩm Lodge Cast Iron đang được ưa thích nhất hiện nay:</p>\n<p> </p>\n<p>- Chảo gang Lodge Cast Iron</p>\n<p>- <a title=\"Chảo thép carbon Lodge Cast Iron\" href=\"../../chao-thep-carbon/brands/lodgecastiron\">Chảo thép carbon Lodge Cast Iron</a></p>\n<p>- Nồi gang Lodge Cast Iron</p>\n<p>- Nồi gang phủ gốm Lodge Cast Iron</p>\n<p>-<a title=\" Khay và bếp nướng Lodge\" href=\"../../khay-va-bep-nuong/brands/lodgecastiron\"> Khay và bếp nướng</a></p>\n<p>- <a title=\"Phụ kiện Lodge Cast Iron\" href=\"../../phu-kien-lodge/brands/lodgecastiron\">Phụ kiện Lodge Cast Iron</a></p>\n<p>  </p>\n<p>Nếu bạn đang tìm kiếm dụng cụ bếp bền bỉ, linh hoạt, mang lại những bữa ăn hấp dẫn, an toàn cho sức khỏe, hãy tham khảo các sản phẩm của<a title=\"Lodge Cast Iron\" href=\"../../brands/lodgecastiron\"><strong> Lodge Cast Iron</strong> </a>nhé!</p>\n<p><span xss=\"removed\" style=\"color: #993300;\"><strong><em>Hãy liên hệ ngay Chef Studio để đặt hàng và được tư vấn chi tiết. </em></strong></span></p>\n<p><span xss=\"removed\" style=\"color: #993300;\"><strong><em>Chef Studio tự hào là đơn vị phân phối chính hãng các sản phẩm hãng Lodge Cast Iron tại Việt Nam.</em></strong></span></p>\n<p><img src=\"../../uploads/r/thuong-hieu/chefstudio-viet-nam.jpg\" alt=\"Chefstudio Việt Nam\" /></p>\n<p><strong>CHEF STUDIO </strong></p>\n<p><strong>Hotline:</strong><strong> <span style=\"color: #993300;\">084 888 18 00</span></strong></p>\n<p>Email: <span style=\"color: #993300;\"><a title=\"contact@chefstudio.vn\" href=\"mailto:contact@chefstudio.vn\" style=\"color: #993300;\">contact@chefstudio.vn</a></span></p>', 'Lodge Cast Iron', 'Thương hiệu Lodge Cast Iron nổi tiếng của Mỹ, chuyên về dụng cụ bếp bằng gang như nồi gang, chảo gang, khay và bếp nướng', 'lodgecastiron', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0: not valid, 1 valid',
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(65) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `group_id` (`group_id`);

--
-- Chỉ mục cho bảng `admin_group`
--
ALTER TABLE `admin_group`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Chỉ mục cho bảng `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`menugroup_id`),
  ADD KEY `head_id` (`menuhead_id`);

--
-- Chỉ mục cho bảng `admin_menuhead`
--
ALTER TABLE `admin_menuhead`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `admin_permission`
--
ALTER TABLE `admin_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`,`category`);

--
-- Chỉ mục cho bảng `admin_permission_group`
--
ALTER TABLE `admin_permission_group`
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Chỉ mục cho bảng `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `agency_category`
--
ALTER TABLE `agency_category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer_comment`
--
ALTER TABLE `customer_comment`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menu_banner`
--
ALTER TABLE `menu_banner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD KEY `order_id` (`order_id`);

--
-- Chỉ mục cho bảng `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `popup`
--
ALTER TABLE `popup`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `price_product`
--
ALTER TABLE `price_product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level1_id` (`parent_id`);

--
-- Chỉ mục cho bảng `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `trademark`
--
ALTER TABLE `trademark`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `admin_group`
--
ALTER TABLE `admin_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9426;

--
-- AUTO_INCREMENT cho bảng `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT cho bảng `admin_menuhead`
--
ALTER TABLE `admin_menuhead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `admin_permission`
--
ALTER TABLE `admin_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86508;

--
-- AUTO_INCREMENT cho bảng `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `agency_category`
--
ALTER TABLE `agency_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `customer_comment`
--
ALTER TABLE `customer_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT cho bảng `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT cho bảng `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `menu_banner`
--
ALTER TABLE `menu_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=423;

--
-- AUTO_INCREMENT cho bảng `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT cho bảng `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `popup`
--
ALTER TABLE `popup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `price_product`
--
ALTER TABLE `price_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=762;

--
-- AUTO_INCREMENT cho bảng `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT cho bảng `product_type`
--
ALTER TABLE `product_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT cho bảng `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `trademark`
--
ALTER TABLE `trademark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `admin_log`
--
ALTER TABLE `admin_log`
  ADD CONSTRAINT `admin_log_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD CONSTRAINT `admin_menu_ibfk_1` FOREIGN KEY (`menuhead_id`) REFERENCES `admin_menuhead` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `admin_permission_group`
--
ALTER TABLE `admin_permission_group`
  ADD CONSTRAINT `admin_permission_group_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `admin_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_permission_group_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `admin_permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `order_detail_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `product_type`
--
ALTER TABLE `product_type`
  ADD CONSTRAINT `product_type_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `product_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
