<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 09:59
 */
class Breadcrumb_controller extends MController
{
    function index(...$slugs)
    {
        $vars['slugs'] = $slugs;
        $this->load->view(PUBLIC_THEME . "/breadcrumb_view", $vars);
    }
}