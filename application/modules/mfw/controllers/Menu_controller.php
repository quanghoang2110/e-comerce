<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

/**
 * Class Slider_controller
 */
class Menu_controller extends MController
{
    /**
     * load num sliders
     * @param int $num
     */
    function index($num = 7)
    {
        $this->_load_data_from("menu");
        $vars["list"] = $this->_model->_get_limit(
            array('valid' => 1, 'type' => 0),
            array('position' => 'asc'),
            1, $num
        );
        $this->load->view(PUBLIC_THEME . "/menu_view", $vars);
    }

    /**
     * @param int $num
     * @param string $body_id
     */
    function vertical($num = 10, $body_id = "index")
    {
        $this->_load_data_from("menu");
        $vars["list"] = $this->_model->_get_limit(
            array('valid' => 1, 'type' => 1),
            array('position' => 'asc'),
            1, $num
        );
        $vars['body_id'] = $body_id;
        $this->load->view(PUBLIC_THEME . "/menu_vertical_view", $vars);
    }

    /**
     * @param $num
     */
    function banner($num)
    {
        $this->_load_data_from("menu_banner");
        $vars["list"] = $this->_model->_get_limit(
            array('valid' => 1),
            array('position' => 'asc'),
            1, $num
        );
        $this->load->view(PUBLIC_THEME . "/menu_banner_view", $vars);
    }

    /**
     * @param $num
     */
    function menu_frontend($num){
        $this->_load_data_from('product_category');
        $list_product_category = $this->_model->_get_limit(
            array('valid' => 1, 'is_menu' => 1),
            array('position' => 'asc'),
            1, $num
        );
        $arrayCateId = [];
        foreach ($list_product_category as $index => $item){
            $arrayCateId[] = $item->id;
            $this->_load_data_from('product_category');
           $childrens  = $this->_model->_get_limit(
                array('valid' => 1, 'parent_id' => $item->id),
                array('position' => 'asc'),
                1, 10
            );
            foreach ($childrens as $itemChildrend){
                $arrayCateId[] = $itemChildrend->id;
                $this->_load_data_from('product_category');
                $children  = $this->_model->_get_limit(
                    array('valid' => 1, 'parent_id' => $itemChildrend->id),
                    array('position' => 'asc'),
                    1, 10
                );
                $itemChildrend->children = $children;
                foreach ($children as $val){
                    $arrayCateId[] = $val->id;
                }
            }
            $list_product_category[$index]->childrens = $childrens;

            $array_trademark_id = $item->brand_id ? json_decode($item->brand_id) : [];
            if($array_trademark_id) {
                $this->_load_data_from('trademark');
                $this->_model->_where_row = 'id';
                $this->_model->_where_in_array = $array_trademark_id;
                $list_product_category[$index]->trademark = $this->_model->_get_limit(['valid' => 1], ['position' => 'asc'], 1, $num);
            }
        }

        $vars["list"] = $list_product_category;
            $this->load->view(PUBLIC_THEME . "/menu_view", $vars);
    }

    /**
     * @param $num
     */
    function menu_mobi_frontend($num){
        $this->_load_data_from('product_category');
        $list_product_category = $this->_model->_get_limit(
            array('valid' => 1, 'is_menu' => 1),
            array('position' => 'asc'),
            1, $num
        );
        $arrayCateId = [];
        foreach ($list_product_category as $index => $item){
            $arrayCateId[] = $item->id;
            $this->_load_data_from('product_category');
            $childrens  = $this->_model->_get_limit(
                array('valid' => 1, 'parent_id' => $item->id),
                array('position' => 'asc'),
                1, 10
            );
            foreach ($childrens as $itemChildrend){
                $arrayCateId[] = $itemChildrend->id;
                $this->_load_data_from('product_category');
                $children  = $this->_model->_get_limit(
                    array('valid' => 1, 'parent_id' => $itemChildrend->id),
                    array('position' => 'asc'),
                    1, 7
                );
                $itemChildrend->children = $children;
                foreach ($children as $val){
                    $arrayCateId[] = $val->id;
                }
            }
            $list_product_category[$index]->childrens = $childrens;

            $array_trademark_id = $item->brand_id ? json_decode($item->brand_id) : [];
            if($array_trademark_id) {
                $this->_load_data_from('trademark');
                $this->_model->_where_row = 'id';
                $this->_model->_where_in_array = $array_trademark_id;
                $list_product_category[$index]->trademark = $this->_model->_get_limit(['valid' => 1], ['position' => 'asc'], 1, $num);
            }
        }

        $vars["list"] = $list_product_category;
        $this->load->view(PUBLIC_THEME . "/menu_mobi_view", $vars);
    }

    /**
     * @param $num
     */
    function menu_top_header($num){
        $this->_load_data_from('page');
        $vars["pages"] = $this->_model->_get_limit(['is_menu' => 1]);
        $this->load->view(PUBLIC_THEME . "/menu_top_header_view", $vars);
    }
}