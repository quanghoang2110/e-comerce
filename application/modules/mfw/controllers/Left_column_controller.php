<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 09:59
 */
class Left_column_controller extends MController
{
    /**
     * @param int $num_banner
     * @param int $num_product
     */
    function index($num_banner = 3, $num_product = 5)
    {
        $this->_load_data_from("banner");
        $vars["list_banner"] = $this->_model->_get_limit(
            array('valid' => 1),
            array('position' => 'asc'),
            1, $num_banner
        );

        //hot products
        $this->_load_data_from('product');
        $vars['list_product'] = $this->_model->_get_limit(
            array('valid' => 1, 'is_hot' => 1),
            array('position' => 'asc'),
            1, $num_product, 'id,name,image,category_slug,price_1,price_2'
        );

        $this->load->view(PUBLIC_THEME . "/left_column_view", $vars);
    }
}