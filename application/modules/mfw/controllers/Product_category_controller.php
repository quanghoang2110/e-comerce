<?php

/**
 * Class Product_controller
 */
class Product_category_controller extends MFWController
{
    function __construct()
    {
        parent::__construct();
        $this->_load_data_from('product_category');
        $this->_folder_view = PUBLIC_THEME . '/product_category/';
    }

    function hot($num_products = 5)
    {
        $vars['list'] = $this->_model->_get_limit(
            array('valid' => 1),
            array('is_hot' => 'desc'),
            1,
            $num_products
        );
        $this->load->view($this->_folder_view . "hot_view", $vars);
    }

    function children($cate_id, $num_products = 5)
    {
        if(empty($cate_id)){
            $vars['list'] = $this->_model->_get_limit(
                array('valid' => 1, 'is_menu' => 1),
                array('is_hot' => 'desc'),
                1,
                $num_products
            );
        }else {
            $vars['list'] = $children = $this->_model->_get_limit(
                array('valid' => 1, 'parent_id' => $cate_id),
                array('position' => 'asc'),
                1,
                $num_products
            );
            if (empty($children)) {
                $cate = $this->_model->_get_row(['id' => $cate_id]);
                $vars['list'] = $list_cate = $this->_model->_get_limit(
                    array('valid' => 1, 'parent_id' => $cate->parent_id),
                    array('position' => 'asc'),
                    1,
                    $num_products
                );
                if (empty($list_cate)) {
                    $vars['list'] = $this->_model->_get_limit(
                        array('valid' => 1, 'is_menu' => 1),
                        array('is_hot' => 'desc'),
                        1,
                        $num_products
                    );
                }
            }
        }
        $this->load->view($this->_folder_view . "category_children_view", $vars);
    }

    function children_1($cate_id, $num_products = 5)
    {
        if(empty($cate_id)){
            $vars['list'] = $this->_model->_get_limit(
                array('valid' => 1, 'is_menu' => 1),
                array('is_hot' => 'desc'),
                1,
                $num_products
            );
        }else {
            $vars['list'] = $children = $this->_model->_get_limit(
                array('valid' => 1, 'parent_id' => $cate_id),
                array('position' => 'asc'),
                1,
                $num_products
            );
            if (empty($children)) {
                $cate = $this->_model->_get_row(['id' => $cate_id]);
                $vars['list'] = $list_cate = $this->_model->_get_limit(
                    array('valid' => 1, 'parent_id' => $cate->parent_id),
                    array('position' => 'asc'),
                    1,
                    $num_products
                );
                if (empty($list_cate)) {
                    $vars['list'] = $this->_model->_get_limit(
                        array('valid' => 1, 'is_menu' => 1),
                        array('is_hot' => 'desc'),
                        1,
                        $num_products
                    );
                }
            }
        }
        $this->load->view($this->_folder_view . "category_children_view_1", $vars);
    }

    /**
     * @param $num
     */
    function sidebar($num){
        $this->_load_data_from('product_category');
        $vars["list"] = $this->_model->_get_limit(['is_menu' => 1]);
        $this->load->view($this->_folder_view . "/sidebar_discount", $vars);
    }
}