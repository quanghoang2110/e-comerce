<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 09:59
 */
class Banner_controller extends MController
{
    /**
     * @param int $nums
     */
    function index($nums = 1, $page = 1)
    {
        $this->_load_data_from("banner");
        $vars["list"] = $this->_model->_get_limit(
            array('valid' => 1, 'is_hot' => 0),
            array('position' => 'asc'),
            $page, $nums
        );
        $this->load->view(PUBLIC_THEME . "/banner_view", $vars);
    }

    function home($nums = 7, $page = 1){
        $this->_load_data_from("banner");
        $vars["list"] = $this->_model->_get_limit(
            array('valid' => 1, 'is_hot' => 1),
            array('position' => 'asc'),
            $page, $nums
        );
        $this->load->view(PUBLIC_THEME . "/banner_hot_view", $vars);
    }
}