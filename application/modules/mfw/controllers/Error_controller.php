<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Error_controller extends MController
{
    function page_not_found(){
        $this->load->view(PUBLIC_THEME . "/page_not_found_view");
    }
}