<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Pagination_controller extends MFrontendController
{
    /**
     * @param $url
     * @param $total
     * @param $limit
     */
    function index($url, $total, $limit)
    {
        $this->load->library('pagination');
        $config['base_url'] = $url;
        $config['total_rows'] = $total;
        $config['per_page'] = $limit;
//        $config['attributes'] = array('class' => 'js-search-link');

        $config['full_tag_open'] = '<ul class="paginations">';
        $config['full_tag_close'] = '</ul>';

        //num tag
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';

        //current tag
        $config['cur_tag_open'] = '<li class="active"><a class="smooth" href="#">';
        $config['cur_tag_close'] = '</a></li>';

        //prev tag
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        //next
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        //first
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        //last
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        //
        $config['num_links'] = 2;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        //

        $this->pagination->initialize($config);
        $vars['paging'] = $paging = $this->pagination->create_links();
        if ($paging) {
            $this->_init_page();
            $vars['paging_info'] = $this->_base_get_page_info($total);
            $this->load->view(PUBLIC_THEME . '/pagination_view', $vars);
        }
    }
}