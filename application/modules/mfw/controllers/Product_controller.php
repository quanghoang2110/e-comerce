<?php

/**
 * Class Product_controller
 */
class Product_controller extends MFWController
{
    function __construct()
    {
        parent::__construct();
        $this->_load_data_from('product');
        $this->_folder_view = PUBLIC_THEME . '/product/';
    }

    /**
     * @param $id
     * @param $page
     * @param $limit
     * @return View_object
     */
    function index($id, $page, $limit): View_object
    {
        $limit = $this->_input_get('limit');
        $limit = !empty($limit) ? $limit : 12;
        $view = new View_object();
        $this->_load_data_from("product_category");
        $vars['list_category'] = $this->_model->_get(['valid' => 1]);
        $vars['category'] = $category = $this->_model->_get_row(['id' => $id]);
        if ($category) {
            if ($category->image) {
                $view->image = base_url($category->image);
            }
            $view->title = get_text_not_empty($category->meta_title, $category->name);
            $view->desc = get_text_not_empty($category->meta_desc, $category->desc);
        }
        $this->load->model('model_product');
        $model_product = new Model_product();
        $array_child_id = $model_product->_get_category_children($id);
        $this->_load_data_from("product");
        $this->_model->_where_row = 'category_id';
        $this->_model->_where_in_array = $array_child_id;
        $where = array('valid' => 1);
        $uri_segment_1 = $this->uri->segment(1);
        $uri_segment_2 = $this->uri->segment(2);
        $uri_segment_3 = $this->uri->segment(3);
        $brands = [];
        if($uri_segment_3){
            $brands = $model_product->getBrandsBySlug($uri_segment_3);
            $pagingUrl = base_url($uri_segment_1.'/'.$uri_segment_2.'/'.$uri_segment_3);
        }else {
            $pagingUrl = base_url($category->slug);
        }
        $order = array();
        $order['position'] = 'asc';
        $order['id'] = 'desc';

        $trademark = $this->_input_get('brands');
        $size = $this->_input_get('size');
        $price = $this->_input_get('price');
        $prod_type = $this->_input_get('prod_type');

        $sort_by = $this->_input_get('sort_by');
        $order = array();
        if (in_array($sort_by, array('gia-cao-thap', 'gia-thap-cao'))) {
            $order_by = $sort_by == 'gia-cao-thap' ? 'desc' : 'asc';
            $order = array(
                'price_2' => $order_by,
                'price_1' => $order_by
            );
        } else if ($sort_by == 'ban-chay') {
            $order = array(
                'is_best_seller' => 'desc'
            );
        }
        if(empty($trademark) && empty($size) && empty($price) && empty($prod_type)){
            $pagingUrl = $pagingUrl.'?sort_by='.$sort_by.'&limit='.$limit;
        }else{
            $pagingUrl = $this->setting_url($pagingUrl, $trademark, $size, $price, $prod_type);
            $pagingUrl = $pagingUrl.'&sort_by='.$sort_by.'&limit='.$limit;
        }

        $vars['paging_url'] = $pagingUrl;
        $trademark = ($trademark !=="" && $trademark !== NULL) ? explode(',', rtrim($trademark, ',')) : [];
        $size = ($size !== "" && $size !== NULL) ? explode(',', rtrim($size, ',')) : [];
        $price = ($price !== "" && $price !== NULL) ? explode(',', rtrim($price, ',')) : [];
        $prod_type = ($prod_type !== "" && $prod_type !== NULL) ? explode(',', rtrim($prod_type, ',')) : [];
        if(!empty($brands)) {
            array_push($trademark, $brands->id);
        }

        $this->load->model('model_product');
        $model = new Model_product();
        $vars['list'] = $model->filter($array_child_id, $trademark, $size, $price, $prod_type, $order, $page, $limit);
        $vars['total'] = count($model->filter($array_child_id, $trademark, $size, $price, $prod_type, $order,$page, null));
        $vars['limit'] = $limit;
        $vars['category'] = $category;
        $vars['brands_id'] = $brands ? $brands->id : '';
        $vars['prod_type'] = $prod_type;
        $vars['page_css'] = 'product/product_css';
        $vars['page_js'] = 'product/product_js';
        $view->content = $this->load->view($this->_folder_view . "product_view", $vars, true);
        return $view;
    }

    /**
     * @param $keyword
     * @param $page
     * @param $limit
     * @return View_object
     */
    function search($keyword, $page, $limit): View_object
    {
        $limit = $this->_input_get('limit');
        $limit = $this->_input_get('limit');
        $limit = !empty($limit) ? $limit : 12;
        $view = new View_object();
        $trademark = $this->_input_get('brands');
        $arr_trademark = ($trademark !=="" && $trademark !== NULL) ? explode(',', rtrim($trademark, ',')) : [];
        $size = $this->_input_get('size');
        $arr_size = ($size !== "" && $size !== NULL) ? explode(',', rtrim($size, ',')) : [];
        $price = $this->_input_get('price');
        $arr_price = ($price !== "" && $price !== NULL) ? explode(',', rtrim($price, ',')) : [];
        $prod_type = $this->_input_get('prod_type');
        $arr_prod_type = ($prod_type !== "" && $prod_type !== NULL) ? explode(',', rtrim($prod_type, ',')) : [];

        $this->_load_data_from("product_category");
        $vars['list_category'] = $this->_model->_get(['valid' => 1]);

        $this->load->model('model_product');
        $model = new Model_product();

        // products
        $order = array();
        $order['position'] = 'asc';
        $order['id'] = 'desc';

        $category = new StdClass();

        $sort_by = $this->_input_get('sort_by');
        $order = array();
        if (in_array($sort_by, array('gia-cao-thap', 'gia-thap-cao'))) {
            $order_by = $sort_by == 'gia-cao-thap' ? 'desc' : 'asc';
            $order = array(
                'price_2' => $order_by,
                'price_1' => $order_by
            );
        } else if ($sort_by == 'ban-chay') {
            $order = array(
                'is_best_seller' => 'desc'
            );
        }
        $category->name = "Tìm kiếm: " . $keyword;
        $vars['current_url'] = 'tim-kiem';
        $vars['list'] = $list = $model->get_search($keyword, $order, $arr_trademark, $arr_size, $arr_price, $arr_prod_type, $page, $limit);
        $vars['total'] = count($model->get_search($keyword, $order, $arr_trademark, $arr_size, $arr_price, $arr_prod_type, $page, null));
        $pagingUrl = base_url(ROUTE_SEARCH . '?search=' . $keyword);
        if (empty($trademark) && empty($size) && empty($price) && empty($prod_type)) {
            $pagingUrl = $pagingUrl . '&sort_by=' . $sort_by . '&limit=' . $limit;
        } else {
            $pagingUrl = $this->setting_url($pagingUrl, $trademark, $size, $price, $prod_type, 'search');
            $pagingUrl = $pagingUrl . '&sort_by=' . $sort_by . '&limit=' . $limit;
        }
        $vars['paging_url'] = $pagingUrl;


        $category->id = 0;
        $category->desc = "";
        $vars['category'] = $category;
        $vars['limit'] = $limit;
        $vars['prod_type'] = $arr_prod_type;
        $vars['keyword'] = $keyword;
        $vars['page_css'] = 'product/product_css';
        $vars['page_js'] = 'product/product_js';
        $vars['brands_id'] = null;
        $view->content = $this->load->view($this->_folder_view . "product_view", $vars, true);
        return $view;
    }

    /**
     * @param $id
     * @return View_object
     */
    function content($id)
    {
        $view = new View_object();
        $this->_load_data_from('site_setting');
        $vars['setting'] = $this->_model->_get_row();
        $this->_load_data_from("product");
        $vars['data'] = $product = $this->_model->_get_row(array('id' => $id, 'valid' => 1));
        if(empty($product)){
            $view->content = get_mfw_module("error", "page_not_found");
        }
        $vars['gifts'] = [];
        if ($product) {
            $this->_load_data_from("product_category");
            $vars['category'] = $this->_model->_get_row(['id' => $product->category_id]);

            $images = json_decode($product->image);
            if (is_array($images)) {
                $view->image = base_url($images[0]);
            }
            $view->title = get_text_not_empty($product->meta_title, $product->name);
            $view->desc = get_text_not_empty($product->meta_desc, word_limiter(strip_tags($product->content), 160));

            $gift_id = $product->gift ? json_decode($product->gift) : [];
            if($gift_id) {
                $this->load->model('model_product');
                $model = new Model_product();
                $vars['gifts'] = $model->_get_list_gif($gift_id);
            }

            $vars['page_css'] = 'product/product_content_css';
            $vars['page_js'] = 'product/product_content_js';
            $view->content = $this->load->view($this->_folder_view . "product_content_view", $vars, true);
        }
        return $view;
    }

    /**
     * ajax quick view
     * @param $id
     * @return string
     */
    function quick_view($id)
    {
        if ($id) {
            $this->_load_data_from("product");
            $vars['item'] = $this->_model->_get_row(['id' => $id]);
            $ajax_resp["product"] = array("id" => $id, "id_product_attribute" => $id);
            $ajax_resp["quickview_html"] = $this->load->view($this->_folder_view . "product_quick_view", $vars, TRUE);
            return json_encode($ajax_resp);
        }
    }

    /**
     * @param Product_object $product_object
     */
    function slider(Product_object $product_object)
    {
        $this->_load_data_from("product");
        $vars['title'] = $product_object->title;

        $order = array("is_new" => "desc", 'position' => 'asc');
        $where['valid'] = 1;
        if ($product_object->id) {
            $where['category_id'] = $product_object->id;
            array_reverse($order);
        }

        if ($product_object->same_id) {
            $where['id!='] = $product_object->same_id;
        }

        $vars["list"] = $this->_model->_get_limit(
            $where,
            $order,
            1, $product_object->num_posts
        );
        $this->load->view($this->_folder_view . "slider_product_view", $vars);
    }

    function hot($num_products = 5)
    {
        $this->load->model('model_product');
        $model = new Model_product();
        $vars['list'] = $model->get_hot(1, $num_products);
        $this->load->view($this->_folder_view . "product_hot_view", $vars);
    }

    function best_seller($num_products = 5, $categoryId = '')
    {
        $this->load->model('model_product');
        $model = new Model_product();
        $vars["list"] = $list = $model->get_best_seller(1, $num_products, $categoryId);
        if ($list) {
            $this->load->view($this->_folder_view . "product_best_seller_view", $vars);
        }
    }

    function best_seller_left($num_products = 5)
    {
        $this->load->model('model_product');
        $model = new Model_product();
        $vars["list"] = $list = $model->get_best_seller(1, $num_products);
        if ($list) {
            $this->load->view($this->_folder_view . "product_best_seller_left_view", $vars);
        }
    }

    function discount($num_products = 5, $categoryId = '')
    {
        $this->load->model('model_product');
        $model = new Model_product();
        $vars["list"] = $list = $model->get_discount(1, $num_products, $categoryId);
        if ($list) {
            $this->load->view($this->_folder_view . "product_discount_view", $vars);
        }
    }

    function related($product_arr)
    {
        $this->load->model('model_product');
        $model = new Model_product();
        $vars["list"] = $model->get_related($product_arr);
        $this->load->view($this->_folder_view . "product_slider_view", $vars);
    }

    function same($except_id, $num_products = 5)
    {
        $this->load->model('model_product');
        $model = new Model_product();
        $vars["list"] = $model->get_same($except_id, $num_products);
        $this->load->view($this->_folder_view . "product_slider_view", $vars);
    }

    /**
     * news - mega menu
     * @param int $num_products
     */
    function mega_menu($num_products = 3)
    {
        $this->_load_data_from("product_category");
        $cats = $this->_model->_get_limit(array('valid' => 1), array('position' => 'asc'), 1, 6);
        if ($cats) {
            $this->_load_data_from('product');
            $order = array('position' => 'asc');
            $where = array('is_mega_menu' => 1, 'valid' => 1);
            foreach ($cats as $item) {
                $where['category_id'] = $item->id;
                $item->products = $this->_model->_get_limit(
                    $where,
                    $order,
                    1, $num_products
                );
            }
        }
        $vars['list'] = $cats;

        $this->load->view($this->_folder_view . "mega_menu_view", $vars);
    }

    function list_discount($page, $limit){
        $view = new View_object();
        $where = array('valid' => 1, 'is_discount' => 1);

        $order = array();
        $pagingUrl = base_url('khuyen-mai');
        $order['position'] = 'asc';
        $order['id'] = 'desc';
        $this->_load_data_from("product");
        $vars['list'] = $this->_model->_get_limit($where, $order, $page, $limit);
        $vars['total'] = $this->_model->_count($where);
        $vars['paging_url'] = $pagingUrl;
        $vars['limit'] = $limit;
        $vars['page_css'] = 'product/product_css';
        $vars['page_js'] = 'product/product_js';
        $view->content = $this->load->view($this->_folder_view . "product_list_discount_view", $vars, true);
        return $view;
    }

    function list_product_related_news($products_related , $num = 5){
        $str = rtrim($products_related,",");
        $array_id = explode(',', $str);
        $this->_load_data_from('product');
        $this->_model->_where_row = 'id';
        $this->_model->_where_in_array = $array_id;
        $vars['list'] = $this->_model->_get_limit(['valid' => 1], ['position' => 'asc'], 1, $num);
        $this->load->view($this->_folder_view . "product_related_news_view", $vars);
    }

    function list_accessories($number = 5){
        $this->_load_data_from('product');
        $vars['list'] = $this->_model->_get_limit(['valid' => 1, 'category_slug' => 'phu-kien'], ['position' => 'asc', 'id' => 'desc'], 1, $number);
        $this->load->view($this->_folder_view . "accessories_view", $vars);
    }

    function filter_product($page, $limit): View_object
    {
        $view = new View_object();
        $this->_load_data_from("product_category");
        $vars['list_category'] = $this->_model->_get(['valid' => 1]);
        $category = new StdClass();
        $category->name = "";
        $category->id = 0;
        $category->desc = "";
        $vars['category'] = $category;

        $trademark = $this->_input_get('trademark');
        $trademark = ($trademark !=="" && $trademark !== NULL) ? explode(',', rtrim($trademark, ',')) : [];
        $size = $this->_input_get('size');
        $size = ($size !== "" && $size !== NULL) ? explode(',', rtrim($size, ',')) : [];
        $price = $this->_input_get('price');
        $price = ($price !== "" && $price !== NULL) ? explode(',', rtrim($price, ',')) : [];

        $this->load->model('model_product');
        $model = new Model_product();
        $vars['list'] = $list = $model->filter($trademark, $size, $price, $page, $limit);
        $vars['total'] = count($list);
        $vars['limit'] = $limit;
        $vars['page'] = $page;
        $vars['paging_url'] = base_url(ROUTE_FILTER . '?trademark=' . $trademark .'&size='. $size . '&price='. $price);

        $vars['page_css'] = 'product/product_css';
        $vars['page_js'] = 'product/product_js';
        $view->content = $this->load->view($this->_folder_view . "product_view", $vars, true);
        return $view;
    }

    function list_product_related_other_product($category_id, $number){
        $this->load->model('model_product');
        $model_product = new Model_product();
        $array_child_id = $model_product->_get_category_children($category_id);

        $this->_load_data_from("product");
        $this->_model->_where_row = 'category_id';
        $this->_model->_where_in_array = $array_child_id;
        $where = array('valid' => 1);
        $order = array();
        $order['id'] = 'RANDOM';
        $vars['list'] = $this->_model->_get_limit($where, $order, 1, $number);
        $this->load->view($this->_folder_view . "product_related_other_product_view", $vars);
    }

    function setting_url($pagingUrl, $trademark, $size, $price, $prod_type,$type = 'product'){
        if(!empty($trademark) && empty($size) && empty($price) && empty($prod_type)){
            if($type == 'product'){
                $pagingUrl = $pagingUrl.'?brands=' . $trademark;
            }else{
                $pagingUrl = $pagingUrl.'&brands=' . $trademark;
            }
        }else if(empty($trademark) && !empty($size) && empty($price) && empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?size=' . $size;
            }else{
                $pagingUrl = $pagingUrl . '&size=' . $size;
            }
        }else if(empty($trademark) && empty($size) && !empty($price) && empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?price=' . $price;
            }else{
                $pagingUrl = $pagingUrl . '&price=' . $price;
            }
        }else if(empty($trademark) && empty($size) && empty($price) && !empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?prod_type=' . $prod_type;
            }else{
                $pagingUrl = $pagingUrl . '&prod_type=' . $prod_type;
            }
        }else if(!empty($trademark) && !empty($size) && empty($price) && empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?brands=' . $trademark . '&size=' . $size;
            }else{
                $pagingUrl = $pagingUrl . '&brands=' . $trademark . '&size=' . $size;
            }
        }else if(!empty($trademark) && empty($size) && !empty($price) && empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?brands=' . $trademark . '&price=' . $price;
            }else{
                $pagingUrl = $pagingUrl . '&brands=' . $trademark . '&price=' . $price;
            }
        }else if(!empty($trademark) && empty($size) && empty($price) && !empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?brands=' . $trademark . '&prod_type=' . $prod_type;
            }else{
                $pagingUrl = $pagingUrl . '&brands=' . $trademark . '&prod_type=' . $prod_type;
            }
        }else if(empty($trademark) && !empty($size) && !empty($price) && empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?size=' . $size . '&price=' . $price;
            }else{
                $pagingUrl = $pagingUrl . '&size=' . $size . '&price=' . $price;
            }
        }else if(empty($trademark) && !empty($size) && empty($price) && !empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?size=' . $size . '&prod_type=' . $prod_type;
            }else{
                $pagingUrl = $pagingUrl . '&size=' . $size . '&prod_type=' . $prod_type;
            }
        }else if(!empty($trademark) && !empty($size) && !empty($price) && empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?brands=' . $trademark . '&size=' . $size . '&price=' . $price;
            }else{
                $pagingUrl = $pagingUrl . '&brands=' . $trademark . '&size=' . $size . '&price=' . $price;
            }
        }else if(!empty($trademark) && !empty($size) && empty($price) && !empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?brands=' . $trademark . '&size=' . $size . '&prod_type=' . $prod_type;
            }else{
                $pagingUrl = $pagingUrl . '&brands=' . $trademark . '&size=' . $size . '&prod_type=' . $prod_type;
            }
        }else if(empty($trademark) && !empty($size) && !empty($price) && !empty($prod_type)){
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?price=' . $price . '&size=' . $size . '&prod_type=' . $prod_type;
            }else{
                $pagingUrl = $pagingUrl . '&price=' . $price . '&size=' . $size . '&prod_type=' . $prod_type;
            }
        } else{
            if($type == 'product') {
                $pagingUrl = $pagingUrl . '?brands=' . $trademark . '&size=' . $size . '&price=' . $price.'&prod_type='.$prod_type;
            }else{
                $pagingUrl = $pagingUrl . '&brands=' . $trademark . '&size=' . $size . '&price=' . $price.'&prod_type='.$prod_type;
            }
        }
        return $pagingUrl;
    }

    function video_related($json_video_id){
        $arr_video_id = json_decode($json_video_id);
        $vars['list'] = [];
        if($arr_video_id) {
            $this->load->model('model_news');
            $model = new Model_news();
            $vars['list'] = $model->get_video_related_product($arr_video_id);
        }
        $this->load->view($this->_folder_view . "product_video_related_view", $vars);
    }
}