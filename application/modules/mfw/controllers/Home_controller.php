<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 09:59
 */
class Home_controller extends MController
{
    public function __construct()
    {
        parent::__construct();
        $this->_folder_view = 'home';
    }

    function index()
    {
        $this->_load_data_from("popup");
        $vars['popup'] = $this->_model->_get_row(['valid' => 1]);
        $vars['page_css'] = 'home/home_css';
        $vars['page_js'] = 'home/home_js';

        $this->load->view(PUBLIC_THEME . "/home/home_view", $vars);
    }
}