<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Slider_controller
 */
class Facebook_controller extends MController
{
    /**
     * @param Facebook_object $fb_obj
     */
    function index(Facebook_object $fb_obj)
    {
        $vars["fb_obj"] = $fb_obj;
        $this->load->view("facebook_view", $vars);
    }
}