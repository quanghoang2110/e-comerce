<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 1/23/2017
 * Time: 10:35
 */
class Model_product extends MModel
{
    private $tbl_product = 'product';

    function get_same($except_id, $limit)
    {
        $this->db->where("a.category_id IN (SELECT x.category_id FROM product x WHERE x.id={$except_id})", null, false);
        $this->db->where('a.id!=', $except_id);
        return $this->get_list(1, $limit);
    }

    function get_search($keyword, $order, $trademark = [], $size = [], $price = [], $arr_prod_type, $page, $limit)
    {
        if(!empty($price)) {
            $this->db->select('*');
            $this->db->from('price_product');
            $this->db->where_in('id', $price);
            $this->db->order_by('id', 'asc');
            $array_price = $this->db->get()->result();
        }

        if ($keyword) {
            $arr = explode(" ", $keyword);
            $keyword = "";
            $this->db->group_start();
            foreach ($arr as $index => $k) {
                $keyword = $k;
                $this->db->where("(a.name LIKE '%{$keyword}%' OR a.code LIKE '%{$keyword}%')");
            }
            $this->db->group_end();
        }

        if (!empty($trademark)) {
            $this->db->where_in('trademark', $trademark);
        }
        if (!empty($size)) {
            $this->db->where_in('size', $size);
        }

        if (!empty($arr_prod_type)) {
            $this->db->where_in('brand', $arr_prod_type);
        }

        if (!empty($price)) {
            $this->db->group_start();

            foreach ($array_price as $index => $item) {

                if(empty($item->price_2)){
                    $this->db->or_where('price_1 >='. $item->price_1);
                }else if(empty($item->price_1)){
                    $this->db->or_where('price_1 <='. $item->price_2);
                }else{
                    $this->db->or_where('price_1 BETWEEN ' . $item->price_1 . ' AND ' . $item->price_2);
                }
            }
            $this->db->group_end();
        }

        if ($order) {
            foreach ($order as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        return $this->get_list($page, $limit);
    }

    function get_related($products_id)
    {
        $this->db->where_in('a.id', $products_id);
        $this->db->order_by('a.is_hot', 'desc');
        return $this->_get_list(1, 5);
    }

    function get_best_seller($page, $limit, $categoryId = '')
    {
        $this->db->where('a.is_best_seller', 1);
        if ($categoryId) {
            $this->db->where('a.category_id', $categoryId);
        }
        return $this->_get_list($page, $limit);
    }

    function get_hot($page, $limit)
    {
        $this->db->order_by('a.is_hot', 'desc');
        $this->db->order_by('a.id', 'desc');
        return $this->_get_list($page, $limit);
    }

    function get_discount($page, $limit, $categoryId = '')
    {
        $this->db->where('a.is_discount', 1);
        $this->db->order_by('a.is_discount', 'desc');
        if ($categoryId) {
            $this->db->where('a.category_id', $categoryId);
        }
        return $this->_get_list($page, $limit);
    }

    function get_list($page, $limit, $search = "", $category = "")
    {
        $this->db->select("a.*, b.name as category");
        $this->db->from('product a');
        $this->db->join('product_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.valid', 1);
        if ($category) {
            $this->db->where('a.category_id', $category);
        }
        if ($search) {
            $this->db->like("a.name", $search);
        }
        if(!empty($limit)) {
            $this->db->limit($limit, ($page - 1) * $limit);
        }
        $this->db->order_by('id', 'desc');
        return $this->db->get()->result();
    }

    private function _get_list($page, $limit)
    {
        $this->db->select("a.*, b.slug as category_slug");
        $this->db->from('product a');
        $this->db->join('product_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.valid', 1);
        $this->db->where('b.valid', 1);
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('a.position', 'asc');
        $this->db->order_by('a.id', 'desc');
        return $this->db->get()->result();
    }

    public function _get_list_trademark($array_category_id)
    {
        $this->db->distinct();
        $this->db->select("trademark");
        $this->db->from('product');
        $this->db->where('valid', 1);
        $this->db->where_in('category_id', $array_category_id);
        $this->db->order_by('position', 'asc');
        return $this->db->get()->result();
    }

    public function _get_category_children($cate_id)
    {
        $this->db->select('id');
        $this->db->from('product_category');
        $this->db->where('parent_id', $cate_id);
        $this->db->where('valid', 1);
        $childrens = $this->db->get()->result();
        $__array_id[] = $cate_id;
        if (!empty($childrens)) {
            foreach ($childrens as $item) {
                $__array_id[] = $item->id;
                $this->db->select('id');
                $this->db->from('product_category');
                $this->db->where('parent_id', $item->id);
                $this->db->where('valid', 1);
                $children_1 = $this->db->get()->result();
                if (!empty($children_1)) {
                    foreach ($children_1 as $item_1) {
                        $__array_id[] = $item_1->id;
                        $this->db->select('id');
                        $this->db->from('product_category');
                        $this->db->where('parent_id', $item_1->id);
                        $this->db->where('valid', 1);
                        $children_2 = $this->db->get()->result();
                        if (!empty($children_2)) {
                            foreach ($children_2 as $item_2) {
                                $__array_id[] = $item_2->id;
                                $this->db->select('id');
                                $this->db->from('product_category');
                                $this->db->where('parent_id', $item_2->id);
                                $this->db->where('valid', 1);
                                $children_3 = $this->db->get()->result();
                                if (!empty($children_3)) {
                                    foreach ($children_3 as $item_3) {
                                        $__array_id[] = $item_3->id;
                                    }
                                };
                            }
                        }
                    }
                }
            }
        }
        return $__array_id;
    }

    public function get_faq_related($product_id, $page, $limit)
    {
        $product_id = $product_id . ',';
        $this->db->select("*");
        $this->db->from('faq');
        $this->db->where('valid', 1);
        $this->db->where("products_related LIKE'%{$product_id}%'");
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('position', 'asc');
        return $this->db->get()->result();
    }

    public function filter($array_id, $trademark = [], $size = [], $price = [], $prod_type=[], $order = [], $page, $limit = null)
    {
        if(!empty($price)) {
            $this->db->select('*');
            $this->db->from('price_product');
            $this->db->where_in('id', $price);
            $this->db->order_by('id', 'asc');
            $array_price = $this->db->get()->result();
        }

        $this->db->select();
        $this->db->from('product');
        $this->db->where('valid', 1);
        $this->db->where_in('category_id', $array_id);
        if (!empty($trademark)) {
            $this->db->where_in('trademark', $trademark);
        }
        if (!empty($size)) {
            $this->db->where_in('size', $size);
        }

        if (!empty($prod_type)) {
            $this->db->where_in('brand', $prod_type);
        }

        if (!empty($price)) {
            $this->db->group_start();

            foreach ($array_price as $index => $item) {

                if(empty($item->price_2)){
                    $this->db->or_where('price_1 >='. $item->price_1);
                }else if(empty($item->price_1)){
                    $this->db->or_where('price_1 <='. $item->price_2);
                }else{
                    $this->db->or_where('price_1 BETWEEN ' . $item->price_1 . ' AND ' . $item->price_2);
                }
            }
            $this->db->group_end();
        }
        if(!empty($order)){
            foreach ($order as $keyOrderBy => $valueOrderBy){
                $this->db->order_by($keyOrderBy, $valueOrderBy);
            }
        }

        if($limit != null) {
            $this->db->limit($limit, ($page - 1) * $limit);
        }
        $this->db->order_by('position', 'asc');
        $this->db->order_by('id', 'desc');
        return $this->db->get()->result();
    }

    public function getListProductByBrandsId($brandsId, $price = [], $size = [], $prod_type = [], $order = [], $page, $limit = null){
        if(!empty($price)) {
            $this->db->select('*');
            $this->db->from('price_product');
            $this->db->where_in('id', $price);
            $this->db->order_by('id', 'asc');
            $array_price = $this->db->get()->result();
        }
        $this->db->where('trademark', $brandsId);

        if (!empty($size)) {
            $this->db->where_in('size', $size);
        }

        if (!empty($prod_type)) {
            $this->db->where_in('brand', $prod_type);
        }

        if (!empty($price)) {
            $this->db->group_start();
            foreach ($array_price as $index => $item) {
                if(empty($item->price_2)){
                    $this->db->or_where('price_1 >='. $item->price_1);
                }else if(empty($item->price_1)){
                    $this->db->or_where('price_1 <='. $item->price_2);
                }else{
                    $this->db->or_where('price_1 BETWEEN ' . $item->price_1 . ' AND ' . $item->price_2);
                }
            }
            $this->db->group_end();
        }

        if ($order) {
            foreach ($order as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        return $this->get_list($page, $limit);
    }

    public function getBrandsBySlug($slug){
        $this->db->select('*');
        $this->db->from('trademark');
        $this->db->where('slug', $slug);
        return $this->db->get()->row();
    }

    public function getProductCategoryByBrandsId($brandsId){
        $this->db->select('product_category.*');
        $this->db->from('product_category');
        $this->db->join('product', 'product.category_id = product_category.id');
        $this->db->where('product.trademark', $brandsId);
        $this->db->group_by('product_category.id');
        $this->db->order_by('product_category.id', 'asc');
        return $this->db->get()->result();
    }

    public function _get_list_gif($arr_gift)
    {
        $this->db->select("a.*, b.slug as category_slug");
        $this->db->from('product a');
        $this->db->join('product_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.valid', 1);
        $this->db->where_in('a.id', $arr_gift);
        $this->db->order_by('a.id', 'desc');
        return $this->db->get()->result();
    }

    public function _get_list_brand($array_category_id)
    {
        $this->db->distinct();
        $this->db->select("brand");
        $this->db->from('product');
        $this->db->where('valid', 1);
        $this->db->where_in('category_id', $array_category_id);
        $this->db->order_by('position', 'asc');
        return $this->db->get()->result();
    }
}