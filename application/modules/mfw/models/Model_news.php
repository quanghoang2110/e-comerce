<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 1/23/2017
 * Time: 10:35
 */
class Model_news extends MModel
{
    function get_hot($page, $limit)
    {
        $this->db->select("a.*, b.slug as category_slug");
        $this->db->from('news a');
        $this->db->join('news_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.type', NEWS_TYPE_CONTENT);
        $this->db->where('a.valid', 1);
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('a.is_hot', 'desc');
        $this->db->order_by('a.position', 'asc');
        return $this->db->get()->result();
    }

    function get_video_related_product($array_id)
    {
        $this->db->select("a.*, b.slug as category_slug");
        $this->db->from('news a');
        $this->db->join('news_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.type', NEWS_TYPE_VIDEO);
        $this->db->where('a.valid', 1);
        $this->db->where('b.valid', 1);
        $this->db->where_in('a.id', $array_id);
        $this->db->order_by('a.id', 'desc');
        return $this->db->get()->result();
    }

    function get_category_hot($page, $limit){
        $this->db->select("*");
        $this->db->from('news_category');
        $this->db->where('valid', 1);
        $this->db->where('is_hot', 1);
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('is_hot', 'desc');
        $this->db->order_by('position', 'asc');
        return $this->db->get()->result();
    }

    function get_list($page, $limit, $cate_id = null)
    {
        $this->db->select("a.*, b.slug as category_slug");
        $this->db->from('news a');
        $this->db->join('news_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.valid', 1);
        if (!empty($cate_id)){
            $this->db->where('a.category_id', $cate_id);
        }
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('a.id', 'desc');
        return $this->db->get()->result();
    }

    function get_news_discount_list($page, $limit)
    {
        $this->db->select("a.*, b.slug as category_slug");
        $this->db->from('news a');
        $this->db->join('news_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.valid', 1);
        $this->db->where('a.type', NEWS_TYPE_DISCOUNT);
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('a.position', 'asc');
        return $this->db->get()->result();
    }

    function get_news_discount_hot($page, $limit)
    {
        $this->db->select("a.*, b.slug as category_slug");
        $this->db->from('news a');
        $this->db->join('news_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.valid', 1);
        $this->db->where('a.type', NEWS_TYPE_DISCOUNT);
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('a.is_hot', 'desc');
        $this->db->order_by('a.position', 'asc');
        $this->db->order_by('a.id', 'desc');
        return $this->db->get()->result();
    }

    function get_related_with_product($product_id, $number=5){
        $product_id = $product_id.',';
        $this->db->select('a.*, b.slug as category_slug');
        $this->db->from('news a');
        $this->db->join('news_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.valid', 1);
        $this->db->where('a.type', NEWS_TYPE_CONTENT);
        $this->db->where("a.products_related LIKE '%{$product_id}%' ");
        $this->db->limit($number);
        $this->db->order_by('a.id', 'desc');
        $this->db->order_by('a.position', 'asc');
        $data = $this->db->get()->result();
        foreach ($data as $item){
            $this->db->select('customer_comment.id');
            $this->db->from('customer_comment');
            $this->db->where('valid', 1);
            $this->db->where('is_deleted', 0);
            $this->db->where('type', 1);
            $this->db->where('category_id', $item->id);
            $comment = $this->db->get()->result();
            $item->total_comment = !empty($comment) ? count($comment) : 0;
        }
        return $data;
    }

    function get_news_manual($product_id, $number=5){
        $product_id = $product_id.',';
        $this->db->select('a.*, b.slug as category_slug');
        $this->db->from('news a');
        $this->db->join('news_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.valid', 1);
        $this->db->where('a.type', NEWS_TYPE_MANUAL);
        $this->db->where("a.products_related LIKE '%{$product_id}%' ");
        $this->db->limit($number);
        $this->db->order_by('a.id', 'desc');
        $this->db->order_by('a.position', 'asc');
        $data = $this->db->get()->result();
        foreach ($data as $item){
            $this->db->select('customer_comment.id');
            $this->db->from('customer_comment');
            $this->db->where('valid', 1);
            $this->db->where('is_deleted', 0);
            $this->db->where('type', 1);
            $this->db->where('category_id', $item->id);
            $comment = $this->db->get()->result();
            $item->total_comment = !empty($comment) ? count($comment) : 0;
        }
        return $data;
    }

    function get_latest($page, $limit)
    {
        $this->db->select("a.*, b.slug as category_slug");
        $this->db->from('news a');
        $this->db->join('news_category b', 'a.category_id=b.id', 'left');
        $this->db->where('a.type', NEWS_TYPE_CONTENT);
        $this->db->where('a.valid', 1);
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('a.id', 'desc');
        return $this->db->get()->result();
    }
}