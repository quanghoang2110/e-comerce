<style>
    main{
        padding-top: 0 !important;
    }

    .btn-yellow{
        background-color: #e18037;
        font-family: "Open Sans";
        color: white;
    }

    .btn-order{
        background-color: #e18037;
        border-color: #e18037;
    }

    .btn-order:hover{
        background-color: #e18037;
        border-color: #e18037;
    }

    .bg-color-chef{
        background-color: #ffffff;
    }

    :root {
        --main-bg-color: #e18037;
        --text-color: #1d1d1d;
        --price-color: #e18037;
        --border-color: #eae9e9;
    }

    @font-face {
        font-family: "Lodge Head";
        font-display: swap;
        src: url('/assets/public/lodge/font/SVN-Nexa-Rush-Sans-Black.ttf');
    }

    @font-face {
        font-family: 'Lodge Regular';
        font-display: swap;
        src: url('/assets/public/lodge/font/icielBrandonText-Bold.ttf');
    }

    body {
        background: #f5f5f5;
        /*font-family: "Arial", sans-serif;*/
        color: #0b0b0b;
        font-size: 14px;
    }

    .font-lodge {
        font-family: 'Lodge Regular';
    }
    b, strong{
        font-weight: bold;
    }

    /*#carousel-example-1z {*/
    /*height: 70%;*/
    /*}*/

    .page-footer, .top-nav-collapse {
        background: #1d1d1d;
    }

    input::placeholder,
    textarea::placeholder {
        font-size: 14px;
        color: #aeadad
    }

    .btn-social {
        border-top-left-radius: 4px;
        border-bottom-right-radius: 4px;
        width: 30px;
        height: 30px;
        display: flex;
        float: left;
        justify-content: center;
        align-items: center;
        margin-right: 0.5rem;
        margin-top: 0.3rem;
        background-color: #fdd344 !important;
        color: #000 !important;
    }

    .input-group.md-form.form-sm.form-2 input {
        border: 1px solid #bdbdbd;
        border-top-left-radius: 0.25rem;
        border-bottom-left-radius: 0.25rem;
        background: #fff;
    }

    .input-group.md-form.form-sm.form-2 input.amber-border {
        border: 1px solid #fdd344;
    }

    .navbar-brand img.logo {
        width: auto;
        height: 54px;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    #navbarSupportedContent i.fa-shopping-cart {
        font-size: 1.6rem;
        color: var(--main-bg-color);
    }

    #navbarSupportedContent a.nav-link:hover,
    #navbarSupportedContent .activated a.nav-link {
        color: var(--main-bg-color);
    }

    input.mfw-search {
        /*margin-left: 30px;*/
        outline: 0;
        border: none;
        box-shadow: unset;
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    .mfw-search::placeholder {
        font-size: 13px;
        color: #aeadad
    }

    .mfw-search:hover,
    .mfw-search:visited,
    .mfw-search:focus,
    .mfw-search:active {
        outline: 0;
        border: none;
        box-shadow: unset;
    }

    /**
    mega menu ===================================================================================================
     */
    #navbarSupportedContent .dropdown .dropdown-item:hover {
        background-color: var(--main-bg-color);
        color: #000 !important;
    }

    #mobile-icon-quick-cart i {
        font-size: 1.6rem;
        color: var(--main-bg-color);
        margin-right: 10px;
    }

    #_mobile_cart{
        position: relative;
        margin-right: 10px;
    }

    #mobile-cart {
        padding: 10px;
        box-shadow: 0 0 1px #1d1d1d;
        border: none;
    }

    .navbar-toggler {
        position: relative;
        z-index: 1;
    }

    .mfw-col-logo {
        width: 100%;
        position: absolute;
        top: -10px;
        display: flex;
        justify-content: center;
    }

    .mfw-nav {
        width: 100%;
        justify-content: center;
        align-items: center;
    }

    .mfw-nav .nav-link {
        font-family: 'Lodge Head';
        font-size: 15px;
    }

    .mfw-nav li.nav-item.show a.nav-link {
        color: var(--main-bg-color) !important;
    }

    .mfw-navbar-search {
        /*width: 280px;*/
        margin-right: 30px;
    }

    .mfw-product-category {
        display: flex;
        justify-content: center;
    }

    .mfw-product-category-desc {
        margin-left: -0.5rem;
        margin-right: -0.5rem;
        color: #1d1d1da6;
    }

    .mfw-mega-menu {
        background: linear-gradient(90deg, #f0efef, white);
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        box-shadow: 0 0 50px var(--text-color);
    }

    .mfw-mega-menu a.waves-effect,
    .mfw-mega-menu a.waves-light {
        /*display: inline;*/
        height: inherit;
        padding: 0;
    }

    .mfw-mega-menu a.news-title {
        font-weight: 500;
    }

    .mfw-mega-menu h4 {
        text-align: center;
        line-height: 1.3rem;
        font-size: 1.3rem;
    }

    .mfw-mega-menu .sub-menu img {
        transform: scale(1);
        max-width: 100%;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-mega-menu .mega-news .sub-menu:hover img {
        transform: scale(1.02);
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-mega-menu li.sub-title {
        padding: 7px 10px;
    }

    .mfw-mega-menu li.sub-title:first-child {
        background: white
    }

    .mfw-mega-menu .cate-img {
        height: 140px;
        width: 100%;
        background-position: center;
        background-size: 100%;
    }

    .mfw-mega-menu .product-name {
        font-size: 0.8rem;
        font-weight: 400;
    }

    .mfw-mega-menu .mega-item {
        width: 100%;
    }

    /*.mega-product-item{*/
    /*max-height: 60px;*/
    /*overflow: hidden;*/
    /*}*/

    /*==============================================================================================================*/

    /**
    READMORE
     */

    /*.readmore-cloud {*/
        /*position: absolute;*/
        /*bottom: 0;*/
        /*left: 0;*/
        /*width: 100%;*/
        /*text-align: center;*/
        /*margin: 0;*/
        /*padding: 40px;*/
        /*!* background: white; *!*/
        /*background-image: linear-gradient(transparent 5%, var(--main-bg-color) 60%);*/
    /*}*/

    .read-more, .info-collapse {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .read-more a, .info-collapse a {
        color: #333;
        margin-bottom: 10px;
        position: relative;
        font-weight: 300;
    }

    .product-content {
        max-height: 320px;
        overflow: hidden;
    }

    .product-cate-desc {
        max-height: 87px;
        overflow: hidden;
    }

    .product-content-full .product-content, .product-cate-desc.product-cate-desc-full {
        max-height: unset !important;
    }

    .product-content-full .readmore-cloud, .product-cate-desc-full .readmore-cloud {
        display: none;
    }

    /*
    END READMORE
    */

    /*NEWS==========================================================================================================*/
    .mfw-date {
        font-size: 0.85rem;
        text-align: right;
        color: gray;
    }

    .mfw-news-title {
        margin-top: 1.2rem;
        margin-bottom: 1.1rem;
        /*padding-bottom: 1.5rem;*/
        border-bottom: 1px solid var(--border-color);
    }

    /*h1, h2, h3, h4, h5, h6,*/
    /*.mfw-news-title h1,*/
    /*.mfw-news-title h2,*/
    /*.mfw-product-title h1,*/
    /*.mfw-title h1 {*/
        /*font-size: 1.3rem;*/
        /*font-weight: 500;*/
    /*}*/

    .mfw-news-content {
        text-align: justify;
    }

    .mfw-news-content img {
        max-width: 100%;
        height: auto;
    }

    /*==============================================================================================================*/

    /*PRODUCT=======================================================================================================*/
    .mfw-product-item {
        background: #fff;
        padding: 0.5rem;
        border: 1px solid #e4e4e4;
    }

    .mfw-product-item h3 {
        color: var(--text-color);
        font-size: 1rem;
        font-weight: 500;
        margin-top: 0.3rem;
        height: 40px;
    }

    .mfw-sale {
        color: var(--price-color);
        font-weight: 700;
        font-size: 1rem;
        /*margin-left: 5px;*/
    }

    .mfw-raw {
        text-decoration-line: line-through;
        color: gray;
        font-size: 0.8rem;
        margin-left: 10px;
        font-weight: 300;
    }

    .btn-add-cart,
    .mfw-btn.btn-add-cart {
        color: white;
        font-weight: 400;
        font-size: 1rem;
        text-transform: unset;
        font-family: 'Roboto' !important;
        background-color: #dab009;
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        box-shadow: unset;
    }

    .btn-add-cart i.fa,
    .mfw-btn.btn-add-cart i.fa {
        font-size: 1.2rem;
    }

    .mfw-btn.btn-add-cart:hover,
    .btn-add-cart:hover {
        color: white;
    }

    .mfw-right {
        text-align: right;
    }

    .mfw-product-item img {
        max-width: 100%;
        height: auto;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        -o-transform: scale(1);
        transform: scale(1);
    }

    .mfw-product-item:hover img {
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -ms-transform: scale(1.1);
        -o-transform: scale(1.1);
        transform: scale(1.1);
    }

    /*==============================================================================================================*/

    .mfw-slider-title {
        font-family: 'Lodge Head';
        font-size: 2.2rem;
    }

    .mfw-desc {
        white-space: pre-line;
    }

    .mfw-button {
        background: #fdd344 !important;
    }

    .mfw-slider-text {
        max-width: 500px;
    }

    .mfw-btn {
        background: #1b9647;
        padding: 0.7rem 1.2rem;
        font-size: 24px;
        border-radius: 5px;
        color: #ffffff !important;
        font-family: 'Opensans';
    }

    .mfw-btn:hover, .mfw-btn:focus {
        /*background-color: #b78e04 !important;*/
        color: #000000
    }

    .mfw-btn.active {
        background-color: #99790d !important;
        box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);
        color: #000000
    }

    .mfw-category-title {
        font-family: 'Lodge Head';
        font-size: 1.5rem;
        /* margin-bottom: 1.8rem; */
    }

    .mfw-dark {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        background: rgba(0, 0, 0, 0.55);
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-sp-hot {
        position: relative;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    .mfw-product-name {
        font-family: "Lodge Head";
        color: #FFF;
        padding: 5px 15px;
        padding-top: 15px;
        font-size: 1.4rem;
        /*text-shadow: 0 0 2px #666;*/
        /*width: 80%;*/
        /*text-align: center;*/
        /*position: absolute;*/
    }

    .mfw-flex-center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .mfw-product-desc {
        position: relative;
        top: 600px;
        padding: 15px;
        text-align: justify;
        color: white;
        overflow: hidden;
        height: 110px;
        font-size: 1rem;
        /*text-shadow: 0 0 4px #333;*/
    }

    .mfw-product-button {
        position: relative;
        top: 600px;
        text-align: center;
    }

    .mfw-product-button .btn {
        border-radius: 20px;
        font-family: "Lodge Head";
    }

    .mfw-product-name.big-size {
        font-size: 1.8rem;
    }

    .mfw-sp-hot:hover .mfw-dark {
        background: #ca9e07;
        opacity: 0.94;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-sp-hot:hover .mfw-product-name {
        position: relative;
        /*width: 100%;*/
        text-align: left;
        /*text-shadow: 0 0 1px #666;*/
        top: 0;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-sp-hot:hover .mfw-info {
        display: inherit;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-sp-hot:hover .mfw-product-desc {
        /*text-shadow: 0 0 1px #333;*/
        top: 0;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-sp-hot:hover .mfw-product-button {
        /*text-shadow: 0 0 0.5px #666;*/
        top: 0;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-sp-hot:hover .mfw-product-name {
        text-shadow: none;
    }

    .mfw-sp-hot:hover .mfw-full-bg,
    .mfw-video:hover .mfw-full-bg,
    .mfw-news:hover .mfw-full-bg,
    .mfw-news:hover img {
        -ms-transform: scale(1.05);
        -moz-transform: scale(1.05);
        -webkit-transform: scale(1.05);
        -o-transform: scale(1.05);
        transform: scale(1.05);
        -webkit-transition: 0.5s linear;
        -moz-transition: 0.5s linear;
        -ms-transition: 0.5s linear;
        transition: 0.5s linear;
    }

    .mfw-banner:hover img {
        -ms-transform: scale(1.01);
        -moz-transform: scale(1.01);
        -webkit-transform: scale(1.01);
        -o-transform: scale(1.01);
        transform: scale(1.01);
        -webkit-transition: 0.5s linear;
        -moz-transition: 0.5s linear;
        -ms-transition: 0.5s linear;
        transition: 0.5s linear;
    }

    .mfw-full-bg {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        -ms-transform: scale(1);
        -moz-transform: scale(1);
        -webkit-transform: scale(1);
        -o-transform: scale(1);
        transform: scale(1);
        background-size: cover;
        background-position: 50%;
        z-index: 0;
        -webkit-transition: 0.5s linear;
        -moz-transition: 0.5s linear;
        -ms-transition: 0.5s linear;
        transition: 0.5s linear;
    }

    .mfw-sp-2 {
        padding-left: 0;
    }

    .margin-top-15 {
        margin-top: 15px;
    }

    .mfw-video {
        display: flex;
        justify-content: center;
        align-items: center;
        overflow: hidden;
        position: relative;
        height: 100%;
    }

    .mfw-video .mfw-video-name {
        position: absolute;
        bottom: 0;
        padding: 15px;
        padding-top: 30px;
        color: var(--main-bg-color);
        font-family: 'Lodge Regular';
        left: 0;
        right: 0;
        background-image: linear-gradient(transparent, #000);
        font-size: 1.5rem;
    }

    .mfw-video .play,
    .mfw-news .play {
        position: relative;
        z-index: 1;
        font-size: 4.5rem;
        color: #fff;
    }

    .mfw-video .mfw-dark {
        background: rgba(0, 0, 0, 0.35);
    }

    .bg-news {
        background: #1d1d1de6;
        opacity: 0.9;
        /*padding-top: 2rem;*/
        /*padding-bottom: 2rem;*/
    }

    .mfw-news {
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .mfw-news .mfw-name.mfw-name-bigger {
        font-size: 1.35rem;
    }

    .mfw-news .mfw-name {
        padding: 7px 0;
        font-family: "Lodge Regular";
        color: var(--main-bg-color);
        font-size: 1.15rem;
        margin: 0;
    }

    .mfwNewsActive .mfw-name-bigger,
    .mfwNewsActive .mfw-full-bg,
    .mfwNewsActiveFirst img,
    .mfwNewsActiveFirst .mfw-news-desc {
        display: none !important;
    }

    .mfwNewsActiveFirst .mfw-full-bg,
    .mfwNewsActiveFirst .mfw-name-bigger {
        display: block !important;
    }

    #mfw-news .mfw-name, .mfwNewsActiveFirst .mfw-name {
        padding: 7px 10px;
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(0, 0, 0, 0.8);
    }

    .owl-carousel-news .owl-dots {
        display: none;
    }

    .mfw-news img {
        max-width: 100%;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-news-desc {
        color: #b6b4b4;
        font-size: 14px;
        line-height: 20px;
        height: 40px;
        overflow: hidden;
    }

    .mfw-single {
        white-space: nowrap;
        -ms-text-overflow: ellipsis;
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .mfw-absolute-full {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
    }

    .mfw-absolute-full .play {
        position: relative;
        z-index: 1;
        font-size: 3rem;
        color: #fff;
    }

    .mfw-news a {
        display: block;
        width: 100%;
        height: 100%;
    }

    .mfw-img {
        max-width: 100%;
        height: auto;
        -webkit-transition: 0.5s linear;
        -moz-transition: 0.5s linear;
        -ms-transition: 0.5s linear;
        transition: 0.5s linear;
    }

    .mfw-h3 {
        font-size: 1.0rem;
    }

    .mfw-link {
        cursor: pointer;
        color: #1d1d1d
    }

    .mfw-link i {
        float: left;
        margin-right: 7px;
    }

    .fixed-sn .navbar {
        background: #1d1d1d;
    }

    .mfw-side-left {
    }

    .mfw-side-left h3 {
        font-size: 1.3rem;
    }

    .mfw-side-left .mfw-item .name {
        width: 100%;
        text-overflow: ellipsis;
        overflow: hidden;
        /* white-space: nowrap; */
        color: #1d1d1d;
        font-weight: 400;
        height: 40px;
        font-size: 0.9rem;
    }

    .mfw-side-left .mfw-item:hover {
        background-color: #e18037;
        -webkit-transition: background-color 0.5s ease;
        -moz-transition: background-color 0.5s ease;
        -ms-transition: background-color 0.5s ease;
        -o-transition: background-color 0.5s ease;
        transition: background-color 0.5s ease;
        color: white;
    }

    .mfw-side-left .mfw-item a:hover {
        color: white;
    }

    .mfw-side-left ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .mfw-side-left ul li {
        /*margin-bottom: 10px;*/
        border-top: 1px solid #eae9e9;
        padding-top: 15px;
        padding-bottom: 15px;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
    }

    .mfw-side-left li.activated {
        background-color: var(--main-bg-color);
        color: #ffffff;
    }

    .mfw-side-left .price {
        font-size: 1.0rem;
        color: var(--price-color);
    }

    .mfw-side-left .mfw-item img {
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        -o-transform: scale(1);
        transform: scale(1);
    }

    .mfw-side-left .mfw-item:hover img {
        -webkit-transform: scale(1.05);
        -moz-transform: scale(1.05);
        -ms-transform: scale(1.05);
        -o-transform: scale(1.05);
        transform: scale(1.05);
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
        -ms-transition: all 0.5s;
        -o-transition: all 0.5s;
        transition: all 0.5s;
        margin-left: 5px;
    }
