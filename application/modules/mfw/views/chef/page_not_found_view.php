<main>
    <div class="container mfw-container">
        <div class="row">
            <div class="d-none d-lg-block d-xl-block col-lg-3 mfw-side-left">
                <?php load_mfw_module("product", "best_seller", 5); ?>
            </div>
            <div class="col-lg-9">
                <section id="main">
                    <div class="mt-5 mb-3 text-center">
                        <img src="/images/404.gif" class="img-m-300"/>
                    </div>
                    <section id="content" class="page-content page-not-found text-center my-5">
                        <h1>
                            Không tìm thấy nội dung mà bạn yêu cầu.
                        </h1>
                        <h4>Chúng tôi rất tiếc vì sự bất tiện này.</h4>
                    </section>
                </section>
            </div>
        </div>
    </div>
</main>
<style>
    .img-m-300{
        max-width: 300px;
    }
</style>