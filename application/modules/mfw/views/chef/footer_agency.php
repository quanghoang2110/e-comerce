<div class="s-content">
    <?php if (!empty($agency)): ?>
        <div class="d-flex justify-content-start"><span><i class="fa fa-home"></i></span>
            <div>Trụ sở chính: <?php echo $agency->name ?></div>
        </div>
        <div class="d-flex justify-content-start"><span><i class='fa fa-map-marker'></i></span>
            <div>Địa chỉ: <?php echo $agency->address ?></div>
        </div>
        <div class="d-flex justify-content-start"><span><i class="fa fa-phone"></i></span>
            <div> Điện thoại: <?php echo $agency->phone ?></div>
        </div>
        <div class="d-flex justify-content-start"><span><i class="fa fa-envelope" style="font-size: 14px"></i></span>
            <div> Email: <?php echo $agency->email ?></div>
        </div>
        <?php if (!empty($agency->note)): ?>
            <div class="d-flex justify-content-start"><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                <div><?php echo $agency->note ?></div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="col-12 mt-2">
        <a href="<?php echo base_url('/diem-ban.html') ?>" class="btn ml-5 px-5 font-weight-bold "
           style="background-color: #e18037; text-decoration: none; color: #512625">Điểm bán</a>
    </div>
</div>