<?php
$discount = !empty($_GET['discount']) ? $_GET['discount'] : '';
$discount = explode(',', rtrim($discount, ','));
?>
<div class="pro-sidebar">
    <h3 class="title">Khuyến mại</h3>
    <div class="ct">
        <?php foreach ($list as $item):?>
        <label class="i-check"><input <?php echo in_array($item->id, $discount) ? 'checked' : ''?>  onchange="filter()" type="checkbox" class="checkbox_discount" name="" value="<?php echo $item->id ?>"><i></i> <?php echo $item->name?></label>
        <?php endforeach;?>
    </div>
</div>