<?php
$slide1 = $list[0];
$imgs1 = $slide1->image;
?>
<?php
$slide2 = $list[3];
$imgs2 = $slide2->image;
?>
<div class="md-cates">
    <h2 class="md-title">SẢN PHẨM CỦA CHEFSTUDIO</h2>
    <div class="row col-mar-3">
        <div class="col-md-4 col-6">
            <div class="md-cate">
                <a class="c-img" href="<?php echo base_url($slide1->slug) ?>" title="<?php echo $slide1->name ?>">
                    <img src="<?php echo get_img_url($imgs1) ?>" alt="" title="<?php echo $slide1->name ?>"/>
                </a>
                <div class="caption">
                    <!--                    <h3 class="title">Dụng cụ <br><strong>Làm bánh</strong></h3>-->
                    <h3 class="title"><?php echo $slide1->name ?></h3>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-6 order-md-last">
            <div class="md-cate">
                <a class="c-img" href="<?php echo base_url($slide2->slug) ?>" title="<?php echo $slide2->name ?>">
                    <img src="<?php echo get_img_url($imgs2) ?>" alt="" title="<?php echo $slide2->name ?>"/>
                </a>
                <div class="caption">
                    <h3 class="title"><?php echo $slide2->name ?></h3>
                </div>
            </div>
        </div>
        <?php foreach ($list as $index => $item): ?>
            <?php $item_url = base_url($item->slug); ?>
            <?php if ($index > 0 && $index < 3): ?>
                <div class="col-md-8">
                    <div class="md-cate v2">
                        <a class="c-img" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                            <img src="<?php echo get_img_url($item->image) ?>" alt=""
                                 title="<?php echo $item->name ?>"/>
                        </a>
                        <div class="caption v2">
                            <h3 class="title"><strong><?php echo $item->name ?></strong></h3>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php foreach ($list as $index => $item): ?>
            <?php $item_url = base_url($item->slug); ?>
            <?php if ($index > 3): ?>
                <div class="col-md-4 col-4">
                    <div class="md-cate">
                        <a class="c-img" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                            <img src="<?php echo get_img_url($item->image) ?>" alt="" title="<?php echo $item->name ?>"/>
                        </a>
                        <div class="caption v2">
                            <h3 class="title"><?php echo $item->name ?></h3>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>