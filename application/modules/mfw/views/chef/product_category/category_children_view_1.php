<?php if (count($list)): ?>
    <div class="pro-sidebar">
        <p class="title v2" onclick="menu_mobi($(this))" data-title="1">Danh mục sản phẩm <i class="fa fa-angle-down float-right mt-1 d-lg-none d-block" aria-hidden="true"></i></p>
        <div class="ct">
            <ul>
                <?php foreach ($list as $item): ?>
                    <li><a class="smooth" href="<?php echo base_url($item->slug)?>" title=""><?php echo $item->name ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>