<div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">

    <!--Indicators-->
    <ol class="carousel-indicators">
        <?php foreach ($list as $index => $item): ?>
            <li data-target="#carousel-example-1z" data-slide-to="<?php echo $index ?>"
                class="<?php echo $index === 0 ? 'active' : '' ?>"></li>
        <?php endforeach; ?>
    </ol>
    <!--/.Indicators-->

    <!--Slides-->
    <div class="carousel-inner" role="listbox">

        <!--First slide-->
        <?php foreach ($list as $index => $item): ?>
            <div class="carousel-item <?php echo $index === 0 ? 'active' : '' ?>">
                <div class="view slider-item"
                     style="background-image: url('<?php echo get_img_url($item->image, '') ?>');">

                    <!-- Mask & flexbox options-->
                    <div class="mask d-flex align-items-center container banner-item">

                        <!-- Content -->
                        <div class="white-text slider-item-content wow fadeIn mfw-slider-text">
                            <h1 class="mfw-slider-title">
                                <strong><?php echo $item->name ?></strong>
                            </h1>

                            <p class="mb-4 d-none d-md-block mfw-desc text-justify d-none d-md-block d-lg-block d-lg-block">
                                <?php echo $item->desc ?>
                            </p>
                            <?php if ($item->button): ?>
                                <a target="_blank" href="<?php echo $item->url ?>"
                                   class="btn mfw-btn m-0 mfw-slider-btn"><?php echo $item->button ?>
                                </a>
                            <?php endif; ?>
                        </div>
                        <!-- Content -->

                    </div>
                    <!-- Mask & flexbox options-->

                </div>
            </div>
        <?php endforeach; ?>
        <!--/Third slide-->

    </div>
    <!--/.Slides-->

    <!--Controls-->
    <!--    <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">-->
    <!--        <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
    <!--        <span class="sr-only">Previous</span>-->
    <!--    </a>-->
    <!--    <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">-->
    <!--        <span class="carousel-control-next-icon" aria-hidden="true"></span>-->
    <!--        <span class="sr-only">Next</span>-->
    <!--    </a>-->
    <!--/.Controls-->

</div>
<style>
    .slider-item {
        /*background-repeat: no-repeat;*/
        background-size: cover;
        background-position: center;
    }
</style>
