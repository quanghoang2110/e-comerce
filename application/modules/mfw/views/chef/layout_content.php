<!-- Header -->
<?php
$user = $this->session->all_userdata();
?>

<header>
    <div class="top text-right">
        <?php load_mfw_module('menu', 'menu_top_header', 2); ?>
    </div>
    <div class="header">
        <div class="container">
            <div class="row align-items-center justify-content-between sm-pad-5">
                <div class="col-lg-4 d-lg-none col-auto">
                    <button class="menu-btn m-nav-open" type="button"><i></i></button>
                </div>
                <div class="col-lg-2 col-auto col-log-mobi">
                    <a class="logo" href="/" title="">
                        <img src="/images/chef/logo.png" alt="" title=""/>
                    </a>
                </div>
                <div class="col-lg-6 d-none d-lg-block">
                    <form method="get" class="search-fr" action="/<?php echo ROUTE_SEARCH ?>" id="mfw-search-form">
                        <input type="text" placeholder="Nhập từ khóa tìm kiếm" name="search" aria-label="Search"
                               id="mfw-search-input"
                               value="<?php echo str_replace('[removed]', '', trim($this->input->get("search", true))); ?>">
                        <button type="submit" id="search-button"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <div class="col-lg-4 text-right col-auto cart">
                    <div class="h-cart dropdown " id="li_desktop_cart">
                        <a class="smooth" data-toggle="dropdown" id="desktop-icon-quick-cart" aria-haspopup="true"
                           aria-expanded="true">
                            <i class="ic ic-cart"></i>
                            <span>Giỏ hàng</span>
                            <strong id="desktop-quick-cart-badge">0</strong>
                            <label class="d-block d-lg-none" id="desktop-quick-cart-mobi">0</label>
                        </a>
                        <div id="desktop-cart"
                             class="cart_block block exclusive dropdown-menu dropdown-content dropdown-menu-right">
                        </div>
                    </div>
                    <div class="h-user">
                        <?php if (empty($user) || empty($user['user_fullname'])): ?>
                        <a href="/dang-nhap.html" class="smooth" title="">
                            <?php else: ?>
                            <a href="/thong-tin-tai-khoan.html" class="smooth" title="">
                                <?php endif; ?>
                                <i class="ic ic-user"></i>
                                <div class="dropdown d-none d-lg-inline-block">
                                    <span class="dropbtn"><?php echo (!empty($user) && !empty($user['user_fullname'])) ? 'Xin chào' : 'Tài khoản' ?></span>
                                    <?php
                                    if (!empty($user) && !empty($user['user_fullname'])):
                                        ?>
                                        <strong><?php echo $user['user_fullname'] ?></strong>
                                    <?php endif; ?>
                                    <div style="top: 20px; left: -10px; min-width: 145px"
                                         class="dropdown-content">
                                        <?php if (empty($user) || empty($user['user_fullname'])): ?>
                                            Đăng nhập
                                            <a href="/dang-ky.html">Đăng ký</a>
                                        <?php else: ?>
                                            Thông tin tài khoản
                                            <a href="/dang-xuat.html">Đăng xuất</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="main-bar">
        <!----menu---->
        <div class="container-fluid">
            <nav class="main-nav">
                <?php load_mfw_module('menu', 'menu_frontend', 15); ?>
            </nav>
        </div>
    </div>
</header>
<!-- Header -->
<div class="m-nav">
    <button class="m-nav-close">&times;</button>
    <div class="nav-ct">
        <form method="get" class="search-fr" action="/<?php echo ROUTE_SEARCH ?>" id="mfw-search-form">
            <input type="text" placeholder="Nhập từ khóa tìm kiếm" name="search" aria-label="Search"
                   id="mfw-search-input"
                   value="<?php echo str_replace('[removed]', '', trim($this->input->get("search", true))); ?>">
            <button type="submit" id="search-button"><i class="fa fa-search"></i></button>
        </form>
        <?php load_mfw_module('menu', 'menu_mobi_frontend', 8); ?>
    </div>
</div>
<?php echo $module_view ?>

<div id="blockcart-modal" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="myBasicModalLabel" aria-hidden="true">
</div>

<!-- Central Modal Small -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <!-- Change class .modal-sm to change the size of the modal -->
    <div class="modal-dialog" role="document">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel">Thông báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-yellow" data-dismiss="modal">Đồng ý</button>
            </div>
        </div>
    </div>
</div>
