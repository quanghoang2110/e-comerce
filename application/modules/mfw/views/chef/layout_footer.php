<?php
$setting = new Setting_object($setting);
?>
<footer>
    <div class="ct">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="fcol-1">
                        <div class="mb-3">
                            <a href="/">
                                <img src="<?php echo base_url($setting->logo) ?>" style="max-width: 150px; height: 50px"
                                     alt="chef studio viet name" title="Chef Studio Việt Nam" class="">
                            </a>
                        </div>
                        <!--agency-->
                        <!--                        --><?php //load_mfw_module("footer", "agency"); ?>
                        <div class="s-content">
                            <?php if (!empty($setting)): ?>
                                <div class="d-flex justify-content-start"><span><i class="fa fa-home"></i></span>
                                    <div>Trụ sở chính: <?php echo $setting->company_name ?></div>
                                </div>
                                <div class="d-flex justify-content-start"><span><i class="fa fa-barcode"></i></span>
                                    <div>Mã số thuế: <?php echo $setting->company_tax_code ?></div>
                                </div>
                                <div class="d-flex justify-content-start"><span><i class="fa fa-envelope"
                                                                                   style="font-size: 14px"></i></span>
                                    <div> Địa chỉ: <?php echo $setting->company_address ?></div>
                                </div>
                                <div class="d-flex justify-content-start"><span><i class="fa fa-phone"></i></span>
                                    <div> Điện thoại: <?php echo $setting->company_phone ?></div>
                                </div>
                                <div class="d-flex justify-content-start"><span><i class="fa fa-envelope"
                                                                                   style="font-size: 14px"></i></span>
                                    <div> Email: <?php echo $setting->company_email ?></div>
                                </div>

                                <div class="d-flex justify-content-start"><span><i class="fa fa-clock-o"
                                                                                   aria-hidden="true"></i></span>
                                    <div>Thời gian mở cửa: <?php echo $setting->company_time ?></div>
                                </div>
                            <?php endif; ?>
                            <div class="col-12 mt-2">
                                <a href="<?php echo base_url('/diem-ban.html') ?>"
                                   class="btn ml-5 px-5 font-weight-bold "
                                   style="background-color: #e18037; text-decoration: none; color: #512625">Điểm bán</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="fcol-2">
                        <div class="row">
                            <div class="col-md-4 col-6">
                                <!-- Grid column -->
                                <?php load_mfw_module("footer", "menu"); ?>
                            </div>
                            <div class="col-md-4 col-6">
                                <p class="f-title">Theo dõi chúng tôi</p>
                                <div class="social">
                                    <a class="smooth" target="_blank" href="<?php echo $setting->fb_fanpage ?>"
                                       title=""><i
                                                class="ic ic-face"></i> Facebook</a>
                                    <a class="smooth" target="_blank" href="<?php echo $setting->instagram ?>" title=""><i
                                                class="ic ic-instagram"></i> Instagram</a>
                                    <a class="smooth" target="_blank" href="<?php echo $setting->linkedin ?>"
                                       title=""><i
                                                class="ic ic-linkedin"></i> Linkedin</a>
                                    <a class="smooth" target="_blank" href="<?php echo $setting->youtube ?>" title=""><i
                                                style="font-size: 20px"
                                                class="fa fa-youtube"></i>Youtube</a>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="facebook">
                                    <div class="fb-page" data-href="<?php echo $setting->fb_fanpage ?>"
                                         data-tabs="" data-width="" data-height="" data-small-header="false"
                                         data-adapt-container-width="true" data-hide-cover="false"
                                         data-show-facepile="true">
                                        <blockquote cite="<?php echo $setting->fb_fanpage ?>"
                                                    class="fb-xfbml-parse-ignore"><a target="_blank"
                                                                                     href="<?php echo $setting->fb_fanpage ?>">Facebook</a>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3">
                            <div class="social">
                                <?php
                                $array_phone_1 = explode('(', $setting->company_phone_1);
                                if (!empty($setting->company_phone_1)):
                                    ?>
                                    <a class="smooth font-weight-bold pl-0 mb-0 hotline" target="_blank"
                                       href="tel:<?php echo $array_phone_1[0] ?>"
                                       title="">
                                        GỌI MUA HÀNG: <span class="font-weight-normal" style="color: #007bff!important">
                                        <i style="padding: 2px 4px; border: 1px solid; border-radius: 50%"
                                           class="fa fa-phone position-relative mr-1"
                                           aria-hidden="true"></i><?php echo $setting->company_phone_1 ?></span>
                                    </a>
                                <?php
                                endif;
                                $array_phone_2 = explode(' ', $setting->company_phone_2);
                                if (!empty($setting->company_phone_2)):
                                    ?>
                                    <a class="smooth  font-weight-bold pl-0 hotline" target="_blank"
                                       href="tel:<?php echo $array_phone_2[0] ?>" title="">
                                        GỌI KHIẾU NẠI: <span class="font-weight-normal pl-1"
                                                             style="color: #007bff!important">
                                        <i style="padding: 2px 4px; border: 1px solid; border-radius: 50%"
                                           class="fa fa-phone position-relative mr-1"
                                           aria-hidden="true"></i><?php echo $setting->company_phone_2 ?></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-3 text-center">
        <a target="_blank" href="<?php echo $setting->url_bct ? $setting->url_bct : '/' ?>"><img
                    src="<?php echo base_url() ?>/images/bct_blue.png" width="150px"></a>
        <a title="DMCA.com Protection Status" class="dmca-badge" target="_blank" href="https://www.dmca.com/Protection/Status.aspx?ID=de66380d-4540-4f5d-a262-d614401c32b3"><img
                    src="<?php echo base_url() ?>/images/DMCA_logo-grn-btn120w.png" width="120px"></a>
    </div>
    <div class="footer">
        <div class="container">
            <ul>
                <li>
                    <a class="smooth" href="<?php echo base_url('/apply-to-be-a-supplier.html') ?>"
                       title="APPLY TO BE A SUPPLIER">
                        APPLY TO BE A SUPPLIER
                    </a>
                </li>
                <li>
                    <a class="smooth" href="<?php echo base_url('/gioi-thieu-ve-chung-toi.html') ?>"
                       title="GIỚI THIỆU">
                        GIỚI THIỆU
                    </a>
                </li>
                <li>
                    <a class="smooth" href="<?php echo base_url('/cong-thuc-nau-an') ?>" title="">
                        CÔNG THỨC NẤU ĂN
                    </a>
                </li>
                <li>
                    <a class="smooth" href="<?php echo base_url('/lien-he.html') ?>" title="">
                        LIÊN HỆ
                    </a>
                </li>
            </ul>
            <div class="copyright">
                <!--                Địa chỉ: --><?php //echo $setting->company_address ?><!-- <br>-->
                Copyrights © 2019 Chef Studio. All Rights Reserved
            </div>
        </div>
    </div>

</footer>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v6.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div id="fb-root"></div>
<!-- Load Facebook SDK for JavaScript -->
<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="750274995329874"
     logged_in_greeting="Xin chào! Chef Studio luôn sẵn sàng hỗ trợ quý khách 🙂"
     logged_out_greeting="Xin chào! Chef Studio luôn sẵn sàng hỗ trợ quý khách 🙂">
</div>

<div style="margin-right: 85px; margin-bottom: 10px" class="zalo-chat-widget" data-oaid="4334458163397897561" data-welcome-message="Rất vui khi được hỗ trợ bạn!" data-autopopup="0" data-width="350" data-height="420"></div>

<script src="https://sp.zalo.me/plugins/sdk.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>
<!--/.Footer-->
<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>js/chef/bootstrap.min.js"></script>
<!--<script type="text/javascript"  src="--><?php //echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?><!--js/bootstrap.min.js"></script>-->
<!-- MDB core JavaScript -->
<!--<script type="text/javascript"  src="--><?php //echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?><!--js/mdb.min.js"></script>-->

<script type="text/javascript" src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>js/jquery.lazy.min.js"></script>

<!--<script src="--><?php //echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?><!--js/chef/jquery-2.2.1.min.js"></script>-->
<script src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>js/chef/wow.min.js"></script>
<script src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>js/chef/slick.min.js"></script>

<script src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>js/chef/script.js"></script>

<script type="text/javascript"
        src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER); ?>lib/fancybox/jquery.fancybox.min.js"></script>

<!--<script async defer crossorigin="anonymous"-->
<!--        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=--><?php //echo $setting->fb_app_id; ?><!--&autoLogAppEvents=1"></script>-->
<!-- Initializations -->
<script type="text/javascript">
    new WOW().init();


    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $(document).ready(function () {
        $('.pagination').find('a').addClass('page-link');

        const alertModal = $('#alertModal');

        $('#subscribe').click(function () {
            const email = $('#input_subscribe').val();
            if (email) {
                if (validateEmail(email)) {
                    $.post('/<?php echo MFW_ROUTE_USER_SUBSCRIBE ?>', {input_email: email}).done(function (data) {
                        var response = JSON.parse(data);
                        alertModal.find('.modal-body').html(response.msg);
                        alertModal.modal('show');
                    });
                } else {
                    alertModal.find('.modal-body').html("Email không hợp lệ, vui lòng kiểm tra lại!");
                    alertModal.modal('show');
                }
            } else {
                alertModal.find('.modal-body').html("Vui lòng nhập email đăng ký");
                alertModal.modal('show');
            }
        });

        $('#search-button').click(function () {
            const keyword = $('#mfw-search-input').val();
            if (!keyword.trim()) {
                alertModal.find('.modal-body').html("Vui lòng nhập tên sản phẩm bạn muốn tìm");
                alertModal.modal('show');
            } else {
                $('#mfw-search-form').submit();
            }
        });
    })
</script>