<div class="page-gray">
    <?php load_mfw_module('breadcrumb', 'index', 'Đặt hàng') ?>
    <div class="container">
        <div class="cart-process">
            <div class="row col-mar-5">
                <div class="col-4">
                    <div class="item green"><span>1</span> Đăng nhập</div>
                </div>
                <div class="col-4">
                    <div class="item red"><span>2</span> Thông tin giao hàng</div>
                </div>
                <div class="col-4">
                    <div class="item"><span>3</span> Hình thức thanh toán</div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-11">
                <div class="row justify-content-center col-mar-8">
                    <div class="col-lg-7">
                        <div class="cart-address">
                            <h3 class="title">Địa chỉ nhận hàng</h3>
                            <div class="ct">
                                <br>
                                <div class="line">
                                    <div class="label">Họ tên:</div>
                                    <input type="text" placeholder="Nhập tên của bạn" name="">
                                </div>
                                <div class="line">
                                    <div class="label">Điện thoại:</div>
                                    <div class="input">
                                        <input type="text" placeholder="Nhập số điện thoại" name="">
                                        <span>Nhân viên giao nhận Cheft roi sẽ liên hệ với SĐT này.</span>
                                    </div>
                                </div>
                                <div class="line">
                                    <div class="label">Email:<p>Không bắt buộc</p></div>
                                    <input type="text" placeholder="Nhập địa chỉ email của bạn" name="">
                                </div>
                                <div class="line">
                                    <div class="label">Tên đường phố:</div>
                                    <input type="text" placeholder="Nhập tên đương phố nơi bạn ở" name="">
                                </div>
                                <div class="line">
                                    <div class="label">Địa chỉ chi tiết:</div>
                                    <input type="text" placeholder="Nhập địa chỉ chi tiết của bạn" name="">
                                </div>
                                <div class="line">
                                    <div class="label">Lời nhắn:<p>Không bắt buộc</p></div>
                                    <textarea  placeholder="Lời nhắn" ></textarea>
                                </div>
                                <label class="pay-check"><input type="checkbox" name=""><i></i> Thông tin người mua hàng giống như trên</label>
                                <label class="pay-check"><input type="checkbox" name=""><i></i> Cập nhật thông tin trên làm địa chỉ hiện tại của tôi</label>
                            </div>
                        </div>
                        <div class="cart-address">
                            <h3 class="title"><label class="pay-check"><input type="checkbox" name=""><i></i></label>Yêu cần xuất hóa đơn GTGT cho đơn hàng này</h3>
                            <div class="ct">
                                <p>Vui lòng điền đầy đủ thông tin công ty để nhận hóa đơn GTGT. <br>
                                    Lưu ý: Giá trị hoá đơn không bao gồm giá trị được tích điểm vào Thẻ 3S (nếu có)</p>
                                <br>
                                <div class="line">
                                    <div class="label">Má số thuế:</div>
                                    <input type="text" placeholder="Nhập mã số thuế cần xuất hóa đơn" name="">
                                </div>
                                <div class="line">
                                    <div class="label">Tên công ty:</div>
                                    <input type="text" placeholder="Nhập tên công ty" name="">
                                </div>
                                <div class="line">
                                    <div class="label">Địa chỉ công ty:</div>
                                    <input type="text" placeholder="Nhập địa chỉ công ty" name="">
                                </div>
                            </div>
                        </div>
                        <div class="text-right"><a class="smooth def-btn" href="#" title="">Tiếp tục</a></div>
                        <br><br class="d-none d-lg-block">
                    </div>
                    <div class="col-lg-4">
                        <div class="sb-order">
                            <div class="head">
                                <h3 class="i-title">Đơn hàng <span>(<?php echo count($list)?> sản phẩm)</span></h3>
                                <a class="smooth" href="<?php echo base_url(MFW_CART_ROUTE_CART)?>" title="">Sửa đơn hàng</a>
                            </div>
                            <div class="ct">
                                <?php $total_price = 0; ?>
                                <?php foreach ($list as $item): ?>
                                    <?php
                                    $product_link = get_detail_url($item->category_slug, $item->name, $item->id);
                                    $images = json_decode($item->image);
                                    $price = $item->price_2 ? $item->price_2 : $item->price_1;
                                    $total_price += $price * $item->soluong;
                                    $id = $item->id;
                                    ?>
                                    <div class="order-pro mb-3 pb-2 border-bottom">
                                        <a class="c-img" href="<?php echo $product_link ?>" title="">
                                            <img src="<?php echo get_img_url($images[0])?>" alt="Not Image" title="<?php echo $item->name?>"/>
                                        </a>
                                        <h3 class="title"><a class="smooth" href="#" title=""><?php echo $item->name?></a>
                                        </h3>
                                        <div class="price">
                                            <p><?php echo get_price($price)?>đ</p>
                                            <p>x<?php echo $item->soluong ?></p>
                                            <p><?php echo get_price($price * $item->soluong)?>đ</p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <table>
                                    <tr>
                                        <td>Tổng tiền</td>
                                        <td><strong><?php echo get_price($total_price)?>đ</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Phí vận chuyển</td>
                                        <td>MIỄN PHÍ</td>
                                    </tr>
                                </table>
                                <table class="total-tb">
                                    <tr>
                                        <td>Số tiền phải thanh toán</td>
                                        <td><span><?php echo get_price($total_price)?>đ</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br class="d-none d-lg-block">
</div>