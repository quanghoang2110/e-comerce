<div class="modal-dialog modal-lg mfw-cart" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title w-100" id="myModalLabel">Sản phẩm
                đã được thêm vào giỏ hàng</h4>
            <button type="button" class="close pull-right" aria-label="Close" data-dismiss="modal">
                <span aria-hidden="true">×</span>
            </button>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-md-6 divide-right">
                    <div class="row">
                        <div class="col-md-6">
                            <?php $images = json_decode($product->image); ?>
                            <?php $name = $product->name; ?>
                            <img class="product-image" src="<?php echo get_img_url($images[0]) ?>"
                                 alt="<?php echo url_title($name) ?>" title="<?php echo $name ?>" itemprop="image">
                        </div>

                        <div class="col-md-6"><h6 class="h6 product-name"><?php echo $name ?></h6>
                            <br>
                            <p><strong>Số lượng:</strong>&nbsp;<?php echo $product->soluong ?>
                            </p>
                        </div>

                    </div>

                </div>

                <div class="col-md-6">
                    <div class="cart-content">
                        <p class="cart-products-count mb-3">
                            Giỏ hàng của bạn hiện có <?php echo count($cart) ?> sản phẩm:
                        </p>
                        <ul class="mb-4">
                            <?php foreach ($list as $item): ?>
                                <li><strong><?php echo $item->name ?></strong> x<?php echo $item->soluong ?></li>
                            <?php endforeach; ?>
                        </ul>
                        <button type="button" class="btn btn-yellow btn-continue" data-dismiss="modal">
                            <i class="fa fa-angle-left icon-left"
                               aria-hidden="true"></i> Tiếp tục mua
                        </button>
                        <a href="<?php echo base_url(MFW_CART_ROUTE_CART) ?>"
                           class="btn btn-primary btn-order">Đặt hàng <i class="fa fa-angle-right icon-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>