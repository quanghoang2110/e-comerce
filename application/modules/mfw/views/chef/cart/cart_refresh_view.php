<?php $total_price = 0; ?>
<?php foreach ($list as $item): ?>
    <?php
    $product_link = get_detail_url($item->category_slug, $item->name, $item->id);
    $images = json_decode($item->image);
    $price = $item->price_2 ? $item->price_2 : $item->price_1;
    $total_price += $price * $item->soluong;
    ?>
    <div class="dropdown-item cart-item pl-2 position-relative border-bottom">
        <div class="cart-image">
            <a href="<?php echo $product_link ?>">
                <?php echo get_img_tag($images[0], $item->name, 'cart-img') ?>
            </a>
        </div>

        <a href="<?php echo $product_link ?>">
            <div class="cart-info">
                <div class="product-name"><?php echo $item->name ?></div>
                <?php ?>
                <div class="product-price my-1"><?php echo get_price($price) ?></div>
                <div class="product-quantity">x<?php echo $item->soluong ?></div>
            </div>
        </a>
        <a class="remove-from-cart position-absolute" style="top: 0"
           rel="nofollow"
           onclick="removeFromCart(<?php echo $item->id ?>)">
            <i class="fa fa-close"></i>
        </a>
    </div>
<?php endforeach; ?>
<div class="cart-item cart-item-total mt-3 px-3">
    <span>
        <strong>Tổng: </strong>
        <span class="float-right mfw-price"><?php echo get_price($total_price); ?></span>
    </span>
</div>
<div class="dropdown-item cart-item cart-item-btn pl-3">
    <a class="mfw-btn text-white btn btn-cart" href="<?php echo base_url(MFW_CART_ROUTE_CART) ?>">Giỏ hàng</a>
    <a class="mfw-btn btn-primary mfw-btn-order text-white btn" href="<?php echo base_url(MFW_CART_ROUTE_CART) ?>">Đặt hàng</a>
</div>