<script type="text/javascript"
        src="/assets/public/js/jquery.number.min.js"></script>
<script>
    var inputNumberObj = $('#mfwInputNumber');
    var mfwTotalPrice = $('#mfwTotalPrice');
    var mfwTotalPriceLbl = $('#mfwTotalPriceLbl');
    var currencyInvoice = $('#currencyInvoice');
    var hdCurrencyInvoice = $('#hdCurrencyInvoice');

    function increaseProduct(id, price) {
        const objNumber = $('#mfwInputNumber' + id);
        const num = parseInt(objNumber.val());
        objNumber.val(num);
        var totalPrice_1 = $('#total_price_' + id);
        var hdTotalPrice_1 = $('#hd_total_price_' + id);
        var newTotalPrice = parseInt(mfwTotalPrice.val()) + parseInt(price);
        var newTotalPrice_1 = parseInt(hdTotalPrice_1.val()) + parseInt(price);
        /*set new totalPrice*/
        mfwTotalPrice.val(newTotalPrice);
        mfwTotalPriceLbl.text($.number(newTotalPrice, 0, ',', '.') + ' đ');
        hdCurrencyInvoice.val(newTotalPrice);
        currencyInvoice.text($.number(newTotalPrice, 0, ',', '.') + ' đ');
        totalPrice_1.text($.number(newTotalPrice_1, 0, ',', '.') + ' đ');
        hdTotalPrice_1.val(newTotalPrice_1);
    }

    function decreaseProduct(id, price) {
        const objNumber = $('#mfwInputNumber' + id);
        const num = parseInt(objNumber.val());

        var totalPrice_1 = $('#total_price_'+id);
        var hdTotalPrice_1 = $('#hd_total_price_'+id);

        if (num > 1) {
            objNumber.val(num - 1);
            var newTotalPrice = parseInt(mfwTotalPrice.val()) - parseInt(price);
            var newTotalPrice_1 = parseInt(hdTotalPrice_1.val()) - parseInt(price);
            /* set new totalPrice*/
            mfwTotalPrice.val(newTotalPrice);
            mfwTotalPriceLbl.text($.number(newTotalPrice, 0, ',', '.') + ' đ');
            hdCurrencyInvoice.val(newTotalPrice);
            currencyInvoice.text($.number(newTotalPrice, 0, ',', '.') + ' đ');
            totalPrice_1.text($.number(newTotalPrice_1, 0, ',', '.') + ' đ');
            hdTotalPrice_1.val(newTotalPrice_1);
        }
    }

    function submitForm() {
        $('#csrfHash').val(encodeURIComponent(Cookies.get('csrf_cookie_name')));
        $('#cart_form').submit();
    }

    (function() {
        'use strict';
        window.addEventListener('load', function() {
            /* Fetch all the forms we want to apply custom Bootstrap validation styles to */
            var forms = document.getElementsByClassName('needs-validation');
            /* Loop over them and prevent submission */
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    showLoading();
                    $(this).find('[type=submit]').hide();
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        hideLoading();
                        $(this).find('[type=submit]').show();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    function onchangeOtherAddress(obj) {
        if($('#customCheck').is(':checked')){
            $('.div_other_address').removeClass('d-none');
            $('#input_other_name').attr('required', true);
            $('#input_other_phone').attr('required', true);
            $('#input_other_address').attr('required', true);
        }else{
            $('.div_other_address').addClass('d-none');
            $('#input_other_name').attr('required', false);
            $('#input_other_phone').attr('required', false);
            $('#input_other_address').attr('required', false);
        }
    }

    function onchangeExportInvoice(obj) {
        if(obj.is(':checked')){
            $('.div_export_invoice').removeClass('d-none');
            $('#input_compay_name').attr('required', true);
            $('#input_tax_code').attr('required', true);
            $('#input_company_address').attr('required', true);
        }else{
            $('.div_export_invoice').addClass('d-none');
            $('#input_compay_name').attr('required', false);
            $('#input_tax_code').attr('required', false);
            $('#input_company_address').attr('required', false);
        }
    }

    function onchangePaymentMethod(obj) {
        if(obj.is(':checked') && obj.val() == 3){
            $('.div_list_bank').removeClass('d-none');
        }else{
            $('.div_list_bank').addClass('d-none');
        }
    }
</script>