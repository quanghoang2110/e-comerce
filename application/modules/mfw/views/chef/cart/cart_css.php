<style>
    input.form-control::placeholder,
    textarea.form-control::placeholder{
        font-size: 14px;
        color: #aeadad
    }
    #cart_form{
        width: 100%;
    }
    .mfw-cart .btn{
        margin: 0;
    }
    .mfw-cart {
        border: 1px solid var(--border-color);
        padding: 15px 10px;
        background-color: white;
    }

    .mfw-cart ul, .mfw-cart li {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .mfw-cart li {
        border-bottom: 1px solid #eaeaea;
    }

    .mfw-cart li:last-child{
        border-bottom: none;
    }

    .mfw-cart .quantity {
        /*width: 60px;*/
        display: inline-block;
        float: left;
        overflow: hidden;
    }

    .mfw-cart .input-number {
        height: 40px;
        max-width: 50px;
        padding: 0 10px;
        text-align: center;
        border: 1px solid #eaeaea;
        border-right: none;
    }

    .mfw-cart .btn-plus {
        width: 20px;
        height: 40px;
        float: right;
        display: inline-block;
        text-align: center;
        background: white;
        border: 1px solid #eaeaea;
    }

    .mfw-cart .btn-plus-up {
        top: -3px;
        position: relative;
    }

    .mfw-cart .btn-plus-down {
        top: -5px;
        position: relative;
    }
    .form-control{
        border-color: var(--border-color);
        border-radius: 0;
    }

    .btn-cart:hover{
        background-color: #1b9647 !important;
    }

    [type="radio"]:checked,
    [type="radio"]:not(:checked) {
        position: absolute;
        left: -9999px;
    }
    [type="radio"]:checked + label,
    [type="radio"]:not(:checked) + label
    {
        position: relative;
        padding-left: 28px;
        cursor: pointer;
        line-height: 20px;
        display: inline-block;
        color: #666;
    }
    [type="radio"]:checked + label:before,
    [type="radio"]:not(:checked) + label:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 18px;
        height: 18px;
        border: 1px solid #ddd;
        border-radius: 100%;
        background: #fff;
    }
    [type="radio"]:checked + label:after,
    [type="radio"]:not(:checked) + label:after {
        content: '';
        width: 10px;
        height: 10px;
        background: #e18037;
        position: absolute;
        top: 4px;
        left: 4px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }
    [type="radio"]:not(:checked) + label:after {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
    [type="radio"]:checked + label:after {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }

</style>