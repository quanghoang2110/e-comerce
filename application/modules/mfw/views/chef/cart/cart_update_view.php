<main>
    <div class="container mfw-container">
        <div class="breadcrumbs">
            <div class="container">
                <ul>
                    <li>
                        <a class="smooth" href="/" title="">Trang chủ</a>
                    </li>
                    <li>
                        <a class="smooth" href="<?php echo base_url('/gio-hang.html') ?>" title="">Giỏ hàng</a>
                    </li>
                    <li class="active">Đặt hàng</li>
                </ul>
            </div>
        </div>
        <?php load_mfw_module('breadcrumb', 'index', 'Đặt hàng') ?>
        <div class="row">
            <div class="d-none d-lg-block d-xl-block col-lg-3 mfw-side-left">
                <?php load_mfw_module("product", "best_seller_left", 5); ?>
            </div>
            <div class="col-lg-9">
                <div class="mfw-title mt-3">
                    <div class="row ">
                        <div class="col-lg-8 col-xl-9 col-xs-12 font-weight-bold py-2" >
                            <h1 style="font-size: 16px">Đặt hàng</h1>
                        </div>
                    </div>
                </div>

                <section class="col-12 mb-5">
                    <div class="row">
                        <div class="col-md-7 pl-0">
                            <form class="needs-validation" id="cart_form" method="post"
                                  action="/<?php echo MFW_CART_ROUTE_SUCCESS ?>" novalidate>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>"
                                       id="csrfHash"/>
                                <div class="mfw-cart p-3">
                                    <!-- Default form register -->

                                    <p class="h6 mb-4">Thông tin người nhận</p>

                                    <div class="form-row mb-4">
                                        <div class="col">
                                            <input type="text" name="input_name" id="defaultRegisterFormFirstName"
                                                   class="form-control"
                                                   placeholder="Họ và tên người nhận *" required
                                                   value="<?php echo !empty($user['user_fullname']) ? $user['user_fullname'] : '' ?>">
                                            <div class="invalid-feedback">
                                                Vui lòng nhập Tên người nhận
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row mb-4">
                                        <div class="col">
                                            <input type="phone" name="input_phone" id="defaultRegisterFormEmail"
                                                   class="form-control"
                                                   value="<?php echo !empty($user['phone']) ? $user['phone'] : '' ?>"
                                                   placeholder="Số điện thoại *" required minlength="10">
                                            <div class="invalid-feedback">
                                                Vui lòng nhập Điện thoại liên hệ
                                            </div>
                                        </div>
                                        <div class="col">
                                            <input type="email" name="input_email" id="defaultRegisterFormEmail"
                                                   class="form-control"
                                                   value="<?php echo !empty($user['username']) ? $user['username'] : '' ?>"
                                                   placeholder="E-mail">
                                        </div>
                                    </div>

                                    <div class="form-row mb-3">
                                        <div class="col">
                                            <input type="text" name="input_address" class="form-control"
                                                   placeholder="Địa chỉ chi tiết *" required
                                                   value="<?php echo !empty($user['address']) ? $user['address'] : '' ?>">
                                            <div class="invalid-feedback">
                                                Vui lòng nhập Địa chỉ nhận hàng
                                            </div>
                                        </div>
                                    </div>

                                    <textarea name="input_note" class="form-control mb-4" rows="5"
                                              placeholder="Ghi chú"></textarea>

                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="customCheck" name="shipping_other" onchange="onchangeOtherAddress($(this))">
                                        <label class="custom-control-label" for="customCheck">Giao hàng tới địa chỉ khác</label>
                                    </div>

                                    <div class="div_other_address d-none">
                                        <div class="form-row mb-4">
                                            <div class="col">
                                                <input type="text" name="input_other_name" id="input_other_name"
                                                       class="form-control"
                                                       placeholder="Họ và tên người nhận *" value="">
                                                <div class="invalid-feedback">
                                                    Vui lòng nhập Tên người nhận
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row mb-4">
                                            <div class="col">
                                                <input type="phone" name="input_other_phone" id="input_other_phone"
                                                       class="form-control"
                                                       value=""
                                                       placeholder="Số điện thoại *" minlength="10">
                                                <div class="invalid-feedback">
                                                    Vui lòng nhập Điện thoại liên hệ
                                                </div>
                                            </div>
                                            <div class="col">
                                                <input type="email" name="input_other_email" id="input_other_email"
                                                       class="form-control"
                                                       value=""
                                                       placeholder="E-mail">
                                            </div>
                                        </div>
                                        <div class="form-row mb-3">
                                            <div class="col">
                                                <input type="text" name="input_other_address" class="form-control" id="input_other_address"
                                                       placeholder="Vui lòng nhập địa chỉ nhận hàng *" value="">
                                                <div class="invalid-feedback">
                                                    Vui lòng nhập Địa chỉ nhận hàng
                                                </div>
                                            </div>
                                        </div>

                                        <textarea name="input_other_note" id="input_other_address" class="form-control mb-4" rows="5"
                                                  placeholder="Ghi chú"></textarea>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="is_call" name="is_call" value="1">
                                        <label class="custom-control-label" for="is_call">Gọi trước khi giao</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="is_invoice" name="is_invoice" value="1" onchange="onchangeExportInvoice($(this))">
                                        <label class="custom-control-label" for="is_invoice">Xuất hóa đơn công ty</label>
                                    </div>
                                    <div class="div_export_invoice d-none">
                                        <div class="form-row mb-4">
                                            <div class="col">
                                                <input type="text" name="input_company_name" id="input_company_name"
                                                       class="form-control"
                                                       placeholder="Tên công ty *" value="">
                                                <div class="invalid-feedback">
                                                    Vui lòng nhập tên công ty
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row mb-4">
                                            <div class="col">
                                                <input type="text" name="input_tax_code" id="input_tax_code"
                                                       class="form-control"
                                                       value=""
                                                       placeholder="Mã số thuế *">
                                                <div class="invalid-feedback">
                                                    Vui lòng nhập mã số thuế
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row mb-3">
                                            <div class="col">
                                                <input type="text" name="input_company_address" class="form-control" id="input_company_address"
                                                       placeholder="Vui lòng nhập địa chỉ công ty *" value="">
                                                <div class="invalid-feedback">
                                                    Vui lòng nhập địa chỉ công ty
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="h6 mb-3">Hình thức giao hàng</p>
                                    <p class="mb-2">
                                        <input type="radio" id="delivery_1" name="delivery" value="1" checked>
                                        <label for="delivery_1">Giao hàng nhanh</label>
                                    </p>
                                    <p class="mb-3">
                                        <input type="radio" id="delivery_2" name="delivery" value="2">
                                        <label for="delivery_2">Giao hàng tiết kiệm</label>
                                    </p>
                                    <p class="h6 mb-3">Phương thức thanh toán</p>
                                    <p class="mb-2">
                                        <input type="radio" id="payment_method_1" value="1" name="payment_method" checked onchange="onchangePaymentMethod($(this))">
                                        <label for="payment_method_1">Thanh toán tiền mặt khi nhận hàng (COD)</label>
                                    </p>
                                    <p class="mb-2">
                                        <input type="radio" id="payment_method_2" value="2" name="payment_method" onchange="onchangePaymentMethod($(this))">
                                        <label for="payment_method_2">VNPay/Internet Banking (Miễn phí thanh toán)</label>
                                    </p>
                                    <p class="mb-2">
                                        <input type="radio" id="payment_method_3" value="3" name="payment_method" onchange="onchangePaymentMethod($(this))">
                                        <label for="payment_method_3">Chuyển khoản </label>
                                    </p>
                                    <div class="div_list_bank border d-none" style="border-radius: 5px">
                                        <?php foreach ($banks as $index => $item):?>
                                        <div class="col-12 py-2 <?php if ($index < (count($banks) - 1)):?> border-bottom <?php endif;?>">
                                            <p>
                                                Chủ tài khoản: <?php echo $item->name?><br/>
                                            </p>
                                            <p>
                                                Ngân hàng: <?php echo $item->bank_name?>
                                            </p>
                                            <p>
                                                Số tài khoản: <?php echo $item->number_card?>
                                            </p>
                                            <p>
                                                Chi nhánh: <?php echo $item->branch?>
                                            </p>
                                        </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <a class="btn btn-yellow m-0 px-4 btn-back-to-cart"
                                       href="/<?php echo MFW_CART_ROUTE_CART ?>"><i
                                                class="fa fa-angle-left"></i> &nbsp;Giỏ hàng</a>
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary m-0 btn-order-success px-4">Đồng ý
                                            &nbsp;<i
                                                    class="fa fa-angle-right"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <ul class="cart-items mfw-cart">
                                    <?php $total_price = 0; ?>
                                    <?php foreach ($list as $item): ?>
                                        <?php
                                        $product_link = get_detail_url($item->category_slug, $item->name, $item->id);
                                        $images = json_decode($item->image);
                                        $price = $item->price_2 ? $item->price_2 : $item->price_1;
                                        $total_price += $price * $item->soluong;
                                        $id = $item->id;
                                        ?>
                                        <li class="cart-item">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-4 pl-0">
                                                        <?php echo get_img_href_tag($product_link, '', $images[0], $item->name, 'img-cart') ?>
                                                    </div>
                                                    <!--  product left body: description -->
                                                    <div class="col-8 py-2">
                                                        <div class="product-line-info">
                                                            <a class="mfw-link"
                                                               href="<?php echo $product_link ?>"
                                                               data-id_customization="0"><?php echo $item->name ?></a>
                                                        </div>
                                                        <div class="mt-2">
                                                            <span><?php echo $item->soluong ?>x</span>
                                                            <span class="mfw-price"><?php echo get_price($price) ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </li>
                                    <?php endforeach; ?>
                                    <li class="pt-3">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-6 pl-0">
                                                    <strong>Tổng đơn giá:</strong>
                                                </div>
                                                <div class="col-6">
                                                    <input type="hidden" id="mfwTotalPrice"
                                                           value="<?php echo $total_price ?>"/>
                                                    <label id="mfwTotalPriceLbl"
                                                           class="mfw-sale"><?php echo get_price($total_price) ?></label>
                                                </div>
                                            </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</main>