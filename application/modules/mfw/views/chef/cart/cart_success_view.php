<main>
    <div class="container mfw-container">
        <?php load_mfw_module('breadcrumb', 'index', 'Đặt hàng') ?>
        <div class="row">
            <div class="d-none d-lg-block d-xl-block col-lg-3 mfw-side-left">
                <?php load_mfw_module("product", "best_seller_left", 5); ?>
            </div>
            <div class="col-lg-9 d-flex flex-wrap">
                <div class="col-md-7 mb-5">
                    <div class="row">
                        <div class="mfw-cart p-4 col-12">
                            <label class='text-success mb-3'>Đơn hàng của bạn đã được đăng ký thành công!</label>
                            <p><b>Thông tin đơn hàng: </b></p>
                            <p class="pl-3">Mã đơn hàng: <b><?php echo $order_code ?></b></p>
                            <p class="pl-3">
                                Họ tên: <b><?php echo $name ?></b></br>
                                SĐT: <b><?php echo $phone ?></b></br>
                                Địa chỉ: <b><?php echo $address ?></b></br>
                                Email: <b><?php echo $email ?></b></br>
                                <?php if (!empty($call)): ?>
                                    Gọi khi giao hàng: <b><?php echo $call; ?></b><br/>
                                <?php endif; ?>
                                <?php if (!empty($invoice)): ?>
                                    In hóa đơn: <b><?php echo $invoice ?></b><br/>
                                    &nbsp;&nbsp;&nbsp;Tên công ty: <b><?php echo $company_name ?></b><br/>
                                    &nbsp;&nbsp;&nbsp;Mã số thuế: <b><?php echo $tax_code ?></b><br/>
                                    &nbsp;&nbsp;&nbsp;Địa chỉ: <b><?php echo $company_address ?></b><br/>
                                <?php endif; ?>
                                Hình thức giao hàng: <b><?php echo $payment_method ?></b><br/>
                                Phương thức thanh toán: <b><?php echo $delivery ?></b><br/>
                                <?php if($note):?>
                                Ghi chú: <b><?php echo $note ?></b>
                                <?php endif;?>
                            </p>
                            <?php if ($shipping_other == 'on'): ?>
                                <p><b>Địa chỉ giao hàng khác: </b></p>
                                <p class="ml-3">
                                    Họ tên: <b><?php echo $other_name ?></b><br/>
                                    SĐT: <b><?php echo $other_phone ?></b><br/>
                                    Địa Chỉ: <b><?php echo $other_address ?></b><br/>
                                    Email: <b><?php echo $other_email ?></b><br/>
                                    <?php if($other_note):?>
                                    Ghi chú: <b><?php echo $other_note ?></b><br/>
                                    <?php endif;?>
                                </p>
                            <?php endif; ?>
                            <p class="mt-3">Chúng tôi sẽ liên hệ theo số điện thoại người nhận để xác nhận thời gian và
                                địa điểm giao
                                hàng trong thời gian sớm nhất.</p>
                            <p>Xin cảm ơn!</p>
                        </div>
                        <div class="mt-3">
                            <a class="btn btn-yellow m-0 text-white" href="/"><i class="fa fa-angle-left"></i> &nbsp;Trang
                                chủ</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 order-md-last order-first">
                    <div class="row">
                        <ul class="cart-items mfw-cart">
                            <?php $total_price = 0; ?>
                            <?php foreach ($products as $item): ?>
                                <?php
                                $product_link = get_detail_url($item->category_slug, $item->name, $item->id);
                                $images = json_decode($item->image);
                                $price = $item->price_2 ? $item->price_2 : $item->price_1;
                                $total_price += $price * $item->soluong;
                                $id = $item->id;
                                ?>
                                <li class="cart-item">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-4 pl-0">
                                                <?php echo get_img_href_tag($product_link, '', $images[0], $item->name, 'img-cart') ?>
                                            </div>
                                            <!--  product left body: description -->
                                            <div class="col-8 py-2">
                                                <div class="product-line-info">
                                                    <a class="mfw-link"
                                                       href="<?php echo $product_link ?>"
                                                       data-id_customization="0"><?php echo $item->name ?></a>
                                                </div>
                                                <div class="mt-2">
                                                    <span><?php echo $item->soluong ?>x</span>
                                                    <span class="mfw-price"><?php echo get_price($price) ?></span>
                                                </div>
                                            </div>
                                        </div>
                                </li>
                            <?php endforeach; ?>
                            <li class="pt-3">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6 pl-0">
                                            <strong>Tổng đơn giá:</strong>
                                        </div>
                                        <div class="col-6">
                                            <input type="hidden" id="mfwTotalPrice"
                                                   value="<?php echo $total_price ?>"/>
                                            <label id="mfwTotalPriceLbl"
                                                   class="mfw-sale"><?php echo get_price($total_price) ?></label>
                                        </div>
                                    </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>