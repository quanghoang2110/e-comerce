<div class="page-gray">
    <?php load_mfw_module('breadcrumb', 'index', 'Giỏ hàng') ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <form id="cart_form" method="post" action="/<?php echo MFW_CART_ROUTE_UPDATE ?>">
                    <div class="cart-box">
                        <h3 class="i-title"><i class="fa fa-shopping-cart"></i> Giỏ hàng của bạn
                            <?php
                            if (count($list) == 0) :
                                echo "Bạn chưa đặt mua sản phẩm nào";
                            else:
                            ?>
                            (<?php count($list); ?> sản phẩm)</h3>
                        <?php $total_price = 0; ?>
                        <?php foreach ($list as $item): ?>
                            <?php
                            $product_link = get_detail_url($item->category_slug, $item->name, $item->id);
                            $images = json_decode($item->image);
                            $price = $item->price_2 ? $item->price_2 : $item->price_1;
                            $price_1 = $item->price_2 ? ($item->price_2 * $item->soluong) : ($item->price_1 * $item->soluong);
                            $total_price += $price * $item->soluong;
                            $id = $item->id;
                            ?>
                            <div class="cart-item">
                                <div class="row align-items-end">
                                    <div class="col-lg-7">
                                        <div class="cart-pro">
                                            <a class="c-img hv-over" href="#" title="">
                                                <img src="<?php echo $images[0] ?>" alt="" title=""/>
                                            </a>
                                            <div class="ct">
                                                <h3 class="title"><a class="smooth" href="#"
                                                                     title=""><?php echo $item->name ?></a></h3>
                                                <input type="hidden"
                                                       name="product[]"
                                                       value="<?php echo $id ?>"/>
                                                <p>Thương hiệu: <span><?php echo $item->brand?></span></p>
                                                <div class="price"><?php echo get_price($price) ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-lg-6 col-md-7 col-sm-10">
                                        <div class="row col-mar-8 justify-content-between align-items-center">
                                            <div class="col-auto">
                                                <div class="i-number">
                                                    <a href="#minus" class="n-ctrl down smooth" type="button"
                                                       onclick="decreaseProduct(<?php echo $id ?>,<?php echo $price ?>)"><i
                                                                class="fa fa-angle-down"></i></a>
                                                    <input type="text" class="numberic" min="1"
                                                           id="mfwInputNumber<?php echo $id ?>"
                                                           value="<?php echo $item->soluong; ?>" name="quantity[]">
                                                    <a href="#add" class="n-ctrl up smooth" type="button"
                                                       onclick="increaseProduct(<?php echo $id ?>,<?php echo $price ?>)"><i
                                                                class="fa fa-angle-up"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-auto"
                                                 id="total_price_<?php echo $id ?>"><?php echo get_price($price_1) ?></div>
                                            <input type="hidden" id="hd_total_price_<?php echo $id ?>"
                                                   value="<?php echo $price_1 ?>">
                                            <div class="col-auto order-lg-first">
                                                <a class="smooth remove" href="?delete=<?php echo $item->id ?>"
                                                   title=""><i>&times;</i><span>Bỏ sản phẩm</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="cart-total">
                            <div class="ct">
                                <input type="hidden" id="mfwTotalPrice"
                                       value="<?php echo $total_price ?>"/>
                                <p class="mb-0">Tổng tiền: <strong
                                            id="mfwTotalPriceLbl"><?php echo get_price($total_price) ?></strong></p>
                                <span>Đã bao gồm VAT nếu có</span>
                            </div>
                            <div class="row justify-content-between col-mar-5">
                                <div class="col-auto"><a class="smooth shop-now" href="/" title=""><i
                                                class="fa fa-angle-left"></i> Tiếp tục mua hàng</a></div>
                                <div class="col-auto"><a class="smooth pay-now text-white" onclick="submitForm()"
                                                         title="">Thanh
                                        toán <i class="fa fa-angle-right"></i></a></div>
                            </div>
                        </div>
                        <?php endif ?>
                    </div>
                </form>
            </div>
            <div class="col-lg-4">
                <div class="sb-cart">
                    <h3 class="title">Hóa đơn</h3>
                    <?php
                    if (count($list) == 0) :
                        ?>
                        <div class="total">
                        <?php echo "Bạn chưa đặt mua sản phẩm nào"; ?>
                        </div>
                    <?php
                    else:
                        ?>
                        <div class="total">
                            <label>Thành tiền</label>
                            <div class="price">
                                <strong id="currencyInvoice"><?php echo get_price($total_price) ?></strong>
                                <input type="hidden" value="<?php echo $total_price ?>" id="hdCurrencyInvoice">
                                <p>Đã bao gồm VAT nếu có</p>
                            </div>
                        </div>
                        <div class="ctrl">
                            <a class="smooth" href="#" onclick="submitForm()" title="">Tiến hành thanh toán</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <br><br><br class="d-none d-lg-block">
</div>
