<div class="page-gray">
    <?php load_mfw_module('breadcrumb', 'index', 'Đặt hàng') ?>
    <div class="container">
        <div class="cart-process">
            <div class="row col-mar-5">
                <div class="col-4">
                    <div class="item green"><span>1</span> Đăng nhập</div>
                </div>
                <div class="col-4">
                    <div class="item green"><span>2</span> Thông tin giao hàng</div>
                </div>
                <div class="col-4">
                    <div class="item red"><span>3</span> Hình thức thanh toán</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="cart-payment">
                    <h3 class="title">VUI LÒNG CHỌN HÌNH THỨC THANH TOÁN</h3>
                    <div class="ct">
                        <label class="pay-radio">
                            <input type="radio" name="pay"><i></i>
                            <span class="icon"><img src="images/ship.png" alt="" title="" /></span>
                            <span class="ct">
                                    <span class="c-title">Thanh toán khi nhận hàng</span>
                                    <span class="desc">Quý khách sẽ thanh toán bằng tiền mặt tại showroom của Cheftstudio</span>
                                </span>
                        </label>
                        <label class="pay-radio">
                            <input type="radio" name="pay"><i></i>
                            <span class="icon"><img src="images/momo.png" alt="" title="" /></span>
                            <span class="ct">
                                    <span class="c-title">Thanh toán bằng ví MoMo hoặc QRCODE</span>
                                    <span class="desc">Quý khách vui lòng cài app MoMo trước khi chọn hình thức này để thuận tiện cho việc thanh toán.</span>
                                </span>
                        </label>
                        <label class="pay-radio">
                            <input type="radio" name="pay"><i></i>
                            <span class="icon"><img src="images/atm.png" alt="" title="" /></span>
                            <span class="ct">
                                    <span class="c-title">Thanh toán bằng thẻ ATM</span>
                                    <span class="desc">Quý khách sẽ được chuyển tới Napas để thanh toán.</span>
                                </span>
                        </label>
                    </div>
                </div>
                <div class="row align-items-center justify-content-between col-mar-5">
                    <div class="col-md-auto">
                        <label class="pay-check"><input type="checkbox" name=""><i></i> Tôi đồng ý với <a class="smooth" href="#" title="">điều khoản & điều kiện</a> giao dịch của Cheft</label>
                    </div>
                    <div class="col-md-auto">
                        <a class="smooth pay-btn" href="#" title="">Thanh toán</a>
                    </div>
                </div>
                <br class="d-lg-none">
            </div>
            <div class="col-lg-4">
                <div class="sb-order">
                    <div class="head">
                        <h3 class="i-title">Đơn hàng <span>(<?php echo count($list)?> sản phẩm)</span></h3>
                        <a class="smooth" href="<?php echo base_url(MFW_CART_ROUTE_CART)?>" title="">Sửa đơn hàng</a>
                    </div>
                    <div class="ct">
                        <?php $total_price = 0; ?>
                        <?php foreach ($list as $item): ?>
                            <?php
                            $product_link = get_detail_url($item->category_slug, $item->name, $item->id);
                            $images = json_decode($item->image);
                            $price = $item->price_2 ? $item->price_2 : $item->price_1;
                            $total_price += $price * $item->soluong;
                            $id = $item->id;
                            ?>
                            <div class="order-pro mb-3 pb-2 border-bottom">
                                <a class="c-img" href="<?php echo $product_link ?>" title="">
                                    <img src="<?php echo get_img_url($images[0])?>" alt="Not Image" title="<?php echo $item->name?>"/>
                                </a>
                                <h3 class="title"><a class="smooth" href="#" title=""><?php echo $item->name?></a>
                                </h3>
                                <div class="price">
                                    <p><?php echo get_price($price)?>đ</p>
                                    <p>x<?php echo $item->soluong ?></p>
                                    <p><?php echo get_price($price * $item->soluong)?>đ</p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <table>
                            <tr>
                                <td>Tổng tiền</td>
                                <td><strong><?php echo get_price($total_price)?>đ</strong></td>
                            </tr>
                            <tr>
                                <td>Phí vận chuyển</td>
                                <td>MIỄN PHÍ</td>
                            </tr>
                        </table>
                        <table class="total-tb">
                            <tr>
                                <td>Số tiền phải thanh toán</td>
                                <td><span><?php echo get_price($total_price)?>đ</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br class="d-none d-lg-block">
</div>