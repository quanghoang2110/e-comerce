<main>
    <div class="container mfw-container">
        <?php load_mfw_module('breadcrumb', 'index', 'Liên hệ') ?>
        <div class="row">
            <div class="d-none d-lg-block d-xl-block col-xl-4 col-lg-5 mfw-side-left">
                <section class="wow fadeIn mb-5">
                    <div class="mfw-header-title">
                        <h3 class="h3">Tư vấn</h3>
                    </div>
                    <!--Grid row-->
                    <div class="row wow fadeIn">
                        <div class="col-md-12">
                            <ul class="bg-white">
                                <?php foreach ($list as $item): ?>
                                    <?php
                                    $name = $item->name;
                                    $id = $item->id;
                                    $url = get_detail_url('hoi-dap', $name, $id);
                                    ?>
                                    <li class="mfw-item"
                                        title="<?php echo $name ?>">
                                        <div class="col-12">
                                            <a href="<?php echo $url ?>" title="<?php echo $name ?>">
                                                <?php echo $name ?>
                                            </a>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <!--/Grid column-->
                        </div>
                    </div>
                    <!--/Grid row-->
                </section>
            </div>
            <div class="col-xl-8 col-lg-7 mfw-list">
                <div class="mfw-news-title mfw-header-title">
                    <h1>Liên hệ với chúng tôi</h1>
                </div>
                <section class="mb-5">
                    <form class="needs-validation" id="cart_form" method="post" novalidate>
                        <div class="col-12">
                            <div class="row">
                                <?php if ($message): ?>
                                    <p class="col-12"><?php echo $message ?></p>
                                <?php endif ?>
                                <div class="col-md-6">
                                    <div class="form-row mb-4">
                                        <div class="col">
                                            <input type="text" name="input_name" id="defaultRegisterFormFirstName"
                                                   class="form-control"
                                                   placeholder="Tên của bạn *" required>
                                            <div class="invalid-feedback">
                                                Vui lòng nhập Tên của bạn
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mb-4">
                                        <div class="col">
                                            <input type="phone" name="input_phone" id="defaultRegisterFormEmail"
                                                   class="form-control"
                                                   placeholder="Số điện thoại *" required minlength="10">
                                            <div class="invalid-feedback">
                                                Vui lòng nhập Điện thoại liên hệ
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mb-4">
                                        <div class="col">
                                            <input type="email" name="input_email" id="defaultRegisterFormEmail"
                                                   class="form-control"
                                                   placeholder="E-mail">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                        <textarea name="input_note" class="form-control mb-4" rows="5"
                                                  placeholder="Nội dung"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="pull-right btn mfw-button text-black m-0">Đồng ý &nbsp;<i
                                                class="fa fa-angle-right"></i></button>
                                </div>
                                <div class="col-md-6">
                                    <?php $setting = new Setting_object($setting); ?>
                                    <p><strong><?php echo $setting->name ?></strong></p>
                                    <div>
                                        <i class="fa fa-location-arrow"></i>&nbsp;&nbsp;<?php echo $setting->company_address ?>
                                    </div>
                                    <div><i class="fa fa-phone"></i>&nbsp;&nbsp;<?php echo $setting->company_phone ?>
                                    </div>
                                    <div>
                                        <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;<?php echo $setting->company_email ?>
                                    </div>
                                    <br/>
                                    <p class="mfw-pre-line"><?php echo $setting->company_info ?></p>
                                </div>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>"
                                       id="csrfHash"/>
                                <input type="hidden" name="r3_token" id="r3token"/>

                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</main>