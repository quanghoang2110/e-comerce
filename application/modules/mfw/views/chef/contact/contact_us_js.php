<script src='https://www.google.com/recaptcha/api.js?render=6Lch3ncUAAAAAArWSOSsg4bxw11pAsVfqoVqZT5n'></script>
<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            /* Fetch all the forms we want to apply custom Bootstrap validation styles to*/
            var forms = document.getElementsByClassName('needs-validation');
            /* Loop over them and prevent submission */
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    grecaptcha.ready(function() {
        grecaptcha.execute('6Lch3ncUAAAAAArWSOSsg4bxw11pAsVfqoVqZT5n', {action: 'contact_us'})
            .then(function(token) {
                $('#r3token').val(token);
            });
    });
</script>