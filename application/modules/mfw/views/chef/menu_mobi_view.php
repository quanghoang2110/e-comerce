<ul>
    <li><a class="smooth <?php echo $current_menu == 'home' ? 'active' : '' ?>" href="/" title="">Trang chủ</a></li>
    <?php foreach ($list as $index => $item): ?>
        <?php $item_url = base_url($item->slug); ?>
        <li>
            <a class="smooth <?php if (!empty($category) && !empty($category->slug) && $category->slug == $item->slug): echo 'active'; endif; ?>"
               href="<?php echo $item_url ?>"
               title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
            <?php if (!empty($item->childrens)): ?>
                <ul>
                    <?php foreach ($item->childrens as $item_children): ?>
                        <?php $item_url_childs = base_url($item_children->slug); ?>
                        <li>
                            <a class="smooth" href="<?php echo $item_url_childs ?>"
                               title="<?php echo $item_children->name ?>">
                                <?php echo $item_children->name ?>
                            </a>
                            <?php if ($item_children->children): ?>
                                <ul>
                                    <?php foreach ($item_children->children as $item_child): ?>
                                        <?php $item_url_child = base_url($item_child->slug); ?>
                                        <li><a class="smooth" href="<?php echo $item_url_child ?>"
                                               title=""><?php echo $item_child->name ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>

                    <?php if (!empty($item->trademark)): ?>
                        <li><span>Theo thương hiệu</span>
                            <ul>
                                <?php foreach ($item->trademark as $item_trademark): ?>
                                    <li><a class="smooth"
                                           href="<?php echo base_url($item->slug . '/brands/' . $item_trademark->slug) ?>"
                                           title=""><?php echo $item_trademark->name ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <li class="view-all d-none d-lg-block"><a class="smooth" href="<?php echo $item_url; ?>" title="">Xem
                            tất cả</a></li>
                </ul>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
    <li><a class="smooth <?php echo $current_menu == 'khuyen-mai' ? 'active' : '' ?>"
           href="<?php echo base_url(ROUTE_DISCOUNT) ?>" title="">Khuyến mại</a></li>
    <?php if (!empty($pages)): ?>
        <?php foreach ($pages as $item): ?>
            <li><a class="smooth" href="<?php echo base_url($item->slug) ?>"
                   title=""><?php echo $item->name ?></a></li>
        <?php endforeach; ?>
    <?php endif; ?>
    <li><a class="smooth" href="/tin-tuc" fixed-sn title="">Tin tức</a></li>
    <li><a class="smooth" href="tel:<?php echo $setting->company_phone ?>" title="">Hotline <strong><?php echo $setting->company_phone ?></strong></a></li>

</ul>