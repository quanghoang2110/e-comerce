<?php $uri = $this->uri->segment(1);?>
<?php if($uri != 'dat-hang.html'):?>
<div class="breadcrumbs">
    <div class="container">
        <ul>
            <li>
                <a class="smooth" href="/" title="">Trang chủ</a>
            </li>
            <?php foreach ($slugs as $index => $slug): ?>
                <?php if ($index < count($slugs) - 1): ?>
                    <li>
                        <a class="smooth" href="<?php echo base_url($slug->slug) ?>" title=""><?php echo $slug->name ?></a>
                    </li>
                <?php endif; ?>
                <?php if ($index == count($slugs) - 1): ?>
                    <li class="active"><?php echo $slug ?></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php endif;?>