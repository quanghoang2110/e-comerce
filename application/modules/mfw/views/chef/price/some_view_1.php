<?php if ($list): ?>
    <div class="pro-sidebar">
        <p class="title" onclick="menu_mobi($(this))" data-title="1">GIÁ<i class="fa fa-angle-down float-right mt-1 d-lg-none d-block" aria-hidden="true"></i></p>
        <div class="ct">
            <?php
            $price = !empty($_GET['price']) ? $_GET['price'] : '';
            if ($price != '') {
                $price = explode(',', rtrim($price, ','));
            } else {
                $price = [];
            }
            ?>
            <?php foreach ($list as $item): ?>
                <label class="i-check"><input <?php echo in_array($item->id, $price) ? 'checked' : '' ?>
                            type="checkbox"
                            onchange="changeSort($('#sort_by').val(), $('#per_size option:selected').val())"
                            class="checkbox_price"
                            value="<?php echo $item->id ?>"><i></i> <?php echo get_price($item->price_1) ?> - <?php echo $item->price_2 ? get_price($item->price_2) : 'Nhiều hơn' ?></label>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>