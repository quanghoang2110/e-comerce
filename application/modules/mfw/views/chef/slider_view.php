<div class="md-slider">
    <?php foreach ($list as $index => $item): ?>
        <div class="item">
            <div class="img">
                <a class="smooth" href="<?php echo $item->url; ?>" title=""><img src="<?php echo get_img_url($item->image, ''); ?>" alt=""></a>
            </div>
        </div>
    <?php endforeach; ?>
</div>