<?php if (count($list) > 0): ?>
    <div class="sidebar">
        <h3 class="sb-title">Hướng dẫn dùng sản phẩm</h3>
        <?php foreach ($list as $item): ?>
            <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
            <div class="sb-post">
                <a class="img hv-over" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                    <img src="<?php echo get_img_url($item->image) ?>" alt="No Image"
                         title="<?php echo $item->name ?>"/>
                </a>
                <div class="ct">
                    <h3 class="title"><a class="smooth" href="<?php echo $item_url ?>"
                                         title=""><?php echo $item->name ?></a></h3>
                    <p><?php echo $item->total_comment ?> bình luận</p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>