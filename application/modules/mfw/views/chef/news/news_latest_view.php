<div class="post-sidebar">
    <h3 class="title">Bài viết mới nhất</h3>
    <ul>
        <?php foreach ($list as $item):?>
            <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
        <li class="px-3 mb-0">
            <a class="smooth" href="<?php echo $item_url ?>" title=""><?php echo $item->name ?></a>
        </li>
        <?php endforeach;?>
    </ul>
</div>