<div class="md-posts-cate md-news-host">
    <p class="md-title">Tin tức</p>
    <div class="posts-cas">
        <?php foreach ($list as $index => $item): ?>
            <?php $item_url = $item->slug; ?>
            <div class="slick-slide">
                <div class="post-cate">
                    <a class="c-img" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                        <img src="<?php echo get_img_url($item->image) ?>" alt="" title="<?php echo $item->name ?>"/>
<!--                        <h3 class="title">Tư vấn <strong>Thiết Bị Bếp</strong></h3>-->

                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>