<?php if(!empty($list)):?>
<div class="post-related">
    <h3 class="title">Bài viết liên quan</h3>
    <ul>
        <?php foreach ($list as $index => $item): ?>
        <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
        <li><a class="smooth" href="<?php echo $item_url ?>" title="<?php echo $item->name?>"><?php echo $item->name?></a>
        </li>
        <?php endforeach;?>
    </ul>
</div>
<?php endif;?>