<main>
    <div class="page-gray">
        <?php $category_name = !empty($category_id) ? $category->name : 'Tin tức' ?>
        <?php load_mfw_module('breadcrumb', 'index', $category_name) ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-9 order-lg-last">
                    <div class="posts-content">
                        <div class="page-bar">
                            <div class="row justify-content-between align-items-center px-3">
                                <?php
                                $paging_url = !empty($category) ? base_url($category->slug) : 'tin-tuc';
                                ?>
                                <h1 class="title-news font-weight-bold text-uppercase"><?php echo !empty($category_id) ? $category->name : 'Tin tức' ?><?php if (!empty($current_menu) && $current_menu == 'khuyen-mai'): echo ' khuyến mại'; endif; ?></h1>
                            </div>
                        </div>
                        <?php if ($category_id || (!empty($current_menu) && $current_menu == 'khuyen-mai')): ?>
                            <?php if (!empty($current_menu) && $current_menu == 'khuyen-mai'): ?>
                                <div class="row">
                                    <?php foreach ($list as $item): ?>
                                        <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
                                        <div class="col-lg-4 col-6">
                                            <div class="post post-discount">
                                                <a <?php if ($item->type == 'VIDEO'): ?> data-fancybox <?php endif; ?>
                                                        class="c-img hv-over"
                                                        href="<?php if ($item->type == 'VIDEO'): echo $item->video_link; else: echo $item_url; endif; ?>"
                                                        title="">
                                                    <img src="<?php echo get_img_url($item->image) ?>" alt="" title=""/>
                                                    <?php if ($item->type == 'VIDEO'): ?>
                                                        <div class="flex-center mfw-absolute-full d-flex justify-content-center align-items-center">
                                                            <i class="fa fa-play-circle-o play" aria-hidden="true"></i>
                                                        </div>
                                                    <?php endif; ?>
                                                </a>
                                                <div class="ct">
                                                    <h2 class="title">
                                                        <a <?php if ($item->type == 'VIDEO'): ?> data-fancybox <?php endif; ?>
                                                                class="smooth"
                                                                href="<?php if ($item->type == 'VIDEO'): echo $item->video_link; else: echo $item_url; endif; ?>"
                                                                title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
                                                    </h2>
                                                    <div class="info">
                                                        Ngày: <?php echo date('d-m-Y', strtotime($item->create_time)) ?>
                                                    </div>
<!--                                                    <p>--><?php //echo $item->desc ?><!--</p>-->
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php else: ?>
                                <?php foreach ($list as $item): ?>
                                    <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
                                    <div class="post">
                                        <a <?php if ($item->type == 'VIDEO'): ?> data-fancybox <?php endif; ?>
                                                class="c-img hv-over"
                                                href="<?php if ($item->type == 'VIDEO'): echo $item->video_link; else: echo $item_url; endif; ?>"
                                                title="">
                                            <img src="<?php echo get_img_url($item->image) ?>" alt="" title=""/>
                                            <?php if ($item->type == 'VIDEO'): ?>
                                                <div class="flex-center mfw-absolute-full d-flex justify-content-center align-items-center">
                                                    <i class="fa fa-play-circle-o play" aria-hidden="true"></i>
                                                </div>
                                            <?php endif; ?>
                                        </a>
                                        <div class="ct">
                                            <h2 class="title">
                                                <a <?php if ($item->type == 'VIDEO'): ?> data-fancybox <?php endif; ?>
                                                        class="smooth"
                                                        href="<?php if ($item->type == 'VIDEO'): echo $item->video_link; else: echo $item_url; endif; ?>"
                                                        title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
                                            </h2>
                                            <div class="info">
                                                Ngày: <?php echo date('d-m-Y', strtotime($item->create_time)) ?>
                                            </div>
                                            <p><?php echo $item->desc ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <div class="md-posts-cate">
                                <div class="row">
                                    <?php foreach ($list as $index => $item): ?>
                                        <?php $item_url = $item->slug; ?>
                                        <div class="col-6 col-sm-6 col-md-4 pb-3 px-2">
                                            <div class="post-cate">
                                                <a class="c-img" href="<?php echo $item_url ?>"
                                                   title="<?php echo $item->name ?>">
                                                    <img src="<?php echo get_img_url($item->image) ?>" alt=""
                                                         title="<?php echo $item->name ?>"/>
                                                </a>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="row justify-content-between align-items-center px-3">
                            <?php load_mfw_module("pagination", "index", $paging_url, $total, $limit) ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mfw-side-left">
                    <?php if (!empty($category_id)): include_once "news_sidebar.php"; endif; ?>
                    <div class="post-sidebar">
                        <h3 class="title">Bạn có thể mua sản phẩm </h3>
                        <div class="sb-buy">
                            <div class="item">
                                <div class="label"><span>1</span> Trên website - nhanh nhất, tiện nhất</div>
                            </div>
                            <?php load_mfw_module("news", "business_channel") ?>
                            <div class="item">
                                <div class="label"><span>3</span> Qua điện thoại</div>
                                <?php foreach ($agencys as $index => $item): ?>
                                    <div class="phone">Cơ sở <?php echo $index + 1; ?>:
                                        <span><?php echo $item->phone ?></span></div>
                                <?php endforeach; ?>
                            </div>
                            <div class="item">
                                <div class="label">
                                    <span>4</span> Điểm Bán
                                </div>
                                <?php foreach ($agencys as $item): ?>
                                    <div class="studio mt-2">
                                        <a target="_blank" href="<?php echo $item->url; ?>">
                                            <i class="fa fa-map-marker"></i>
                                            <?php echo $item->name ?>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <!---tin tuc moi nhat--->
                    <?php load_mfw_module('news', 'latest', 10) ?>
                </div>
            </div>
        </div>
        <br><br>
    </div>
</main>