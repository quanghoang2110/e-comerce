<?php if (count($list)): ?>
<div class="md-promotion">
    <p class="md-title">Chương trình khuyến mãi</h2>
    <div class="promotion-cas">
        <?php foreach ($list as $item): ?>
            <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
            <?php $image = $item->image; ?>
            <div class="slick-slide">
                <div class="promotion">
                    <a class="c-img" href="<?php echo $item_url; ?>" title="<?php echo $item->name ?>">
                        <img src="<?php echo get_img_url($image); ?>" alt="Not Image" title="<?php echo $item->name ?>"/>
                    </a>
                    <h4 class="title">
                        <a class="smooth" href="<?php echo $item_url; ?>" title="">
                            <?php echo $item->name ?>
                        </a>
                    </h4>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>