<div class="mfw-mega-menu dropdown-menu mega-menu pt-0 pb-0"
     aria-labelledby="navbarDropdownMenuLink1">
    <div class="container mega-news">
        <div class="row">
            <div class="col-lg-2 pt-3 pr-0">
                <ul class="list-unstyled">
                    <?php foreach ($cats as $cat): ?>
                        <li class="sub-title">
                            <a class="menu-item pl-1 mt-2" href="<?php echo base_url($cat->slug) ?>">
                                <?php echo $cat->name ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="col-lg-10 pt-3 pb-3 white d-none d-lg-block">
                <div class="row">
                    <?php foreach ($list as $item): ?>
                        <?php $item_url = get_detail_url($cats[0]->slug, $item->name, $item->id) ?>
                        <div class="col-lg-3 sub-menu mb-xl-0 mb-4">
                            <!--Featured image-->
                            <a href="<?php echo $item_url ?>" class="view overlay z-depth-1 p-0">
                                <?php echo get_img_tag($item->image, $item->name, 'img-fluid'); ?>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                            <h4 class="mb-2">
                                <a class="news-title pl-0 pr-0"
                                   href="<?php echo $item_url ?>"><?php echo $item->name ?></a>
                            </h4>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>