<?php if(!empty($list)):?>
<div class="post-sidebar">
    <h3 class="title">Video mới nhất</h3>
    <div class="sb-video">
        <?php foreach ($list as $item): ?>
            <a <?php if ($item->type == 'VIDEO'): ?> data-fancybox <?php endif; ?> class="c-img hv-over" href="<?php echo $item->video_link;?>" title="">
                <img src="<?php echo get_img_url($item->image) ?>" alt="" title=""/>
                <?php if ($item->type == 'VIDEO'): ?>
                    <div class="flex-center mfw-absolute-full d-flex justify-content-center align-items-center">
                        <i class="fa fa-play-circle-o play" aria-hidden="true"></i>
                    </div>
                <?php endif; ?>
            </a>
        <?php endforeach; ?>
    </div>
</div>
<?php endif?>