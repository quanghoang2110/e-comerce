<section class="wow fadeIn mb-5 danh-muc-tin-tuc mt-5 mt-md-0">
    <div class="mfw-header-title p-3 bg-white">
        <h3 class="h3 mb-0">Danh mục</h3>
    </div>
    <!--Grid row-->
    <div class="row wow fadeIn">
        <div class="col-md-12">
            <ul class="bg-white">
                <?php foreach ($list_category as $item): ?>
                    <?php
                    $name = $item->name;
                    $id = $item->id;
                    ?>
                    <li class="mfw-item <?php echo $category->id == $id ? 'activated' : '' ?>"
                        title="<?php echo $name ?>">
                        <div class="col-12">
                            <a href="<?php echo base_url($item->slug) ?>" title="<?php echo $name ?>">
                                <?php echo $name ?>
                            </a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <!--/Grid column-->
        </div>
    </div>
    <!--/Grid row-->
</section>