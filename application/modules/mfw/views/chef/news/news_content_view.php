<main>
    <?php load_mfw_module('breadcrumb', 'index', $category, $data->name) ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 order-lg-last">
                <div class="posts-content">
                    <div class="post-detail">
                        <h1 class="title"><?php echo $data->name; ?></h1>
                        <div class="time">Ngày đăng: <?php echo date('Y-m-d', strtotime($data->create_time));?></div>
                        <div class="s-content">
                            <?php if ($data->type == 'VIDEO'): ?>
                                <iframe id="ytplayer" type="text/html" width="640" height="360"
                                        src="<?php echo $data->video_link?>"
                                        frameborder="0"></iframe>
                            <?php else:?>
                            <div class="col-12 px-5 text-center"><img src="<?php echo $data->image; ?>"></div>
                            <?php echo $data->content?>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="hc-comments">
                        <?php $optComment = ['num' => 10, 'id' => $data->id, 'type' => 'news'] ?>
                        <?php load_mfw_module('comment', 'index', $optComment) ?>
                    </div>
                    <?php load_mfw_module('product', 'list_product_related_news', $data->products_related, 4) ?>
                </div>
            </div>
            <div class="col-lg-3 mfw-side-left">
                <?php  include_once "news_sidebar.php" ?>
                <div class="post-sidebar">
                    <h3 class="title">Bạn có thể mua sản phẩm </h3>
                    <div class="sb-buy">
                        <div class="item">
                            <div class="label"><span>1</span> Trên website - nhanh nhất, tiện nhất</div>
                        </div>
                        <?php load_mfw_module("news", "business_channel") ?>
                        <div class="item">
                            <div class="label"><span>3</span> Qua điện thoại</div>
                            <?php foreach ($agencys as $index => $item): ?>
                                <div class="phone">Cơ sở <?php echo $index + 1; ?>: <span><?php echo $item->phone ?></span></div>
                            <?php endforeach; ?>
                        </div>
                        <div class="item">
                            <div class="label">
                                <span>4</span> Điểm bán
                            </div>
                            <?php foreach ($agencys as $item): ?>
                                <div class="studio">
                                    <a href="<?php echo $item->url?>" target="_blank">
                                        <i class="fa fa-map-marker"></i>
                                        <?php echo $item->name ?>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!----tin tuc moi nhat--->
                <?php load_mfw_module('news', 'latest', 10) ?>
                <!----video moi nhat--->
                <?php load_mfw_module('news', 'hot_video', 2) ?>

            </div>
        </div>
    </div>
</main>