<?php
$slide1 = !empty($list[0]) ? $list[0] : [];
$imgs1 = $slide1->image_2;
?>
<?php
$slide2 = !empty($list[3]) ? $list[3] : [];
$imgs2 = $slide2->image_2;
?>
<div class="md-cates">
    <p class="md-title">SẢN PHẨM CỦA CHEFSTUDIO</p>
    <div class="row col-mar-3">
        <div class="col-md-4 col-6">
            <div class="md-cate">
                <a class="c-img" href="<?php echo $slide1->url ?>" title="<?php echo $slide1->name ?>">
                    <img src="<?php echo get_img_url($imgs1) ?>" alt="" title="<?php echo $slide1->name ?>"/>
                </a>
                <div class="caption">
                    <!--                    <h3 class="title">Dụng cụ <br><strong>Làm bánh</strong></h3>-->
                    <h2 class="title"><?php echo $slide1->name ?></h2>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-6 order-md-last">
            <div class="md-cate">
                <a class="c-img" href="<?php echo $slide2->url ?>" title="<?php echo $slide2->name ?>">
                    <img src="<?php echo get_img_url($imgs2) ?>" alt="" title="<?php echo $slide2->name ?>"/>
                </a>
                <div class="caption">
                    <h2 class="title"><?php echo $slide2->name ?></h2>
                </div>
            </div>
        </div>
        <?php foreach ($list as $index => $item): ?>
            <?php $item_url =$item->url; ?>
            <?php if ($index > 0 && $index < 3): ?>
                <div class="col-md-8">
                    <div class="md-cate v2">
                        <a class="c-img" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                            <img src="<?php echo get_img_url($item->image) ?>" alt=""
                                 title="<?php echo $item->name ?>"/>
                        </a>
                        <div class="caption">
                            <h2 class="title"><strong><?php echo $item->name ?></strong></h2>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php foreach ($list as $index => $item): ?>
            <?php $item_url = $item->url; ?>
            <?php if ($index > 3): ?>
                <div class="col-md-4 col-4">
                    <div class="md-cate">
                        <a class="c-img" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                            <img src="<?php echo get_img_url($item->image_2) ?>" alt="" title="<?php echo $item->name ?>"/>
                        </a>
                        <div class="caption">
                            <h2 class="title"><?php echo $item->name ?></h2>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>