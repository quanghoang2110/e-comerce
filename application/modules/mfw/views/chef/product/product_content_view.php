<main>
    <?php load_mfw_module('breadcrumb', 'index', $category, $data->name) ?>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-md-6">
                        <div class="pro-img-cas">
                            <?php $imgs = $data ? json_decode($data->image) : []; ?>
                            <?php foreach ($imgs as $index => $img): ?>
                                <?php if (!empty($data->video) && $index == 0): ?>
                                    <div class="slick-slide">
                                        <div class="c-img">
                                            <iframe src="<?php echo $data->video ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="slick-slide">
                                        <div class="c-img">
                                            <a data-fancybox="image" href="<?php echo get_img_url($img) ?>">
                                                <img src="<?php echo get_img_url($img) ?>" alt="" title=""/>
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="pro-thumb-cas">
                            <?php foreach ($imgs as $index => $img): ?>
                                <?php if (!empty($data->video) && $index == 0): ?>
                                    <div class="slick-slide">
                                        <div class="c-img video-btn">
                                            <img src="<?php echo get_img_url($img) ?>" alt="" title=""/>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="slick-slide">
                                        <div class="c-img">
                                            <img src="<?php echo get_img_url($img) ?>" alt="" title=""/>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pro-detail">
                            <h1 class="title"><?php echo $data ? $data->name : ''; ?></h1>
                            <div class="rating-star v2">
                                <?php $star = $data ? (int)($data->star * 20) : 0 ?>
                                <div class="star-base" style="width: <?php echo $star ?>%">
                                    <i></i> <i></i> <i></i> <i></i> <i></i>
                                </div>
                                <a href="#1"><i></i></a>
                                <a href="#2"><i></i></a>
                                <a href="#3"><i></i></a>
                                <a href="#4"><i></i></a>
                                <a href="#5"><i></i></a>
                            </div>
                            <div class="d-flex align-items-center show-price">
                                <div class="price"><?php echo !empty($data) && !empty($data->price_2) ? get_price($data->price_2) : get_price($data->price_1) ?></div>
                                <?php if ($data->price_2): ?>
                                    <div class="old-price"
                                         style="font-size: 16px; margin-left: 10px; color: #9c9898; margin-bottom: 8px">
                                        <del><?php echo get_price($data->price_1) ?></del>
                                    </div>
                                <?php endif; ?>
                                <?php
                                if (!empty($data->price_2)):
                                    $percent_discount = intval((($data->price_1 - $data->price_2) / $data->price_1) * 100);
                                    if (!empty($percent_discount)):
                                        ?>
                                        <strong>-<?php echo $percent_discount ?>%</strong>
                                    <?php
                                    endif;
                                endif;
                                ?>
                            </div>
                            <p><strong>Đơn vị:</strong> <?php echo !empty($data) ? $data->pice : '' ?></p>
                            <p><strong>Mã sản phẩm:</strong> <?php echo !empty($data) ? $data->code : '' ?></p>
                            <p><strong>Tình
                                    trạng:</strong> <?php echo !empty($data) && $data->is_empty == 0 ? 'Còn hàng' : 'Hết hàng' ?>
                            </p>
                            <!--                            <p><strong>Xuất xứ:</strong> -->
                            <?php //echo !empty($data) ? $data->from : '' ?><!--</p>-->
                            <!--                            <p><strong>Hãng sản xuất:</strong> -->
                            <?php //echo !empty($data) ? $data->trademark : '' ?><!--</p>-->
                            <!--                            <p><strong>Trọng lượng:</strong> -->
                            <?php //echo !empty($data) ? $data->weight : '' ?><!--</p>-->

                            <div class="desc s-content">
                                <?php if(!empty($data)):?>
                                <?php if ($data->desc !== strip_tags($data->desc, '•')):?>
                                <div class="desc-content">
                                    <?php echo $data->desc ?>
                                </div>
                                <?php else:?>
                                    <p style="white-space: pre-line"><?php echo $data->desc ?></p>
                                <?php endif;?>
                                <?php endif;?>
                            </div>
                            <div class="i-number">
                                <button class="n-ctrl down smooth" type="button"><i class="fa fa-angle-down"></i>
                                </button>
                                <input type="text" class="numberic" min="1" max="6" value="1" id="productQuanlity">
                                <button class="n-ctrl up smooth" type="button"><i class="fa fa-angle-up"></i></button>
                            </div>
                            <button class="add-to-cart smooth"
                                    onclick="addToCart(<?php echo !empty($data) ? $data->id : ''; ?>, $('#productQuanlity').val())">
                                <i class="fa fa-shopping-cart"></i> Thêm vào giỏ hàng
                            </button>

                            <?php
                            if(!empty($setting ) && !empty($setting->company_phone_3)):
                            $company_phone_3 = explode('(', $setting->company_phone_3);
                            ?>
                            <a href="tel:<?php echo $company_phone_3[0]?>" class="add-to-cart smooth text-white" id="btn-hotline-3">
                                HOTLINE <?php echo $company_phone_3[0]?><br/>
                                Đặt hàng, hỏi đáp <?php if(isset($company_phone_3[1])):?>(<?php echo $company_phone_3[1]?><?php endif;?>
                            </a>
                            <?php endif;?>
                        </div>
                        <?php if ($gifts):?>
                        <div class="mt-3 mb-1 text-success" style="font-size: 16px;color: #2dd453!important"><i class="fa fa-gift"></i> Quà tặng miễn phí</div>
                        <div class="pro-thumb-cas-gift">
                            <?php foreach ($gifts as $index => $item): ?>
                                    <?php
                                    $imgs = json_decode($item->image);
                                    $slug = $item->category_slug;
                                    $item_url = get_detail_url($slug, $item->name, $item->id);
                                    ?>
                                    <div class="slick-slide">
                                        <a href="<?php echo $item_url?>" class="c-img">
                                            <img src="<?php echo get_img_url($imgs[0]) ?>" alt="<?php echo $item->name?>" title="<?php echo $item->name?>"/>
                                        </a>
                                    </div>
                            <?php endforeach; ?>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
                <div class="pro-content pb-2">
                    <?php if (!empty($data) && !empty($data->content)): ?>
                        <p class="title">Mô tả về sản phẩm</p>
                        <div class="s-content" id="content_product">
                            <?php echo !empty($data) ? $data->content : '' ?>
                        </div>
                        <div class="text-center mb-3 pb-2 bg-color-chef" id="view_more"><a class="smooth ct-read-more"
                                                                                           href="#" title="">Xem
                                thêm</a></div>
                    <?php endif; ?>
                </div>
                <div class="pro-faq  pb-2 mt-3">
                    <?php load_mfw_module('product', 'video_related', $data->video_related) ?>
                </div>
                <div class="pro-faq  pb-2 border-bottom mt-3">
                    <?php load_mfw_module('product', 'list_product_related_other_product', $data->category_id, 4) ?>
                    <!--                    <h3 class="title">Hỏi, đáp về sản phẩm</h3>-->
                    <!--                                        <div class="form">-->
                    <!--                                            <input type="text" placeholder="Hãy đặt câu hỏi liên quan đến sản phẩm" name="">-->
                    <!--                                            <button>Gửi câu hỏi</button>-->
                    <!--                                        </div>-->
                    <!--                    -cau hoi thuong gap-->
                    <!--                    --><?php //load_mfw_module('faq', 'related', $data->id, 5) ?>
                    <!---->
                </div>
                <!---comment--->
                <div class="hc-comments">
                    <?php $optComment = ['num' => 10, 'id' => $data->id, 'type' => 'product'] ?>
                    <?php load_mfw_module('comment', 'index', $optComment) ?>
                </div>

                <!--                --><?php //$optComment = ['num' => 10, 'id' => $data->id, 'type' => 'product'] ?>
                <!--                --><?php //load_mfw_module('comment', 'comment_product', $optComment) ?>
            </div>
            <div class="col-lg-3 order-lg-first  d-none d-lg-block">
                <?php load_mfw_module('product', 'best_seller_left', $data->id, 5) ?>
                <!---tin tuc lien quan--->
                <?php load_mfw_module('news', 'related', $data->id, 5) ?>

                <!---huong dan dung san pham--->
                <?php load_mfw_module('news', 'manual', $data->id, 5) ?>

                <!----phu kien---->
                <?php load_mfw_module('product', 'list_accessories', 5) ?>


            </div>
        </div>
    </div>
    <br>
    <br class="d-none d-lg-block"><br class="d-none d-lg-block">
</main>