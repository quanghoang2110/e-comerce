<div class="md-featured">
    <p class="md-title">SẢN PHẨM nổi bật nhất</p>
    <div class="featured-box">
        <?php load_mfw_module('banner', 'index', 1, 1) ?>
        <div class="ct">
            <div class="featured-cas">
                <?php
                foreach ($list as $index => $item):
                    ?>
                    <?php
                    if ($index < 5):
                        ?>
                        <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
                        <?php $image = json_decode($item->image); ?>
                        <div class="slick-slide">
                            <div class="pro">
                                <div class="c-img">
                                    <a class="smooth" href="<?php echo $item_url; ?>" title="">
                                        <img src="<?php echo get_img_url($image[0]); ?>" alt=""
                                             title="<?php echo $item->name; ?>"/>
                                    </a>
                                    <?php if (!empty($item->price_2)):
                                        $percent_discount = intval((($item->price_1 - $item->price_2) / $item->price_1) * 100);
                                        ?>
                                        <?php if (!empty($percent_discount)): ?>
                                            <div class="sale-off"><strong>-<?php echo $percent_discount ?>%</strong>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <h3 class="title"><a class="smooth" href="<?php echo $item_url; ?>"
                                                     title="<?php echo $item->name; ?>">
                                        <?php echo $item->name; ?></a>
                                </h3>
                                <div class="d-flex align-items-center flex-wrap justify-content-between">
                                    <div class="price mr-2"><?php echo get_price($item->price_2 ? $item->price_2 : $item->price_1) ?></div>
                                    <?php if (!empty($item->price_2)): ?>
                                        <div class="old-price">
                                            <del><?php echo get_price($item->price_1) ?></del>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    endif;
                    ?>
                <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>
    <div class="featured-box">
        <?php load_mfw_module('banner', 'index', 1, 2) ?>
        <div class="ct">
            <div class="featured-cas">
                <?php if (count($list) > 5): ?>
                    <?php
                    foreach ($list as $index => $item):
                        ?>
                        <?php
                        if ($index >= 5):
                            ?>
                            <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
                            <?php $image = json_decode($item->image); ?>
                            <div class="slick-slide">
                                <div class="pro">
                                    <div class="c-img">
                                        <a class="smooth" href="<?php echo $item_url; ?>" title="">
                                            <img src="<?php echo get_img_url($image[0]); ?>" alt=""
                                                 title="<?php echo $item->name; ?>"/>
                                        </a>
                                        <?php if (!empty($item->price_2)):
                                            $percent_discount = intval((($item->price_1 - $item->price_2) / $item->price_1)*100);
                                            ?>
                                            <?php if (!empty($percent_discount)): ?>
                                            <div class="sale-off"><strong>-<?php echo $percent_discount ?>%</strong>
                                            </div>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <h3 class="title"><a class="smooth" href="<?php echo $item_url; ?>"
                                                         title="<?php echo $item->name; ?>">
                                            <?php echo $item->name; ?></a>
                                    </h3>
                                    <div class="d-flex align-items-center flex-wrap justify-content-between">
                                        <div class="price mr-2"><?php echo get_price($item->price_2 ? $item->price_2 : $item->price_1) ?></div>
                                        <?php if (!empty($item->price_2)): ?>
                                            <div class="old-price">
                                                <del><?php echo get_price($item->price_1) ?></del>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>