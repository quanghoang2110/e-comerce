<?php
$imgs = json_decode($item->image);
$url = get_detail_url($item->category_slug, $item->name, $item->id);
?>
<div class="mfw-product-item">
    <!-- Card image -->
    <div class="view overlay">
        <?php echo get_img_tag($imgs[0], $item->name, '"card-img-top') ?>
        <a href="<?php echo $url ?>" title="<?php echo $item->name ?>">
            <div class="mask rgba-white-slight">
            </div>
        </a>
        <div class="mfw-add-to-cart mfw-flex-center mfwAddToCart" product-id="<?php echo $item->id ?>"><i
                    class="fa fa-shopping-cart"></i> Thêm vào giỏ hàng
        </div>
    </div>
    <!-- Card image -->
    <!-- Card content -->
    <div class="text-left">
        <!-- Category & Title -->
        <a href="<?php echo $url ?>" title="<?php echo $item->name ?>" class="grey-text">
            <h2 class="h3"><?php echo $item->name ?></h2>
        </a>
        <div class="mfw-price">
            <?php if ($item->price_2): ?>
                <span class="mfw-sale"><?php echo get_price($item->price_2) ?></span>
                <?php if ($item->price_1): ?>
                    <span class="mfw-raw"><?php echo get_price($item->price_1) ?></span>
                <?php endif ?>
            <?php else: ?>
                <span class="mfw-sale"><?php echo get_price($item->price_2) ?></span>
            <?php endif; ?>
        </div>
    </div>
</div>