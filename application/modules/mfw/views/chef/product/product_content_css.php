<link rel="stylesheet"
      href="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>lib/owl.carousel/owl.carousel.min.css">
<link rel="stylesheet"
      href="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>lib/owl.carousel/owl.theme.default.min.css">
<link rel="stylesheet"
      href="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>lib/smooth.product/smoothproducts.css">
<?php load_view_exists('comment/comment_css') ?>
<?php //include_once '../comment_css.php'; ?>
<style>
    .sp-wrap {
        max-width: 100%;
        margin: 0;
        background: transparent;
        border: none;
    }

    .sp-wrap .sp-large {
        border: 1px solid var(--border-color);
    }

    .sp-thumbs {
        margin-top: 0.5rem;
    }

    .sp-thumbs a {
        margin-left: 5px;
        margin-right: 5px;
        border: 1px solid var(--border-color);
    }

    .sp-thumbs a:first-child {
        margin-left: 0;
    }

    .sp-thumbs a:last-child {
        margin-right: 0;
    }

    .sp-lightbox {
        z-index: 9999;
    }

    .mfw-product-content .mfw-title h1 {
        /*font-size: 1.5rem;*/
    }

    .mfw-product-content .mfw-sale {
        font-size: 1.2rem;
    }

    .mfw-product-content .mfw-raw {
        font-size: 1.15rem;
    }

    .mfw-discount {
        background: #d60c0c;
        padding: 0.1rem 0.7rem;
        color: white;
        font-size: 0.9rem;
        margin-left: 1rem;
    }

    .mfw-news-title label {
        font-size: 1.1rem;
        font-weight: 500;
        margin-bottom: 0;
        border-bottom: 1px solid var(--price-color);
    }

    .product-desc {
        border-top: 1px solid var(--border-color);
        border-bottom: 1px solid var(--border-color);
    }

    .product-desc span {
        white-space: pre-line;
    }

    .product-content {
        text-align: justify;
    }

    .product-content img {
        max-width: 100%;
        height: auto;
    }

    .product-add-cart .quantity {
        width: 60px;
        display: inline-block;
        float: left;
        overflow: hidden;
    }

    .product-add-cart .quantity input {
        height: 44px;
        border: none;
        max-width: 50px;
        padding: 0 10px;
        text-align: center;
    }

    .product-add-cart .quantity input:focus,
    .product-add-cart .quantity input:active,
    .product-add-cart .quantity input:visited {
        outline: 0;
    }

    .product-add-cart .item-num {
        border: 1px solid #eaeaea;
        width: 90px;
        /*margin-left: 50px;*/
        overflow: hidden;
        background: #FAFAFA;
        float: left;
    }

    .product-add-cart .btn-plus {
        width: 18px;
        height: 44px;
        float: left;
        display: inline-block;
    }

    .product-add-cart .btn-plus a {
        width: 18px;
        height: 22px;
        display: flex;
        justify-content: center;
        align-items: flex-end;
    }

    .product-add-cart .btn-plus i {
        margin-top: -10px !important;
        color: #1d1d1d;
    }

    h1, h2, h3, h4 {
        font-size: 1.3rem;
    }
    .h3{
        color: var(--text-color);
        font-size: 1rem;
        font-weight: 500;
        margin-top: 0.3rem;
        height: 40px;
    }
    #headerPopup{
        width:75%;
        margin:0 auto;
    }

    #headerPopup iframe{
        width:100%;
        margin:0 auto;
    }
</style>
