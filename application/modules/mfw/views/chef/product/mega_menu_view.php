<div class="mfw-mega-menu dropdown-menu mega-menu p-0"
     aria-labelledby="navbarDropdownMenuLink1">
    <div class="col-12 my-lg-2" id="mfw-container-menu">
        <div class="row mfw-product-category">
            <?php foreach ($list as $item): ?>
                <?php $item_url = base_url($item->slug) ?>
                <div class="col-6 col-md-4 col-lg-2 sub-menu mb-xl-0 mb-lg-4 p-2">
                    <a href="<?php echo $item_url ?>" class="mega-item view overlay z-depth-1 p-0">
                        <div class="cate-img" style="background-image: url(<?php echo get_img_url($item->image) ?>)">
                        </div>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                    <h4 class="mb-lg-3">
                        <a class="news-title pl-0 pr-0 text-center"
                           href="<?php echo $item_url ?>"><?php echo $item->name ?></a>
                    </h4>
                    <div class="list d-none d-lg-block d-xl-block">
                        <?php foreach ($item->products as $p): ?>
                            <?php
                            $product_url = get_detail_url($item->slug, $p->name, $p->id);
                            $imgs = json_decode($p->image);
                            ?>
                            <div class="mega-product-item row mb-2">
                                <div class="col-5 pr-0">
                                    <a href="<?php echo $product_url ?>" title="<?php echo $p->name ?>">
                                        <?php echo get_img_tag($imgs[0], $p->name); ?>
                                    </a>
                                </div>
                                <div class="col-7">
                                    <a href="<?php echo $product_url ?>" title="<?php echo $p->name ?>">
                                        <span class="product-name"><?php echo $p->name ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>