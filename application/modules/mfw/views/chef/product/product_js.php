<script>
    function filter(base_url, keyword) {
        var trademark = '';
        var size = '';
        var price = '';
        $('.checkbox_filter_trademark').each(function () {
            if ($(this).is(':checked')) {
                trademark = trademark + $(this).val() + ',';
            }
        });
        trademark = trademark.slice(0, -1);
        $('.checkbox_size').each(function () {
            if ($(this).is(':checked')) {
                size = size + $(this).val() + ',';
            }
        });
        size = size.slice(0, -1);

        $('.checkbox_price').each(function () {
            if ($(this).is(':checked')) {
                price = price + $(this).val() + ',';
            }
        });
        price = price.slice(0, -1);
        if(base_url !== 'tim-kiem') {
            window.location.href = base_url + '?brands=' + trademark + '&size=' + size + '&price=' + price;
        }else{
            window.location.href = base_url + '?search='+keyword+'&brands=' + trademark + '&size=' + size + '&price=' + price;
        }

    }

    function showViewMore() {
        var offsetHeight = document.getElementById('content_product').offsetHeight;
        var obj = $('.ct-read-more');
        if(docWidth > 767){
            if (offsetHeight < 300) {
                obj.parent().prev('.s-content').toggleClass('active');
                $('#view_more').remove();
            } else {
                $(this).parent().prev('.s-content').toggleClass();
                $(this).parent().prev('.s-content').hasClass('active') ? $(this).text('Rút gọn') : $(this).text('Xem thêm');
                $(this).parent().prev('.s-content').hasClass('active') ? $('#view_more').removeClass('bg-color-chef') : $('#view_more').addClass('bg-color-chef');
            }
        }else{
            if (offsetHeight < 100) {
                obj.parent().prev('.s-content').toggleClass('active');
                $('#view_more').remove();
            } else {
                $(this).parent().prev('.s-content').toggleClass();
                $(this).parent().prev('.s-content').hasClass('active') ? $(this).text('Rút gọn') : $(this).text('Xem thêm');
                $(this).parent().prev('.s-content').hasClass('active') ? $('#view_more').removeClass('bg-color-chef') : $('#view_more').addClass('bg-color-chef');
            }
        }
    }

    $(function () {
        showViewMore();
    });

    const formSort = $('#mfw-form-sort');

    function changeSort(sort_by, per_size) {
        $('#input_sort_by').val(sort_by);
        $('#input_limit').val(per_size);
        var trademark = '';
        var size = '';
        var price = '';
        var prod_type = '';

        $('#input_per_page').val(1);

        $('.checkbox_filter_trademark').each(function () {
            if ($(this).is(':checked')) {
                trademark = trademark + $(this).val() + ',';
            }
        });
        trademark = trademark.slice(0, -1);

        if(trademark != ''){
            $('#input_brands').val(trademark);
            $('#input_brands').attr('name', 'brands');
        }else{
            $('#input_brands').val('');
            $('#input_brands').attr('name', '');
        }

        $('.checkbox_size').each(function () {
            if ($(this).is(':checked')) {
                size = size + $(this).val() + ',';
            }
        });

        // size sản pham
        size = size.slice(0, -1);
        if(size != ''){
            $('#input_size').val(size);
            $('#input_size').attr('name', 'size');
        }else{
            $('#input_size').val('');
            $('#input_size').attr('name', '');
        }

        // dong san pham
        $('.checkbox_filter_brand').each(function () {
            if ($(this).is(':checked')) {
                prod_type = prod_type + $(this).val() + ',';
            }
        });
        prod_type = prod_type.slice(0, -1);
        if(prod_type != ''){
            $('#input_prod_type').val(prod_type);
            $('#input_prod_type').attr('name', 'prod_type');
        }else{
            $('#input_prod_type').val('');
            $('#input_prod_type').attr('name', '');
        }

        $('.checkbox_price').each(function () {
            if ($(this).is(':checked')) {
                price = price + $(this).val() + ',';
            }
        });
        price = price.slice(0, -1);
        if(price != ''){
            $('#input_price').val(price);
            $('#input_price').attr('name', 'price');
        }else{
            $('#input_price').val('');
            $('#input_price').attr('name', '');
        }

        formSort.submit();
    }

    function changeBrands(obj, slug_cate, slug_brands){
        if(obj.is(':checked')) {
            window.location.href = '/' + slug_cate + '/' + slug_brands;
        }else{
            window.location.href = '/' + slug_cate;
        }
    }

    function changeDisplayType(type) {
        const gridObj = $('.mfw-grid ');
        const listObj = $('.mfw-list ');
        const gridBtnObj = $('.btn-grid ');
        const listBtnObj = $('.btn-list ');
        if (type === 'grid') {
            gridObj.removeClass('d-none');
            listObj.addClass('d-none');
            gridBtnObj.addClass('active');
            listBtnObj.removeClass('active');
        } else {
            gridObj.addClass('d-none');
            listObj.removeClass('d-none');
            gridBtnObj.removeClass('active');
            listBtnObj.addClass('active');
        }
        $('#input_show_by').val(type);
    }

    $(document).ready(function () {
        $('.read-more a').click(function () {
            $('.product-cate-desc').addClass('product-cate-desc-full');
            $('.product-desc .read-more').addClass('mfw-hidden');
            $('.product-desc .info-collapse').removeClass('mfw-hidden');
        });
        $('.info-collapse a').click(function () {
            $('.product-cate-desc').removeClass('product-cate-desc-full');
            $('.product-desc .read-more').removeClass('mfw-hidden');
            $('.product-desc .info-collapse').addClass('mfw-hidden');
        });
    });

    function menu_mobi(obj){
        var data_title = obj.attr('data-title');
        if(data_title == 1){
            obj.closest('.pro-sidebar').find('.ct').addClass('d-block');
            obj.attr('data-title', 2);
            obj.find('i.fa-angle-down').css('transform', 'rotate(180deg)');
        }else{
            obj.closest('.pro-sidebar').find('.ct').removeClass('d-block');
            obj.attr('data-title', 1);
            obj.find('i.fa-angle-down').css('transform', 'rotate(0)');
        }
    }

</script>