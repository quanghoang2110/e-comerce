<?php
$show = $this->input->get('show_by');
$grid_active = !$show || $show == 'grid' ? 'active' : '';
$list_active = $show && $show == 'list' ? 'active' : '';
$sort_by = $this->input->get('sort_by');
?>
<form id="mfw-form-sort">
    <input type="hidden" name="sort_by" id="input_sort_by"/>
    <input type="hidden" name="show_by" id="input_show_by" value="<?php echo $list_active ? 'list' : 'grid' ?>"/>
    <input type="hidden" name="per_page" value="<?php echo $this->input->get('per_page') ?>"/>
</form>
<main>
    <?php load_mfw_module('breadcrumb', 'index', 'Khuyến mại') ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="page-bar">
                    <div class="row justify-content-between align-items-center">
                        <?php load_mfw_module("pagination", "index", $paging_url, $total, $limit) ?>
                    </div>
                </div>

                <div class="row col-mar-5">
                    <?php foreach ($list as $item): ?>
                        <?php
                        $imgs = json_decode($item->image);
                        $slug = $item->category_slug;
                        $item_url = get_detail_url($slug, $item->name, $item->id);
                        ?>
                        <div class="col-xl-3 col-md-4 col-6">
                            <div class="pro v2">
                                <div class="c-img">
                                    <a class="smooth" href="<?php echo $item_url ?>" title=""<?php echo $item->name ?>">
                                    <img src="<?php echo get_img_url($imgs[0]) ?>" alt="Not Image"
                                         title="<?php echo $item->name ?>"/>
                                    </a>
                                </div>
                                <h3 class="title"><a class="smooth" href="<?php echo $item_url ?>"
                                                     title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
                                </h3>
                                <div class="price">
                                    <?php echo !empty($item->price_2) ? get_price($item->price_2) : get_price($item->price_1) ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="break-10"></div>
                <div class="row justify-content-between align-items-center">
                    <?php load_mfw_module("pagination", "index", $paging_url, $total, $limit) ?>
                </div>
                <br><br>
            </div>
            <div class="col-lg-3 order-lg-first">
                <div class="filter-sidebar">
                    <div class="pro-sidebar">
                        <h3 class="title v2">Danh mục sản phẩm</h3>
                        <div class="ct">
                            <ul>
                                <li><a class="smooth" href="#" title="">Inox</a></li>
                                <li><a class="smooth" href="#" title="">Gang</a></li>
                                <li><a class="smooth" href="#" title="">Thép carbon</a></li>
                                <li><a class="smooth" href="#" title="">Đá</a></li>
                                <li><a class="smooth" href="#" title="">Gang</a></li>
                                <li><a class="smooth" href="#" title="">Thép carbon</a></li>
                                <li><a class="smooth" href="#" title="">Đá</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="pro-sidebar">
                        <h3 class="title">Chất liệu</h3>
                        <div class="ct">
                            <label class="i-check"><input type="checkbox" name=""><i></i> Inox</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Gang</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Thép carbon</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Đá</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Gang</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Thép carbon</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Đá</label>
                        </div>
                    </div>
                    <div class="pro-sidebar">
                        <h3 class="title">Thương hiệu</h3>
                        <div class="ct">
                            <label class="i-check"><input type="checkbox" name=""><i></i> Boss</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Fargo</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Zwilling</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Boss</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Fargo</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Zwilling</label>
                        </div>
                    </div>
                    <div class="pro-sidebar">
                        <h3 class="title">Giá</h3>
                        <div class="ct">
                            <label class="i-check"><input type="checkbox" name=""><i></i> 0 - 100,000đ</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> 100.000 ₫ - 200.000 ₫</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> 200.000 ₫ - 300.000 ₫</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> 300.000 ₫ - 400.000 ₫</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> 400.000 ₫ - 500.000 ₫</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> 500.000 ₫ - 600.000 ₫</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> 600.000 ₫ - 700.000 ₫</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> 700.000 ₫ - 800.000 ₫</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> 900.000 ₫ - Nhiều hơn</label>
                        </div>
                    </div>
                    <div class="pro-sidebar">
                        <h3 class="title">Size</h3>
                        <div class="ct">
                            <label class="i-check"><input type="checkbox" name=""><i></i> Nhỏ</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Trung bình</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Lớn</label>
                        </div>
                    </div>
                    <div class="pro-sidebar">
                        <h3 class="title">Khuyến mại</h3>
                        <div class="ct">
                            <label class="i-check"><input type="checkbox" name=""><i></i> Chảo</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Nồi</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Khay và bếp</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Bếp</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Dao</label>
                            <label class="i-check"><input type="checkbox" name=""><i></i> Kéo, dĩa</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>