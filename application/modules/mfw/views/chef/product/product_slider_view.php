<!-- Section: Products v.5 -->
<section class="text-center">
    <!-- Set up your HTML -->
    <div class="owl-carousel">
        <?php foreach ($list as $item): ?>
            <?php include 'item.php' ?>
        <?php endforeach; ?>
    </div>
</section>
<!-- Section: Products v.5 -->