<div class="md-products">
    <p class="md-title">SẢN PHẨM BÁN CHẠY</p>
    <div class="pro-cas">
        <?php foreach ($list as $item): ?>
            <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
            <?php $imgs = json_decode($item->image) ?>
            <div class="slick-slide">
                <div class="pro">
                    <div class="c-img">
                        <a class="smooth" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                            <img src="<?php echo get_img_url($imgs[0]) ?>" alt="" title="<?php echo $item->name ?>"/>
                        </a>
                        <?php if (!empty($item->price_2)):
                            $percent_discount = intval((($item->price_1 - $item->price_2) / $item->price_1)*100);
                            ?>
                            <?php if (!empty($percent_discount)): ?>
                            <div class="sale-off"><strong>-<?php echo $percent_discount ?>%</strong>
                            </div>
                        <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <h3 class="title"><a class="smooth" href="<?php echo $item_url ?>"
                                         title="<?php echo $item->name ?>"><?php echo $item->name ?></a></h3>
                    <div class="d-flex align-items-center flex-wrap justify-content-between">
                        <div class="price mr-2"><?php echo get_price($item->price_2 ? $item->price_2 : $item->price_1) ?></div>
                        <?php if ($item->price_2): ?>
                            <div class="old-price">
                                <del><?php echo get_price($item->price_1) ?></del>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>