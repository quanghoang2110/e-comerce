<?php if (count($list) > 0):?>
<div class="md-products" id="product_video_related">
    <p class="md-title text-left title-video-relate mt-4 mb-3 pb-2 border-bottom">Video sản phẩm</p>
    <div class="pro-cas" style="padding: 0 1.6rem">
        <?php foreach ($list as $item): ?>
            <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
            <div class="slick-slide">
                <div class="pro">
                    <div class="c-img" style="padding-top: 68%">
                        <a class="smooth" data-fancybox href="<?php echo $item->video_link;?>" title="<?php echo $item->name ?>">
                            <img src="<?php echo $item->image ?>" alt="" title="<?php echo $item->name ?>"/>
                            <?php if ($item->type == 'VIDEO'): ?>
                                <div class="flex-center mfw-absolute-full d-flex justify-content-center align-items-center">
                                    <i class="fa fa-play-circle-o play" aria-hidden="true"></i>
                                </div>
                            <?php endif; ?>
                        </a>
                    </div>
                    <h3 class="title"><a style="font-size: 14px" class="smooth font-weight-normal" href="<?php echo $item_url ?>"
                                         title="<?php echo $item->name ?>"><?php echo $item->name ?></a></h3>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif;?>