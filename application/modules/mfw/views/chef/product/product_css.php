<style>
    .mfw-select {
        display: block !important;
        float: left;
        background: #fff;
        padding: 0 15px;
        height: 31.8px;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        font-size: 13px;
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        border: none;
    }

    .mfw-select:focus,
    .mfw-select:visited {
        outline: none;
    }

    .vertical-center {
        display: flex;
        align-items: center;
    }

    .mfw-product-title {
        background: #ededed;
        padding: 0.5rem;
        margin-left: -0.5rem;
        /* overflow: hidden; */
        margin-right: -0.5rem;
    }

    .lis-grid .btn.btn-flat {
        background: #ababab;
        padding-bottom: 0.3rem !important;
    }

    .lis-grid .btn.btn-flat.active {
        background: var(--main-bg-color);
        box-shadow: none;
    }

    .lis-grid i.fa {
        font-size: 0.9rem;
        color: #fff;
    }

    .filter {
        border: none;
        border-right: 1px solid #ededed;
    }

    .filter i.fa {
        color: #000;
        border;
    }

    .mfw-list {
        margin-left: -0.5rem;
        margin-right: -0.5rem;
    }

    .mfw-list .mfw-product-item {
        background: transparent;
        padding: 0;
        border: none;
    }
    .mfw-product-item h3{
        overflow: hidden;
    }
    .mfw-product-item h2{
        color: var(--text-color);
        font-size: 1rem;
        font-weight: 500;
        margin-top: 0.3rem;
        height: 40px;
    }

    .title-news {
        font-size: 28px;
    }

    @media all and (max-width: 767px) {
        .title-news {
            font-size: 24px;
        }
    }
</style>