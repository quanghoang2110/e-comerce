<?php if (count($list)): ?>
    <div class="sidebar">
        <h3 class="sb-title">Phụ kiện</h3>
        <?php foreach ($list as $item): ?>
            <?php
            $imgs = json_decode($item->image);
            $slug = $item->category_slug;
            $item_url = get_detail_url($slug, $item->name, $item->id);
            ?>
            <div class="sb-pro">
                <a class="img hv-over" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                    <img src="<?php echo get_img_url($imgs[0]) ?>" alt="" title="<?php echo $item->name ?>"/>
                </a>
                <div class="ct">
                    <h3 class="title"><a class="smooth" href="<?php echo $item_url ?>"
                                         title=""><?php echo $item->name ?></a></h3>
                    <div class="price">
                        <strong><?php if ($item->price_2): echo get_price($item->price_2); else: echo get_price($item->price_1); endif; ?></strong>

                        <?php if ($item->price_2): ?>
                            <del>
                                <?php echo get_price($item->price_1); ?>
                            </del>
                        <?php endif; ?>
                    </div>
                    <?php
                    if (!empty($item->price_2)):
                        $percent_discount = intval((($item->price_1 - $item->price_2)/$item->price_1) * 100);
                        if ($percent_discount):
                            ?>
                            <div class="label">- <?php echo $percent_discount ?>%</div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>