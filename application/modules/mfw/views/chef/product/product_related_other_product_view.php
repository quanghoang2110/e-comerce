<?php if(!empty($list)): ?>
<div class="post-product mt-3">
    <h3 class="i-title">Sản phẩm cùng loại</h3>
    <div class="row col-mar-5">
        <?php foreach ($list as $item): ?>
        <?php
        $imgs = json_decode($item->image);
        $slug = $item->category_slug;
        $item_url = get_detail_url($slug, $item->name, $item->id);
        ?>
        <div class="col-xl-3 col-md-4 col-6">
            <div class="pro v2 bg-white">
                <div class="c-img">
                    <a class="smooth" href="<?php echo $item_url ?>" title="">
                        <img src="<?php echo get_img_url($imgs[0]);?>" alt="Not Image" title="<?php echo $item->name ?>"/>
                    </a>
                    <?php if (!empty($item->price_2)):
                        $percent_discount = intval((($item->price_1 - $item->price_2) / $item->price_1)*100);
                        ?>
                        <?php if (!empty($percent_discount)): ?>
                        <div class="sale-off"><strong>-<?php echo $percent_discount ?>%</strong>
                        </div>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
                <h5 class="title"><a class="smooth" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
                </h5>
                <div class="d-flex align-items-center flex-wrap">
                    <div class="price mr-2">
                        <?php echo !empty($item->price_2) ? get_price($item->price_2) : get_price($item->price_1) ?>
                    </div>
                    <?php if ($item->price_2): ?>
                        <div class="old-price">
                            <del><?php echo get_price($item->price_1) ?></del>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</div>
<?php endif;?>