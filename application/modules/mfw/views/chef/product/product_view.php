<?php
$show = $this->input->get('show_by');
$grid_active = !$show || $show == 'grid' ? 'active' : '';
$list_active = $show && $show == 'list' ? 'active' : '';
$sort_by = $this->input->get('sort_by');
?>
<form id="mfw-form-sort">
    <input type="hidden" name="<?php if (!empty($this->input->get('search'))): ?>search<?php endif; ?>" value="<?php echo $this->input->get('search') ?>"/>
    <input type="hidden" name="sort_by" id="input_sort_by"/>
    <input type="hidden" id="input_per_page" name="<?php if (!empty($this->input->get('per_page'))): ?>per_page<?php endif; ?>" value="<?php echo $this->input->get('per_page') ?>"/>
    <?php if(!empty($current_url) && $current_url == 'tim-kiem'):?>
    <input type="hidden" id="input_brands" name="<?php if (!empty($this->input->get('trademark'))): ?>brands<?php endif; ?>" value="<?php echo $this->input->get('trademark') ?>"/>
    <?php endif ?>
    <input type="hidden" id="input_size" name="<?php if (!empty($this->input->get('size'))): ?>size<?php endif; ?>" value="<?php echo $this->input->get('size') ?>"/>
    <input type="hidden" id="input_price" name="<?php if (!empty($this->input->get('price'))): ?>price<?php endif; ?>" value="<?php echo $this->input->get('price') ?>"/>
    <input type="hidden" id="input_prod_type" name="<?php if (!empty($this->input->get('prod_type'))): ?>prod_type<?php endif; ?>" value="<?php echo $this->input->get('prod_type') ?>"/>
    <input type="hidden" name="limit" id="input_limit"
           value="<?php echo empty($this->input->get('limit')) ? 12 : $this->input->get('limit') ?>"/>
</form>
<main>
    <?php load_mfw_module('breadcrumb', 'index', $category->name) ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="page-bar">
                    <div class="row" style="background: #ededed;">
                        <div class="col-md-7">
                            <?php if (!empty($category) && $category->name): ?>
                                <h1 class="title-news" style="font-weight: 500"><?php echo $category->name ?></h1>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-5 pb-2 pb-md-0">
                            <div class="btn-group float-right" style="padding-top: .3rem!important;">
                                <button class="filter" style="background: #c7c5c5; padding: 5px;"><i
                                            class="fa fa-filter"></i></button>
                                <select class="mfw-select mr-2" id="sort_by" title=""
                                        onchange="changeSort($(this).val(), $('#per_size option:selected').val())">
                                    <option value="">-- Lọc theo --</option>
                                    <option value="ban-chay"
                                        <?php echo $sort_by == 'ban-chay' ? 'selected' : '' ?>>Bán chạy
                                    </option>
                                    <option value="gia-cao-thap"
                                        <?php echo $sort_by == 'gia-cao-thap' ? 'selected' : '' ?>>Giá cao → thấp
                                    </option>
                                    <option value="gia-thap-cao"
                                        <?php echo $sort_by == 'gia-thap-cao' ? 'selected' : '' ?>>Giá thấp → cao
                                    </option>
                                </select>
                                <select class="mfw-select" id="per_size" title=""
                                        onchange="changeSort($('#sort_by option:selected').val(), $(this).val())">
                                    <option value="12"
                                        <?php echo $limit == 12 ? 'selected' : '' ?>>12 sản phẩm
                                    </option>
                                    <option value="20"
                                        <?php echo $limit == 20 ? 'selected' : '' ?>>20 sản phẩm
                                    </option>
                                    <option value="32"
                                        <?php echo $limit == 32 ? 'selected' : '' ?>>32 sản phẩm
                                    </option>
                                    <option value="40"
                                        <?php echo $limit == 40 ? 'selected' : '' ?>>40 sản phẩm
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($category->desc): ?>
                <div class="pro-content my-4">
                    <div class="s-content" id="content_product">
                        <?php if ($category->desc): ?>
                            <?php echo $category->desc; ?>
                        <?php endif; ?>
                    </div>
                    <?php if ($category->desc): ?>
                        <div class="text-center mb-3 pb-2 bg-color-chef" id="view_more">
                            <a class="smooth ct-read-more" href="#" title="">Xem thêm</a>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endif ?>
                <div class="row col-mar-5">
                    <?php foreach ($list as $item): ?>
                        <?php
                        $imgs = json_decode($item->image);
                        $slug = $item->category_slug;
                        $item_url = get_detail_url($slug, $item->name, $item->id);
                        ?>
                        <div class="col-xl-3 col-md-4 col-6">
                            <div class="pro v2 bg-white px-2">
                                <div class="c-img">
                                    <a class="smooth" href="<?php echo $item_url ?>" title=""<?php echo $item->name ?>">
                                    <img src="<?php echo get_img_url($imgs[0]) ?>" alt="Not Image"
                                         title="<?php echo $item->name ?>"/>
                                    </a>
                                    <?php if (!empty($item->price_2)):
                                        $percent_discount = intval((($item->price_1 - $item->price_2) / $item->price_1) * 100);
                                        ?>
                                        <?php if (!empty($percent_discount)): ?>
                                        <div class="sale-off"><strong>-<?php echo $percent_discount ?>%</strong>
                                        </div>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <h2 class="title"><a class="smooth" href="<?php echo $item_url ?>"
                                                     title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
                                </h2>
                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <div class="price mr-2">
                                        <?php echo !empty($item->price_2) ? get_price($item->price_2) : get_price($item->price_1) ?>
                                    </div>
                                    <?php if ($item->price_2): ?>
                                        <div class="old-price">
                                            <del><?php echo get_price($item->price_1) ?></del>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="break-10"></div>
                <div class="col-12">
                    <div class="row justify-content-between align-items-center">
                        <?php load_mfw_module("pagination", "index", $paging_url, $total, $limit) ?>
                    </div>
                </div>
                <br>
            </div>
            <div class="col-lg-3 order-first">
                <div class="filter-sidebar">
                    <?php load_mfw_module('product_category', 'children_1', $category->id, 20) ?>

                    <?php load_mfw_module('trademark', 'menu_left_1', $category->id, $brands_id, 20) ?>

                    <?php load_mfw_module('brand', 'menu_left_product', $category->id, $prod_type, $brands_id, 20) ?>

                    <?php load_mfw_module('price', 'some_1', 10) ?>
                </div>
            </div>
        </div>
    </div>
</main>