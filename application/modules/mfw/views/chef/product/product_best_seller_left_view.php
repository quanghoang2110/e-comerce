<section class="wow fadeIn mb-3">
    <div class="mfw-header-title mb-2">
        <h3 class="h3">Sản phẩm bán chạy</h3>
    </div>
    <!--Grid row-->
    <div class="row wow fadeIn">
        <div class="col-md-12">
            <ul>
                <?php foreach ($list as $item): ?>
                    <?php $item_url = get_detail_url($item->category_slug, $item->name, $item->id); ?>
                    <?php $imgs = json_decode($item->image) ?>
                    <div class="sb-pro mb-3 pb-3 border-bottom">
                        <a class="img hv-over" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>">
                            <img src="<?php echo get_img_url($imgs[0]) ?>" alt="" title="<?php echo $item->name ?>"/>
                        </a>
                        <div class="ct">
                            <h3 class="title"><a class="smooth" href="<?php echo $item_url ?>"
                                                 title=""><?php echo $item->name ?></a></h3>
                            <div class="price">
                                <strong><?php if ($item->price_2): echo get_price($item->price_2); else: echo get_price($item->price_1); endif; ?></strong>

                                <?php if ($item->price_2): ?>
                                    <del>
                                        <?php echo get_price($item->price_1); ?>
                                    </del>
                                <?php endif; ?>
                            </div>
                            <?php if (!empty($item->price_2)):
                                $percent_discount = intval((($item->price_1 - $item->price_2)/$item->price_1) * 100);
                                ?>
                                <?php if ($percent_discount): ?>
                                    <div class="label">- <?php echo $percent_discount ?>%</div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </ul>
            <!--/Grid column-->
        </div>
    </div>
    <!--/Grid row-->
</section>