<?php if(!empty($list)): ?>
<div class="post-product">
    <h3 class="i-title">Sản phẩm liên quan</h3>
    <div class="row col-mar-5">
        <?php foreach ($list as $item): ?>
        <?php
        $imgs = json_decode($item->image);
        $slug = $item->category_slug;
        $item_url = get_detail_url($slug, $item->name, $item->id);
        ?>
        <div class="col-xl-3 col-md-4 col-6">
            <div class="pro v2">
                <div class="c-img">
                    <a class="smooth" href="" title="">
                        <img src="<?php echo get_img_url($imgs[0]);?>" alt="Not Image" title="<?php echo $item->name ?>"/>
                    </a>
                </div>
                <h2 class="title"><a class="smooth" href="<?php echo $item_url ?>" title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
                </h2>
                <div class="price"><?php echo $item->price_2 ? get_price($item->price_2) : get_price($item->price_1) ?></div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</div>
<?php endif;?>