<link href="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER); ?>lib/fancybox/jquery.fancybox.min.css" rel="stylesheet">

<link rel="stylesheet"
      href="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER); ?>lib/owl.carousel/owl.carousel.min.css">
<link rel="stylesheet"
      href="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER); ?>lib/owl.carousel/owl.theme.default.min.css">

<style>
    main {
        background-color: white;
        background-position: right bottom, left top;
        background-size: contain, contain;
        background-repeat: no-repeat;
    }
    @media screen and (min-width: 1200px) {
        .navbar-brand img.logo {
            height: 70px;
        }

        .top-nav-collapse .navbar-brand img.logo {
            height: 54px;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            -ms-transition: all 0.5s;
            -o-transition: all 0.5s;
            transition: all 0.5s;
        }
    }
    .mfw-full-bg{
        background-position: left center;
    }
    .owl-carousel-slider .owl-dots{
        position: absolute;
        bottom:0;
        left: 0;
        right: 0;
    }
    .owl-carousel-slider .owl-dots .owl-dot.active span, .owl-carousel-slider .owl-dots .owl-dot:hover span {
        background: #869791;
    }
    .slider-item{
        position: relative
    }
    .slider-inner{
        position: absolute;
        top: 20px;
        right: 0;
        bottom: 0;
        left: 0
    }
    .slider-full-width{
        width: 100%;
        height: auto;
    }
    .slider-full-height{
        width: auto!important;
        height: 100%;
        max-width: unset!important;
    }
    .owl-nav .owl-prev, .owl-nav .owl-next{
        position: absolute;
        font-size: 3rem!important;
        color: white!important;
    }
    .owl-nav .owl-prev{
        left: -5px;
    }
    .owl-nav .owl-next{
        right: -5px;
    }
    .owl-carousel-video .owl-dots{
        display: none;
    }
    @media screen and (min-width: 992px) {
        .slider-inner{
            top: 102px;
        }
    }
    @media screen and (min-width: 1200px) {
        .slider-inner{
            top: 124px;
        }
    }
    @media (max-width: 768px){
        .mfw-slider-title{
            font-size: 1.2rem;
        }
        .owl-carousel-slider .mfw-btn{
            font-size: 1rem;
        }
        .owl-carousel-slider{
            height: 30%;
        }
        .owl-carousel-slider .owl-stage-outer,
        .owl-carousel-slider .owl-stage,
        .owl-carousel-slider .owl-item,
        .owl-carousel-slider .slider-item{
            height:100%;
        }
        .slider-inner{
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            display: flex;
            align-items: center;
        }
    }

    #myPopup .modal-dialog {
        -webkit-transform: translate(0,-50%);
        -o-transform: translate(0,-50%);
        transform: translate(0,-50%);
        top: 50%;
        margin: 0 auto;
    }
</style>