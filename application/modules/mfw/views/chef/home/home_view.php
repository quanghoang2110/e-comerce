<!--Main layout-->
<main class="pt-3">
    <!---Loại sản phẩm nổi bật--->
    <?php load_mfw_module('banner', 'home', 7); ?>

    <!---Sản phẩm bán chạy--->
    <?php load_mfw_module('product', 'best_seller', 10); ?>

    <br><br class="d-none d-md-block">

    <!---Chương trinh khuyến mãi--->
    <?php load_mfw_module('news', 'news_hot_discount', 10); ?>

    <!---Tin tức--->
    <?php load_mfw_module('news', 'hot', 10); ?>

</main>
<?php if(!empty($popup)): ?>
<div id="myPopup" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <button style="font-size: 34px; color: white" type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content">
            <div class="modal-body p-0">
                <a href="<?php echo $popup->link ?>">
                    <img src="<?php echo get_img_url($popup->image) ?>" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<!--Main layout-->
