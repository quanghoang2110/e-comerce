<script src="<?php echo get_asset_url(PUBLIC_ASSETS_FOLDER) ?>lib/owl.carousel/owl.carousel.min.js"></script>
<script type="application/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                420: {
                    items: 2,
                    nav: false
                },
                768: {
                    items: 3,
                    nav: false
                },
                992: {
                    items: 3,
                    nav: false,
                },
                1200: {
                    items: 4,
                    nav: false,
                    loop: false
                }
            }
        });
    });
</script>