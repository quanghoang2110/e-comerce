<div class="container">
    <nav class="top-nav">
        <ul>
            <?php if (!empty($pages)): ?>
                <?php foreach ($pages as $item): ?>
                    <li><a class="smooth" href="<?php echo base_url($item->slug) ?>"
                           title=""><?php echo $item->name ?></a></li>
                <?php endforeach; ?>
            <?php endif; ?>
            <li><a class="smooth" href="/tin-tuc" fixed-sn title="">Tin tức</a></li>
            <li><a class="smooth" href="tel:<?php echo $setting->company_phone ?>" title="">Hotline <strong><?php echo $setting->company_phone ?></strong></a></li>
        </ul>
    </nav>
</div>