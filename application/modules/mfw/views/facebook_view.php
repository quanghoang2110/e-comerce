<?php $fb_object = new Facebook_object($fb_obj); ?>
<?php if ($fb_object->method == FACBOOK_ACTION_COMMENT): ?>
    <div class="fb-comments"
         data-href="<?php echo $fb_object->url ?>"
         data-numposts="5"></div>
<?php else: ?>
    <div class="fb-like" data-href="<?php echo $fb_object->url ?>" data-layout="<?php echo $fb_object->data_layout ?>"
         data-action="like" data-size="<?php echo $fb_object->data_size ?>"
         data-show-faces="<?php echo $fb_object->show_face ?>"
         data-share="<?php echo $fb_object->share ?>"></div>
<?php endif; ?>