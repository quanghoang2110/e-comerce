<?php
$order_invoice = "#" . $code;
$order_name = $name;
$order_phone = $phone;
$order_mail = $email;
$order_address = $address;
$order_other_addess = $other_address;
$order_note = $note;
$order_total_price = $total_price;
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#dcf0f8"
       style="margin:0;padding:0;background-color:#f2f2f2;width:100%!important;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">
    <tbody>
    <tr>
        <td align="center" valign="top"
            style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal; background: white">

            <table border="0" cellpadding="0" cellspacing="0" width="600" style="margin-top:15px">
                <tbody>
                <tr>
                    <td align="center" valign="bottom" id="m_-1058975054787509934headerImage">
                        <table width="100%" cellpadding="0" cellspacing="0"
                               style="border-bottom:3px solid #e18037;padding-bottom:10px;background-color:#fff">
                            <tbody>
                            <tr>
                                <td valign="top" bgcolor="#FFFFFF" width="100%" style="padding: 20px 0 0 0;text-align: center">
                                    <a style="border:medium none;text-decoration:none;color:#007ed3;"
                                       href="<?php echo MFW_MAIL_URL ?>"
                                       target="_blank">
                                        <img src="<?php echo base_url('/images/chef/logo.png') ?>" class="CToWUd">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr style="background:#fff">
                    <td align="left" width="600" height="auto" style="padding:15px">
                        <table>
                            <tbody>
                            <tr>
                                <td>
                                    <h1 style="font-size:17px;font-weight:bold;color:#444;padding:0 0 5px 0;margin:0"><?php echo $order_name ?>
                                        thân mến,</h1>

                                    <p style="margin:4px 0;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal">
                                        Yêu cầu đặt hàng cho đơn hàng <?php echo $order_invoice ?> của quý khách đã được
                                        tiếp nhận và đang trong quá trình xử lý. Thời gian đặt hàng
                                        vào <?php echo date('H:i, \n\g\à\y d \t\h\á\n\g m \n\ă\m Y') ?>. Chúng tôi sẽ
                                        liên hệ theo số điện thoại người nhận để xác nhận thời gian và địa điểm giao
                                        hàng trong thời gian sớm nhất.
                                        <br/>
                                        <br/>
                                        Cảm ơn quý khách đã đặt hàng tại <?php echo MFW_MAIL_NAME ?>!
                                    </p>


                                    <h3 style="font-size:13px;font-weight:bold;color:#e18037;text-transform:uppercase;margin:20px 0 0 0;border-bottom:1px solid #ddd">
                                        Thông tin người nhận
                                    </h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">

                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td valign="top"
                                                style="padding:3px 9px 9px 9px;border-top:0;border-left:0;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal">
                                                <span style="text-transform:capitalize">Họ tên: <?php echo $order_name ?></span><br>
                                                <a href="mailto:<?php echo $order_mail ?>"
                                                   target="_blank">Email: <?php echo $order_mail ?></a><br>
                                                Địa chỉ: <?php echo $order_address ?><br>
                                                Ghi chú: <?php echo $order_note ?><br>
                                                <?php if ($call):?>
                                                Goi điện thoại: <?php echo $call ?><br>
                                                <?php endif;?>
                                                <?php if ($invoice):?>
                                                    In hóa đơn: <?php echo $invoice ?><br>
                                                    &nbsp&nbsp;Thông tin công ty:<br/>
                                                    &nbsp&nbsp&nbsp;&nbsp;Tên: <?php echo $company_name?><br/>
                                                    &nbsp&nbsp&nbsp;&nbsp;Mã số thuế: <?php echo $tax_code?><br/>
                                                    &nbsp&nbsp&nbsp;&nbsp;Địa chỉ: <?php echo $company_address?><br/>
                                                <?php endif;?>
                                                Hình thức giao hàng: <?php echo $delivery?><br>
                                                Phương thức thanh toán: <?php echo $payment_method?><br>
                                                <?php if($shipping_other == 'on'):?>
                                                Địa chỉ nhận hàng khác: <br/>
                                                Họ tên: <?php echo $other_name ?><br/>
                                                Địa chỉ: <?php echo $order_other_addess ?><br/>
                                                SĐT: <?php echo $other_phone ?><br/>
                                                Email: <?php echo $other_email ?><br/>
                                                <?php if ($other_note): ?>
                                                    <p style="white-space: pre-line">
                                                        <?php echo $other_note ?>
                                                    </p>
                                                <?php endif; ?>
                                                <?php endif?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h2 style="text-align:left;margin:10px 0;border-bottom:1px solid #ddd;padding-bottom:5px;font-size:13px;color:#e18037">
                                        CHI TIẾT ĐƠN HÀNG</h2>

                                    <table cellspacing="0" cellpadding="0" border="0" width="100%"
                                           style="background:#f5f5f5">
                                        <thead>
                                        <tr>
                                            <th align="left" bgcolor="#e18037"
                                                style="padding:6px 9px;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px">
                                                Sản phẩm
                                            </th>
                                            <th align="left" bgcolor="#e18037"
                                                style="padding:6px 9px;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px">
                                                Đơn giá
                                            </th>
                                            <th align="left" bgcolor="#e18037"
                                                style="padding:6px 9px;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px">
                                                Số lượng
                                            </th>
                                            <th align="right" bgcolor="#e18037"
                                                style="padding:6px 9px;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:14px">
                                                Tổng tạm
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody bgcolor="#eee"
                                               style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">
                                        <?php foreach ($products as $item): ?>
                                            <?php
                                            $price = $item->price_2 ? $item->price_2 : $item->price_1;
                                            $soluong = $item->soluong;
                                            ?>
                                            <tr>
                                                <td align="left" valign="top" style="padding:3px 9px">
                                                    <span class="m_-1058975054787509934name"><?php echo $item->name ?></span><br>
                                                </td>
                                                <td align="left" valign="top" style="padding:3px 9px">
                                                    <span><?php echo get_price($price, ' ₫') ?></span></td>
                                                <td align="left" valign="top"
                                                    style="padding:3px 9px"><?php echo $soluong ?></td>
                                                <td align="right" valign="top" style="padding:3px 9px">
                                                    <span><?php echo get_price($price * $soluong, ' ₫') ?></span></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                        <tfoot style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">

                                        <tr bgcolor="#eee">
                                            <td colspan="3" align="right" style="padding:7px 9px"><strong><big>Tổng giá
                                                        trị đơn hàng</big></strong></td>
                                            <td align="right" style="padding:7px 9px">
                                                <strong><big><span><?php echo get_price($order_total_price, ' ₫') ?></span></big></strong>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <br>

                                    <p style="font-family:Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0;line-height:18px;color:#444;font-weight:bold">
                                        Cảm ơn quý
                                        khách.
                                    </p>

                                    <p style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal;text-align:right">

                                        <strong><a style="color:#00a3dd;text-decoration:none;font-size:14px"
                                                   href="<?php echo MFW_MAIL_URL ?>"
                                                   target="_blank"><span
                                                        class="il"><?php MFW_MAIL_DOMAIN ?></span></a></strong><br>
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>

    </tr>

    <tr>
        <td align="center">
            <table width="600">
                <tbody>
                <tr>
                    <td>
                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:11px;line-height:18px;color:#4b8da5;padding:10px 0;margin:0px;font-weight:normal"
                           align="left">
                            Quý khách nhận được email này vì đã mua hàng tại <span
                                    class="il"><?php echo MFW_MAIL_DOMAIN ?></span>.<br>
                            Đây là email tự động, vui lòng không trả lời email này. <br>
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>