<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#dcf0f8"
       style="margin:0;padding:0;background-color:#f2f2f2;width:100%!important;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px">
    <tbody>
    <tr>
        <td align="center" valign="top"
            style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#444;line-height:18px;font-weight:normal">

            <table border="0" cellpadding="0" cellspacing="0" width="600" style="margin-top:15px">
                <tbody>
                <tr>
                    <td align="center" valign="bottom" id="m_-1058975054787509934headerImage">
                        <table width="100%" cellpadding="0" cellspacing="0"
                               style="border-bottom:3px solid #fdd344;padding-bottom:10px;background-color:#fff">
                            <tbody>
                            <tr>
                                <td valign="top" bgcolor="#FFFFFF" width="100%" style="padding:0;text-align: center">
                                    <a style="border:medium none;text-decoration:none;color:#007ed3;"
                                       href="<?php echo MFW_MAIL_URL ?>"
                                       target="_blank">
                                        <img src="<?php echo base_url('/images/logo.png') ?>" class="CToWUd">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr style="background:#fff">
                    <td align="left" width="600" height="auto" style="padding:15px">
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td>
                                    <h2 style="text-align:left;margin:10px 0;border-bottom:1px solid #ddd;padding-bottom:5px;font-size:13px;color:#dab009">
                                        THÔNG TIN LIÊN HỆ</h2>
                                    <ul>
                                        <li>Tên: <strong><?php echo $name ?></strong></li>
                                        <li>Điện thoại: <strong><?php echo $phone ?></strong></li>
                                        <li>Email: <strong><?php echo $email ?></strong></li>
                                        <li>Nội dung: <strong><?php echo $content ?></strong></li>
                                    </ul>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>

    </tr>

    <tr>
        <td align="center">
            <table width="600">
                <tbody>
                <tr>
                    <td>
                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:11px;line-height:18px;color:#4b8da5;padding:10px 0;margin:0px;font-weight:normal"
                           align="left">
                            Đây là email tự động, vui lòng không trả lời email này. <br>
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>