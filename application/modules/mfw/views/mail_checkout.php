<div>
    <h4>Bạn nhận được 1 đơn hàng mới!</h4>
    <ul>
        <li>Time: <?php echo date('H:i, d/m/Y', strtotime($time)) ?></li>
        <li>Mã: <?php echo $code ?></li>
        <li>Đơn giá: <?php echo get_price($price, ' đ') ?></li>
        <li><strong>Người mua:</strong>
            <ul>
                <li>Tên: <?php echo $name ?></li>
                <li>Phone: <?php echo $phone ?></li>
                <li>Địa chỉ: <?php echo $address ?></li>
            </ul>
        </li>
        <li><strong>Người nhận:</strong>
            <ul>
                <li>Tên: <?php echo $name2 ?></li>
                <li>Phone: <?php echo $phone2 ?></li>
                <li>Địa chỉ: <?php echo $address2 ?></li>
            </ul>
        </li>
        <li>
            <strong>Đơn hàng gồm: </strong>
            <ul>
                <?php foreach ($products as $p): ?>
                    <li><?php echo '<strong>'.$p->number .'</strong> sản phẩm <i>'.$p->name . '(' . $p->code . ')</i>' ?></li>
                <?php endforeach; ?>
            </ul>
        </li>
        <li>Ghi chú: <?php echo $note ?></li>
    </ul>
</div>
<div style="margin-top: 100px">
    <p>
        Đây là email tự động! Vui lòng không trả lời email này.
    </p>
</div>