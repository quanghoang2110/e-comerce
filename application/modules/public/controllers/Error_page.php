<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Error_page extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->_module_view = get_mfw_module("error","page_not_found");
        $this->_load_tmp();
    }
}