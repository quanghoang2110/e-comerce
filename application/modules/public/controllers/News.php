<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class News extends MFrontendController
{

    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "news";
    }

    /**
     * @param int $id
     */
    function index($id = null)
    {
        $this->_init_page();
        $view = get_mfw_module_view("news", "index", $id, $this->_page, $this->_limit);
        $this->_module_view = $view->content;
        $this->_seo_image = $view->image ?? $this->_seo_image;
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->desc ?? $this->_seo_desc;
        $this->_load_tmp();
    }

    function news_discount($id = null)
    {
        $this->_init_page();
        $this->_current_menu = 'khuyen-mai';
        $view = get_mfw_module_view("news", "news_discount", $id, $this->_page, $this->_limit);
        $this->_module_view = $view->content;
        $this->_seo_image = $view->image ?? $this->_seo_image;
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->desc ?? $this->_seo_desc;
        $this->_load_tmp();
    }

    function content($slug)
    {
        if ($slug) {
            $id = get_id_from_slug($slug);
            if (is_numeric($id)) {
                $this->_facebook_sdk = true;
                $view = get_mfw_module_view("news", "content", $id);
                $this->_seo_image = $view->image ?? $this->_seo_image;
                $this->_seo_title = $view->title ?? $this->_seo_title;
                $this->_seo_desc = $view->desc ?? $this->_seo_desc;
                $this->_module_view = $view->content;
//                $this->_module_view = get_mfw_module("news", "content", $id);
                $this->_load_tmp();
            }
        } else
            load_mfw_module('error', 'page_not_found');
    }
}