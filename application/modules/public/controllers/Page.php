<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "about";
    }

    function content($id)
    {
        $view = get_mfw_module_view('page', 'content', $id);
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->seo_desc ?? $this->_seo_desc;
        $this->_module_view = $view->content;
        $this->_load_tmp();
    }
}