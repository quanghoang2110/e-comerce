<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Home extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = 'home';
    }

    function index()
    {
        $this->_body_class = "";
        $this->_module_view = get_mfw_module("home");
        $this->_load_tmp();
    }

    function encrypt($plaintext)
    {
        $password = 'YNKJe9hsVXeRmRglS';
        $method = 'aes-256-cbc';

        $key = substr(hash('sha256', $password, true), 0, 32);

        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

        $encrypted = base64_encode(openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv));
        return array(
            'key' => $key,
            'key_hex' => base64_encode($key),
            'iv' => $iv,
            'encrypted' => $encrypted
        );
    }
}