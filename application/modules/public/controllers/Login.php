<?php defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "login";
    }

    public function index()
    {
        $view = get_mfw_module_view('login', 'index', '');
        $this->_seo_title = $view->title ??
         $this->_seo_title;
        $this->_seo_desc = $view->seo_desc ?? $this->_seo_desc;
        $this->_module_view = $view->content;
        $this->_load_tmp();
    }

    public function register()
    {
        $view = get_mfw_module_view('login', 'register', '');
        $this->_seo_title = $view->title ??
            $this->_seo_title;
        $this->_seo_desc = $view->seo_desc ?? $this->_seo_desc;
        $this->_module_view = $view->content;
        $this->_load_tmp();
    }

    public function post_register()
    {
        load_mfw_module('login', 'post_register');
    }

    public function post_login()
    {
        load_mfw_module('login', 'post_login');
    }

    public function logout()
    {
        $view = get_mfw_module_view('login', 'logout', '');
        $this->_load_tmp();
    }
}
