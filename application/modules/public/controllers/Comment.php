<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 09:59.
 */
class Comment extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->_module_view = get_mfw_module('comment', 'index');
        $this->_load_tmp();
    }

    public function add_comment(){
        load_mfw_module('comment', 'add_comment');
    }

    public function refresh_comment(){
        echo load_mfw_module('comment', 'refresh_comment');
    }

    public function logout_comment(){
        echo load_mfw_module('comment', 'logout_comment');
    }

    public function upload(){
        load_mfw_module('comment', 'upload');
    }

    public function remove_comment(){
        load_mfw_module('comment', 'remove_comment');
    }

    public function load_page_comment()
    {
        $id = $this->_input_post('id');
        $type = $this->_input_post('type');
        $page = $this->_input_post('page');
        $limit = $this->_input_post('limit');
        load_mfw_module('comment', 'load_page_comment', $id, $type, $page, $limit);
    }
}
