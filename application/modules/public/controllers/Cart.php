<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 09:59.
 */
class Cart extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->_module_view = get_mfw_module('cart', 'index');
        $this->_load_tmp();
    }

    public function success()
    {
        $this->_module_view = get_mfw_module('cart', 'success');
        $this->_load_tmp();
    }

    public function update()
    {
        $this->_module_view = get_mfw_module('cart', 'update');
        $this->_load_tmp();
    }

    public function cart_refresh()
    {
//        echo 'TEST';
        load_mfw_module('cart', 'refresh');
//        echo '1';
//        $res['token'] = $this->security->get_csrf_hash();
//        echo json_encode($res);
    }

    public function add_product()
    {
        $id = $this->input->post('product_id');
        if ($id) {
            load_mfw_module('cart', 'add_product', $id);
        }
    }

    public function remove_product()
    {
        $id = $this->input->post('product_id');
        if ($id) {
            load_mfw_module('cart', 'remove_product', $id);
        }
    }

    public function add_product_review()
    {
        load_mfw_module('cart', 'add_product_review');
    }

    public function info_shipping()
    {
        $this->_module_view = get_mfw_module('cart', 'info_shipping');
        $this->_load_tmp();
    }

    public function method_payment()
    {
        $this->_module_view = get_mfw_module('cart', 'method_payment');
        $this->_load_tmp();
    }

    public function payment_success()
    {
        $this->_module_view = get_mfw_module('cart', 'payment_success');
        $this->_load_tmp();
    }

    public function callbackVNPay()
    {
        load_mfw_module('cart', 'callbackVNPay');
    }
}
