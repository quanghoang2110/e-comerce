<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "about";
    }

    function user_profile()
    {
        $view = get_mfw_module_view('user', 'user_profile');
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->seo_desc ?? $this->_seo_desc;
        $this->_module_view = $view->content;
        $this->_load_tmp();
    }

    public function update_profile()
    {
        load_mfw_module('user', 'update_profile');
    }

    public function update_password()
    {
        load_mfw_module('user', 'update_password');
    }

    function order_finished()
    {
        $this->_init_page();
        $view = get_mfw_module_view('user', 'order_finished', $this->_page, $this->_limit);
        $this->_module_view = $view->content;
        $this->_load_tmp();
    }

    function order_wait()
    {
        $this->_init_page();
        $view = get_mfw_module_view('user', 'order_wait', $this->_page, $this->_limit);
        $this->_module_view = $view->content;
        $this->_load_tmp();
    }

}