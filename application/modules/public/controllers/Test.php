<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Test extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = 'home';
    }

    function index()
    {
        $this->config->load('email');
        $from_email = $this->config->item('smtp_user');
        $user_agent = $this->config->item('useragent');

        $to_email = 'mokeke@yopmail.com';
        $message = "Test mail";
        $code = "CODE";

        $this->load->library("email");
        $this->email->from($from_email, $user_agent);
        $this->email->to($to_email);
        $this->email->subject('Xác nhận đơn hàng #' . $code);
//        $config = file_get_contents(BASE_CONF_FILE_SITE_SETTING);
//        if ($config) {
//            $setting = new Setting_object(json_decode($config));
//            $emails = explode(PHP_EOL, $setting->support_email);
//            $cc_mail = '';
//            foreach ($emails as $index => $item) {
//                $cc_mail .= $item;
//                if ($index < count($emails) - 1) {
//                    $cc_mail .= ",";
//                }
//            }
//            if ($cc_mail) {
//                $this->email->bcc($cc_mail);
//            }
//        }
        $this->email->message($message);
        $this->email->send();
        echo $this->email->print_debugger();
    }

}