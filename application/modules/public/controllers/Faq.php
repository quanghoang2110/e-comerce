<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Faq extends MFrontendController
{

    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "faq";
    }

    /**
     * @param string $slug
     */
    function index($slug)
    {
        $this->_init_page();
        $view = get_mfw_module_view("faq", "index", $slug);
        $this->_module_view = $view->content;
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->desc ?? $this->_seo_desc;
        $this->_load_tmp();
    }

    function content($slug)
    {
        if ($slug) {
            $id = get_id_from_slug($slug);
            if (is_numeric($id)) {
                $this->_facebook_sdk = true;
                $view = get_mfw_module_view("news", "content", $id);
                $this->_seo_title = $view->title ?? $this->_seo_title;
                $this->_seo_desc = $view->desc ?? $this->_seo_desc;
                $this->_module_view = $view->content;
//                $this->_module_view = get_mfw_module("news", "content", $id);
                $this->_load_tmp();
            }
        } else
            load_mfw_module('error', 'page_not_found');
    }
}