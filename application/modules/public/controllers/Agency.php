<?php defined('BASEPATH') or exit('No direct script access allowed');

class Agency extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "agency";
    }

    public function index()
    {
        $view = get_mfw_module_view('agency', 'index', '');
        $this->_seo_title = $view->title ??
         $this->_seo_title;
        $this->_seo_desc = $view->seo_desc ?? $this->_seo_desc;
        $this->_module_view = $view->content;
        $this->_load_tmp();
    }
}
