<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 09:59
 */
class Product extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "product";
    }

    function index($id)
    {
        $this->_init_page();
        $this->_limit = 12;
        $view = get_mfw_module_view("product", "index", $id, $this->_page, $this->_limit);
        $this->_module_view = $view->content;
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->desc ?? $this->_seo_desc;
        $this->_seo_image = $view->image ?? $this->_seo_image;
        $this->_load_tmp();
    }

    function filter_product()
    {
        $this->_init_page();
        $view = get_mfw_module_view("product", "filter_product", $this->_page, $this->_limit);
        $this->_module_view = $view->content;
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->desc ?? $this->_seo_desc;
        $this->_seo_image = $view->image ?? $this->_seo_image;
        $this->_load_tmp();
    }

    /**
     * @param $slug
     */
    function content($slug)
    {
        if ($slug) {
            $id = get_id_from_slug($slug);
            if (is_numeric($id)) {
                $this->_facebook_sdk = true;
                $view = get_mfw_module_view("product", "content", $id);
                $this->_seo_image = $view->image ?? $this->_seo_image;
                $this->_seo_title = $view->title ?? $this->_seo_title;
                $this->_seo_desc = $view->desc ?? $this->_seo_desc;
                $this->_module_view = $view->content;
                $this->_load_tmp();
            } else {
                //page not found
            }
        }
    }

    function quick_view()
    {
        $id = $this->input->post("id_product");
        if ($id) {
            load_mfw_module("product", "quick_view", $id);
        }
    }

    function search()
    {
        $this->_init_page();
        $this->_limit = 12;
        $view = get_mfw_module_view("product", "search", $this->_search, $this->_page, $this->_limit);
        $this->_module_view = $view->content;
        $this->_load_tmp();
    }

    function list_discount()
    {
        $this->_init_page();
        $view = get_mfw_module_view("product", "list_discount", $this->_page, $this->_limit);
        $this->_current_menu = 'khuyen-mai';
        $this->_module_view = $view->content;
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->desc ?? $this->_seo_desc;
        $this->_seo_image = $view->image ?? $this->_seo_image;
        $this->_load_tmp();
    }
}