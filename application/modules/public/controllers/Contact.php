<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "about";
    }

    function index()
    {
        $this->_module_view = get_mfw_module('contact', 'index');
        $this->_load_tmp();
    }

    function success()
    {
        $this->_module_view = get_mfw_module('contact', 'success');
        $this->_load_tmp();
    }
}