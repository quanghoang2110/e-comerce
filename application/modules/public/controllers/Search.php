<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Search extends MFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->_module_view = get_mfw_module("search");
        $this->_load_tmp();
    }
}