<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Trademark extends MFrontendController
{

    public function __construct()
    {
        parent::__construct();
        $this->_current_menu = "trademark";
    }

    /**
     * @param int $id
     */
    function index($id = null)
    {
        $this->_init_page();
        $view = get_mfw_module_view("trademark", "index", $id, $this->_page, $this->_limit);
        $this->_module_view = $view->content;
        $this->_seo_image = $view->image ?? $this->_seo_image;
        $this->_seo_title = $view->title ?? $this->_seo_title;
        $this->_seo_desc = $view->desc ?? $this->_seo_desc;
        $this->_load_tmp();
    }
}