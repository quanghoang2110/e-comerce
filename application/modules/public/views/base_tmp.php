<?php
$setting = new Setting_object($setting);
function load_view_exists($file_name, $extension = '.php')
{
    if (file_exists(APPPATH.'modules/mfw/views/'.PUBLIC_THEME.'/'.$file_name.$extension)) {
        $ci = &get_instance();
        $ci->load->view($file_name);
    }
}
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo get_text_not_empty($setting->meta_title, $setting->name); ?></title>
    <meta name="description"
          content="<?php echo $setting->meta_desc; ?>"/>
    <meta name="keywords"
          content="<?php echo $setting->meta_keyword; ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--    facebook tag-->
    <meta property="fb:app_id" content="<?php echo $setting->fb_app_id; ?>"/>
    <?php $current_url = base_url($_SERVER['REQUEST_URI']); ?>
    <meta property="og:url"
          content="<?php echo get_text_not_empty($setting->meta_url, $current_url); ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="<?php echo $setting->meta_title; ?>"/>
    <meta property="og:description" content="<?php echo $setting->meta_desc; ?>"/>
    <meta property="og:image"
          content="<?php echo get_text_not_empty($setting->meta_image, base_url($setting->logo)); ?>"/>

    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:type" content="website"/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="<?php echo $setting->meta_title; ?>"/>

    <!--    favicon  -->
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/logo.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favicon/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/logo.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/logo.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- favicon -->

    <link rel="shortcut icon" href="<?php echo $setting->favicon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $setting->favicon; ?>" type="image/x-icon">

    <?php if ($setting->gg_tag_manager_id):?>
    <!-- Google Tag Manager -->
    <script>
        setTimeout((function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?php echo $setting->gg_tag_manager_id; ?>'),5000);
    </script>
    <!-- End Google Tag Manager -->
    <?php endif; ?>
    
    <?php if ($setting->gg_analytics_id):?>
    <!-- Google Analytics -->
    <script>
    setTimeout((function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'),5000);

    ga('create', '<?php echo $setting->gg_analytics_id; ?>', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    <?php endif; ?>

    <!-- load head tag -->
    <?php
    load_view_exists('layout_head');
    load_view_exists('styles');
    if (isset($page_css)) {
        load_view_exists($page_css);
    }
    ?>
    <?php include_once 'base_css.php'; ?>
</head>

<body class="<?php echo $body_class; ?>">
<div class='thetop'></div>
<?php if ($setting->gg_tag_manager_id): ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $setting->gg_tag_manager_id; ?>"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php endif; ?>
<div class="d-none">
    <h1><?php echo $setting->name ?></h1>
</div>
<!--load content-->
<?php
load_view_exists('layout_content');
load_view_exists('layout_footer');
load_view_exists('theme_js');
if (isset($page_js)) {
    load_view_exists($page_js);
}
?>

<div class='scrolltop'>
    <div class='scroll icon' title="Về đầu trang"><i class="fa fa-4x fa-angle-up"></i></div>
</div>

<div id="ajaxLoading">
    <?php include_once 'loading_new.php'; ?>
</div>

<?php include_once 'facebook_sdk.php'; ?>
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="1817718021606102"
     theme_color="#0084ff"
     logged_in_greeting="Xin chào! Lodge luôn sẵn sàng hỗ trợ quý khách "
     logged_out_greeting="Xin chào! Lodge luôn sẵn sàng hỗ trợ quý khách :)">
</div>


<script type="text/javascript" src="/assets/public/js/js.cookie.js"></script>
<?php include_once 'ecomerce.php'; ?>
<?php include_once 'base_js.php'; ?>
<script>
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (settings.data.indexOf('csrf_test_name') === -1) {
                settings.data += '&csrf_test_name=' + encodeURIComponent(Cookies.get('csrf_cookie_name'));
            }
        }
    });
    <?php if (MFW_CART_ENABLED):?>
    $(document).ready(function () {
        refreshCart();
    });
    <?php endif; ?>
</script>

</body>

</html>