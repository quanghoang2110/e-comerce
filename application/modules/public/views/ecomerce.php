<script>
    /**
     * Created by Max Mun on 1/8/2016.
     */

    var docWidth = $(document).width();
    var onCheckoutStep1 = false;
    var cartObj = $('#desktop-cart');
    var quickCartObj = $('#desktop-icon-quick-cart');
    var iconCartBadge = $('#desktop-quick-cart-badge');
    var iconCartMobi = $('#desktop-quick-cart-mobi');
    var iconCartScroll = $('#desktop-quick-cart-scroll');

    // if(docWidth <= 991){
    //     cartObj = $('#mobile-cart');
    //     quickCartObj = $('#mobile-icon-quick-cart');
    // }

    $('.mfwAddToCart').click(function () {
        const id = $(this).attr('product-id');
        addToCart(id, 1);
    });

    function showLoading() {
        $('#ajaxLoading').show();
    }

    function hideLoading() {
        $("#ajaxLoading").fadeOut("slow");
    }

    function changeNumberItem(new_num, price, id) {
        var old_price = $('#old_num_item_' + id).val() * price;
        var new_price = new_num * price;
        $('#price_hidden_' + id).val(new_price);
        $('#new_price_' + id).html(new_price.formatMoney(0, ',', '.') + " VNĐ");

        var new_total = parseInt($('#total_price_hidden').val()) + new_price - old_price;
        console.log(old_price);
        $('#total_price_hidden').val(new_total);
        $('#total_price_show').html(new_total.formatMoney(0, ',', '.') + ' VNĐ');

        $('#old_num_item_' + id).val(new_num);
    }

    function refreshCart() {
        $.post('/<?php echo MFW_CART_ROUTE_REFRESH ?>', {params: 1}).done(function (data) {
            const obj = JSON.parse(data);
            cartObj.html(obj.viewRefresh);
            iconCartBadge.text(obj.total);
            if(obj.total > 0){
                iconCartScroll.removeClass('d-none');
                iconCartScroll.text(obj.total);
            }else{
                iconCartScroll.addClass('d-none');
            }
            iconCartMobi.text(obj.total);
            if (!obj.viewRefresh) {
                cartObj.addClass('mfw-hidden');
            }
            $('#csrfHash').val(encodeURIComponent(Cookies.get('csrf_cookie_name')));
        });
    }

    function addToCart(id, soluong) {
        if (id && soluong > 0) {
            showLoading();
            $.post("/<?php echo MFW_CART_ROUTE_ADD_PRODUCT ?>",
                {product_id: id, number: soluong}
            ).done(function (data) {
                hideLoading();
                const obj = JSON.parse(data);
                $("#blockcart-modal").html(obj.viewAdded).modal("show");
                cartObj.html(obj.viewRefresh).removeClass('mfw-hidden');
                iconCartBadge.text(obj.total);
                if(obj.total > 0){
                    iconCartScroll.removeClass('d-none');
                    iconCartScroll.text(obj.total);
                }else{
                    iconCartScroll.addClass('d-none');
                }
                iconCartMobi.text(obj.total);
            });
        }
    }

    function removeFromCart(id, soluong = 1) {
        if (id && soluong > 0) {
            showLoading();
            $.post("/<?php echo MFW_CART_ROUTE_REMOVE_PRODUCT ?>",
                {product_id: id, number: soluong}
            ).done(function (data) {
                hideLoading();
                const obj = JSON.parse(data);
                iconCartBadge.text(obj.total);
                if(obj.total > 0){
                    iconCartScroll.removeClass('d-none');
                    iconCartScroll.text(obj.total);
                }else{
                    iconCartScroll.addClass('d-none');
                }
                iconCartMobi.text(obj.total);
                if (!obj.viewRefresh) {
                    cartObj.html(obj.viewRefresh).addClass('mfw-hidden');
                } else {
                    cartObj.html(obj.viewRefresh);
                    quickCartObj.click();
                }
            });
        }
    }

    function showRemoveItemInCartOnStep1(id) {
        onCheckoutStep1 = true;
        showRemoveItemInCart(id);
    }

    function showRemoveItemInCart(id) {
        productId = id;
        $('#modal_confirm_msg .modal-body').html('<p>Bạn muốn xóa sản phẩm này khỏi giỏ hàng?</p>');
        $('#modal_confirm_msg .btn-primary').attr('onclick', 'removeItemInCart()');
        $('#modal_confirm_msg').modal('show');
    }

    function removeQuickCart() {
        console.log(productId);
        $('#li-p-id-' + productId).remove();
    }

    function thanhtoan() {
        $.post("/check-login").done(function (data) {
            if (data == 1) {
                $('#checkout-step-1').submit();
            } else {
                $('#xx-modal').html(data);
                $('#myModal').modal('show');
            }
        }).done(function () {
            hideLoading();
        });
    }

    $(document).ready(function () {
        $(".cart-block").animate({scrollTop: 10000});
    });

    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
</script>
