<style>
    .scrolltop {
        display: none;
        width: 100%;
        margin: 0 auto;
        position: fixed;
        bottom: 20px;
        right: 5px;
    }

    .scroll {
        position: absolute;
        right: 0;
        bottom: 80px;
        background: #b2b2b2;
        background: rgba(178, 178, 178, 0.7);
        padding: 10px;
        text-align: center;
        margin: 0 0 0 0;
        cursor: pointer;
        transition: 0.5s;
        -moz-transition: 0.5s;
        -webkit-transition: 0.5s;
        -o-transition: 0.5s;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }

    .scroll:hover {
        background: rgba(178, 178, 178, 1.0);
        transition: 0.5s;
        -moz-transition: 0.5s;
        -webkit-transition: 0.5s;
        -o-transition: 0.5s;
    }

    .scroll:hover .fa {
        padding-top: -10px;
    }

    .scroll .fa {
        font-size: 30px;
        margin-top: -5px;
        margin-left: 1px;
        transition: 0.5s;
        -moz-transition: 0.5s;
        -webkit-transition: 0.5s;
        -o-transition: 0.5s;
    }
</style>