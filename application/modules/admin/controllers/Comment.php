<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/10/2016
 * Time: 15:25
 */
class Comment extends MBackendController
{
    function __construct()
    {
        $this->_permission_controller_name = "Bình luận";
        parent::__construct();
        $this->_load_data_from('customer_comment');
    }

    function index()
    {
        $this->_init_page();

        $this->_load_data_from('customer_comment');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'fullname',
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count(['is_deleted' => 0]);

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page = $this->_page--;

        $vars['list'] = [];

        if ($total > 0) {
            $this->load->model('Model_comment');
            $comment = new Model_comment();
            $vars['list'] = $comment->get_list_comment($this->_page, $this->_limit, $this->_search);
        }

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp(__FUNCTION__);
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $id = $this->input->get('id');

            $info = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $info = $this->_model->_get_top_one($where);
                if (!$info) redirect(BASE_PAGE_NOT_FOUND);
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }

            $vars['data'] = $info;

            $this->_module_vars = $vars;
            $this->_load_tmp(__FUNCTION__);
        }
    }

    function ajax_update()
    {
        $config = array(

        );
        if ($config && !$this->_validation($config)) {
            $data['error'] = 1;
            $data['error_detail'] = strip_tags(validation_errors());
            $data['msg'] = "Lỗi dữ liệu";
            echo json_encode($data);
            return;
        } else {
            $id = url_title($this->input->post("input_id"));
            $slug = strtolower(url_title($this->input->post("input_slug")));
            $slug = $slug ? $slug : get_urltitle($this->input->post("input_name"));
            $this->_load_data_from("slug");
            $where['name'] = $slug;
            if ($id) {
                $where['route !='] = 'public/page/content/' . $id;
            }
            if ($this->_model->_count($where)) {
//                $this->_last_query();
                $data['error'] = 1;
                $data['msg'] = "Short link đã được sử dụng!";
                echo json_encode($data);
            } else {
                $this->_update_general();
            }
        }
    }

    function _update_general()
    {

        $valid = $this->_input_post('input_valid');
        $data = array(
            'valid' => $valid
        );

        $id = $this->_input_post('input_id', true);
        $where = $id ? array(
            'id' => $id
        ) : array();

        $response = $this->update_to_db($data, $where, 'customer_comment');
        echo $response;
    }

    function reply_comment_ajax(){
        $parent_id = $this->_input_post('id');
        $content = $this->_input_post('content');
        if (!$content) {
            $data['status'] = false;
            $data['msg'] = "Nhập nội dung trả lời bình luận";
            echo json_encode($data);
            return;
        } else{
            $user_id = $this->_sess_userid;
            $this->_load_data_from('admin');
            $user = $this->_model->_get_row(['id' => $user_id]);

            $this->_load_data_from('customer_comment');
            $comment_parent = $this->_model->_get_row(['id'=>$parent_id]);
            $data_comment = array(
                'fullname' => $user->fullname,
                'user_id' => $user->id,
                'desc' => $content,
                'type' => $comment_parent->type,
                'category_id' => $comment_parent->category_id,
                'valid' => 1,
                'role' => 0,
                'create_time' => date('Y-m-d H:i:s'),
                'parent_id' => $parent_id
            );
            $this->_load_data_from('customer_comment');
            $this->_model->_insert($data_comment);

            $data['status'] = true;
            echo json_encode($data);
            return;
        }
    }
}