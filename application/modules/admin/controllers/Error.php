<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: quanghv
 * Date: 12/13/2016
 * Time: 3:45 PM
 */
class Error extends MBackendController
{
	function index()
	{
		$this->_module_title = "Error 404";
		$this->_content = '404';
		$this->use_theme_custom();
		$this->_page_folder = '';
		$this->_load_tmp();
	}

}