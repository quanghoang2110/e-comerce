<?php

/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */
class Site_setting extends MBackendController
{
    function __construct()
    {
        //ten chuc nang
        $this->_permission_controller_name = "thông tin website";
        parent::__construct();
        $this->_load_data_from(BASE_TABLE_SITE_SETTING);
    }

    function index()
    {
        $this->_init_page();

        $vars['info'] = $this->_model->_get_row();
        $this->_module_vars = $vars;

//        $config = file_get_contents(BASE_CONF_FILE_SITE_SETTING);
//        if ($config) {
//            $setting = new Setting_object(json_decode($config));
//            $setting->support_email;
//            var_dump($setting->support_email);
//            $emails = explode(PHP_EOL, $setting->support_email);
//            print_r($emails);
//        }

        $this->use_theme_index(false);
        $this->disable_pagin_action_button();
        $this->them_contain_form();
        $this->_load_tmp(__FUNCTION__);
    }

    function update()
    {
        $err = 0;
        $msg = '';
        $data = array(
            'logo' => $this->input->post('input_logo'),
//			'logo2' => $this->input->post('input_logo_2'),
            'name' => $this->input->post('input_name', true),
            'company_name' => $this->input->post('input_company_name', true),
            'company_address' => $this->input->post('input_company_address'),
            'company_email' => $this->input->post('input_company_email', true),
            'company_phone' => $this->input->post('input_company_phone', true),
            'company_phone_1' => $this->input->post('input_company_phone1', true),
            'company_phone_2' => $this->input->post('input_company_phone2', true),
            'company_phone_3' => $this->input->post('input_company_phone3', true),
            'company_time' => $this->input->post('input_company_time', true),
            'company_info' => $this->input->post('input_company_info', true),
            'company_tax_code' => $this->input->post('input_company_tax_code', true),
            'twitter' => $this->input->post('input_twitter', true),
            'linkedin' => $this->input->post('input_linkedin', true),
            'pinterest' => $this->input->post('input_pinterest', true),
            'instagram' => $this->input->post('input_instagram', true),
            'gg_plus_id' => $this->input->post('input_gg_plus', true),
            'gg_analytics_id' => $this->input->post('input_gg_analytics_id', true),
            'gg_tag_manager_id' => $this->input->post('input_gg_tag_manager_id', true),
            'fb_fanpage' => $this->input->post('input_fb_fanpage', true),
            'fb_app_id' => $this->input->post('input_fb_app_id', true),
            'meta_title' => $this->input->post('input_meta_title', true),
            'meta_desc' => $this->input->post('input_meta_desc', true),
            'meta_keyword' => $this->input->post('input_meta_keyword', true),
//			'company_address2' => $this->input->post('company_address2'),
//			'google_map' => $this->input->post('input_google_map', true),
            'youtube' => $this->input->post('input_youtube', true),
            'skype' => $this->input->post('input_company_skype', true),
            'support_email' => $this->input->post('input_support_email', true),
            'url_bct' => $this->_input_post('input_url_bct')
        );

//        $this->debug_data($data,1);

        $this->_model->_delete(array('id>' => 0));
        $id = $this->_model->_insert($data);

        //save setting to file
        $this->save_to_file();

        if (!$id) {
            $err = 0;
            $msg = 'Đã có lỗi xảy ra';
        } else {
            $msg = "Cập nhật thông tin thành công";
        }
        $result = array(
            'err' => $err,
            'msg' => $msg,
            'id' => $id
        );
        echo json_encode($result);
    }

    function save_to_file()
    {
        $this->load->helper('file');
        $this->_load_data_from(BASE_TABLE_SITE_SETTING);
        $setting = $this->_model->_get_row();
        if ($setting) {
            if (!write_file(BASE_CONF_FILE_SITE_SETTING, json_encode($setting))) {
//            echo 'Unable to write the file';
            } else {
//            echo 'File written!';
            }
        }
    }
}