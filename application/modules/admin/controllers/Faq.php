<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: hung
 * Date: 12/13/2016
 * Time: 1:52 PM
 */
class Faq extends MBackendController
{

	function __construct()
	{
		$this->_permission_controller_name = 'FAQ';
		parent::__construct();
		$this->_load_data_from('faq');
	}


	function index()
	{
		$this->_init_page();

		$this->_load_data_from('faq');
		$query_arr = array(
			'keyword' => $this->_search,
			'search_row' => 'title'
		);
		$this->_set_query($query_arr);
		$total = $this->_model->_count();

		if ($this->_page > ceil($total / $this->_limit))
			$this->_page--;

		$order_arr = array(
			"type" => "asc",
			"position" => "asc",
			"id" => 'desc'
		);
		$vars['list'] = array();
		if ($total > 0)
			$vars['list'] = $this->_model->_get_limit("", $order_arr, $this->_page, $this->_limit, "*");

		$url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

		$vars['paging_info'] = $this->_get_page_info($total);
		$vars['paging'] = get_paging($url, $total, $this->_limit);

		//permission
		$vars['permission_edit'] = $this->check_permission($this->_permission_edit);
		$vars['permission_delete'] = $this->check_permission($this->_permission_delete);

		$vars['user_id'] = $this->_sess_userid;

		$this->_module_vars = $vars;
		$this->_load_tmp(__FUNCTION__);
	}

	function update()
	{
		if ($this->input->post()) {
			$this->ajax_update();
		} else {
			$id = $this->input->get('id');
			$info = array();
			if (trim($id) > 0) {
				$where = array(
					'id' => $id
				);
				$info = $this->_model->_get_top_one($where);
				$this->_module_desc = "Cập nhật";
			} else {
				$this->_module_desc = "Thêm mới";
			}
			$vars['data'] = $info;
            $this->_load_data_from('product');
            $vars['products'] = $this->_model->_get_limit(array('valid' => 1), array('name' => 'asc'), 1, 100, 'id,name');
			$this->_module_vars = $vars;
			$this->_load_tmp(__FUNCTION__);
		}
	}

	function ajax_update()
	{
		$config = array(
			array(
				'field' => 'input_name',
				'label' => 'Tiêu đề',
				'rules' => 'trim|required|max_length[255]'
			),
			array(
				'field' => 'input_content',
				'label' => 'Nội dung',
				'rules' => 'trim|required'
			)
		);

		if (!$this->_validation($config)) {
			$data['error'] = 1;
			$data['error_detail'] = strip_tags(validation_errors());
			$data['msg_error'] = "Lỗi dữ liệu";
			echo json_encode($data);
			return;
		} else {
			$this->_update_general();
		}
	}

	function _update_general()
	{
		$title = $this->input->post('input_name');
		$content = $this->input->post('input_content');
//		$type = $this->input->post('type');
		$position = $this->input->post('input_position');
		$valid = $this->input->post('input_valid');
		$data = array(
			'name' => $title,
//			'type' => $type,
			'position' => $position,
			'content' => $content,
			'valid' => in_array($valid, array(1, 'on')) ? 1 : 0
			);
        $product_related = $this->input->post('input_products_related');
        $str_product_related = $product_related ? implode(',', $product_related) : '';
        $data['products_related'] = $str_product_related.',';
		$id = $this->_input_post('input_id', true);
		$where = $id ? array(
			'id' => $id
		) : array();

		echo $this->update_to_db($data, $where, 'faq', true);
	}

}