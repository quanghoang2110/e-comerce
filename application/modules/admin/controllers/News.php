<?php

/**
 * Created by PhpStorm.
 * User: quanghv
 * Date: 12/13/2016
 * Time: 3:45 PM
 */
class News extends MBackendController
{

    function __construct()
    {
        $this->_permission_controller_name = 'nội dung';
        parent::__construct();
        $this->_load_data_from('news');
    }


    function index()
    {
        $this->_init_page();

        $category = $this->input->get('cate');
        $where = array();
        if ($category) {
            $where = array('category_id' => $category);
        }
        $this->_model->_where_not_in_array = [NEWS_TYPE_DISCOUNT];
        $this->_model->_where_row = 'type';
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'name'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count($where);

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page--;

        $this->load->model('model_news');
        $model = new Model_news();
        $vars['list'] = array();
        if ($total > 0)
            $vars['list'] = $model->get_list($this->_page, $this->_limit, $this->_search, $category);

        $this->_load_data_from('news_category');
        $vars['category'] = $this->_model->_get(array('valid' => 1), array('id' => 'asc'));
//		$this->_last_query();

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);

        $vars['user_id'] = $this->_sess_userid;

        $this->_module_vars = $vars;
        $this->_load_tmp(__FUNCTION__);
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {

            $id = $this->input->get('id');
            $vars = array();

            $this->_load_data_from('news_category');
            $vars['category'] = $this->_model->_get(array('valid' => 1), array('name' => 'asc'));

            $this->_load_data_from('product');
            $vars['products'] = $this->_model->_get(array('valid' => 1), array('name' => 'asc'), 'id,name');

            $info = array();
            if ($id) {
                $where = array(
                    'id' => $id
                );
                $this->_load_data_from('news');
                $info = $this->_model->_get_row($where);
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }
            $vars['data'] = $info;

            $this->_module_vars = $vars;
            $this->_load_tmp(__FUNCTION__);
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                'field' => 'input_name',
                'label' => 'Tiêu đề',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'input_image',
                'label' => 'Ảnh',
                'rules' => 'trim|required'
            )
        );

        if (!$this->_validation($config)) {
            $data['error'] = 1;
            $data['error_detail'] = strip_tags(validation_errors());
            $data['msg'] = "Lỗi dữ liệu";
            echo json_encode($data);
            return;
        } else {
            $type = $this->input->post('input_type');
            $video_link = $this->input->post('input_video_link');
            $content = $this->input->post('input_content');
            if (($type == 'VIDEO' && !$video_link) || ($type == 'CONTENT' && !$content)
                || ($type == 'MANUAL' && !$content) || ($type == 'DISCOUNT' && !$content)) {
                $data['error'] = 1;
                $data['error_detail'] = strip_tags(validation_errors());
                $data['msg'] = "Lỗi dữ liệu";
                echo json_encode($data);
                return;
            }
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $title = $this->input->post('input_name');
        $cate_id = $this->input->post('input_category');
        $type = $this->input->post('input_type');
        $desc = $this->input->post('input_desc');
        $content = $this->input->post('input_content', false);
        $video_link = $this->input->post('input_video_link');
        $image = $this->input->post('input_image');
        $position = $this->input->post('input_position');
        $hot = $this->input->post('input_hot');
        $valid = $this->input->post('input_valid');

        //seo meta
        $meta_title = $this->input->post('input_meta_title');
        $meta_desc = $this->input->post('input_meta_desc');

        $data = array(
            'name' => $title,
            'type' => $type,
            'category_id' => $cate_id,
            'image' => $image,
            'is_hot' => in_array($hot, array('on', 1)) ? 1 : 0,
            'valid' => in_array($valid, array('on', 1)) ? 1 : 0,
            'position' => $position,
            'meta_title' => $meta_title,
            'meta_desc' => $meta_desc
        );

        if ($type === 'CONTENT' || $type === 'MANUAL' || $type === 'DISCOUNT') {
            $product_related = $this->input->post('input_products_related');
            $str_product_related = $product_related ? implode(',', $product_related) : '';
            $data['products_related'] = $str_product_related.',';
            $data['desc'] = $desc;
            $data['content'] = html_entity_decode($content);
        } else {
            $data['video_link'] = $video_link;
        }

        $id = $this->_input_post('input_id', true);
        $where = $id ? array('id' => $id) : array();

        $now = date('Y-m-d H:i:s');
        if (!$id) $data['create_time'] = $now;
        else $data['update_time'] = $now;

        echo $this->update_to_db($data, $where, 'news', true);
    }

    function ajax_valid()
    {
        $type = $this->input->post('type');
        if ($type != "mega_menu") {
            parent::ajax_valid();
        } else {
            $id = $this->input->post("id", true);
            $valid = $this->input->post("valid", true);
            $valid = $valid == 1 ? 0 : 1;
            $data = array('is_mega_menu' => $valid);

            if ($id > 0) {
                echo $this->_ajax_update($id, $data);
            }
        }
    }
}