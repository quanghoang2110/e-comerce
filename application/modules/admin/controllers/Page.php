<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/10/2016
 * Time: 15:25
 */
class Page extends MBackendController
{
    function __construct()
    {
        $this->_permission_controller_name = "về chúng tôi";
        parent::__construct();
        $this->_load_data_from('page');
    }

    function index()
    {
        $this->_init_page();

        $this->_load_data_from('page');
        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'name'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page = $this->_page--;

        $vars['list'] = [];

        if ($total > 0)
            $vars['list'] = $this->_model->_get_limit("", array('id' => 'asc'), $this->_page, $this->_limit);

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->_module_vars = $vars;
        $this->_load_tmp(__FUNCTION__);
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $id = $this->input->get('id');

            $info = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $info = $this->_model->_get_top_one($where);
                if (!$info) redirect(BASE_PAGE_NOT_FOUND);
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }

            $vars['data'] = $info;

            $this->_module_vars = $vars;
            $this->_load_tmp(__FUNCTION__);
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                'field' => 'input_name',
                'label' => 'Tiêu đề',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'input_content',
                'label' => 'Nội dung',
                'rules' => 'trim|required'
            )
        );
        if (!$this->_validation($config)) {
            $data['error'] = 1;
            $data['error_detail'] = strip_tags(validation_errors());
            $data['msg'] = "Lỗi dữ liệu";
            echo json_encode($data);
            return;
        } else {
            $id = url_title($this->input->post("input_id"));
            $slug = strtolower(url_title($this->input->post("input_slug")));
            $slug = $slug ? $slug : get_urltitle($this->input->post("input_name"));
            $this->_load_data_from("slug");
            $where['name'] = $slug;
            if ($id) {
                $where['route !='] = 'public/page/content/' . $id;
            }
            if ($this->_model->_count($where)) {
//                $this->_last_query();
                $data['error'] = 1;
                $data['msg'] = "Short link đã được sử dụng!";
                echo json_encode($data);
            } else {
                $this->_update_general();
            }
        }
    }

    function _update_general()
    {
        $name = $this->_input_post('input_name');
        $slug = $this->_input_post('input_slug');
        $content = $this->input->post('input_content');
        $type = $this->input->post('input_type');

        //seo meta
        $meta_title = $this->_input_post('input_meta_title');
        $meta_desc = $this->_input_post('input_meta_desc');

        $slug = $slug ? $slug : get_urltitle($name);

        $data = array(
            'name' => $name,
            'slug' => $slug,
            'content' => $content,
            'meta_title' => $meta_title,
            'meta_desc' => $meta_desc,
            'type' => $type
        );

        $id = $this->_input_post('input_id', true);
        $where = $id ? array(
            'id' => $id
        ) : array();

        $response = $this->update_to_db($data, $where, 'page');
        $res_obj = json_decode($response);
        if ($res_obj->id) {
            $this->_load_data_from('slug');
            $this->_model->_insert_duplicate(array(
                'name' => $slug,
                'route' => 'public/page/content/' . $res_obj->id,
                'type' => 'page'
            ));
        }
        echo $response;
    }

    function ajax_delete()
    {
        parent::ajax_delete();
        $id = $this->input->post("id", true);
        $this->_load_data_from('slug');
        $this->_model->_delete(array(
            'route' => 'public/page/content/' . $id,
            'type' => 'page'
        ));
    }
}