<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: quanghv
 * Date: 02/09/2016
 * Time: 3:45 PM
 */
class Product_type extends MBackendController
{

	function __construct()
	{
		$this->_permission_controller_name = 'Loại sản phẩm';
		parent::__construct();
		$this->_load_data_from('product_type');
	}

	function index()
	{
		$list = $this->get_all_category();
//		$this->debug_data($list, 1);

		$vars['list'] = $list['list'];
		$vars['level'] = 2;//$list['level'];

		$vars['user_id'] = $this->_sess_userid;
		$this->disable_paging();

		$this->_module_vars = $vars;
		$this->_load_tmp(__FUNCTION__);
	}

	function update()
	{

		if ($this->input->post()) {
			$type = $this->input->post('type');
			if ($type == 'choose_level') {
				$level1_id = $this->input->post('level1_id');
				if ($level1_id) {
					$data['list'] = $this->_model->_get(array('valid' => 1, 'level' => 2, 'parent_id' => $level1_id, 'id!=' => $this->input->post('id')));
					$this->load->vars($data);
					$this->load->helper('mfw_template_helper');
					load_view("admin/{$this->_theme}/product_type/level_res");
//					$this->load->view("admin/{$this->_theme}/product_type/level_res", $data);
				}
			} else if ($type == "cate_level") {
				$this->load->view("admin/{$this->_theme}/product_type/sortable", $list = $this->get_all_category());
			} else $this->ajax_update();
		} else {
			$id = $this->input->get('id');
			$this->_content = __FUNCTION__;

			$data = "";
			if (trim($id) > 0) {
				$where = array(
					'id' => $id
				);
				$data = $this->_model->_get_row($where);
			}

			//cho phep cap nhat level danh muc
			$vars['level_avaiable'] = 2;

			$vars['categories1'] = $this->_model->_get(array('valid' => 1, 'level' => 1, 'id!=' => $id));
			if ($id && $data) {
				$c_level = $data->level;
				if ($c_level > 1) {
					$vars['categories2'] = $this->_model->_get(array('valid' => 1, 'level' => 2, 'parent_id' => $data->parent_id, 'id!=' => $id));
				}
				if ($c_level < 3) {
					$where = array('parent_id' => $id);
					$exists_sub = $this->_model->_count($where);

					if ($exists_sub) {
						if ($c_level == 1) $vars['level_avaiable'] = 1;
						else $vars['level_avaiable'] = 2;
					}
				}
				$vars['id'] = $id;
				$this->_module_desc = "Cập nhật";
			} else {
				$vars['id'] = 0;
				$this->_module_desc = "Thêm mới";
			}

			$vars['data'] = $data;

			$this->_module_vars = $vars;
			$this->_load_tmp(__FUNCTION__);
		}
	}

	function ajax_update()
	{
		$config = array(
			array(
				'field' => 'input_name',
				'label' => 'Tên loại sản phẩm',
				'rules' => 'trim|required|max_length[255]'
			)
		);
		if (!$this->_validation($config)) {
			$data['error'] = 1;
			$data['error_detail'] = strip_tags(validation_errors());
			$data['msg'] = "Lỗi dữ liệu";
			echo json_encode($data);
			return;
		} else {
			$this->_update_general();
		}
	}

	function _update_general()
	{
		$err = 0;
		$title = $this->input->post('input_name');
		$desc = $this->input->post('input_desc');
		$image = $this->input->post('input_image');
		$position = $this->input->post('input_position');
		$valid = $this->input->post('input_valid');

		$level = $this->input->post('input_level');
		$level1_id = $this->input->post('input_level1_id');
		$level2_id = $this->input->post('input_level2_id');

		$meta_title = $this->input->post('input_meta_title');
		$meta_desc = $this->input->post('input_meta_desc');

		$data = array(
			'name' => $title,
			'level' => $level,
			'parent_id' => $level == 1 ? NULL : ($level == 2 ? $level1_id : $level2_id),
			'image' => $image,
			'desc' => $desc,
			'position' => $position > 0 ? $position : 99,
			'valid' => in_array($valid, array('on', 1)) ? 1 : 0,
			'meta_title' => $meta_title,
			'meta_desc' => $meta_desc
		);

		$id = $this->_input_post('input_id', true);
		$where = $id ? array('id' => $id) : array();

		echo $this->update_to_db($data, $where, 'product_type', true);
	}

	/**
	 * get all data category
	 * @return array
	 */
	function get_all_category()
	{
		$order_arr = array(
			'level' => 'asc',
			'position' => 'asc',
			"name" => "asc"
		);

		$this->_load_data_from('product_type');
		$list = $this->_model->_get("", $order_arr);

		return explore_cate_level($list);
	}

	//pending
	function data_to_file($str)
	{
		if (!write_file(APPPATH . 'm_cache/product_type', $str)) {
//			echo 'Unable to write the file';
		} else {
//			echo 'File written!';
		}
	}

	/*
	 * change category level
	 */
	function ajax_change_level()
	{
	}
}