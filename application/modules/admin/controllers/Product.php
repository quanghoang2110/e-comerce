<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: quanghv
 * Date: 12/13/2016
 * Time: 3:45 PM
 */
class Product extends MBackendController
{

    function __construct()
    {
        $this->_permission_controller_name = 'sản phẩm';
        parent::__construct();
        $this->_load_data_from('product');
    }


    function index()
    {
        $this->_init_page();
        $this->_limit = 10;
        $category = $this->input->get('cate');
        $category_child_id = array();
        if ($category) {
            $this->load->model('model_product');
            $model = new Model_product();
            $category_child_id = $model->_get_category_children($category);
        }
        $this->load->model('model_product');
        $model = new Model_product();
        $total = count($model->get_list(1, 10000000, $this->_search, $category_child_id));

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page--;

        $this->load->model('model_product');
        $model = new Model_product();
        $vars['list'] = array();
        if ($total > 0)
            $vars['list'] = $model->get_list($this->_page, $this->_limit, $this->_search, $category_child_id);

        $this->_load_data_from('product_category');
        $order_arr = array(
            "name" => "asc"
        );
        $list = $this->_model->_get(array('valid' => 1), $order_arr);
        $vars['category'] = $list;// = explore_cate_level($list)['list'];
        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit . "&cate=". $category ."&search=" . $this->_search;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);

        $vars['user_id'] = $this->_sess_userid;


        $this->_module_vars = $vars;
        $this->_load_tmp(__FUNCTION__);
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {
            $id = $this->input->get('id');
            $vars = array();

            $this->_load_data_from('product_category');
            $order_arr = array(
                "name" => "asc"
            );
            $list = $this->_model->_get(array('valid' => 1), $order_arr);
            $vars['category'] = $list;// explore_cate_level($list)['list'];
            $vars['data'] = array();
            if ($id) {
                $where = array(
                    'id' => $id
                );
                $this->_load_data_from('product');
                $vars['data'] = $this->_model->_get_row($where);
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }


            $this->_load_data_from('trademark');
            $vars['trademark'] = $this->_model->_get(['valid' => 1]);

            $this->_load_data_from('news');
            $vars['videos'] = $this->_model->_get(['valid' => 1, 'type' => 'VIDEO']);

            $this->_load_data_from('product');
            $vars['gifts'] = $this->_model->_get(['valid' => 1]);

            $this->_load_data_from('brand');
            $vars['brands'] = $this->_model->_get(['valid' => 1]);

            $this->_module_vars = $vars;
            $this->_load_tmp(__FUNCTION__);
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                'field' => 'input_name',
                'label' => 'Tên sản phẩm',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                "field" => "input_image[]",
                "label" => "Ảnh sản phẩm",
                "rules" => "trim|required"
            )
        );
        $flag = $this->_validation($config);

        if (!$flag) {
            $data['error'] = 1;
            $data['error_detail'] = strip_tags(validation_errors());
            $data['msg'] = "Lỗi dữ liệu";
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $err = 0;
        $title = $this->input->post('input_name');
        $code = $this->input->post('input_code');
        $category = $this->input->post('input_category');
        $desc = $this->input->post('input_desc');
        $content2 = $this->input->post('input_content2', false);
        $hot = $this->input->post('input_is_hot');//is_hot
        $is_new = $this->input->post('input_is_new');//is_new
        $valid = $this->input->post('input_valid');
        $position = $this->input->post('input_position');
        $is_empty = $this->input->post('input_empty');
        $best_seller = $this->input->post('input_is_best_seller');
        $weight = $this->input->post('input_weight');
        $from = $this->input->post('input_from');
        $manufacturer = $this->input->post('input_manufacturer');
        $trademark = $this->input->post('input_trademark');
        $video = $this->input->post('input_video');
        $gift = $this->_input_post('input_gift');
        $brand = $this->_input_post('input_brand');

        //seo meta
        $meta_title = $this->input->post('input_meta_title');
        $meta_desc = $this->input->post('input_meta_desc');

        $image = $this->input->post('input_image[]');
        $json_image = $image ? json_encode($image) : NULL;

        $price_1 = $this->input->post('input_price_1');
        $price_2 = $this->input->post('input_price_2');
        $video_related = $this->input->post('input_video_related');

        $cate_info = explode("@", $category);
        $cate_id = $cate_info[1];
        $cate_slug = $cate_info[0];
        $percent_discount = !empty($price_2) ? intval((($price_1 - $price_2)/$price_1) * 100) : null;

        $data = array(
            'name' => $title,
            'category_id' => $cate_id,
            'category_slug' => $cate_slug,
            'code' => $code,
            'image' => $json_image,
            'video' => $video,
            'desc' => html_entity_decode($desc),
            'content' => html_entity_decode($content2),
            'position' => $position,
            'size' => $this->_input_post('input_size'),
            'is_hot' => in_array($hot, array('on', 1)) ? 1 : 0,
            'price_1' => $price_1,
            'price_2' => $price_2,
            'pice' => $this->input->post('input_pice'),
            'is_new' => in_array($is_new, array('on', 1)) ? 1 : 0,
            'is_best_seller' => in_array($best_seller, array('on', 1)) ? 1 : 0,
            'is_empty' => in_array($is_empty, array('on', 1)) ? 1 : 0,
            'is_discount' => in_array($this->input->post('input_is_discount'), array('on', 1)) ? 1 : 0,
            'percent_discount' => $percent_discount,
            'star' => $this->input->post('input_star'),
            'valid' => in_array($valid, array('on', 1)) ? 1 : 0,
            'meta_title' => $meta_title,
            'meta_desc' => $meta_desc,
            'weight' => $weight,
            'from' => $from,
            'manufacturer' => $manufacturer,
            'trademark' => $trademark,
            'brand' => $brand,
            'video_related' => json_encode($video_related),
            'gift' => json_encode($gift)
        );


        $id = $this->_input_post('input_id', true);
        $where = $id ? array('id' => $id) : array();

        $now = date('Y-m-d H:i:s');
        if (!$id) $data['create_time'] = $now;
        else $data['update_time'] = $now;

        echo $this->update_to_db($data, $where, 'product', true);
    }

    /**
     * thay doi type san pham
     */
    function ajax_valid()
    {
        if ($this->_permission_edit) {
            $id = $this->input->post("id", true);
            $valid = $this->input->post("valid", true);
            $valid = $valid == 1 ? 0 : 1;
            $type = $this->input->post('type');
            if (in_array($type, array("hot", "valid", "is-home"))) {
                parent::ajax_valid();
            } else {
                $data = array();
                switch ($type) {
                    case 'is_discount'://giam gia
                        $data = array('is_discount' => $valid);
                        break;
                    case 'is_empty'://het hang
                        $data = array('is_empty' => $valid);
                        break;
                    case 'is_new'://san pham moi
                        $data = array('is_new' => $valid);
                        break;
                    case 'is_best_seller'://ban chay nhat
                        $data = array('is_best_seller' => $valid);
                        break;
                    case 'is_popular':
                        //san pham pho bien
                        $data = array('is_popular' => $valid);
                        break;
                    case 'is_mega_menu':
                        //san pham pho bien
                        $data = array('is_mega_menu' => $valid);
                        break;
                }
                if ($id > 0 && $data) echo $this->_ajax_update($id, $data);
            }
        }
    }

}