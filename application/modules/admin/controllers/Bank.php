<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Bank extends MBackendController
{
	function __construct()
	{
		$this->_permission_controller_name = "Tài khoản ngân hàng";
		parent::__construct();
		$this->_load_data_from('bank');
	}

	function index()
	{
		if ($this->input->post()) {
			$this->ajax_update();
			//comment
		} else {
			$this->_init_page();
			$query_arr = array(
				'keyword' => $this->_search,
				'search_row' => 'name'
			);
            $this->_set_query($query_arr);
			$total = $this->_model->_count();

			if ($this->_page > ceil($total / $this->_limit))
				$this->_page--;

			$order_arr = array(
				"id" => "desc"
			);
			$vars['list'] = array();
			if ($total > 0)
				$vars['list'] = $this->_model->_get_limit(array(), $order_arr, $this->_page, $this->_limit, "*");

			$url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

			$vars['paging_info'] = $this->_get_page_info($total);
			$vars['paging'] = get_paging($url, $total, $this->_limit);

			$vars['user_id'] = $this->_sess_userid;

			$this->_module_vars = $vars;
			$this->_load_tmp(__FUNCTION__);
		}
	}

	function update()
	{
		$data['admin_group_data'] = array();
		$id = $this->input->post('id');

		$data['permission_view'] = $this->check_permission($this->_permission_view) ? true : false;
		$data['permission_edit'] = $this->check_permission($this->_permission_edit) ? true : false;
		$data['permission_delete'] = $this->check_permission($this->_permission_delete) ? true : false;

		$data['admin_data'] = array();
        if (trim($id) > 0) {
            $where = array(
                'id' => $id
            );
            $data['admin_data'] = $info = $this->_model->_get_row($where);
            $data['title'] = "Cập nhật tài khoản ngân hàng";
        } else {
            $data['title'] = "Thêm tài khoản ngân hàng";
        }

		$this->load->vars($data);
		load_view($this->_theme_view . '/bank/update');
	}

	function ajax_update()
	{

		$config = array(
			array(
				'field' => 'name',
				'label' => 'Tên chủ tài khoản',
				'rules' => 'trim|required|max_length[255]'
			),
			array(
				'field' => 'bank',
				'label' => 'Ngân hàng',
				'rules' => 'trim|required'
			),
            array(
                'field' => 'number_card',
                'label' => 'Số tài khoản',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'branch',
                'label' => 'Chi nhánh',
                'rules' => 'trim|required'
            )
		);
		if (!$this->_validation($config)) {
			$data['error'] = 1;
			$data['msg'] = strip_tags(validation_errors());
			$data['msg_error'] = "Vui lòng nhập đầy đủ thông tin";
			echo json_encode($data);
			return;
		} else {
			$this->_update_general();
		}
	}

	function _update_general()
	{
		$err = 0;
		$msg = '';
		$name = trim($this->input->post('name'));
		$valid = trim($this->input->post('input_valid'));
		$bank = $this->input->post('bank');
		$number_card = $this->input->post('number_card');
		$branch = $this->input->post('branch');
		$data = array(
			'name' => $name,
			'bank_name' => $bank,
			'number_card' => $number_card,
            'branch' => $branch,
			'valid' => $valid == 'on' ? 1 : 0,
            'create_time' => date('Y-m-d H:i:s')
		);
        $this->_load_data_from('user');
		$id = $this->input->post('hdID');
        if (!$id) {//them moi tai khoan admin
            $this->_load_data_from('bank');
            $id = $this->_model->_insert($data);
            if (!$id) {
                $err = 1;
                $msg = 'Đã có lỗi xảy ra';
            } else {
                $msg = 'Thêm mới thành công';
                //admin log
                $this->admin_log(BASE_ADMIN_ACTION_INSERT, json_encode(array_merge(array('id' => $id), $data)), $id);
            }
        } else {
            $this->_model->_update($data, array('id' => $id));
            $msg = 'Cập nhật thông tin thành công!';

            $this->admin_log(BASE_ADMIN_ACTION_INSERT, json_encode(array_merge(array('id' => $id), $data)), $id);
        }
		$result = array(
			'error' => $err,
			'msg' => $msg,
			'id' => $id
		);
		echo json_encode($result);
	}
}