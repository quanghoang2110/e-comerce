<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Class Profile
 */
class Profile extends MBackendController
{
	function __construct()
	{
		//ten chuc nang
		$this->_permission_controller_name = "profile";
		parent::__construct();
		$this->_load_data_from(BASE_TABLE_ADMIN);
	}

	function index()
	{
		$this->_init_page();

		$this->_module_title = "Profile";
		$this->_module_desc = "thông tin tài khoản";
		$this->_page_folder = "";
		$this->_content = 'profile';

		$vars['info'] = $info = $this->_model->_get_row(array('id' => $this->_sess_userid));

		$this->_load_data_from(BASE_TABLE_ADMIN_LOG);
		$vars['logs'] = $this->_model->_get_limit(array('admin_id' => $this->_sess_userid, 'create_time >=' => date('Y-m-d', time() - 3 * 24 * 60 * 60)), array('id' => 'desc'), 1, 100);

		$this->use_theme_custom();
		$this->them_contain_form();

		$this->_module_vars = $vars;
		$this->_load_tmp();
	}

	function update()
	{
		$img = $this->input->post('input_image', true);
		$fullname = $this->input->post('input_fullname', true);
		$data = array(
			'img' => $img,
			'fullname' => $fullname
		);

		//set new session
		$this->session->set_userdata(BASE_SESSION_ADMIN_IMG, $img);
		$this->session->set_userdata(BASE_SESSION_ADMIN_FULLNAME, $fullname);

		$where = array('id' => $this->_sess_userid);

		echo $this->update_to_db($data, $where, 'admin', true);
	}


}