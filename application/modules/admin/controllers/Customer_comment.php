<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * cam nhan tu khach hang
 * Created by PhpStorm.
 * Date: 12/13/2016
 * Time: 3:45 PM
 */
class Customer_comment extends MBackendController
{

	function __construct()
	{
		$this->_permission_controller_name = 'cảm nhận khách hàng';
		parent::__construct();
		$this->_load_data_from('customer_comment');
	}


	function index()
	{
		$this->_init_page();

		$query_arr = array(
			'keyword' => $this->_search,
			'search_row' => 'title'
		);
		$this->_set_query($query_arr);
		$total = $this->_model->_count();

		if ($this->_page > ceil($total / $this->_limit))
			$this->_page--;

		$order_arr = array(
			"id" => "desc"
		);
		$vars['list'] = array();
		if ($total > 0)
			$vars['list'] = $this->_model->_get_limit("", $order_arr, $this->_page, $this->_limit, "*");

		$url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

		$vars['paging_info'] = $this->_get_page_info($total);
		$vars['paging'] = get_paging($url, $total, $this->_limit);

		$vars['user_id'] = $this->_sess_userid;

		$this->_module_vars = $vars;
		$this->_load_tmp(__FUNCTION__);
	}

	function update()
	{
		if ($this->input->post()) {
			$this->ajax_update();
		} else {
			$id = $this->input->get('id');
			$info = array();
			if (trim($id) > 0) {
				$where = array(
					'id' => $id
				);
				$info = $this->_model->_get_top_one($where);
				$this->_module_desc = "Cập nhật";
			} else {
				$this->_module_desc = "Thêm mới";
			}
			$vars['data'] = $info;
			$this->_module_vars = $vars;
			$this->_load_tmp(__FUNCTION__);
		}
	}

	function ajax_update()
	{
		$config = array(
			array(
				'field' => 'input_name',
				'label' => 'Tên khách hàng',
				'rules' => 'trim|required|max_length[255]'
			),
			array(
				'field' => 'input_image',
				'label' => 'Ảnh',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'input_career',
				'label' => 'Nghề nghiệp',
				'rules' => 'trim|required|max_length[255]'
			),
			array(
				'field' => 'input_desc',
				'label' => 'Nội dung',
				'rules' => 'trim|required'
			)
		);

		if (!$this->_validation($config)) {
			$data['error'] = 1;
			$data['error_detail'] = strip_tags(validation_errors());
			$data['msg'] = "Lỗi dữ liệu";
			echo json_encode($data);
			return;
		} else {
			$this->_update_general();
		}
	}

	function _update_general()
	{
		$err = 0;
		$name = $this->input->post('input_name');
		$career = $this->input->post('input_career');
		$position = $this->input->post('input_position');
		$content = $this->input->post('input_desc');
		$image = $this->input->post('input_image');
		$valid = $this->input->post('input_valid');
		$data = array(
			'name' => $name,
			'career' => $career,
			'image' => $image,
			'desc' => html_entity_decode($content),
			'position' => $position,
			'valid' => in_array($valid, array(1, 'on')) ? 1 : 0
		);

		$id = $this->_input_post('input_id', true);
		$where = $id ? array(
			'id' => $id
		) : array();

		echo $this->update_to_db($data, $where, 'customer_comment', true);
	}

}