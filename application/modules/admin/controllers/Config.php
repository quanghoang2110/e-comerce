<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/6/2016
 * Time: 11:01
 */
class Config extends MBackendController
{
	function __construct()
	{
		$this->_sess_required = false;
		parent::__construct();
	}

	/**
	 * first: run config for admin sys
	 */
	function index()
	{
		$this->menu_conf();
	}

	function delete_setup_files()
	{
		$strs = file(APPPATH . 'config/routes.php');
//		print_r($strs);
		unset($strs[count($strs) - 1]);
		$routes_lines = "";
		foreach ($strs as $position => $line) {
			$routes_lines .= $line;
		}
		file_put_contents(APPPATH . 'config/routes.php', $routes_lines);
		unlink(APPPATH . 'controllers/Setup.php');
		rrmdir(APPPATH . 'setup');
		rrmdir(APPPATH . 'views/setup');
	}

	function admin_routes()
	{
		//same MBackendController
		//$this->update_routes();

		$this->load->helper('file');
		$this->_load_data_from(BASE_TABLE_ADMIN_MENU);

		$this->_model->delete_invalid_admin_menu();

		$menu = $this->_model->_get(array('valid' => 1, 'is_group' => NULL));
		$version = ADMIN_MODULE.'/';
		$routes = "<?php \n";

		foreach ($menu as $item) {
			$routes .= "\$route['" . $item->url . "']='{$version}{$item->category}';\n";
			$routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_DETAIL . "']='{$version}{$item->category}/detail';\n";
			$routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_UPDATE . "']='{$version}{$item->category}/update';\n";
			$routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_UPDATE_POSITION . "']='{$version}{$item->category}/ajax_position';\n";
			$routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_VALID . "']='{$version}{$item->category}/ajax_valid';\n";
			$routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_DELETE . "']='{$version}{$item->category}/ajax_delete';\n";
		}

		if (!write_file(APPPATH . 'config/routes_mfw_db.php', $routes)) {
//			echo 'Unable to write the file';
			echo '<p class="text-danger">Unable to write ROUTES_BASE_DB file!</p>';
		} else {
//			echo 'File routes_go_db is written!';
			echo "<p class=\"text-success\">File ROUTES_BASE_DB is written!</p>";
		}
	}

	function admin_menu()
	{
		$this->_delete_unused_permission();

		$this->_load_data_from(BASE_TABLE_ADMIN_MENUHEAD);
		$head = $this->_model->_get(array('valid' => 1), array('position' => 'asc'));

		$head = $head ? $head : ["LABEL"];

		$this->_load_data_from(BASE_TABLE_ADMIN_MENU);
		foreach ($head as $index => $item) {
			$menu = $this->_model->_get(array('valid' => 1, 'menuhead_id' => $item->id), array('position' => 'asc'));
			if ($menu) {
				$group = $menu;
				foreach ($menu as $i => $v) {
					$category_group = $v->category;
					$group[$i]->is_group = $v->is_group == 1 ? true : false;
					if ($group[$i]->is_group) {
						$menux = $this->_model->_get(array('menugroup_id' => $v->id, 'valid' => 1), array('position' => 'asc'));
						foreach ($menux as $a => $b) {
							$category_group .= $b->category;
							if ($a < count($menux) - 1) {
								$category_group .= ",";
							}
						}
						$group[$i]->menu = $menux;
					}
					$group[$i]->role = $category_group;
				}
				$head[$index]->menu = $group;
			}
		}

		$str = json_encode($head);

		if (!write_file(BASE_CONF_FILE_MENU_ROOT, $str)) {
//			echo 'Unable to write the file';
			echo '<p class="text-danger">Unable to write MENU for ROOT file!</p>';
		} else {
//			echo 'File menu root is written!';
			echo "<p class=\"text-success\">File MENU for ROOT is written!</p>";

			//config for group
			$this->admin_menugroup();
//			$this->debug_data($head);
		}
	}

	function _delete_unused_permission(){
		$dir = MODULES_PATH_ADMIN_CONTROLLER;
		$files = scandir($dir);

		$category_menu = array();
		foreach($files as $file){
			if(strpos($file, '.php')!==false){
				$category_menu[] = str_replace('.php', '', strtolower($file));
			}
		}
//		print_r($category_menu);
		//delete all unused permission
		$this->_load_data_from(BASE_TABLE_ADMIN_ROLE);
		$this->_model->_where_row = "category";
		$this->_model->_where_not_in_array = $category_menu;
		$this->_model->_delete();
		//$this->_last_query();
		//delete all unused menu
		$this->_load_data_from(BASE_TABLE_ADMIN_MENU);
		$this->_model->_where_row = "category";
		$this->_model->_where_not_in_array = $category_menu;
		$this->_model->_delete(array('category !='=> NULL));
//		$this->_last_query();
	}

	/**
	 * write menu for group
	 */
	function admin_menugroup()
	{
//		$menu_root = json_decode(file_get_contents(BASE_CONF_FILE_MENU_ROOT));

		$this->_load_data_from(BASE_TABLE_ADMINGROUP);

		foreach ($this->_model->_get(array('valid' => 1)) as $group) {
			$group_pers = $this->group_permission($group->id);

			if ($group_pers) {

				$tmp = json_decode(file_get_contents(BASE_CONF_FILE_MENU_ROOT));

				//read from menu_root
				foreach ($tmp as $i0 => $adm) {
					$show_this = false;

					if (isset($adm->menu)) {
						foreach ($adm->menu as $i1 => $adm_1) {

							if ($adm_1->is_group) {
								$show_this_1 = false;
								foreach ($adm_1->menu as $i2 => $adm_2) {
									if (in_array($adm_2->id, $group_pers)) {
										$show_this = true;
										$show_this_1 = true;
									} else {
										unset($adm_1->menu[$i2]);
									}
								}
								if (!$show_this_1) unset($adm->menu[$i1]);
							} else {
								if (in_array($adm_1->id, $group_pers)) {
									$show_this = true;
								} else {
									unset($adm->menu[$i1]);
								}
							}
						}
					}
					if (!$show_this) unset($tmp[$i0]);
				}

				if (!write_file(BASE_CONF_FILE_MENUGROUP_FOLDER . $group->id, json_encode($tmp))) {
					echo '<p class="text-danger">Unable to write the file ' . $group->id . '</p>';
				} else {
					echo "<p class=\"text-success\">File MENU for group {$group->id} is written!</p>";
					//$this->debug_data($tmp);
				}

			}
		}
	}

	function group_permission($group_id)
	{
		$count = 0;
		$this->load->model('model_permission_group');
		$model = new Model_permission_group();
		$query = "valid=1 and category in (";

		foreach ($model->get_permission_read($group_id) as $item) {
			$query .= "'" . $item->category . "',";
			$count++;
		}
		$str = [];
		if ($count > 0) {

//			$query = substr($query, 0, -1) . ")";

			//permission cua group
			$this->_load_data_from(BASE_TABLE_ADMIN_MENU);
			$this->_model->_query = substr($query, 0, -1) . ")";
			$menu = $this->_model->_get(
				'',
				array('position' => 'asc', 'id' => 'asc'),
				'id'
			);

			foreach ($menu as $item) {
				$str[] = $item->id;
			}
		}
		return $str;
//		$this->debug_data($str);
	}

	function menu_conf()
	{
		$opt_type = $this->input->post('opt');
		if ($opt_type) {
			$id = $this->input->post('id');
			switch ($opt_type) {
				case 'label':
					$this->_load_data_from(BASE_TABLE_ADMIN_MENUHEAD);
					$data = [
						'name' => $this->input->post('name'),
						'position' => $this->input->post('position'),
						'valid' => $this->input->post('valid')
					];
					if ($id) $this->_model->_update($data, ['id' => $id]);
					else $this->_model->_insert($data);
					break;
				case 'menu':
					$this->_load_data_from(BASE_TABLE_ADMIN_MENU);
					$new_group = $this->input->post('group');
					if ($new_group) {
						$data = [
							'menuhead_id' => NULL,
							'position' => $this->input->post('position'),
							'menugroup_id' => $new_group,
							'icon' => $this->input->post('icon'),
							'valid' => $this->input->post('valid')
						];
					} else {
						$data = [
							'menuhead_id' => $this->input->post('label'),
							'position' => $this->input->post('position'),
							'menugroup_id' => NULL,
							'icon' => $this->input->post('icon'),
							'valid' => $this->input->post('valid')
						];
					}
					$this->_model->_update($data, ['id' => $id]);
					break;
				case 'menugroup':
					$this->_load_data_from(BASE_TABLE_ADMIN_MENU);
					$data = [
						'name' => $this->input->post('name'),
						'menuhead_id' => $this->input->post('label'),
						'position' => $this->input->post('position'),
						'menugroup_id' => NULL,
						'icon' => $this->input->post('icon'),
						'valid' => $this->input->post('valid'),
						'is_group' => true
					];
					if ($id) $this->_model->_update($data, ['id' => $id]);
					else $this->_model->_insert($data);
					break;
			}
		} else {
			if ($this->input->get('i')) {
				$id = $this->input->get('i');
				$method = $this->input->get('m');
				$focus = $this->input->get('t');
				if ($method == 'delete') {
					if ($focus == 'label') {
						$this->_load_data_from(BASE_TABLE_ADMIN_MENUHEAD);
						$this->_model->_delete(['id' => $id]);
					} else if ($focus == 'group') {
						$this->_load_data_from(BASE_TABLE_ADMIN_MENU);
						$this->_model->_delete(array('id' => $id));
					}
				}
			}
		}

		$this->_load_data_from(BASE_TABLE_ADMIN_MENUHEAD);
		$vars['heads'] = $this->_model->_get();

		$menu = $this->get_root_menu();
		$this->_load_data_from(BASE_TABLE_ADMIN_MENU);
		foreach ($menu as $i => $mn) {
			$menu[$i]->sub = [];
			if ($mn->is_group) $menu[$i]->sub = $this->_model->_get(['menugroup_id' => $mn->id, 'deleted' => 0], ['position' => 'asc']);
		}
//		$this->debug_data($menu);
		$vars['menu'] = $menu;

		$this->load->vars($vars);
		$this->load->view('admin/config_menu');
	}

	/**
	 * @return mixed
	 */
	function get_root_menu()
	{
		$this->db->select('a.*');
		$this->db->select("b.name as head");
		$this->db->from(BASE_TABLE_ADMIN_MENU . ' a');
		$this->db->join(BASE_TABLE_ADMIN_MENUHEAD . ' b', 'a.menuhead_id=b.id', 'left');
		$this->db->where('a.menugroup_id', NULL);
		$this->db->where('a.deleted', 0);
		$this->db->order_by('a.menuhead_id', 'asc');
		$this->db->order_by('a.position', 'asc');
		return $this->db->get()->result();
	}


	function all_form_element()
	{
		$this->_module_title = "All FORM ELEMENT";
		$this->_content = 'all_form_element';
		$this->use_theme_index(false);
		$this->disable_pagin_action_button();
		$this->them_contain_form();
		$this->_page_folder = "";
		$this->_load_tmp();
	}
}