<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: hung
 * Date: 12/13/2016
 * Time: 3:45 PM
 */
class Contact extends MBackendController
{

	function __construct()
	{
		$this->_permission_controller_name = 'Liên hệ';
		parent::__construct();
		$this->_load_data_from('contact');
	}


	function index()
	{
		$this->_init_page();
		$this->_content = 'index';

		$query_arr = array(
			'keyword' => $this->_search,
			'search_row' => 'title'
		);
		$this->_set_query($query_arr);
		$total = $this->_model->_count();

		if ($this->_page > ceil($total / $this->_limit))
			$this->_page--;

		$order_arr = array(
			"id" => "desc"
		);
		$vars['list'] = array();
		if ($total > 0)
			$vars['list'] = $this->_model->_get_limit("", $order_arr, $this->_page, $this->_limit, "*");

		$url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

		$vars['paging_info'] = $this->_get_page_info($total);
		$vars['paging'] = get_paging($url, $total, $this->_limit);

		//permission
		$vars['permission_edit'] = $this->check_permission($this->_permission_edit);
		$vars['permission_delete'] = $this->check_permission($this->_permission_delete);

		$vars['user_id'] = $this->_sess_userid;
		$this->disable_pagin_action_button();

		$this->_module_vars = $vars;
		$this->_load_tmp();
	}

}