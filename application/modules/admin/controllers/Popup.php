<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: quanghv
 * Date: 02/09/2016
 * Time: 3:45 PM
 */
class Popup extends MBackendController
{

    private $_table = "popup";

    function __construct()
    {
        $this->_permission_controller_name = 'Popup';
        parent::__construct();
        $this->_load_data_from($this->_table);
    }

    function index()
    {
        $this->_init_page();

        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'name'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page--;

        $order_arr = array(
            "id" => "desc"
        );
        $vars['list'] = array();
        if ($total > 0)
            $vars['list'] = $this->_model->_get_limit("", $order_arr, $this->_page, $this->_limit, "*");

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);

        $vars['user_id'] = $this->_sess_userid;

        $this->_module_vars = $vars;
        $this->_load_tmp(__FUNCTION__);
    }

    function update()
    {
        $this->_load_data_from($this->_table);
        if ($this->input->post()) {
            $this->ajax_update();
        } else {

            $id = $this->input->get('id');
            $info = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $info = $this->_model->_get_top_one($where);
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }
            $vars['data'] = $info;
            $vars['category'] = $this->_model->_get();
            $this->_module_vars = $vars;
            $this->_load_tmp(__FUNCTION__);
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                'field' => 'input_name',
                'label' => 'Tên chuyên mục',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'input_link',
                'label' => 'Short link',
                'rules' => 'trim|required|max_length[255]'
            )
        );
        if (!$this->_validation($config)) {
            $data['error'] = 1;
            $data['error_detail'] = strip_tags(validation_errors());
            $data['msg'] = "Lỗi dữ liệu";
            echo json_encode($data);
            return;
        } else {
            $this->_update_general();
        }
    }

    function _update_general()
    {
        $title = $this->input->post('input_name');
        $link = $this->input->post('input_link');
        $image = $this->input->post('input_image');
        $valid = $this->input->post('input_valid');


        $data = array(
            'name' => $title,
            'link' => $link,
            'image' => $image,
            'valid' => in_array($valid, array('on', 1)) ? 1 : 0,
        );

        $id = $this->_input_post('input_id', true);
        if ($id) {
            $where = array(
                'id' => $id
            );
        } else {
            $where = [];
        }

        $response = $this->update_to_db($data, $where, $this->_table);
        $res_obj = json_decode($response);
        echo $response;
    }

    function ajax_delete()
    {
        parent::ajax_delete();
    }

}