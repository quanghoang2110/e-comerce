<?php defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Created by PhpStorm.
 * User: quanghv
 * Date: 02/09/2016
 * Time: 3:45 PM
 */
class Order extends MBackendController
{
    private $_table = "order";

    function __construct()
    {
        $this->_permission_controller_name = 'Đơn hàng';
        parent::__construct();
        $this->_load_data_from($this->_table);
    }

    function index()
    {
        $this->_init_page();

        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'phone'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page--;

        $order_arr = array(
            "id" => 'desc'
        );
        $vars['list'] = array();
        if ($total > 0) {
            $vars['list'] = $list = $this->_model->_get_limit("", $order_arr, $this->_page, $this->_limit, "*");
            $this->load->model('Model_order');
            $model = new Model_order();
            foreach ($list as $index => $item) {
                $list[$index]->subs = $model->get_order_detail($item->id);
            }
        }

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);
        $this->disable_pagin_action_button();

        $this->_module_vars = $vars;
        $this->_load_tmp(__FUNCTION__);
    }

    function update()
    {
        if ($this->input->post()) {
            $this->ajax_update();
        } else {

            $id = $this->input->get('id');
            $info = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $info = $this->_model->_get_top_one($where);
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }
            $this->_load_data_from('agency_category');
            $vars['category'] = $this->_model->_get(array('valid' => 1), array('name' => 'asc'));
            $vars['data'] = $info;
            $this->_module_vars = $vars;
            $this->_load_tmp(__FUNCTION__);
        }
    }

    function ajax_update()
    {
        $this->_update_general();
    }

    function _update_general()
    {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $data = array(
            'is_finished' => $status,
        );
        $where = [];
        if ($id) {
            $where = array(
                'id' => $id
            );
        }

        echo $this->update_to_db($data, $where);
    }

}