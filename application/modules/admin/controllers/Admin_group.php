<?php

/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */
class Admin_group extends MBackendController
{
	function __construct()
	{
		$this->_permission_controller_name = "nhóm quản trị";
		parent::__construct();
		$this->_load_data_from(BASE_TABLE_ADMINGROUP);
	}

	function index()
	{

		$query_arr = array(
			'keyword' => $this->_search,
			'search_row' => 'username'
		);
		$this->_set_query($query_arr);

		$order_arr = array(
			"id" => "asc"
		);

		$vars['list'] = $this->_model->_get('', $order_arr);

		$this->disable_paging();

		$this->_module_vars = $vars;
		$this->_load_tmp(__FUNCTION__);
	}

	function update()
	{
		if ($this->input->post()) {
			$data = array(
				'name' => $this->input->post('name')
			);

			$error = $this->_ajax_update($this->input->post('input_id'), $data) ? 0 : 1;
			echo json_encode(array('err' => $error));
		}
//        echo 0;
	}

	function ajax_view_permission()
	{
		$data['group_id'] = $group_id = $this->input->post('group_id');
		$data['group_name'] = $this->input->post('group_name');

		$data['funcs'] = $this->_model->get_permission_func();

//        $this->debug_data($data['funcs']);

		$data['theme_assets'] = $this->_theme_assets;

		//get group
		$this->load->model('model_role_group');
		$model = new Model_role_group();
		$data['list'] = $model->get_group_permission($group_id);
//        print_r($data['list']);
//        echo $this->_last_query();

		$this->load->view($this->_theme_view . '/admin_group/permission', $data);
	}

	function ajax_update_permission()
	{
		$permission = $this->input->post('permission[]');
		$group_id = $this->input->post('group_id');
		$this->_load_data_from(BASE_TABLE_ADMIN_ROLE_GROUP);
		$this->_model->_delete(array('group_id' => $group_id));
		$insert = array();
		if (is_array($permission))
			foreach ($permission as $item) {
				$insert[] = array(
					'group_id' => $group_id,
					'permission_id' => $item
				);
			}
		if ($insert) {
			$this->_model->_insert_batch($insert);

			//cap nhat lai menu
			$this->_cf_admin_menu_group($group_id);
		}
	}
}