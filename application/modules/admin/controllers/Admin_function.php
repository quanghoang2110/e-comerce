<?php

/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */
class Admin_function extends MBackendController
{
	function __construct()
	{
		$this->_permission_controller_name = "chức năng quản trị";
		parent::__construct();
		$this->_load_data_from(BASE_TABLE_ADMIN_ROLE);
	}

	function index()
	{
		if ($this->input->post()) {
			$this->ajax_update();
		} else {
			$this->load->model('model_role');
			$model = new Model_role();

			$total = ($this->_model->_count()) / 3;

			$vars['list'] = array();
			if ($total > 0) {
				$vars['list'] = $model->get_list();
			}

			$this->disable_paging();
			$this->disable_pagin_action_button();

			$this->_module_vars = $vars;
			$this->_load_tmp(__FUNCTION__);
		}
	}

	function update()
	{
		$data['admin_group_data'] = array();
		$id = $this->input->post('id');

		$data['permission_view'] = $this->check_permission($this->_permission_view) ? true : false;
		$data['permission_edit'] = $this->check_permission($this->_permission_edit) ? true : false;
		$data['permission_delete'] = $this->check_permission($this->_permission_delete) ? true : false;

		$data['admin_data'] = array();

		if (trim($id) > 0) {
			$where = array(
				'id' => $id
			);

			$data['admin_data'] = $info = $this->_model->_get_row($where);
			$data['title'] = "Cập nhật tài khoản quản trị";
		} else {
			$data['title'] = "Thêm tài khoản quản trị";
		}

		$this->_load_data_from(BASE_TABLE_ADMINGROUP);
		$data['groups'] = $this->_model->_get(array('valid' => 1));

		$this->load->vars($data);
		//$this->load->helper('mfw_template_helper');
		load_view($this->_theme_view . '/admin/update');
	}

	function ajax_update()
	{

		$config = array(
			array(
				'field' => 'username',
				'label' => 'Tài khoản',
				'rules' => 'trim|required|max_length[255]'
			),
			array(
				'field' => 'fullname',
				'label' => 'Tên hiển thị',
				'rules' => 'trim|required|max_length[255]'
			)
		);

		if (!$this->_validation($config)) {
			$data['error'] = 1;
			$data['error_detail'] = validation_errors();
			$data['msg_error'] = "Vui lòng nhập đầy đủ thông tin";
			echo json_encode($data);
			return;
		} else {
			$this->_update_general();
		}
	}

	function _update_general()
	{
		$err = 0;
		$msg = '';
		$username = trim($this->input->post('username'));
		$valid = trim($this->input->post('input_valid'));
		$group = $this->input->post('group');
		$fullname = $this->input->post('fullname');
		$data = array(
			'username' => $username,
			'valid' => $valid == 'on' ? 1 : 0,
			'group_id' => $group,
			'fullname' => $fullname
		);
		$this->_load_data_from('admin');
		$id = $this->input->post('hdID');
		//check exists username
		if (!$this->_model->_count(array('id!=' => $id, "username" => $username))) {

			if (!$id) {//them moi tai khoan admin
				$data['password'] = encryption_password(BASE_ADMIN_PASS_DF);
				$id = $this->_model->_insert($data);
				if (!$id) {
					$err = 1;
					$msg = 'Đã có lỗi xảy ra';
				} else {
					$msg = 'Thêm mới thành công';
					//admin log
					$this->admin_log(BASE_ADMIN_ACTION_INSERT, json_encode(array_merge(array('id' => $id), $data)), $id);
				}
			} else {//cap nhat thong tin tai khoan admin
				$this->_model->_update($data, array('id' => $id));
				$msg = 'Cập nhật thông tin tài khoản thành công!';

				//admin log
				$this->admin_log(BASE_ADMIN_ACTION_INSERT, json_encode(array_merge(array('id' => $id), $data)), $id);
			}
		} else {
			$err = 1;
			$msg = "Tài khoản đã tồn tại!";
		}
		$result = array(
			'error' => $err,
			'msg' => $msg,
			'id' => $id
		);
		echo json_encode($result);
	}

	function ajax_valid()
	{
		if ($this->_is_root) {
			$id = $this->input->post("id", true);
			if ($id) {
				$this->_load_data_from(BASE_TABLE_ADMIN_ROLE);
				$info = $this->_model->_get_row(array('id' => $id));
				if ($info) {
					$valid = $this->input->post("valid", true);
					$valid = $valid == 1 ? 0 : 1;

					if ($id > 0) {
						$this->_model->_update(array('valid' => $valid), array('category' => $info->category));
					}
				}
			}
		}
	}
}