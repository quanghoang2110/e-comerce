<?php

class Dashboard extends MBackendController
{
    function index()
    {
    	$this->_page_folder = "";
        $this->_module_title = "Trang chủ";
        $this->_module_desc = "trang chủ hệ thống";
        $this->_content = "dashboard";
        $this->disable_paging();
        $this->disable_pagin_action_button();
        $this->_load_tmp();
    }
}