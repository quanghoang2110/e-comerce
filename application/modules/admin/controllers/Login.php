<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

//require_once APPPATH.'controllers/object/Admin_object.php';
class Login extends MBackendController
{

	function __construct()
	{
		$this->_sess_required = false;
		parent::__construct();
	}

	function index()
	{
		$back_url = $this->session->flashdata("back_url");

		if ($this->_sess_userid) {
			redirect($back_url ? $back_url : BASE_ROUTE_ADMIN_DASHBOARD);
		}

		echo encryption_password($this->input->post("password"));

		if ($this->input->post()) {
			//check username and pass
			$this->_load_data_from('admin');

			//get admin_object data
			$user = $this->_model->_get_custom_row_object(array(
				"username" => $this->input->post("username"),
				"password" => encryption_password($this->input->post("password")),
                "role" => 1
			), 'Admin_object');

			//parse data
//			$user = new Admin_info($data);

			if ($user) {
//				$this->debug_data($user);
				if ($user->valid) {

					$_SESSION['abc-test'] = "ALLL";

					//set session for permission check
					$this->_set_permission_session($user);
					//config menu

					filemanager_subfolder($user->id);

					redirect($back_url ? $back_url : BASE_ROUTE_ADMIN_DASHBOARD);

				} else {
					//TODO show message error
					$msg = "<p style='color: red'>Tài khoản đang bị khóa</p>";
				}
			} else {
				//TODO show message error
				$msg = "<p style='color: red'>Tài khoản hoặc mật khẩu không hợp lệ</p>";
			}
			$this->load->vars(array('msg' => $msg));
		}

		$this->load->view("admin/" . Theme_object::$name . "/login");
	}

	function change_pass()
	{

		$this->_load_data_from('admin');
		$current_pass = $this->input->post("cur_pass");
		$status = -1;
		$msg = "Vui lòng đăng nhập";
		if ($this->_sess_userid) {
			if ($this->_model->_count(
				array(
					'id' => $this->_sess_userid,
					'password' => encryption_password($current_pass)
				)
			)
			) {
				$new_pass = $this->input->post("new_pass");
				$this->_model->_update(
					array('password' => encryption_password($new_pass)), array("id" => $this->_sess_userid)
				);
				$status = 1;
				$msg = "Bạn đã thay đổi mật khẩu thành công!";
			} else {
				$status = 0;
				$msg = "Mật khẩu hiện tại không chính xác!";
			}
		}

		echo json_encode(array(
			'status' => $status,
			'msg' => $msg
		));
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(BASE_ROUTE_ADMIN_LOGIN);
	}

}