<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: quanghv
 * Date: 02/09/2016
 * Time: 3:45 PM
 */
class TradeMark extends MBackendController
{

    private $_table = "trademark";

    function __construct()
    {
        $this->_permission_controller_name = 'Thương hiệu';
        parent::__construct();
        $this->_load_data_from($this->_table);
    }

    function index()
    {
        $this->_init_page();

        $query_arr = array(
            'keyword' => $this->_search,
            'search_row' => 'name'
        );
        $this->_set_query($query_arr);
        $total = $this->_model->_count();

        if ($this->_page > ceil($total / $this->_limit))
            $this->_page--;

        $order_arr = array(
            "position" => "asc"
        );
        $vars['list'] = array();
        if ($total > 0)
            $vars['list'] = $this->_model->_get_limit("", $order_arr, $this->_page, $this->_limit, "*");

        $url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;

        $vars['paging_info'] = $this->_get_page_info($total);
        $vars['paging'] = get_paging($url, $total, $this->_limit);

        $vars['user_id'] = $this->_sess_userid;

        $this->_module_vars = $vars;
        $this->_load_tmp(__FUNCTION__);
    }

    function update()
    {
        $this->_load_data_from($this->_table);
        if ($this->input->post()) {
            $this->ajax_update();
        } else {

            $id = $this->input->get('id');
            $info = array();
            if (trim($id) > 0) {
                $where = array(
                    'id' => $id
                );
                $info = $this->_model->_get_top_one($where);
                $this->_module_desc = "Cập nhật";
            } else {
                $this->_module_desc = "Thêm mới";
            }
            $vars['data'] = $info;
            $this->_module_vars = $vars;
            $this->_load_tmp(__FUNCTION__);
        }
    }

    function ajax_update()
    {
        $config = array(
            array(
                'field' => 'input_name',
                'label' => 'Tên thương hiệu',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'input_image',
                'label' => 'Ảnh thương hiệu',
                'rules' => 'required'
            ),
            array(
                'field' => 'input_slug',
                'label' => 'Short link',
                'rules' => 'trim|required|max_length[255]'
            )
        );
        if (!$this->_validation($config)) {
            $data['error'] = 1;
            $data['error_detail'] = strip_tags(validation_errors());
            $data['msg'] = "Lỗi dữ liệu";
            echo json_encode($data);
            return;
        } else {
            $id = url_title($this->input->post("input_id"));
            $slug = strtolower(url_title($this->input->post("input_slug")));
            $this->_load_data_from("slug");
            $where['name'] = 'brands/'.$slug;
            if ($id) {
                $where['route !='] = 'public/trademark/index/' . $id;
            }
            if ($this->_model->_count($where)) {
                $data['error'] = 1;
                $data['msg'] = "Short link đã được sử dụng!";
                echo json_encode($data);
            } else {
                $this->_update_general();
            }
        }
    }

    function _update_general()
    {
        $title = $this->input->post('input_name');
        $video = $this->input->post('input_video_link');
        $image = $this->input->post('input_image');
        $position = $this->input->post('input_position');
        $valid = $this->input->post('input_valid');
        $desc = $this->input->post('input_desc', false);
        $meta_title = $this->input->post('input_meta_title');
        $meta_desc = $this->input->post('input_meta_desc');
        $slug = url_title($this->input->post('input_slug'));

        $data = array(
            'name' => $title,
            'image' => $image,
            'video_link' => $video,
            'position' => $position > 0 ? $position : 99,
            'valid' => in_array($valid, array('on', 1)) ? 1 : 0,
            'desc' => html_entity_decode($desc),
            'meta_title' => $meta_title,
            'meta_desc' => $meta_desc,
            'slug' => $slug
        );

        $id = $this->_input_post('input_id', true);
        if ($id) {
            $where = array(
                'id' => $id
            );
        } else {
            $where = [];
        }

        $response = $this->update_to_db($data, $where, $this->_table);
        $res_obj = json_decode($response);
        $slug = 'brands/'.$slug;
        if ($res_obj->id) {
            $this->_load_data_from('slug');
            $this->_model->_insert_duplicate(array(
                'name' => $slug,
                'route' => 'public/trademark/index/' . $res_obj->id,
                'type' => 'trademark'
            ));
        }
        echo $response;
    }

    function ajax_delete()
    {
        parent::ajax_delete();
        $id = $this->input->post("id", true);
        $this->_load_data_from('slug');
        $this->_model->_delete(array(
            'route' => 'public/news/index/' . $id,
            'type' => 'news'
        ));
    }

}