<?php
$this->config->load('gosol');
$theme_backend = "admin/{$this->config->item('admin_version_folder')}/";
?>
<link href="/assets/base/lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/assets/admin/alt/dist/css/font-awesome.min.css" rel="stylesheet"/>
<script src="/assets/base/lib/jquery/jquery-3.1.1.min.js" type="application/javascript"></script>
<script src="/assets/base/lib/bootstrap-3.3.7/js/bootstrap.min.js" type="application/javascript"></script>
<style>
    .bs-callout {
        padding: 20px;
        margin: 20px 0;
        border: 1px solid #eee;
        border-left-width: 5px;
        border-radius: 3px;
    }

    .bs-callout-warning h4 {
        color: #aa6708;
    }

    .bs-callout h4 {
        margin-top: 0;
        margin-bottom: 5px;
    }
</style>
<script>
    function goBackendController() {
        var controller = $('#basic-url').val();
        console.log(controller);
        if (controller) {
            window.open('<?php echo base_url($theme_backend)?>/' + controller,'_blank');
//            location.href = '<?php //echo base_url($theme_backend)?>///' + controller;
        }
    }
</script>

<div class="container">
    <div style="height: 40px;"></div>
    <div class="row">
        <div class="col-md-6">
            <a class="btn btn-default" href="/<?php echo BASE_ROUTE_ADMIN_DASHBOARD ?>">&nbsp;<i class="fa fa-home"></i>&nbsp;</a>
            <a class="btn btn-default" href="/admin/config/admin_menu">Update Admin <strong>MENU</strong></a>
            <a class="btn btn-default" href="/admin/config/admin_routes">Update Admin <strong>ROUTES</strong></a>
        </div>
        <div class="col-md-6" align="right">
            <button class="btn btn-default" onclick="addModalLabel()">Thêm mới Label</button>
            <button class="btn btn-default" onclick="addMenu()">Thêm mới Menu</button>
        </div>
    </div>
    <div class="bs-callout bs-callout-warning">
        <label for="basic-url">BACKEND <?php echo strtoupper($this->config->item('admin_version_folder')) ?>
            CONTROLLER</label>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon3"><?php echo base_url($theme_backend) ?>/</span>
            <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button" onclick="goBackendController()">GO</button>
          </span>
        </div>
        <div style="height: 40px;"></div>
        <div class="text-muted">
            <ul>
                <li>First: run backend controller to update routes to DB</li>
            </ul>
        </div>
    </div>

    <div class="bs-callout bs-callout-warning">
        <h4>UPDATE DATA</h4>
        <ol>
            <li><a href="/backend/config/admin_routes">Update routes</a></li>
            <li><a href="/backend/config/admin_menu">Update backend menu</a></li>
        </ol>
    </div>

    <div class="bs-callout bs-callout-warning">
        <h4>Config</h4>
        <ol>
            <li><a href="?type=config_menu">Config backend menu</a></li>
        </ol>
    </div>
</div>