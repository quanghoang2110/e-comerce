<thead>
<tr>
    <th class="width_50">STT</th>
    <th>Tên nhóm</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_150">Cập nhật quyền</th>
    <th class="width_110">Action</th>
</tr>
</thead>
<tbody>
<?php foreach ($list as $index => $item): ?>
	<?php
	$arr['index'] = $index;
	$arr['item'] = $item;
	$this->load->vars($arr);
	?>
    <tr id="item-<?php echo $item->id; ?>">
		<?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_NAME, BASE_ADMIN_TABLE_TD_VALID)) ?>
        <td align="center">
            <button class="btn_permission btn btn-flat"
                    onclick="updatePermission(<?php echo $item->id ?>, '<?php echo $item->name ?>')"><i
                        class="fa fa-user-o"></i></button>
        </td>
		<?php load_table_td(BASE_ADMIN_TABLE_TD_ACTION) ?>
    </tr>
<?php endforeach; ?>
</tbody>
</table>
<div id="div_phanquyen" style="display: none"></div>
<form id="form_update" method="post">
    <div class="modal" id="myModal_add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="modal-header block-header bg-primary-dark">
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="fa fa-close"></i></span></button>
                        <h4 class="modal-title block-title">Nhóm quản trị</h4>
                    </div>
                </div>
                <div css="modal-body">
                    <div class="form-group col-sm-12">
                        <label class="control-label" id="err_name" for="inputError"></label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Tên nhóm" required/>
                        <input type="hidden" name="input_id" id="input_id"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-right">Lưu</button>
                    <button type="button" class="btn btn-default pull-right margin-right-10" data-dismiss="modal">Hủy
                    </button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</form>
<?php load_view($theme_template . '/script'); ?>
<script>

    function updateAjax(objId, objName) {
        var obj = $('#myModal_add');
        if (objId && objName) {
            obj.find('.block-title').html('Cập nhật nhóm quản trị');
            obj.find('#name').val(objName);
            obj.find('#input_id').val(objId);
        }
        obj.modal('show');
    }

    objFormSubmit = $("#form_update");
    $(document).ready(function () {
        var options = {
            beforeSubmit: showRequest,  // pre-submit callback
            success: showResponse  // post-submit callback
        };

        objFormSubmit.attr('action', urlUpdate);

        objFormSubmit.validate({
            ignore: "",
            submitHandler: function (form) {
                objFormSubmit.ajaxSubmit(options);
                return false;
            }
        });
    });

    // pre-submit callback
    function showRequest(formData, jqForm, options) {
        //show loading progress
        showLoading();
        return true;
    }
    // post-submit callback
    function showResponse(responseText, statusText, xhr, $form) {
        hideLoading();
        var obj = $.parseJSON(responseText);
        console.log(obj);
        $('#myModal_add').modal('hide');
        if (obj.error == 1) {
            swal("Lỗi!", obj.msg, "error");
            swal({
                title: "Lỗi",
                text: obj.msg,
                confirmButtonText: "Đồng ý",
                type: "error"
            }, function () {
                $('#myModal_add').modal('show');
            });
        } else {
            swal({
                title: "Thành công",
                text: obj.msg,
                confirmButtonText: "Đồng ý",
                type: "success"
            }, function () {
                window.location.reload();
            });
        }
    }

    function updatePermission(group_id, group_name) {
        $.post("/<?php echo BASE_ROUTE_ADMIN_PERMISSION_VIEW_AJAX?>", {
            group_id: group_id,
            group_name: group_name
        }).done(function (data) {
            $('#div_phanquyen').html(data).fadeIn();
        });
    }
</script>