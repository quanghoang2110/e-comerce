<table class="js-table-sections table table-hover">
    <thead>
    <tr>
        <th class="width_50">STT</th>
        <th class="width_50">Code</th>
        <th class="width_120">Thời gian</th>
        <th class="width_110">Đơn giá</th>
        <th class="width_120">Điện thoại</th>
        <th class="width_110">Email</th>
        <th>Người mua</th>
        <th>Ghi chú</th>
        <th>Phương thức thanh toán</th>
    </tr>
    </thead>

    <?php if ($list) { ?>
        <?php foreach ($list as $index => $item) { ?>
            <tbody class="js-table-sections-header">
            <tr id="item-<?php echo $item->id; ?>">
                <?php
                $arr['index'] = $index;
                $arr['item'] = $item;
                $this->load->vars($arr);
                ?>
                <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT)); ?>
                <td><label class="label label-primary">LC-<?php echo $item->id ?></label></td>
                <td><em><?php echo date('H:i d/m/Y', strtotime($item->created_time)) ?></em></td>
                <td align="right"><?php echo get_price($item->total_price) ?></td>
                <td><?php echo $item->phone ?></td>
                <td><?php echo $item->email ?></td>
                <td><?php echo $item->name ?></td>
                <td><?php echo $item->note ?></td>
<!--                <td align="center">-->
<!--                    <select class="form-control" onchange="updateAjax('--><?php //echo $item->id ?> <!--',$(this))">-->
<!--                        <option value="0" <?php //echo $item->is_finished == 0 ? 'selected' : '' ?>Đang chờ</option>-->
<!--                        <option value="1" --><?php //echo $item->is_finished == 1 ? 'selected' : '' ?><!--Hoàn thành</option>-->
<!--                    </select>-->
<!--                </td>-->
                <td>
                    <?php
                        $payment_method = 'Thanh toán khi giao hàng';
                        if($item->payment_method == 2){
                            $payment_method = 'Thanh toán qua VNPay';
                            if($item->status == 1){
                                $payment_method .= '(Thanh toán thành công)';
                            }else{
                                $payment_method .= '(Thanh toán thất bại)';
                            }
                        }else if($item->payment_method == 3){
                            $payment_method = 'Thanh toán qua tài khoản ngân hàng';
                        }
                        echo $payment_method;
                    ?>
                </td>
            </tr>
            </tbody>
            <tbody class="order-detail">
            <?php foreach ($item->subs as $sIndex => $s_item): ?>
                <tr>
                    <td class="stt"></td>
                    <td align="right"></td>
                    <td align="right"><?php echo get_price($s_item->price_per_once) ?></td>
                    <td colspan="4">
                        <span class="xNumber">&nbsp;&nbsp;x</span>
                        <span><?php echo $s_item->number ?></span>
                        <span>&nbsp;&nbsp;<?php echo $s_item->name ?></span>
                    </td>
                    <?php if ($sIndex == 0): ?>
                        <td class="tdAddress" rowspan="<?php echo count($item->subs) ?>" colspan="2">
                            <p style="margin-bottom: 5px"><?php echo $item->address ?></p>
                            <?php if( !empty($item->other_name) || !empty($item->other_address)):?>
                            <p style="margin-bottom: 5px">Địa chỉ nhận hàng khác:<br/>
                                Họ tên: <?php echo $item->other_name ?><br/>
                                Số điện thoại: <?php echo $item->other_phone ?><br/>
                                Email: <?php echo $item->other_email ?><br/>
                                Địa chỉ: <?php echo $item->other_address ?><br/>
                                Ghi chú: <?php echo $item->other_note ?></p>
                            <?php endif ?>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        <?php } ?>
    <?php } ?>
</table>
<?php load_view($theme_template . '/script'); ?>
<style>
    .open td {
        color: #f0f0f0;
        background-color: #2c343f;
        border-right: 1px solid #2c343f;
        border-bottom: 1px solid #2c343f;
    }

    .js-table-sections-header td {
        border-top: 1px solid #ccc;
    }

    .order-detail td.stt {
        border-left: 1px solid #2c343f;
    }

    .order-detail td {
        background-color: #f0f0f0;
        border-bottom: 1px solid #e0e0e0;
        color: #0b0b0b;
    }

    .order-detail tr:last-child td, td.tdAddress {
        border-bottom: 1px solid #2c343f;
    }

    .order-detail td.tdAddress {
        border-right: 1px solid #2c343f;
    }

    .xNumber {
        font-family: fantasy;
    }

</style>
<script>
    function updateAjax(id, obj) {
        var data = {};
        var status = obj.val();
        data = {id: id, status: status};
        $.post(urlUpdate, data)
            .done(function (data) {

                }
            );
    }
</script>