<div class="col-xs-12">
    <!--	    element name-->
    <div class="form-group">
        <div class="col-sm-6 col-lg-4">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tiêu đề"); ?>
        </div>
        <div class="col-sm-6 col-lg-4">
            <label for="input_type">Kiểu nội dung</label>
            <select id="input_type" class="select2 form-control" name="input_type">
                <option value="<?php echo PAGE_TYPE_VE_CHUNG_TOI ?>" <?php echo $data && $data->type == PAGE_TYPE_VE_CHUNG_TOI ? 'selected' : '' ?>>
                    Về chúng tôi
                </option>
                <option value="<?php echo PAGE_TYPE_THANH_TOAN ?>" <?php echo $data && $data->type == PAGE_TYPE_THANH_TOAN ? 'selected' : '' ?>>
                    Thanh toán
                </option>
                <option value="<?php echo PAGE_TYPE_CHINH_SACH ?>" <?php echo $data && $data->type == PAGE_TYPE_CHINH_SACH ? 'selected' : '' ?>>
                    Chính sách
                </option>
                <option value="<?php echo PAGE_TYPE_CAU_HOI ?>" <?php echo $data && $data->type == PAGE_TYPE_CAU_HOI ? 'selected' : '' ?>>
                    Câu hỏi thường gặp
                </option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-9">
            <?php
            $form_obj = new Form_element_object();
            $form_obj->label = "Short link";
            $form_obj->field = "input_slug";
            $form_obj->field_value = $data ? $data->slug : "";
            $form_obj->help_block = "VD: tin-tuc cho link đầy đủ " . base_url("tin-tuc");
            ?>
            <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj); ?>
        </div>
        <div class="col-xs-3"></div>
    </div>

    <!--	    element name-->
    <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_CONTENT, BASE_ADMIN_FORM_ELEMENT_SEO, BASE_ADMIN_FORM_ELEMENT_ACTION)); ?>
</div>
<?php
//load script for submit form
$before_submit_data = array(
    'id_tinymce' => 'input_content'
);
load_view($theme_template . '/script', $before_submit_data);
?>