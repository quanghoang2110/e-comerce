<thead>
<tr>
    <th class="width_50">STT</th>
    <th class="width_200">Tên trang</th>
    <th class="width_300">Đường dẫn</th>
    <th class="visible-lg">Mô tả</th>
    <th>Vị trí</th>
    <th>Menu</th>
    <th class="width_120">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
    <?php foreach ($list as $index => $item) { ?>
        <?php
        $arr['index'] = $index;
        $arr['item'] = $item;
        $arr['permission_delete'] = false;
        $this->load->vars($arr);
        ?>
        <tr id="item-<?php echo $item->id; ?>">
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_NAME)); ?>
            <td>
                <a href="<?php echo base_url($item->slug) ?>" target="_blank">
                    <?php echo base_url($item->slug); ?>
                </a>
            </td>
            <td class="visible-lg text-muted"><?php echo word_limiter(strip_tags($item->content), 30); ?></td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION)); ?>
            <td align="center">
                <?php $is_menu = $item->is_menu; ?>
                <button class="is_menu btn-mega btn btn-flat"
                        title="Hiển thị trên menu" data-toggle="tooltip" data-placement="left"
                    <?php if ($permission_edit && $is_menu != -1): ?>
                        onclick="validAjax(<?php echo $item->id ?>, <?php echo $is_menu ?>, 'is_menu', 'is_menu', 'fa fa-check-square-o', 'fa fa-square-o')"
                    <?php endif; ?>>
                    <i class="fa <?php if ($is_menu == 1) echo 'fa-check-square-o'; else echo 'fa-square-o'; ?>"></i>
                </button>
            </td>
            <td class="table_td_action" align="center">
                <?php if ($permission_edit): ?>
                    <button class="btn-repair-question btn btn-icon-toggle" data-toggle="tooltip" data-placement="left" title="Sửa"
                            onclick="updateAjax(<?php echo $item->id ?>, '<?php echo isset($item->name) ? $item->name : '' ?>')">
                        <i class="fa fa-edit"></i>
                    </button>
                <?php endif; ?>
                <?php if ($permission_delete) { ?>
                    <button class="btn btn-icon-toggle" title="Xóa" data-toggle="tooltip" data-placement="left"
                            onclick="deleteAjax(<?php echo $item->id ?>)">
                        <i class="fa fa-close"></i>
                    </button>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

