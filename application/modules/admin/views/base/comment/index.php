<thead>
<tr>
    <th class="width_50">STT</th>
    <th class="width_150">Ảnh</th>
    <th class="">Nội dung</th>
    <th class="width_100">Phê duyệt</th>
    <th class="width_120">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
    <?php foreach ($list as $index => $item) { ?>
        <?php
        $arr['index'] = $index;
        $arr['item'] = $item;
        $item->name = $item->fullname;
        $arr['permission_delete'] = false;
        $this->load->vars($arr);
        ?>
        <tr id="item-<?php echo $item->id; ?>">
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_IMG)); ?>
            <td>
                <div class=" image-user">
                    <i class="si si-user"></i>
                </div>
                <div class="content-comment">
                    <?php
                    $type = $item->type == 1 ? 'Tin tức' : 'Sản phẩm';
                    $name = $item->type == 1 ? $item->news_name : $item->product_name;
                    $item_url = $item->type == 1 ? get_detail_url(get_route_name($item->news_category_name), $item->news_name, $item->news_id) : get_detail_url($item->category_slug, $item->product_name, $item->product_id);
                    ?>
                    <span>
                        <strong class="font-w600"><?php echo $item->fullname; ?></strong>
                        <strong class="font-w400"
                                style="font-size: 12px; color: #ccc"><?php echo ' .Đã bình luận về - ' ?>
                            <a target="_blank" href="<?php echo $item_url ?>"><?php echo $name . ' - ' . $type ?></a>
                        </strong>
                    </span>
                </div>
                <div>
                    <?php echo $item->desc; ?>
                </div>
            </td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_VALID)); ?>
            <td class="table_td_action" align="center">
                <button class="btn btn-icon-toggle" title="Trả lời" data-toggle="tooltip" data-placement="left"
                        onclick="showPopupReply(<?php echo $item->parent_id ? $item->parent_id : $item->id ?>)">
                    <i class="fa fa-reply"></i>
                </button>
                <?php if ($permission_delete) { ?>
                    <button class="btn btn-icon-toggle" title="Xóa" data-toggle="tooltip" data-placement="left"
                            onclick="deleteAjax(<?php echo $item->id ?>)">
                        <i class="fa fa-close"></i>
                    </button>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
<?php } ?>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Trả lời bình luận</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-material floating floating-label">
                <textarea id="reply_content" name="input_content" rows="4"
                          class="form-control"></textarea>
                        <label for="reply_content">Nội dung trả lời</label>
                        <div class="help-block" style="color: #d26a5c !important;" id="error_content"></div>
                    </div>
                    <input type="hidden" id="input_parent_id">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" onclick="replyAjax()">Trả lời</button>
            </div>
        </div>

    </div>
</div>
</tbody>
<style>
    .content-comment {
        display: inline-block;
        position: absolute;
        margin-left: 20px;
    }

    .image-user {
        display: inline-block;
    }

    .table-striped > tbody > tr {
        background-color: #f9f9f9 !important;
    }
</style>
<?php load_view($theme_template . '/script'); ?>
<script>
    function showPopupReply(id) {
        $('#input_parent_id').val(id);
        $('#myModal').modal('show');
    }

    function replyAjax() {
        var id = $('#input_parent_id').val();
        var content = $('#reply_content').val();
        $.post('/<?php echo BASE_ROUTE_REPLY_COMMENT?>', {id: id, content: content})
            .done(function (data) {
                var obj = JSON.parse(data);
                if (obj.status == true) {
                    $('#myModal').modal('hide');
                    swal({
                            title: "Trả lời bình luận",
                            text: "Trả lời thành công!",
                            type: "success",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Đồng ý",
                            showLoaderOnConfirm: true,
                        },
                        function () {
                            location.reload(true);
                        });
                } else {
                    $('#error_content').html(obj.msg);
                }
            });
    }
</script>