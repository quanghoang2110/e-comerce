<div class="col-xs-12">
    <div class="form-group">
        <div class="col-sm-4">
            <label for="price_2">Tên người dùng</label>
            <input id="fullname" class="form-control" type="text" name="fullname"
                   value="<?php echo $data ? $data->fullname : ''; ?>" readonly/>
        </div>
        <div class="col-sm-4">
            <label for="price_2">Email</label>
            <input id="email" class="form-control" type="text" name="email"
                   value="<?php echo $data ? $data->email : ''; ?>" readonly/>
        </div>
        <div class="col-sm-4">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <?php
            $form_element = new Form_element_object();
            $form_element->label = "Nội dung";
            $form_element->field = "input_content";
            $form_element->field_value = $data ? $data->content : '';
            $form_element->attr = 'rows=5 disabled';
            load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC, $form_element);
            ?>
        </div>
    </div>
    <!--	    element name-->
    <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_ACTION)); ?>
</div>
<?php
//load script for submit form
$before_submit_data = array(
    'id_tinymce' => ''
);
load_view($theme_template . '/script', $before_submit_data);
?>