<div class="col-xs-12">
    <div class="col-sm-4 col-lg-3">
        <?php
        $form_obj = new Form_element_object();
        $form_obj->cropped = 1;
        $form_obj->crop_ratio = 450/600;
        $form_obj->help_block = '450x600';
        ?>
        <!-- element image   -->
        <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
    </div>

    <div class="col-sm-8 col-lg-9">
        <div class="type-video" style="display: <?php echo $data && $data->type == 'CONTENT' ? 'none' : 'block' ?>">
            <?php $obj = new Form_element_object();
            $obj->label = "Link video";
            $obj->field = "input_video_link";
            $obj->field_value = $data ? $data->video_link : '';
            ?>
            <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $obj); ?>
        </div>
        <div class="form-group">
            <div class="col-md-7">
                <!-- element name-->
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tiêu đề"); ?>
            </div>
            <div class="col-md-5">
                <div class="col-xs-12">
                    <!-- element category-->
                    <?php $obj = new Form_element_object();
                    $obj->label = "Chuyên mục";
                    $obj->field = "input_category";
                    $obj->field_value = $data ? $data->category_id : '';
                    ?>
                    <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_CATEGORY, $obj); ?>
                    <!--                    --><?php //load_form_element(BASE_ADMIN_FORM_ELEMENT_CATEGORY); ?>
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <div class="form-group">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <!--        element position-->
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION); ?>
            </div>

            <div class="col-lg-9">
                <div class="col-xs-4">
                    <!--        element valid-->
                    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID); ?>
                </div>
                <div class="col-xs-4 custom-text-center">
                    <!--        element position-->
                    <?php $obj = new Form_element_object();
                    $obj->label = "Nổi bật";
                    $obj->field = "input_hot";
                    $obj->field_value = $data ? $data->is_hot : '';
                    ?>
                    <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_VALID, $obj); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>
    <div class="type-content" style="display: <?php echo $data && $data->type == 'VIDEO' ? 'none' : 'block' ?>">
        <div style="margin-top: 15px"></div>
        <div class="form-group">
            <label for="input_products_related">Sản phẩm liên quan</label>
            <select class="js-select2 form-control" id="input_products_related" name="input_products_related[]"
                    style="width: 100%;" data-placeholder="Sản phẩm liên quan tới bài viết.." multiple>
                <option></option>
                <?php $products_related = explode(',', $data->products_related); ?>
                <?php foreach ($products as $item): ?>
                    <option value="<?php echo $item->id ?>"
                        <?php echo is_array($products_related) && in_array($item->id, $products_related) ? 'selected' : '' ?>>
                        <?php echo $item->name ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="clear"></div>

    <div class="type-content" style="display: <?php echo $data && $data->type == 'VIDEO' ? 'none' : 'block' ?>">
        <!--element desc,content,seo,action-->
        <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_DESC); ?>
        <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_CONTENT); ?>
    </div>

    <div class="clear"></div>
    <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_SEO, BASE_ADMIN_FORM_ELEMENT_ACTION)); ?>
</div>
<?php
//load script for submit form
$before_submit_data = array('id_tinymce' => 'input_content');
load_view($theme_template . '/script', $before_submit_data);
?>
<script>
    function change_alias(alias) {
        var str = alias;
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, "-");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        return str;
    }

    // $("#input_name").change(function(){
    //    $("#input_slug").val(change_alias($('#input_name').val()));
    // });
    //
    // $("#input_category").change(function () {
    //     console.log("test");
    // });
    contentTypeChanged();

    function contentTypeChanged() {
        const value = $('#input_type').val();
        console.log(value);
        if (value === 'VIDEO') {
            $('.type-video').show();
            $('.type-content').hide();
            $('#input_content').attr('name', 'input_content_2');
            $('#input_desc').attr('name', 'input_desc_2');
        } else {
            $('.type-content').show();
            $('.type-video').hide();
            $('#input_content').attr('name', 'input_content');
            $('#input_desc').attr('name', 'input_desc');
        }
    }
</script>
