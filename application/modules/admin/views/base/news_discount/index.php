<thead>
<tr>
    <th class="width_50">STT</th>
    <th class="width_50">Ảnh</th>
    <th>Tiêu đề</th>
    <!--    <th class="visible-lg">Mô tả</th>-->
    <th class="width_180">Chuyên mục</th>
    <th class="width_80">Mega menu</th>
    <th class="width_80">Vị trí</th>
    <th class="width_80">Nổi bật</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
    <?php foreach ($list as $index => $item) { ?>
        <tr id="item-<?php echo $item->id; ?>">
            <?php
            $arr['index'] = $index;
            $arr['item'] = $item;
            $this->load->vars($arr);
            ?>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_IMG)); ?>
            <td>
                <a href="<?php echo get_detail_url(get_route_name($item->category), $item->name, $item->id) ?>"
                   target="_blank">
                    <?php echo $item->name ?>
                </a>
            </td>
            <!--            <td class="visible-lg">--><?php //echo $item->desc; ?><!--</td>-->
            <td><?php echo $item->category; ?></td>
            <td align="center">
                <?php $is_mega_menu = $item->is_mega_menu; ?>
                <button class="mega_menu btn-mega btn btn-flat"
                        title="Hiển thị trên mega menu" data-toggle="tooltip" data-placement="left"
                        <?php if ($permission_edit && $is_mega_menu != -1){ ?>onclick="validAjax(<?php echo $item->id ?>, <?php echo $is_mega_menu ?>, 'mega_menu', 'mega_menu', 'fa fa-star', 'fa fa-star-o')"<?php } ?>>
                    <i class="fa <?php if ($is_mega_menu == 1) echo 'fa-star'; else echo 'fa-star-o'; ?>"></i>
                </button>
            </td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION, BASE_ADMIN_TABLE_TD_HOT, BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)); ?>
        </tr>
    <?php } ?>
<?php } ?>
</tbody>

<?php load_view($theme_template . '/script'); ?>

