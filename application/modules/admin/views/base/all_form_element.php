<?php $this->load->vars(array('data' => "")) ?>
    <div class="col-sm-4 col-lg-2">
		<?php
		$form_obj = new Form_element_object();
		$form_obj->cropped = 1;
		$form_obj->crop_ratio = 1;
		?>
        <!-- element image   -->
		<?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
    </div>

    <div class="col-sm-8 col-lg-10">

        <div class="form-group">
            <div class="col-md-7">
                <!-- element name-->
				<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tiêu đề"); ?>
            </div>
            <div class="col-md-5">
                <div class="col-xs-12">
                    <!-- element category-->
					<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_CATEGORY); ?>
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <div class="form-group">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <!--        element position-->
				<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION); ?>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 custom-text-center">
                <div class="col-xs-12">
                    <!--        element valid-->
					<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID); ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 custom-text-center">
                <div class="col-xs-12">
                    <!--        element position-->
					<?php $obj = new Form_element_object();
					$obj->label = "Nổi bật";
					$obj->field = "input_hot";
					$obj->field_value = 1;
					?>
					<?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_VALID, $obj); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>
    <div class="form-group">
		<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tiêu đề 2"); ?>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <!-- element category-->
				<?php $obj = new Form_element_object();
				$obj->label = "Nổi bật";
				$obj->field = "input_hot";
				$obj->field_value = 1;
				$obj->category = array();
				?>
				<?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_CATEGORY, $obj); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-9 no-padding">
			<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tiêu đề 2"); ?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <!--        element position-->
				<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION); ?>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 custom-text-center">
                <!--        element valid-->
				<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID); ?>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 custom-text-center">
                <!--        element position-->
				<?php $obj = new Form_element_object();
				$obj->label = "Nổi bật";
				$obj->field = "input_hot";
				$obj->field_value = 1;
				?>
				<?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_VALID, $obj); ?>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <!--element desc,content,seo,action-->
<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_DESC); ?>
<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_CONTENT); ?>
<?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_SEO, BASE_ADMIN_FORM_ELEMENT_ACTION)); ?>

<?php
//load script for submit form
$before_submit_data = array('id_tinymce' => 'input_content');
load_view($theme_template . '/script', $before_submit_data);
?>