<div class="form-group">
    <div class="col-sm-6 col-md-4">
        <?php
        $form_obj = new Form_element_object();
        $form_obj->cropped = 1;
        $form_obj->crop_ratio = 380 / 280;
        $form_obj->label = "Slider";
        $form_obj->help_block = "Tỉ lệ: 380x280";
        ?>
        <!-- element image   -->
        <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
    </div>
    <div class="col-sm-6 col-md-8">
        <div class="form-group">
            <div class="col-xs-9">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "Tiêu đề";
                $form_obj->field = "input_name";
                $form_obj->field_value = $data ? $data->name : "";
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj);
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-9">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "URL";
                $form_obj->field = "input_url";
                $form_obj->field_value = $data ? $data->url : "";
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj);
                ?>
            </div>
        </div>
        <div class="clear"></div>
        <div class="form-group">
            <div class="col-md-4 col-lg-3">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION) ?>
            </div>
            <div class="col-md-4 col-lg-3 custom-text-center">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
            </div>
        </div>

    </div>
    <div class="clear"></div>
    <div class="col-xs-12" style="margin-left:30px">
        <!--element action-->
        <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_ACTION), false, "col-md-11"); ?>
    </div>
</div>
<?php load_view($theme_template . '/script'); ?>