<thead>
<tr>
    <th class="width_50">STT</th>
    <th>Khoảng giá tiền</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) {
	?>
	<?php foreach ($list as $index => $item) { ?>
		<?php
		$arr['index'] = $index;
		$arr['item'] = $item;
		$this->load->vars($arr);
		$price_2 = !empty($item->price_2) ? get_price($item->price_2) : 'Nhiều hơn';
		?>
        <tr id="item-<?php echo $item->id; ?>">
			<?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT)) ?>
            <td>
                <?php echo get_price($item->price_1).' -> '. $price_2 ?>
            </td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)) ?>
        </tr>
	<?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

