<script src="/assets/base/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<!-- element name-->
<div class="form-group">
    <div class="col-sm-3">
        <label for="price_1">Giá 1</label>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <input id="price_1" class="form-control is-price" type="text" name="input_price_1" min="0"
                   value="<?php echo $data ? $data->price_1 : ''; ?>"/>
        </div>
    </div>
    <div class="col-sm-3">
        <label for="price_2">Giá 2</label>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <input id="price_2" class="form-control is-price" type="text" name="input_price_2" min="0"
                   value="<?php echo $data ? $data->price_2 : ''; ?>"/>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="col-lg-3 col-md-4 col-sm-6 custom-text-center">
    <!--        element valid-->
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID); ?>
</div>
<div class="clear"></div>
<div class="col-xs-12" style="margin-top: 30px">
    <!--element Noi dung & Action-->
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_ACTION); ?>
</div>

<?php load_view($theme_template . '/script'); ?>
<script>
    $(function () {
        //format number on input
        $('.is-price').number(true, 0);
    });
</script>
<!--format currency-->
<script src="/assets/base/lib/jquery-number/jquery.number.min.js"></script>