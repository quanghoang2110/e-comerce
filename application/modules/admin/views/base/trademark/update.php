<div class="form-group">
    <div class="col-sm-5 col-lg-3">
        <?php
        $form_obj = new Form_element_object();
        $form_obj->cropped = 1;
        $form_obj->crop_ratio = 200/80;
        $form_obj->help_block = "200x80";
        ?>
        <!-- element image   -->
        <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
    </div>

    <div class="col-sm-7 col-lg-9">
        <!--	    element name-->
        <div class="form-group">
            <div class="col-xs-9">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên"); ?>
            </div>
            <div class="col-xs-3"></div>
        </div>
        <div class="form-group">
            <div class="col-xs-9">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "Video";
                $form_obj->field = "input_video_link";
                $form_obj->field_value = $data ? $data->video_link : "";
                ?>
                <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj); ?>
            </div>
            <div class="col-xs-3"></div>
        </div>
        <div class="form-group">
            <div class="col-xs-9">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "Short link";
                $form_obj->field = "input_slug";
                $form_obj->field_value = $data ? $data->slug : "";
                $form_obj->help_block = "VD: logde cho link đầy đủ " . base_url("brands/logde");
                ?>
                <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj); ?>
            </div>
            <div class="col-xs-3"></div>
        </div>
        <div class="clear"></div>
        <div class="form-group">
            <div class="col-md-4 col-lg-3">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION) ?>
            </div>
            <div class="col-md-4 col-lg-3 custom-text-center">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
            </div>
        </div>

    </div>
</div>
<div class="clear"></div>


<div class="">
    <div class="col-xs-12">
        <div class="form-group">
            <div class="col-xs-12">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "Mô tả";
                $form_obj->field = "input_desc";
                $form_obj->attr = "";
                $form_obj->field_value = $data ? $data->desc : "";
                ?>
                <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC_EDITOR, $form_obj) ?>
            </div>
        </div>
        <!--element content-->
        <!--element seo-->
        <!--element action-->
        <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_SEO, BASE_ADMIN_FORM_ELEMENT_ACTION), false, "col-md-11"); ?>
    </div>
</div>
<style>
    #img-update{
        width: auto !important;
    }
    .image-update {
        height: 50px;
    }
</style>
<?php $before_submit_data = array('id_tinymce' => 'input_desc'); ?>
<?php load_view($theme_template . '/script', $before_submit_data); ?>