<div class="form-group">
    <div class="col-sm-5">
        <?php
        $form_obj = new Form_element_object();
        $form_obj->cropped = 1;
        $form_obj->crop_ratio = 1366 / 374;
        $form_obj->label = "Slider";
        $form_obj->help_block = "1366x374";
        ?>
        <!-- element image   -->
        <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
    </div>
    <div class="col-sm-7">
        <div class="form-group">
            <div class="col-xs-9">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "Tiêu đề";
                $form_obj->field = "input_name";
                $form_obj->field_value = $data ? $data->name : "";
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj);
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4 col-lg-3">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION) ?>
            </div>
            <div class="col-md-4 col-lg-3 custom-text-center">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
            </div>
        </div>
        <div class="clear"></div>

        <div class="form-group">
            <div class="col-xs-9">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "Button text";
                $form_obj->field = "input_button";
                $form_obj->field_value = $data ? $data->button : "";
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj);
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-9">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "URL";
                $form_obj->field = "input_url";
                $form_obj->field_value = $data ? $data->url : "";
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj);
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-9">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "Mô tả";
                $form_obj->field = "input_desc";
                $form_obj->field_value = $data ? $data->desc : "";
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC, $form_obj);
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <div>
            <!--element action-->
            <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_ACTION), false, "col-md-11"); ?>
        </div>

    </div>
</div>
<style>
    .image-update{
        width: 80%;
    }
</style>
<?php load_view($theme_template . '/script'); ?>