<thead>
<tr>
    <th class="width_50">STT</th>
    <th class="width_100">Ảnh</th>
    <th class="width_200">Tên</th>
    <th class="width_200">Nghề nghiệp</th>
    <th class="visible-lg">Nội dung</th>
    <th class="width_100">Vị trí</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
	<?php foreach ($list as $index => $item) { ?>
		<?php
		$arr['index'] = $index;
		$arr['item'] = $item;
		$this->load->vars($arr);
		?>
        <tr id="item-<?php echo $item->id; ?>">
			<?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_IMG, BASE_ADMIN_TABLE_TD_NAME)) ?>
            <td><?php echo $item->career; ?></td>
            <td class="visible-lg"><?php echo $item->desc; ?></td>
			<?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION, BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)) ?>
        </tr>
	<?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

