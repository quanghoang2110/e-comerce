<div class="col-sm-4 col-lg-2">
	<?php
	$form_obj = new Form_element_object();
	$form_obj->cropped = 1;
	$form_obj->crop_ratio = 1;
	$form_obj->attr = 'required';
	?>
    <!-- element image   -->
	<?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
</div>

<div class="col-sm-8 col-lg-10">

    <div class="form-group">
        <div class="col-md-7">
            <!-- element name-->
			<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên khách hàng"); ?>
        </div>
        <div class="col-md-5">
            <!-- element name-->
			<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Nghề nghiệp", "input_career", $data ? $data->career : ''); ?>
        </div>
    </div>
    <div class="clear"></div>

    <div class="form-group">
        <div class="col-md-4 col-lg-3">
			<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION) ?>
        </div>
        <div class="col-md-4 col-lg-3 custom-text-center">
			<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
        </div>
    </div>

</div>
<div class="clear"></div>
<div class="form-group">
    <div class="col-xs-12">
        <!--element Noi dung & Action-->
        <?php
        $form_obj = new Form_element_object();
        $form_obj->label = "Cảm nhận";
        $form_obj->field = "input_desc";
        $form_obj->field_value = $data->desc ?? "";
        ?>
		<?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC, $form_obj); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12">
        <!--element Noi dung & Action-->
		<?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_ACTION)); ?>
    </div>
</div>
<?php
//load script for submit form
$before_submit_data = array('id_tinymce' => 'input_content');
load_view($theme_template . '/script', $before_submit_data);
?>