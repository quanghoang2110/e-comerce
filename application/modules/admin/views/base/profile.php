<div class="block-content block-content-full">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary box-border">
            <div class="box-body box-profile">
				<?php echo get_img_tag($info->img, "", "admin-img profile-user-img img-responsive img-circle", BASE_IMG_DEFAULT) ?>
                <h3 class="profile-username text-center"><?php echo $info->username ?></h3>
                <p class="text-muted text-center"><?php echo $info->fullname ?></p>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary hidden">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
                <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>

                <hr>

                <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                <p class="text-muted">Malibu, California</p>

                <hr>

                <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                </p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#timeline" data-toggle="tab">Hoạt động</a></li>
                <li><a href="#settings" data-toggle="tab">Cấu hình</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="timeline">
                    <!-- The timeline -->
                    <ul class="timeline timeline-inverse">
						<?php $time = "" ?>
						<?php foreach ($logs as $item): ?>
							<?php if (date('Y-m-d', strtotime($item->create_time)) != $time): ?>
                                <!-- timeline time label -->
                                <li class="time-label">
                        <span class="<?php echo date('Y-m-d', strtotime($item->create_time)) == date('Y-m-d') ? 'bg-red' : 'bg-green' ?>">
                          <?php echo date('d.m.Y', strtotime($item->create_time)); ?>
                        </span>
                                </li>
                                <!-- /.timeline-label -->
							<?php endif;
							$time = date('Y-m-d', strtotime($item->create_time));
							?>
                            <!-- timeline item -->
							<?php
							switch ($item->action) {
								case BASE_ADMIN_ACTION_DELETE:
									$icon = "fa fa-trash bg-red";
									$label = "Xóa";
									break;
								case BASE_ADMIN_ACTION_INSERT:
									$icon = "fa fa-plus bg-green";
									$label = "Thêm mới";
									break;
								case BASE_ADMIN_ACTION_UPDATE:
									$icon = "fa fa-pencil bg-blue";
									$label = "Cập nhật";
									break;
							}
							?>
                            <li>
                                <i class="<?php echo $icon ?>"></i>
                                <div class="timeline-item">
                                <span class="time"><i
                                            class="fa fa-clock-o"></i> <?php echo date('H:i, d/m/Y', strtotime($item->create_time)) ?></span>
                                    <h3 class="timeline-header"><a><?php echo $label ?></a> dữ liệu
                                        <strong><?php echo $item->category ?></strong>
                                        (<?php echo $item->table ?>/<?php echo $item->row_id ?>)</h3>
                                    <div class="timeline-body hidden">
										<?php //print_r(json_decode($item->content)) ?>
                                    </div>
                                </div>
                            </li>
						<?php endforeach; ?>
						<?php if (!$logs) echo "<li><i class='fa fa-info bg-yellow color-palette'></i><div class='timeline-item'><h3 class='timeline-header no-border'>&nbsp; Không có hoạt động nào gần đây.</h3></div></li>"; ?>
                    </ul>
                </div><!-- /.tab-pane -->

				<?php $user = new Admin_object($info); ?>

                <div class="tab-pane" id="settings">
                    <form class="form" id="form_update">
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="form-group">
									<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_IMG, "Ảnh đại diện", "input_image", $user->img); ?>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9 col-md-8">
										<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tài khoản", "input_username", $user->username, "", "readonly"); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9 col-md-8">
										<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên hiển thị", "input_fullname", $user->fullname); ?>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="form-group">
                                    <div class="col-sm-9 col-md-8">
                                        <button type="submit" class="btn btn-danger">Đồng ý</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- /.nav-tabs-custom -->
    </div><!-- /.col -->
</div>
<style>
    .admin-img {
        max-width: 150px !important;
    }
    
    .block-content{
        overflow: hidden;
    }

    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 20px;
        width: 100%;
        /*box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);*/
        /*border: 1px solid #e0e0e0;*/
    }

    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 10px;
    }

    .profile-user-img {
        margin: 0 auto;
        width: 100px;
        padding: 3px;
        border: 3px solid #d2d6de;
    }

    .img-circle {
        border-radius: 50%;
    }

    .nav-tabs-custom {
        margin-bottom: 20px;
        background: #fff;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        border-radius: 3px;
    }

    .nav-tabs-custom > .nav-tabs {
        margin: 0;
        border-bottom-color: #f4f4f4;
        border-top-right-radius: 3px;
        border-top-left-radius: 3px;
        margin-bottom: -1px;
    }

    .nav-tabs {
        border-bottom: 1px solid #ddd;
    }

    .nav-tabs-custom > .tab-content {
        background: #fff;
        padding: 10px;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        border: 1px solid #e0e0e0;
    }

    .timeline {
        position: relative;
        margin: 0 0 30px 0;
        padding: 0;
        list-style: none;
    }

    .timeline > li {
        position: relative;
        margin-right: 10px;
        margin-bottom: 15px;
    }

    .timeline > .time-label > span {
        font-weight: 600;
        padding: 5px;
        display: inline-block;
        background-color: #fff;
        border-radius: 4px;
        color: #fff;
    }

    .bg-green, .callout.callout-success, .alert-success, .label-success, .modal-success .modal-body {
        background-color: #00a65a !important;
    }

    .bg-blue {
        background-color: #0073b7 !important;
    }

    .timeline > li > .timeline-item > .time {
        color: #999;
        float: right;
        padding: 10px;
        font-size: 12px;
    }

    .timeline-inverse > li > .timeline-item > .timeline-header {
        border-bottom-color: #ddd;
    }

    .timeline > li > .timeline-item > .timeline-header {
        margin: 0;
        color: #555;
        border-bottom: 1px solid #f4f4f4;
        padding: 10px;
        font-size: 16px;
        line-height: 1.1;
    }

    .timeline-inverse > li > .timeline-item {
        background: #f0f0f0;
        border: 1px solid #ddd;
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .timeline > li > .timeline-item {
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        border-radius: 3px;
        margin-top: 0;
        background: #fff;
        color: #444;
        margin-left: 60px;
        margin-right: 15px;
        padding: 0;
        position: relative;
    }

    .timeline:before {
        content: '';
        position: absolute;
        top: 0;
        bottom: 0;
        width: 4px;
        background: #ddd;
        left: 31px;
        margin: 0;
        border-radius: 2px;
    }

    .timeline > li:before, .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li > .fa, .timeline > li > .glyphicon, .timeline > li > .ion {
        width: 30px;
        height: 30px;
        font-size: 15px;
        line-height: 30px;
        position: absolute;
        color: #fff;
        background: #d2d6de;
        border-radius: 50%;
        text-align: center;
        left: 18px;
        top: 0;
    }

    i.fa.fa-trash.bg-red {
        background-color: red;
    }
    .tab-content{
        overflow: hidden;
        padding-bottom: 60px!important;
    }
</style>
<script type="text/javascript">
    //    backMainPage = false;
</script>
<?php
//load script for submit form
load_view($theme_template . '/script');
?>