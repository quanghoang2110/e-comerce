<thead>
<tr>
    <th style="width: 50px">STT</th>
    <th>Chức năng</th>
	<?php if ($permission_edit) { ?>
        <th class="width_150">Trạng thái</th>
        <th class="width_120">Thao tác</th>
	<?php } ?>
</tr>
</thead>
<tbody>

<?php foreach ($list as $index => $item) { ?>
	<?php
	$arr['index'] = $index;
	$arr['item'] = $item;
	$this->load->vars($arr);
	?>
    <tr id="item-<?php echo $item->id; ?>">
		<?php load_table_td(BASE_ADMIN_TABLE_TD_STT) ?>
	    <td><?php echo ucfirst_utf8($item->category_name) ?></td>
		<?php if ($permission_edit || $permission_delete) { ?>
			<?php load_table_td(BASE_ADMIN_TABLE_TD_VALID) ?>
			<?php if ($permission_edit) { ?>
				<?php load_table_td(BASE_ADMIN_TABLE_TD_ACTION) ?>
			<?php } ?>
		<?php } ?>
    </tr>
<?php } ?>
</tbody>
<!--<div class="modal" id="modal_update"></div>-->
<?php load_view($theme_template . '/script'); ?>

<script type="text/javascript">
    function updateAjax(id) {
        return false;
    }
</script>
