<thead>
<tr>
    <th class="width_50">STT</th>
    <th class="width_100">Ảnh</th>
    <th>Tiêu đề</th>
    <th class="visible-lg">URL</th>
    <th class="width_80 text-center">Home</th>
    <th class="width_80 text-center">Vị trí</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
    <?php foreach ($list as $index => $item) { ?>
        <tr id="item-<?php echo $item->id; ?>">
            <?php
            $arr['index'] = $index;
            $arr['item'] = $item;
            $this->load->vars($arr);
            ?>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_IMG, BASE_ADMIN_TABLE_TD_NAME)); ?>
            <td class="visible-lg">
                <a href="<?php echo $item->url ?>" target="_blank">
                    <?php echo $item->url ?>
                </a>
            </td>
            <td align="center">
                <?php $status = $item->is_hot ?>
                <button class="item_home btn-home btn btn-flat"
                        title="Trạng thái hiển thị Nhóm sản phẩm nổi bật trên trang chủ" data-toggle="tooltip" data-placement="left"
                        <?php if ($permission_edit && $status != -1): ?>onclick="validAjax(<?php echo $item->id ?>, <?php echo $status ?>, 'hot', 'item_home', 'fa fa-check', 'fa fa-ban')"
                    <?php endif; ?> >
                    <i class="fa <?php if ($status == 1) echo 'fa-check'; else if ($status == 0) echo 'fa-ban'; else echo 'fa-reply' ?>"></i>
                </button>
            </td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION, BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)); ?>
        </tr>
    <?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

