<div class="col-sm-4 col-lg-2">
    <?php
    $form_obj = new Form_element_object();
    $form_obj->cropped = 1;
    $form_obj->crop_ratio = 400/400;
    $form_obj->help_block = 'Tỉ lệ: 400x400';
    ?>
    <!-- element image   -->
    <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
</div>

<div class="col-sm-8 col-lg-10">

    <!--	    element name-->
    <div class="form-group">
        <div class="col-xs-9">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên"); ?>
        </div>
        <div class="col-xs-3"></div>
    </div>
    <div class="form-group">
        <div class="col-xs-9">
            <?php
            $form_obj = new Form_element_object();
            $form_obj->label = "Short link";
            $form_obj->field = "input_slug";
            $form_obj->field_value = $data ? $data->slug : "";
            $form_obj->help_block = "VD: tin-tuc cho link đầy đủ ".base_url("tin-tuc");
            ?>
            <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj); ?>
        </div>
        <div class="col-xs-3"></div>
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <div class="col-md-4 col-lg-3">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION) ?>
        </div>
        <div class="col-md-4 col-lg-3 custom-text-center">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
        </div>
    </div>

</div>

<div class="clear"></div>

<div class="form-group no-margin">
    <div class="col-xs-12">
        <!--element content-->
        <!--element seo-->
        <!--element action-->
        <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_SEO, BASE_ADMIN_FORM_ELEMENT_ACTION), false, "col-md-11"); ?>
    </div>
</div>

<?php load_view($theme_template . '/script'); ?>