<thead>
<tr>
    <th class="width_50">STT</th>
    <th class="width_100">Ảnh</th>
    <th class="width_150">Tên</th>
    <th class="visible-lg">Mô tả</th>
    <th class="width_80">Vị trí</th>
    <th class="width_50">MENU</th>
<!--    <th class="width_50">HOME</th>-->
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
    <?php foreach ($list as $index => $item) { ?>
        <tr id="item-<?php echo $item->id; ?>">
            <?php
            $arr['index'] = $index;
            $arr['item'] = $item;
            $this->load->vars($arr);
            ?>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_IMG)); ?>
            <td>
                <a href="<?php echo get_detail_url('', $item->name, '',NO_SUFFIX) ?>"
                   target="_blank">
                    <?php echo $item->name ?>
                </a>
            </td>
            <td class="visible-lg"><?php echo word_limiter($item->desc, 40); ?></td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION)); ?>
            <td align="center">
                <?php $status = $item->is_menu ?>
                <button class="item_menu btn-menu btn btn-flat"
                        title="Trạng thái hiển thị Nhóm sản phẩm trên menu" data-toggle="tooltip" data-placement="left"
                        <?php if ($permission_edit && $status != -1): ?>onclick="validAjax(<?php echo $item->id ?>, <?php echo $status ?>, 'is_menu', 'item_menu', 'fa fa-check', 'fa fa-ban')"
                    <?php endif; ?> >
                    <i class="fa <?php if ($status == 1) echo 'fa-check'; else if ($status == 0) echo 'fa-ban'; else echo 'fa-reply' ?>"></i>
                </button>
            </td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)); ?>
        </tr>
    <?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

