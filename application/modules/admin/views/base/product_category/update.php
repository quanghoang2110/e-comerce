<div class="col-sm-4 col-lg-2">
    <?php
    $form_obj = new Form_element_object();
    $form_obj->cropped = 1;
    $form_obj->crop_ratio = 750/750;
    $form_obj->help_block = 'Tỉ lệ: 750x750';
    ?>
    <!-- element image   -->
    <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
</div>

<div class="col-sm-8 col-lg-10">

    <!--	    element name-->
    <div class="form-group">
        <div class="col-xs-7">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên"); ?>
        </div>
        <div class="col-md-5">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="input_parent_id">Danh mục cha</label>
                    <select id="input_parent_id" class="select2 form-control"
                            name="input_parent_id">
                        <option value="">Chọn danh mục cha</option>
                        <?php foreach ($category as $item): ?>
                            <option value="<?php echo $item->id ?>"
                                <?php echo $data && $item->id == $data->parent_id ? "selected" : "" ?>><?php echo $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="input_brand">Thương hiệu hiển thị menu</label>
                <select class="js-select2 form-control" id="input_brand" name="input_brand[]"
                        style="width: 100%;" data-placeholder="Thướng hiệu liên quan tới loại sản phẩm..." multiple>
                    <option></option>
                    <?php
                    foreach ($brands as $item):
                        ?>
                        <?php $arr_id_brand = $data && $data->brand_id ? json_decode($data->brand_id) : []?>
                        <option value="<?php echo $item->id?>" <?php if (!empty($arr_id_brand) && in_array($item->id, $arr_id_brand)):?> selected <?php endif;?>><?php echo $item->name?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-9">
            <?php
            $form_obj = new Form_element_object();
            $form_obj->label = "Short link";
            $form_obj->field = "input_slug";
            $form_obj->field_value = $data ? $data->slug : "";
            $form_obj->help_block = "VD: tin-tuc cho link đầy đủ " . base_url("tin-tuc");
            ?>
            <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj); ?>
        </div>
        <div class="col-xs-3"></div>
    </div>

    <div class="clear"></div>
    <div class="form-group">
        <div class="col-md-4 col-lg-3">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION) ?>
        </div>
        <div class="col-md-4 col-lg-3 custom-text-center">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
        </div>
    </div>

</div>

<div class="clear"></div>

<div class="">
    <div class="col-xs-12">
        <div class="form-group">
            <div class="col-xs-12">
                <?php
                $form_obj = new Form_element_object();
                $form_obj->label = "Mô tả";
                $form_obj->field = "input_desc";
                $form_obj->attr = "";
                $form_obj->field_value = $data ? $data->desc : "";
                ?>
                <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC_EDITOR, $form_obj) ?>
            </div>
        </div>
        <!--element content-->
        <!--element seo-->
        <!--element action-->
        <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_SEO, BASE_ADMIN_FORM_ELEMENT_ACTION), false, "col-md-11"); ?>
    </div>
</div>

<?php
//load script for submit form
$before_submit_data = array('id_tinymce' => 'input_desc');
load_view($theme_template . '/script', $before_submit_data);
?>