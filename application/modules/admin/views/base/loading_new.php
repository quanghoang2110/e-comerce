<div id="loading">
    <div class="cssload-container">
        <div class="cssload-whirlpool"></div>
        <div class="ajax_loading_text">Đang tải dữ liệu...</div>
    </div>
</div>
<script>
    function showLoading() {
        $('#loading').removeClass('hidden');
    }
    function hideLoading() {
        $('#loading').addClass('hidden');
    }

</script>
<script>
    showLoading();
    $(window).on('load', function () {
        hideLoading();
    });
</script>
<style>
    #loading {
        /*display: none;*/
        position: fixed;
        text-align: center;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        background: rgba(0, 0, 0, 0.9);
        z-index: 999999999;
    }

    .ajax_loading_text {
        color: #f0f0f0;
        margin-top: 100px;
    }

    .cssload-container {
        position: absolute;
        top: calc(50% - 150px);
        left: calc(50% - 50px);
    }

    .cssload-whirlpool,
    .cssload-whirlpool::before,
    .cssload-whirlpool::after {
        position: absolute;
        top: 50%;
        left: 50%;
        border: 1px solid rgb(255, 255, 255);
        border-left-color: rgb(255, 255, 255);
        border-radius: 974px;
        -o-border-radius: 974px;
        -ms-border-radius: 974px;
        -webkit-border-radius: 974px;
        -moz-border-radius: 974px;
    }

    .cssload-whirlpool {
        margin: -24px 0 0 -24px;
        height: 49px;
        width: 49px;
        animation: cssload-rotate 1150ms linear infinite;
        -o-animation: cssload-rotate 1150ms linear infinite;
        -ms-animation: cssload-rotate 1150ms linear infinite;
        -webkit-animation: cssload-rotate 1150ms linear infinite;
        -moz-animation: cssload-rotate 1150ms linear infinite;
    }

    .cssload-whirlpool::before {
        content: "";
        margin: -22px 0 0 -22px;
        height: 43px;
        width: 43px;
        animation: cssload-rotate 1150ms linear infinite;
        -o-animation: cssload-rotate 1150ms linear infinite;
        -ms-animation: cssload-rotate 1150ms linear infinite;
        -webkit-animation: cssload-rotate 1150ms linear infinite;
        -moz-animation: cssload-rotate 1150ms linear infinite;
    }

    .cssload-whirlpool::after {
        content: "";
        margin: -28px 0 0 -28px;
        height: 55px;
        width: 55px;
        animation: cssload-rotate 2300ms linear infinite;
        -o-animation: cssload-rotate 2300ms linear infinite;
        -ms-animation: cssload-rotate 2300ms linear infinite;
        -webkit-animation: cssload-rotate 2300ms linear infinite;
        -moz-animation: cssload-rotate 2300ms linear infinite;
    }

    @keyframes cssload-rotate {
        100% {
            transform: rotate(360deg);
        }
    }

    @-o-keyframes cssload-rotate {
        100% {
            -o-transform: rotate(360deg);
        }
    }

    @-ms-keyframes cssload-rotate {
        100% {
            -ms-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes cssload-rotate {
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @-moz-keyframes cssload-rotate {
        100% {
            -moz-transform: rotate(360deg);
        }
    }
</style>