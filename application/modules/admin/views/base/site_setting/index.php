<?php $this->load->vars(array('data' => $info)) ?>
<?php
$setting = new Setting_object($info);
//print_r($setting);
?>
<div class="col-sm-4 col-lg-2">
    <div class="col-xs-12">
        <?php
        $form_obj = new Form_element_object();
        $form_obj->label = "Logo header";
        $form_obj->field = "input_logo";
        $form_obj->attr = "required";
        $form_obj->field_value = $setting->logo;
        $form_obj->cropped = 1;
        $form_obj->crop_ratio = 207 / 90;
        $form_obj->help_block = "Size: 207x90";
        ?>
        <!-- element image   -->
        <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
    </div>

</div>

<div class="col-sm-8 col-lg-10">
    <div class="form-group">
        <div class="col-md-7">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên website", "", $info ? $info->name : ''); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_SEO); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <?php
            $form_obj = new Form_element_object();
            $form_obj->label = "SEO keywords";
            $form_obj->field = "input_meta_keyword";
            $form_obj->field_value = $setting->meta_keyword;
            $form_obj->help_block = "SEO keywords";
            ?>
            <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC, $form_obj); ?>
        </div>
    </div>
</div>

<div class="clear"></div>
<!-- meta title input -->

<div class="form-group">
    <div class="col-md-6">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Facebook fanpage", "input_fb_fanpage", $info ? $info->fb_fanpage : '', "Fanpage của website"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Twitter", "input_twitter", $info ? $info->twitter : '', 'Twitter page'); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Pinterest", "input_pinterest", $info ? $info->pinterest : '', 'Pinterest page'); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Kênh Youtube", "input_youtube", $info ? $info->youtube : '', "Liên kết Youtube Channel"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "URL bộ công thương", "input_url_bct", $info ? $info->url_bct : '', "URL bộ công thương"); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Instagram", "input_instagram", $info ? $info->instagram : '', "Instagram page"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Google+", "input_gg_plus", $info ? $info->gg_plus_id : '', "Google+ page"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Skype", "input_company_skype", $info ? $info->skype : '', "Skype hỗ trợ"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Linkedin", "input_linkedin", $info ? $info->linkedin : '', "Linkedin page"); ?>
        </div>
    </div>
</div>

<div class="clear"></div>
<div class="block-header m-header">
    <h3 class="block-title">Các mã theo dõi</h3>
</div>
<div class="form-group">
    <div class="col-md-4">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Facebook APP_ID", "input_fb_app_id", $setting->fb_app_id, "APP_ID sử dụng cho các dịch vụ của facebook"); ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Google Analytics ID", "input_gg_analytics_id", $setting->gg_analytics_id, "Sử dụng cho theo dõi dữ liệu website"); ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Google Tag Manager", "input_gg_tag_manager_id", $setting->gg_tag_manager_id, "Sử dụng cho theo dõi dữ liệu website"); ?>
        </div>
    </div>
</div>

<div class="clear"></div>
<div class="block-header m-header">
    <h3 class="block-title">Thông tin công ty</h3>
</div>

<div class="form-group">
    <div class="col-md-6">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên công ty", "input_company_name", $info ? $info->company_name : '', "Tên công ty"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Email", "input_company_email", $info ? $info->company_email : '', "Email liên hệ"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Giờ làm việc", "input_company_time", $info ? $info->company_time : '', "Thời gian làm việc"); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Mã số thuế", "input_company_tax_code", $info ? $info->company_tax_code : '', "Mã số thuế"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Điện thoại", "input_company_phone", $info ? $info->company_phone : '', "Điện thoại liên hệ"); ?>
        </div>
    </div>
</div>

<div class="clear"></div>
<div class="block-header m-header">
    <h3 class="block-title">Liên hệ</h3>
</div>
<div class="form-group">
    <div class="col-md-6">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Hotline mua hàng", "input_company_phone1", $info ? $info->company_phone_1 : '', "Hotline mua hàng"); ?>
        </div>
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Hotline đặt hàng, hỏi đáp", "input_company_phone3", $info ? $info->company_phone_3 : '', "Hotline đặt hàng hỏi đáp"); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-xs-12">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Hotline khiếu lại", "input_company_phone2", $info ? $info->company_phone_2 : '', "Hotline khiếu lại"); ?>
        </div>
    </div>
</div>

<div class="col-xs-12">
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Địa chỉ", "input_company_address", $info ? $info->company_address : '', "Địa chỉ công ty"); ?>
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_DESC, "Thông tin khác", "input_company_info", $info ? $info->company_info : '', "Các thông tin khác"); ?>
</div>

<div class="clear"></div>
<div class="block-header m-header" style="margin-top: 30px">
    <h3 class="block-title">Thông tin khác</h3>
</div>
<div class="col-xs-12">
    <div class="form-group">
        <div class="col-xs-12">
            <?php
            $form_obj = new Form_element_object();
            $form_obj->label = "Email nhận thông báo";
            $form_obj->field = "input_support_email";
            $form_obj->field_value = $setting->support_email;
            $form_obj->help_block = "Nhập email trên từng dòng";
            ?>
            <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC, $form_obj); ?>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-primary">Lưu</button>
    </div>
</div>
<style>
    .image-update {
        height: 50px;
    }

    #img-update {
        width: unset !important;
    }

    .block-header.m-header {
        padding: 0;
        padding-bottom: 15px;
    }
</style>
<script type="text/javascript">
    backMainPage = false;
</script>
<?php
//load script for submit form
load_view($theme_template . '/script');
?>
