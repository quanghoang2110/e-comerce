<thead>
<tr>
    <th style="width: 80px">STT</th>
    <th>Tài khoản</th>
    <th>Họ tên</th>
    <th>Số điện thoại</th>
    <th>Địa chỉ</th>
    <?php if ($permission_edit) { ?>
        <th class="width_150">Trạng thái</th>
        <th class="width_120">Thao tác</th>
    <?php } ?>
</tr>
</thead>
<tbody>
<?php if ($list) {
    foreach ($list as $index => $item) { ?>
        <?php
        $arr['index'] = $index;
        $arr['item'] = $item;
        $this->load->vars($arr);
        ?>
        <tr id="item-<?php echo $item->id; ?>">
            <?php load_table_td(BASE_ADMIN_TABLE_TD_STT) ?>
            <td><?php echo($item->username); ?></td>
            <td><?php echo($item->fullname); ?></td>
            <td><?php echo($item->phone); ?></td>
            <td><?php echo($item->address); ?></td>
            <?php if ($permission_edit || $permission_delete) { ?>
                <?php load_table_td(BASE_ADMIN_TABLE_TD_VALID) ?>
                <td class="text-center">
                    <?php if ($permission_delete) { ?>
                        <button class="btn btn-icon-toggle" title="Xóa" data-toggle="tooltip" data-placement="left"
                                onclick="deleteAjax(<?php echo $item->id ?>)">
                            <i class="fa fa-close"></i>
                        </button>
                    <?php } ?>
                </td>
            <?php } ?>
        </tr>
    <?php }
} ?>
</tbody>
<div class="modal" id="modal_update"></div>
<?php load_view($theme_template . '/script'); ?>

<script type="text/javascript">

    function updateAjax(id) {
        var data = {};
        if (typeof (id) !== 'undefined') {
            data = {id: id};
        }
        $.post(urlUpdate, data)
            .done(function (data) {
                    $('#modal_update').html(data).modal("show");
                    var frm = $('#form_update');
                    frm.attr('action', "<?php echo base_url($permission_url)?>");
                }
            );
    }

    function resetAdminPass(id, name) {
        swal({
                title: "Reset pass: " + name,
                text: "Reset về mật khẩu mặc định cho tài khoản này?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Hủy",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Đồng ý",
                showLoaderOnConfirm: true,
                closeOnConfirm: false
            },
            function () {
                $.post("<?php echo base_url(BASE_ROUTE_ADMIN_RESET_PASS)?>", {id: id})
                    .done(function (data) {
                            var obj = JSON.parse(data);
                            console.log(obj);
                            if (obj.error == 1) {
                                swal("Lỗi!", obj.msg, "error");
                            } else {
                                swal("Thành công", obj.msg, "success");
                            }
                        }
                    )
                ;
            })
        ;
    }

</script>
