<?php
$module_title = $module_title ? $module_title : "ADMIN MODULES";
$theme_template = $theme_template ? $theme_template : "oneui";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title><?php echo strip_tags($module_title) ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <!--    <link rel="shortcut icon" href="--><?php //echo $favicon; ?><!--" type="image/x-icon">-->
    <!--    <link rel="icon" href="--><?php //echo $favicon; ?><!--" type="image/x-icon">-->

    <?php $this->load->view('admin/base/base_head'); ?>

    <!-- load head tag -->
    <?php
    if (file_exists(MODULES_PATH_ADMIN_VIEWS . $theme_template . "/head.php"))
        $this->load->view("{$theme_template}/head");
    ?>

</head>

<body>

<?php include_once 'spinner.php' ?>
<?php include_once 'loading_new.php' ?>

<!--load content-->
<?php
//echo $theme_template;
$mfw_base_content = $theme_template . 'content';
if (file_exists(MODULES_PATH_ADMIN_VIEWS . $mfw_base_content . ".php")) {
    $this->load->view($mfw_base_content);
} ?>

<!--load footer-->
<?php
$mfw_base_footer = $theme_template . 'footer';
if (file_exists(MODULES_PATH_ADMIN_VIEWS . $mfw_base_footer . ".php"))
    $this->load->view($mfw_base_footer);
?>

</body>

</html>