<div class="col-sm-5 col-lg-4">
    <?php
    $form_obj = new Form_element_object();
    $form_obj->cropped = 1;
    $form_obj->crop_ratio = 1200/670;
    $form_obj->help_block = 'Tỉ lệ: 1200x670';
    ?>
    <!-- element image   -->
    <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
</div>

<div class="col-sm-7 col-lg-6">

    <!--	    element name-->
    <div class="form-group">
        <div class="col-xs-7">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên"); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-9">
            <?php
            $form_obj = new Form_element_object();
            $form_obj->label = "Link";
            $form_obj->field = "input_link";
            $form_obj->field_value = $data ? $data->link : "";
            $form_obj->help_block = "VD: tin-tuc cho link đầy đủ " . base_url("tin-tuc");
            ?>
            <?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj); ?>
        </div>
        <div class="col-xs-3"></div>
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <div class="col-md-4 col-lg-3 custom-text-center">
            <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
        </div>
    </div>

</div>

<div class="clear"></div>

<div class="">
    <div class="col-xs-12">
        <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_ACTION), false, "col-md-11"); ?>
    </div>
</div>

<?php
//load script for submit form
$before_submit_data = array('id_tinymce' => 'input_desc');
load_view($theme_template . '/script', $before_submit_data);
?>