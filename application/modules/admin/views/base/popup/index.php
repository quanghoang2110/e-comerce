<thead>
<tr>
    <th class="width_50">STT</th>
    <th class="width_100">Ảnh</th>
    <th class="width_150">Tên</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
    <?php foreach ($list as $index => $item) { ?>
        <tr id="item-<?php echo $item->id; ?>">
            <?php
            $arr['index'] = $index;
            $arr['item'] = $item;
            $this->load->vars($arr);
            ?>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT)); ?>
            <td align="center">
                <div class="theme-img width_200">
                    <a href="#img" onclick="viewRealImg($(this))" data-toggle="modal"
                       data-target="#modalRealImg">
                        <?php echo get_img_tag($item->image, $item->name, 'table-image img-thumbnail', BASE_IMG_DEFAULT); ?>
                    </a>
                </div>

            </td>
            <td>
                <a href="<?php echo get_detail_url('', $item->name, '',NO_SUFFIX) ?>"
                   target="_blank">
                    <?php echo $item->name ?>
                </a>
            </td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)); ?>
        </tr>
    <?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

