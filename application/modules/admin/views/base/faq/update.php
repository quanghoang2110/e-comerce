<!-- element name-->
<div class="col-xs-8">
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tiêu đề"); ?>
</div>

<div class="clear"></div>
<!--            element array position & valid-->
<div class="col-lg-3 col-md-4 col-sm-6">
    <!--        element position-->
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION); ?>
</div>
<div class="col-lg-3 col-md-4 col-sm-6 custom-text-center">
    <!--        element valid-->
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID); ?>
</div>
<div class="col-xs-12">
    <div class="type-content" style="display: <?php echo $data && $data->type == 'VIDEO' ? 'none' : 'block' ?>">
        <div style="margin-top: 15px"></div>
        <div class="form-group">
            <label for="input_products_related">Sản phẩm liên quan</label>
            <select class="js-select2 form-control" id="input_products_related" name="input_products_related[]"
                    style="width: 100%;" data-placeholder="Sản phẩm liên quan tới bài viết.." multiple>
                <option></option>
                <?php $products_related = explode(',', $data->products_related); ?>
                <?php foreach ($products as $item): ?>
                    <option value="<?php echo $item->id ?>"
                        <?php echo is_array($products_related) && in_array($item->id, $products_related) ? 'selected' : '' ?>>
                        <?php echo $item->name ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="col-xs-12">
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_CONTENT); ?>
</div>
<div class="clear"></div>
<div class="col-xs-12">
    <!--element Noi dung & Action-->
    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_ACTION); ?>
</div>
<?php
//load script for submit form
$before_submit_data = array('id_tinymce' => 'input_content');
load_view($theme_template . '/script', $before_submit_data);
?>