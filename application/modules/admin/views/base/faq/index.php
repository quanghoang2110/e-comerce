<thead>
<tr>
    <th class="width_50">STT</th>
    <th>Tiêu đề</th>
    <!--        <th class="width_100">Loại</th>-->
    <th class="width_50">Vị trí</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) {
//	$type = unserialize(FAQ_TYPE);
	?>
	<?php foreach ($list as $index => $item) { ?>
		<?php
		$arr['index'] = $index;
		$arr['item'] = $item;
		$this->load->vars($arr);
		?>
        <tr id="item-<?php echo $item->id; ?>">
			<?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT)) ?>
            <td>
                <a href="<?php echo get_detail_url(ROUTE_FAQ, $item->name, $item->id) ?>"
                   target="_blank">
                    <?php echo $item->name ?>
                </a>
            </td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION, BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)) ?>
        </tr>
	<?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

