<div class="col-sm-4 col-lg-2">
	<?php
	$form_obj = new Form_element_object();
	$form_obj->cropped = 1;
	$form_obj->crop_ratio = 1;
	?>
    <!-- element image   -->
	<?php load_form_element_object(BASE_ADMIN_FORM_ELEMENT_IMG, $form_obj); ?>
</div>

<div class="col-sm-8 col-lg-10">

    <div class="form-group">
        <div class="col-xs-9">
            <!-- element image   -->
			<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên danh mục"); ?>
        </div>
    </div>

    <div class="clear"></div>

    <div class="form-group">
        <div class="col-sm-4">
            <div class="form-material floating inline-select2">
                <label for="input_level">Cấp danh mục <i
                            title="Có thể cập nhật sang cấp danh mục cùng cấp hoặc cao hơn"
                            data-toggle="tooltip" data-placement="top"
                            class="is-link fa fa-question-circle"></i> </label>
                <select id="input_level" class="form-control" name="input_level"
                        onchange="changeCategoryLevel($(this).val())">
					<?php for ($i = 1; $i <= $level_avaiable; $i++) : ?>
                        <option value="<?php echo $i ?>" <?php echo $data && $data->level == $i ? 'selected="selected"' : ''; ?>><?php echo "Danh mục cấp " . $i ?></option>
					<?php endfor; ?>
                </select>
            </div>
        </div>
        <div id="dmlv1"
             class="col-sm-4 <?php echo $data && $data->level > 1 ? '' : 'hidden' ?>">
            <div class="form-material floating inline-select2">
                <label for="input_level1_id">Danh mục cha cấp 1</label>
                <select id="input_level1_id" class="form-control" name="input_level1_id"
                        onchange="onChangeLevel1($(this).val())">
					<?php foreach ($categories1 as $cate) : ?>
                        <option value="<?php echo $cate->id ?>" <?php echo $data && $data->parent_id == $cate->id ? 'selected="selected"' : ''; ?>><?php echo $cate->name ?></option>
					<?php endforeach; ?>
                </select>
            </div>
        </div>
        <div id="dmlv2" class="col-sm-4 <?php echo $data && $data->level > 2 ? '' : 'hidden' ?>">
            <div class="form-material floating inline-select2 open">
                <label for="input_level2_id">Danh mục cha cấp 2</label>
                <select id="input_level2_id" class="form-control" name="input_level2_id">
					<?php foreach ($categories2 as $cate) : ?>
                        <option value="<?php echo $cate->id ?>" <?php echo $cate->id == $this->input->get('id') ? 'selected="selected"' : ''; ?>><?php echo $cate->name ?></option>
					<?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>

    <div class="clear"></div>
    <div class="form-group">
        <div class="col-md-4 col-lg-3">
			<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION) ?>
        </div>
        <div class="col-md-4 col-lg-3 custom-text-center">
			<?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
        </div>
    </div>

</div>

<div class="clear"></div>

<div class="form-group">
    <div class="col-xs-12">
        <!--element desc, seo, action-->
		<?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_DESC, BASE_ADMIN_FORM_ELEMENT_SEO, BASE_ADMIN_FORM_ELEMENT_ACTION)); ?>
    </div>
</div>

<?php
//load script for submit form
load_view($theme_template . '/script');
?>
<script>

	<?php if($data && $data->level > 2):?>
    changeCategoryLevel(3);
	<?php endif; ?>

    function changeCategoryLevel(level) {
        if (level > 1) {
            $('#dmlv1').removeClass('hidden');
            if (level > 2) {
                $('#dmlv2').removeClass('hidden');
                onChangeLevel1();
            }
            else $('#dmlv2').addClass('hidden');
        } else {
            $('#dmlv1').addClass('hidden');
            $('#dmlv2').addClass('hidden');
        }
    }

    function onChangeLevel1() {
//        console.log($('#input_level').val() + '|' + urlUpdate);
        if ($('#input_level').val() > 2) {
            showLoading();
            $.post(urlUpdate, {
                type: 'choose_level',
                level1_id: $('#input_level1_id').val(),
                id:<?php echo $id?>}).done(function (data) {
                hideLoading();
                $('#input_level2_id').html(data);
                $('#dmlv2').removeClass('hidden');
            });
        }
    }
</script>
