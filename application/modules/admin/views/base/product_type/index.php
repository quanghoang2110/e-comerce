<thead>
<tr>
    <th class="width_50" colspan="<?php echo $level ?>">STT</th>
    <th class="width_300">Tên</th>
    <th class="visible-lg">Mô tả</th>
    <th class="width_80">Vị trí</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php show_item($list, 1, $level, $permission_delete); ?>
<?php
function show_item($list, $level, $level_max, $permission_delete)
{
    $arr = $level == 1 ? $list : (isset($list->sub) ? $list->sub : array());
    foreach ($arr as $i3 => $item):
        $arr['index'] = $i3;
        $arr['item'] = $item;
        $arr['permission_delete'] = key_exists('sub', $item) ? false : $permission_delete;
        $ci = &get_instance();
        $ci->load->vars($arr);
        ?>
        <tr id="item-<?php echo $item->id; ?>" class="level<?php echo $level ?>">
            <?php for ($x = 1; $x < $level; $x++): ?>
                <td style="width: 10px"></td>
            <?php endfor; ?>
            <td align="center" class="x10" colspan="<?php echo $level_max ?>"><?php echo($i3 + 1); ?></td>
            <td>
                <a href="#img" onclick="viewRealImg($(this))" data-toggle="modal"
                   data-target="#modalRealImg">
                    <?php echo get_img_tag($item->image, $item->name, 'table-image icon-image', BASE_IMG_DEFAULT); ?>
                </a>
                <?php echo $item->name; ?>
            </td>
            <td class="visible-lg"><?php echo word_limiter($item->desc, 60); ?></td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION, BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)); ?>
        </tr>
        <?php show_item($item, $level + 1, $level_max - 1, $permission_delete); ?>
    <?php endforeach ?>
<?php } ?>

</tbody>
<!--<div id="ajax_content"></div>-->
<?php load_view($theme_template . '/script'); ?>
<style>
    .level1 td {
        background: #eee;
        border-top: 1px solid #ddd !important;
        border-bottom: 1px solid #ddd !important;
        font-weight: bolder;
    }

    .level1 td:last-child,
    .level2 td:last-child {
        border-right: 1px solid #eee !important;
    }

    .level1 td .select2-container--default .select2-selection--single {
        /*background: #eee;*/
        border-bottom-color: #eee;
    }

    .level1 td button, .level2 td button, .level3 td button {
        border: 1px solid #eee;
    }

    .level2 td {
        /*background: #e9e9e9;*/
        border-bottom: 1px solid #eee !important;
    }

    .level2 td:first-child {
        border-left: 1px solid #eee !important;
        background: #f9f9f9;
        border-top: none;
        border-bottom: none !important;
    }

    .level2 td .select2-container--default .select2-selection--single {
        background: #e9e9e9;
        border-bottom-color: #eee;
    }

    .level3 td {
        font-style: italic;
        border-bottom: 1px solid #eee !important;
    }

    .level3 td .select2-container--default .select2-selection--single {
        background: #fff;
        border-bottom-color: #eee;;
    }

    .x10 {
        width: 50px;
        border-left: 1px solid #eee !important;
    }

    .icon-image, .theme-img img {
        width: 40px;
        height: 40px;
        border: 1px solid #eee;
    }

    #ajax_content {
        display: none;
    }

</style>
<!--<script>-->
<!--    function updateLevelAjax() {-->
<!--        $.post(urlUpdate, {type: "cate_level"}).done(function (data) {-->
<!--            $('#main_content').fadeOut();-->
<!--            $('#ajax_content').fadeIn().html(data);-->
<!--        });-->
<!--    }-->
<!--</script>-->
