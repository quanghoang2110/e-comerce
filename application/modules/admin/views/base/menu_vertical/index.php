<thead>
<tr>
    <th class="width_50">STT</th>
    <th>Tiêu đề</th>
    <th class="visible-lg">URL</th>
    <th class="width_80">Vị trí</th>
    <th class="width_110">Khả dụng</th>
    <th class="width_110">Thao tác</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
    <?php foreach ($list as $index => $item) { ?>
        <tr id="item-<?php echo $item->id; ?>">
            <?php
            $arr['index'] = $index;
            $arr['item'] = $item;
            $this->load->vars($arr);
            ?>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_NAME)); ?>
            <td class="visible-lg">
                <a href="<?php echo $item->url ?>" target="_blank">
                    <?php echo $item->url ?>
                </a>
            </td>
            <?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION, BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)); ?>
        </tr>
    <?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

