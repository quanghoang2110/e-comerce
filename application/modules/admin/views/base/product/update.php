<script src="/assets/base/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>

<div class="">
    <div class="form-group">
        <div class="col-md-7">
            <div class="col-xs-12">
                <!--	    element name-->
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_NAME, "Tên sản phẩm"); ?>
            </div>
        </div>
        <div class="col-md-5">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="input_category">Loại sản phẩm</label>
                    <select id="input_category" class="select2 form-control"
                            name="input_category">
                        <?php foreach ($category as $item): ?>
                            <option value="<?php echo $item->slug . "@" . $item->id ?>"
                                <?php echo $data && $item->id == $data->category_id ? "selected" : "" ?>><?php echo $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-3">
            <div class="col-xs-12">
                <?php
                $form_element = new Form_element_object();
                $form_element->label = "Mã sản phẩm";
                $form_element->field = "input_code";
                $form_element->field_value = $data ? $data->code : '';
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_element);
                ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-xs-12">
                <?php
                $form_element = new Form_element_object();
                $form_element->label = "Trọng lượng";
                $form_element->field = "input_weight";
                $form_element->field_value = $data ? $data->weight : '';
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_element);
                ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-xs-12">
                <?php
                $form_element = new Form_element_object();
                $form_element->label = "Đơn vị";
                $form_element->field = "input_pice";
                $form_element->field_value = $data ? $data->pice : '';
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_element);
                ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="input_size">Kích thước</label>
                    <select id="input_size" class="select2 form-control"
                            name="input_size">
                        <option value="1" <?php echo ($data && $data->size == 1) ? "selected" : "" ?>>Nhỏ</option>
                        <option value="2" <?php echo ($data && $data->size == 2) ? "selected" : "" ?>>Trung bình
                        </option>
                        <option value="3" <?php echo ($data && $data->size == 3) ? "selected" : "" ?>>Lơn</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-xs-12">
                <?php
                $form_element = new Form_element_object();
                $form_element->label = "Xuất xứ";
                $form_element->field = "input_from";
                $form_element->field_value = $data ? $data->from : '';
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_element);
                ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-xs-12">
                <?php
                $form_element = new Form_element_object();
                $form_element->label = "Hãng sản xuất";
                $form_element->field = "input_manufacturer";
                $form_element->field_value = $data ? $data->manufacturer : '';
                load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_element);
                ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="input_trademark">Thương hiệu</label>
                    <select id="input_trademark" class="select2 form-control"
                            name="input_trademark">
                        <?php foreach ($trademark as $item): ?>
                            <option value="<?php echo $item->id ?>" <?php if (!empty($data) && $data->trademark == $item->id): echo 'selected'; endif; ?>><?php echo $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="input_brand">Dòng</label>
                    <select id="input_brand" class="select2 form-control"
                            name="input_brand">
                        <option value="">Chọn dòng sản phẩm</option>
                        <?php foreach ($brands as $itemBrand): ?>
                            <option value="<?php echo $itemBrand->id ?>" <?php if (!empty($data) && $data->brand == $itemBrand->id): echo 'selected'; endif; ?>><?php echo $itemBrand->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-xl-1 col-md-3">
            <div class="col-xs-12">
                <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION); ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label for="input_video_related">Video liên quan</label>
            <select class="js-select2 form-control" id="input_video_related" name="input_video_related[]"
                    style="width: 100%;" data-placeholder="Video liên quan tới sản phẩm.." multiple>
                <option></option>
                <?php
                    foreach ($videos as $item):
                ?>
                    <?php $arr_id_video = ($data && !empty($data->video_related)) ? json_decode($data->video_related) : []?>
                    <option value="<?php echo $item->id?>" <?php if (!empty($arr_id_video) && in_array($item->id, $arr_id_video)):?> selected <?php endif;?>><?php echo $item->name?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label for="input_gift">Quà tặng</label>
            <select class="js-select2 form-control" id="input_gift" name="input_gift[]"
                    style="width: 100%;" data-placeholder="Quà tặng liên quan tới sản phẩm.." multiple>
                <option></option>
                <?php
                foreach ($gifts as $item):
                    ?>
                    <?php $arr_gift = ($data && !empty($data->gift)) ? json_decode($data->gift) : []?>
                    <option value="<?php echo $item->id?>" <?php if (!empty($arr_gift) && in_array($item->id, $arr_gift)):?> selected <?php endif;?>><?php echo $item->name?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
</div>

<div class="clear"></div>

<div class="form-group">
    <div class="col-xs-12">
        <label for="title">Ảnh sản phẩm</label>
        <div class="">
            <ul class="list-unstyled left no-sort">
                <li><a href="#" class="pro-img" onclick="addProductImg()" title="Thêm ảnh"><i
                                class="fa fa-plus"></i> </a></li>
            </ul>
            <ul class="list-unstyled" id="ls_product_img">
                <?php $total = 0; ?>
                <?php if ($data && $data->image) { ?>
                    <?php $img_arr = json_decode($data->image); ?>
                    <?php $total = count($img_arr); ?>
                    <?php foreach ($img_arr as $i => $img): ?>
                        <li class="left">
                            <div class="img" onclick="changeProductImg('img_<?php echo $i ?>')">
                                <?php echo get_img_tag($img, '', 'is-link', BASE_IMG_DEFAULT); ?>
                                <input type="text" id="img_<?php echo $i ?>" name="input_image[]"
                                       class="rfm-link hidden"
                                       value="<?php echo $img; ?>" required>
                                <a href="#delete" class="absolute delete-this-img" title="Xóa ảnh này"><i
                                            class="fa fa-close"></i></a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<div class="clear"></div>

<div class="form-group">
    <div class="col-md-3">
        <div class="col-xs-12">
            <?php
            $form_element = new Form_element_object();
            $form_element->label = "Video";
            $form_element->field = "input_video";
            $form_element->field_value = $data ? $data->video : '';
            load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_element);
            ?>
        </div>
    </div>
    <div class="col-sm-3">
        <label for="price_1">Giá niêm yết</label>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <input id="price_1" class="form-control is-price" type="text" name="input_price_1" min="0"
                   value="<?php echo $data ? $data->price_1 : ''; ?>"/>
        </div>
    </div>
    <div class="col-sm-3">
        <label for="price_2">Giá khuyến mại</label>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <input id="price_2" class="form-control is-price" type="text" name="input_price_2" min="0"
                   value="<?php echo $data ? $data->price_2 : ''; ?>"/>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="input_star">Đánh giá sao</label>
            <select id="input_star" class="select2 form-control"
                    name="input_star">
                <option value="5" <?php echo $data && $data->star == 5 ? 'selected' : ''?>>5</option>
                <option value="4" <?php echo $data && $data->star == 4 ? 'selected' : ''?>>4</option>
                <option value="3" <?php echo $data && $data->star == 3 ? 'selected' : ''?>>3</option>
                <option value="2" <?php echo $data && $data->star == 2 ? 'selected' : ''?>>2</option>
                <option value="1" <?php echo $data && $data->star == 1 ? 'selected' : ''?>>1</option>
            </select>
        </div>
    </div>
</div>
<div class="clear"></div>

<div class="form-group">
    <div class="col-lg-2 col-md-3">
        <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID); ?>
    </div>
    <div class="col-md-3 col-lg-2">
        <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID, "SP nổi bật", "input_is_hot", $data && $data->is_hot); ?>
    </div>
    <div class="col-md-3 col-lg-2">
        <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID, "Bán chạy", "input_is_best_seller", $data && $data->is_best_seller); ?>
    </div>
    <div class="col-md-3 col-lg-2">
        <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID, "SP mới", "input_is_new", $data && $data->is_new); ?>
    </div>
    <div class="col-md-3 col-lg-2">
        <?php
        $form_element = new Form_element_object();
        $form_element->label = "Hết hàng";
        $form_element->field = "input_empty";
        $form_element->field_value = $data ? $data->is_empty : '';
        $form_element->label_class = "switch-danger";
        load_form_element_object(BASE_ADMIN_FORM_ELEMENT_VALID, $form_element);
        ?>
    </div>
    <div class="col-md-3 col-lg-2">
        <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID, "SP giảm giá", "input_is_discount", $data && $data->is_discount); ?>
    </div>
</div>
<div class="clear"></div>
<div class="col-xs-12">
    <?php
    $form_element = new Form_element_object();
    $form_element->label = "Mô tả";
    $form_element->field = "input_desc";
    $form_element->field_value = $data ? $data->desc : '';
    $form_element->attr = 'rows=5';
    load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC, $form_element);
    ?>
</div>
<!--    <div class="col-xs-12">-->
<?php
//        $form_element = new Form_element_object();
//        $form_element->label = "Thông số kỹ thuật";
//        $form_element->field = "input_content1";
//        $form_element->field_value = $data ? $data->content1 : '';
//        $form_element->attr = 'none';
//        load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC, $form_element); ?>
<!--    </div>-->
<div class="col-xs-12">
    <?php
    $form_element = new Form_element_object();
    $form_element->label = "Thông tin sản phẩm";
    $form_element->field = "input_content2";
    $form_element->field_value = $data ? $data->content : '';
    $form_element->attr = 'none';
    load_form_element_object(BASE_ADMIN_FORM_ELEMENT_DESC, $form_element); ?>
</div>
<div class="col-xs-12">
    <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_SEO, BASE_ADMIN_FORM_ELEMENT_ACTION)); ?>
</div>

<?php
//load script for submit form
$before_submit_data = array('id_tinymce' => 'input_desc,input_content2');
load_view($theme_template . '/script', $before_submit_data);
?>
<style>
    .left {
        float: left;
    }

    .no-sort {
        margin-right: 7.5px;
    }

    .c_checked {
        padding-top: 0;
    }

    .pro-img {
        width: 100px;
        height: 100px;
        display: inline-block;
        text-align: center;
        line-height: 100px;
        color: #ccc;
        border: 1px dashed #ccc;
        /*border-style: dashed;*/
        font-size: 20px;
        background: #f0f0f0;
    }

    #ls_product_img li {
        float: left;
        margin-right: 7.5px;
        position: relative;
    }

    #ls_product_img li div {
        width: 100px;
        height: 100px;
        border: 1px solid #ccc;
        padding: 2px;
    }

    #ls_product_img li div a {
        position: absolute;
        top: 0;
        right: 5px;
        color: #333;
    }

    #ls_product_img li div:hover a {
        color: darkred;
    }

    #ls_product_img li div:hover img {
        opacity: 0.3;
    }

    #ls_product_img li div img {
        max-width: 100%;
        max-height: 100%;
    }

    .list-unstyled li {
        margin-top: 5px;
    }

    .clear {
        clear: both;
    }
</style>

<!-- responsive file manager -->
<div class="modal fade modal-fullheight" id="modalProductImg" style="z-index: 9999;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ảnh sản phẩm</h4>
            </div>
            <div class="modal-body">
                <iframe id="if_product_img"
                        src="<?php echo base_url(); ?>filemanager/dialog.php?lang=vi&type=1&field_id=input_image"
                        frameborder="0" height="100%" width="100%"></iframe>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.end responsive file manager -->
<!--model file manager fullscreen-->

<ul class="hidden" id="img_hidden_x">
    <li class="left">
        <div class="img">
            <img src="<?php echo BASE_IMG_DEFAULT ?>" class="is-link"/>
            <input type="text" class="hidden">
            <a href="#delete" class="absolute delete-this-img"><i class="fa fa-close"></i></a>
        </div>
    </li>
</ul>

<script>
    var total_img = <?php echo $total ?>;
    var onAddProductImg = false;
    var iframeProductImgObj = $('#if_product_img');
    var modalProductImgObj = $('#modalProductImg');

    function addProductImg() {
        onAddProductImg = true;
        total_img++;
        var imgDataObj = $('#img_hidden_x');
        imgDataObj.find('input').attr('id', 'img_' + total_img);
        imgDataObj.find('div.img').attr({
            'onclick': 'changeProductImg(\'img_' + total_img + '\')'
        });
        $('#ls_product_img').fadeIn(300, function () {
            $(this).prepend(imgDataObj.html());
            $(this).find('input').attr({'name': 'input_image[]', 'required': true});
        });
        iframeProductImgObj.attr('src', base_url + 'filemanager/dialog.php?type=1&resize=true&field_id=img_' + total_img);
        showModalChangeProductImg();
        deleteProductImg();
    }

    function changeProductImg(id) {
        onAddProductImg = false;
        iframeProductImgObj.attr('src', base_url + 'filemanager/dialog.php?type=1&field_id=' + id);
        showModalChangeProductImg();
    }

    // on hidden modal
    modalProductImgObj.on('hide.bs.modal', function () {
        if (onAddProductImg && !$('#img_' + total_img).val()) {
            $('#img_' + total_img).parent().fadeOut(300, function () {
                $(this).remove();
            });
            total_img--;
            onAddProductImg = false;
        }
    });

    function responsive_filemanager_callback_onProductImg(field_id) {
        $('#' + field_id).parent().find('img').attr('src', $('#' + field_id).val());
    }

    function showModalChangeProductImg() {
        modalProductImgObj.modal('show');
        onProductImg = true;
    }

    deleteProductImg();

    function deleteProductImg() {
        $('.delete-this-img').click(function () {
            $(this).parent().parent().fadeOut(300, function () {
                $(this).remove();
            });
            return false;
        });
    }

    $(function () {
        $("#ls_product_img").sortable();
        $("#ls_product_img").disableSelection();

        //format number on input
        $('.is-price').number(true, 0);
    });

    function responsive_filemanager_callback(field_id) {
        $('#' + field_id).parent().find('img').attr('src', $('#' + field_id).val());
    }

</script>
<!--sort able-->


<!--format currency-->
<script src="/assets/base/lib/jquery-number/jquery.number.min.js"></script>