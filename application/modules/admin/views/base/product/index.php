<table class="table table-striped table-vcenter table-bordered">
    <thead>
    <tr>
        <th class="width_50">STT</th>
        <th class="width_100">Ảnh</th>
        <th>Tiêu đề</th>
        <th>$ Niêm yết</th>
        <th>$ Khuyến mại</th>
        <th>Loại sản phẩm</th>
        <th class="width_80">Mega menu</th>
        <th class="width_80">Vị trí</th>
        <th class="width_80">Nổi bật</th>
        <th class="width_110">Khả dụng</th>
        <th class="width_110">Thao tác</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($list) { ?>
        <?php foreach ($list as $index => $item) { ?>
            <?php
            $arr['index'] = $index;
            $imgs = json_decode($item->image);
            $item->image = isset($imgs[0]) ? $imgs[0] : "";
            $arr['item'] = $item;
            $this->load->vars($arr);
            ?>
            <tr id="item-<?php echo $item->id; ?>">
                <?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_IMG)); ?>
                <td>
                    <a href="<?php echo get_detail_url(get_route_name($item->category), $item->name, $item->id) ?>"
                       target="_blank">
                        <?php echo $item->name ?>
                    </a>
                </td>
                <td class="text-right">
                    <?php echo get_price($item->price_1, '') ?>
                </td>
                <td class="text-right">
                    <?php echo get_price($item->price_2, '') ?>
                </td>
                <td><?php echo $item->category; ?></td>
                <td align="center">
                    <?php $is_mega_menu = $item->is_mega_menu; ?>
                    <button class="is_mega_menu btn-mega btn btn-flat"
                            title="Hiển thị trên mega menu" data-toggle="tooltip" data-placement="left"
                            <?php if ($permission_edit && $is_mega_menu != -1){ ?>onclick="validAjax(<?php echo $item->id ?>, <?php echo $is_mega_menu ?>, 'is_mega_menu', 'is_mega_menu', 'fa fa-star', 'fa fa-star-o')"<?php } ?>>
                        <i class="fa <?php if ($is_mega_menu == 1) echo 'fa-star'; else echo 'fa-star-o'; ?>"></i>
                    </button>
                </td>
                <?php load_table_td(array(BASE_ADMIN_TABLE_TD_POSITION)); ?>
                <?php load_table_td(BASE_ADMIN_TABLE_TD_HOT); ?>
                <?php load_table_td(array(BASE_ADMIN_TABLE_TD_VALID, BASE_ADMIN_TABLE_TD_ACTION)); ?>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>

</table>
<?php load_view($theme_template . '/script'); ?>
<script>
    function typeAjax(id, cValid, type, str_btn_class) {
        showLoading();
        $.post(urlValid, {id: id, valid: cValid, type: type})
            .done(function (data) {
                    hideLoading();
                    var nValid = (cValid == 1) ? 0 : 1;
//                    var str_btn_class = type == 'hot' ? 'item_hot' : 'item_valid';
                    $('#item-' + id).find('button.' + str_btn_class).attr('onclick', 'validAjax(' + id + ',' + nValid + ', "' + type + '")');
                    if (nValid == 1) {
                        if (type == 'hot') {
                            $('#item-' + id).find('button.item_hot i').removeClass().addClass("fa fa-star");
                        } else {
                            $('#item-' + id).find('button.item_valid i').removeClass().addClass("fa fa-check");
                            $('#item-' + id).find('button.item_valid').attr("title", "Mở");
                            $('#item-' + id).find('.text-show-valid').html('Mở &nbsp');
                            $('#item-' + id).find('.text-show-watched-comment').html('Đã xem &nbsp &nbsp');
                        }
                    } else {
                        if (type == 'hot') {
                            $('#item-' + id).find('button.item_hot i').removeClass().addClass("fa fa-star-o");
                        } else {
                            $('#item-' + id).find('button.item_valid i').removeClass().addClass("fa fa-ban");
                            $('#item-' + id).find('button.item_valid').attr("title", "Khóa");
                            $('#item-' + id).find('.text-show-valid').html('Khóa');
                            $('#item-' + id).find('.text-show-watched-comment').html('Chưa xem');
                        }
                    }
                }
            )
        ;
    }
</script>

