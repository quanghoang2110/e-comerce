<div class="pull-left page-filter">
    <div class="page_filter_label">Lọc theo &nbsp;</div>
    <label><select name="cate" class="form-control" onchange="pageFilter()">
            <option value="0">Tất cả</option>
			<?php foreach ($category as $item): ?>
				<?php show_cate($item, 1, $this->input->get('cate')); ?>
			<?php endforeach; ?>
        </select>
		<?php function show_cate($item, $level, $selected_id)
		{ ?>
            <option value="<?php echo $item->id ?>" <?php echo ($selected_id ? ($item->id == $selected_id) : 0) ? 'selected' : '' ?>>
				<?php for ($i = 1; $i < $level; $i++) {
					echo "....";
				} ?>
				<?php echo $item->name ?>
            </option>
			<?php $level++; ?>
			<?php if (isset($item->sub)) { ?>
			<?php foreach ($item->sub as $iem) { ?>
				<?php show_cate($iem, $level, $selected_id); ?>
			<?php }
		} ?>
		<?php } ?>
    </label>
</div>
<style>
	.page-filter{
		margin-left: 50px
	}
</style>