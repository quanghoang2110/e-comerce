<td class="middle">
	<?php if (Theme_object::$is_material_design): ?>
        <div data-toggle="tooltip" data-placement="left" title="Thay đổi vị trí" class="position">
            <select class="js-select2 form-control"
                    onchange="changePosition(<?php echo $item->id ?>, $(this).val())">
				<?php for ($i = 1; $i < 100; $i++) : ?>
                    <option value="<?php echo $i ?>" <?php echo $item->position == $i ? 'selected="selected"' : ''; ?>><?php echo $i ?></option>
				<?php endfor ?>
            </select>
        </div>
	<?php else: ?>
        <div class="form-material floating">
            <div class="col-xs-12 position" data-toggle="tooltip" data-placement="left" title="Thay đổi vị trí">
                <select class="form-control select2"
                        onchange="changePosition(<?php echo $item->id ?>, $(this).val())">
					<?php for ($i = 1; $i < 100; $i++) : ?>
                        <option value="<?php echo $i ?>" <?php echo $item->position == $i ? 'selected="selected"' : ''; ?>><?php echo $i ?></option>
					<?php endfor ?>
                </select></div>
        </div>
	<?php endif; ?>
</td>