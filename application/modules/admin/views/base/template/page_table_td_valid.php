<td align="center">
    <button class="item_valid btn-valid btn btn-flat"
            title="Trạng thái khả dụng" data-toggle="tooltip" data-placement="left"
	        <?php if ($permission_edit && $item->valid != -1): ?>onclick="validAjax(<?php echo $item->id ?>, <?php echo $item->valid ?>, 'valid', 'item_valid', 'fa fa-check', 'fa fa-ban', 'Mở', 'Khóa')"
		<?php endif; ?> >
        <i class="fa <?php if ($item->valid == 1) echo 'fa-check'; else if ($item->valid == 0) echo 'fa-ban'; else echo 'fa-reply' ?>"></i>
        <span><?php if ($item->valid == 1) echo 'Mở'; else if ($item->valid == 0) echo 'Khóa'; else echo 'Khôi phục' ?></span>
    </button>
</td>