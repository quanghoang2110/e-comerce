<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

$form_element = new Form_element_object($obj);

$field_value = $form_element->field_value != DF_VALUE ? $form_element->field_value : (isset($data) && $data ? $data->name : '');
$label = $form_element->label ? $form_element->label : 'Tên/Tiêu đề';
$field = $form_element->field != DF_VALUE && $form_element->field ? $form_element->field : 'input_name';
$attr = $form_element->attr != DF_VALUE ? $form_element->attr : '';
//$grid_col = $form_element->grid_col ? $form_element->grid_col : 'col-sm-9';
$help_block = $form_element->help_block != DF_VALUE ? $form_element->help_block : '';
$custom_element_name = $field == "input_name" && $label != DF_VALUE;

if (Theme_object::$is_material_design): ?>
    <div class="form-group">
        <div class="form-material floating floating-label">
            <input id="<?php echo $field ?>" class="form-control" type="text"
                   name="<?php echo $field ?>" <?php echo $attr ?> value="<?php echo $field_value; ?>"/>
            <label for="<?php echo $field ?>"><?php echo $label ?></label>
        </div>
		<?php if ($help_block): ?>
            <div class="help-block"><?php echo $help_block ?></div>
		<?php endif; ?>
    </div>
	<?php if ($custom_element_name): ?>
        <script>
            elementNameLabel = "<?php echo $label ?>";
        </script>
	<?php endif; ?>
<?php else: ?>
    <div class="form-group">
        <label for="<?php echo $field ?>"><?php echo $label ?></label>
        <input id="<?php echo $field ?>" class="form-control" type="text"
               name="<?php echo $field ?>" <?php echo $attr ?> value="<?php echo $field_value; ?>"/>
		<?php if ($help_block): ?>
            <div class="help-block"><?php echo $help_block ?></div>
		<?php endif; ?>
    </div>
	<?php if ($custom_element_name): ?>
        <script>
            elementNameLabel = "<?php echo $label ?>";
        </script>
	<?php endif; ?>
<?php endif; ?>