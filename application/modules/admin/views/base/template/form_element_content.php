<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

?>
<?php if (Theme_object::$is_material_design): ?>
	<?php if (Theme_object::$name != THEME_ONEUI): ?>
        <div class="form-group">
            <div class=""><label for="input_content">Nội dung</label></div>
            <div class="">
                    <textarea id="input_content" name="input_content" minlength="50" class="form-control"
                              placeholder="Nội dung ..."><?php echo $data ? $data->content : ''; ?></textarea>
            </div>
        </div>
	<?php else: ?>
        <div class="form-group">
            <div class="row">
                <div class="col-xs-12">
                    <label for="input_content">Nội dung</label>
                    <textarea id="input_content" name="input_content" minlength="50" class="form-control"
                              placeholder="Nội dung ..."><?php echo $data ? $data->content : ''; ?></textarea>
                </div>
            </div>
        </div>
	<?php endif; ?>
<?php else: ?>
    <div class="form-group">
        <label for="input_content">Nội dung</label>
        <textarea id="input_content" name="input_content" minlength="50" class="form-control"
                  placeholder="Nội dung ..."><?php echo $data ? $data->content : ''; ?></textarea>
    </div>
<?php endif; ?>