<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */
$form_element = new Form_element_object($obj);

$grid_col = $form_element->grid_col ? $form_element->grid_col : 'col-lg-3 col-md-4 col-xs-6';
$label = $form_element->label ? $form_element->label : 'Khả dụng';
$field = $form_element->field != DF_VALUE && $form_element->field ? $form_element->field : 'input_valid';
$field_value = $form_element->field_value != DF_VALUE ? $form_element->field_value : '1';
$checked = $form_element->field_value != DF_VALUE ? ($form_element->field_value == 1 ? 'checked' : '') : 'checked';
?>
<?php if (Theme_object::$name == THEME_ONEUI): ?>
	<?php if (!Theme_object::$is_material_design): ?>
            <label for="<?php echo $field ?>"><?php echo $label ?></label>
            <div>
                <label class="css-input switch <?php echo $form_element->label_class ? $form_element->label_class : 'switch-primary' ?>">
                    <input name="<?php echo $field ?>"
                           type="checkbox" <?php echo $checked ?>><span></span>
                </label>
            </div>
	<?php else: ?>
        <div class="form-material floating">
            <div class="custom-text-center c_checked">
                <label class="css-input switch <?php echo $form_element->label_class ? $form_element->label_class : 'switch-primary' ?>">
                    <input name="<?php echo $field ?>"
                           type="checkbox" <?php echo $checked ?>><span></span>
					<?php echo $label ?>
                </label>
            </div>
        </div>
	<?php endif; ?>
<?php elseif (Theme_object::$is_material_design): ?>
        <div class="form-group">
            <select id="<?php echo $field ?>" class="form-control" name="<?php echo $field ?>">
                <option value="1" <?php echo $field_value == 1 ? 'selected' : '' ?>>Có</option>
                <option value="0" <?php echo $field_value == 0 ? 'selected' : '' ?>>Không</option>
            </select>
            <label for="<?php echo $field ?>"><?php echo $label ?></label>
        </div>
<?php else: ?>
        <label for="<?php echo $field ?>"><?php echo $label ?></label>
        <select id="<?php echo $field ?>" class="form-control" name="<?php echo $field ?>">
            <option value="1" <?php echo $field_value == 1 ? 'selected' : '' ?>>Có</option>
            <option value="0" <?php echo $field_value == 0 ? 'selected' : '' ?>>Không</option>
        </select>
<?php endif; ?>
