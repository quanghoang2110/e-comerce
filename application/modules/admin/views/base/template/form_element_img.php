<?php
//print_r($obj);
$form_element = new Form_element_object($obj);
//print_r($form_element);
$field_value = $form_element->field_value != DF_VALUE ? $form_element->field_value : (isset($data) && $data ? $data->image : '');
$label = $form_element->label ? $form_element->label : 'Ảnh đại diện';
$field = $form_element->field != DF_VALUE ? $form_element->field : 'input_image';
$attr = $form_element->attr != DF_VALUE ? $form_element->attr : false;
$grid_col = $form_element->grid_col ? $form_element->grid_col : 'col-sm-4 col-lg-2';
$help_block = $form_element->help_block != DF_VALUE ? $form_element->help_block : 'Tỉ lệ 300x300';

//array plus
//cropped

$crop_enabled = $form_element->cropped ? $form_element->cropped : false;
$crop_ratio = $form_element->crop_ratio ? $form_element->crop_ratio : 1;
?>
<style>
    .help-img-update{
        margin-top: 0;
    }
</style>
<?php if ($crop_enabled && $crop_ratio != 1) { ?>
    <style>
        #img-update {
            height: 150px;
            width: <?php echo 150*$crop_ratio ?>px;
            /*background: #ccc;*/
        }
        .image-update{
            width: auto;
            height: 150px;
            max-height: none;
            max-width: none;
        }
    </style>
<?php } ?>

<?php $iframe_src = base_url("filemanager/dialog.php?lang=vi&type=1&field_id={$field}"); ?>

    <div class="form-group" id="img-update" onclick="showModalChooseImage()">
        <?php echo get_img_tag($field_value, '', 'rfm-image image-update img-thumbnail is-link', BASE_IMG_DEFAULT); ?>
        <input type="text" id="<?php echo $field ?>" name="<?php echo $field ?>"
               class="rfm-link hidden form-control"
            <?php echo $attr ?> value="<?php echo $field_value; ?>"><br/>
        <label for="<?php echo $field ?>"><?php echo $label ?></label>
        <?php if ($help_block): ?>
            <div class="help-block help-img-update"><?php echo $help_block ?></div>
        <?php endif; ?>
    </div>

    <!-- responsive file manager -->
    <div class="modal fade modal-filemanger modal-fullheight" id="modal_choose_img" style="z-index: 9999;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary-dark">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Quản lý file</h4>
                </div>
                <div class="modal-body">
                    <iframe id="rfm-iframe"
                            src=""
                            frameborder="0" height="100%" width="100%"></iframe>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.end responsive file manager -->
    <!--model file manager fullscreen-->
    <script>

        var objImgUpdate = $('#img-update');

        function showModalChooseImage() {
            $('#rfm-iframe').attr('src', '<?php echo $iframe_src ?>');
            $('#modal_choose_img').modal("show");
        }

        function responsive_filemanager_callback(field_id) {
            var url = $('#' + field_id).val();
            //console.log(url);
            if (typeof (url) !== 'undefined') {
                objImgUpdate.find("img").attr('src', url).removeClass('img-error');
                //remove error validation
                objImgUpdate.removeClass('has-error');

                <?php if($crop_enabled){?>
                var imgChoosen = new Image();
                imgChoosen.src = url;
                imgChoosen.onload = function () {
                    //console.log(imgChoosen.width+'x'+imgChoosen.height);
                    var ratio = '<?php echo number_format($crop_ratio, 2)?>';
                    var imgChoosenRatio = (imgChoosen.width / imgChoosen.height).toFixed(2);
                    console.log(ratio + '|' + imgChoosenRatio);
                    if (imgChoosenRatio != ratio) {
                        $('#img_cropper').attr('src', url);
                        $('#img_cropper').cropper('destroy').attr('src', url).cropper(options);
                        $('#modal_cropper').modal('show');
                    }
                };
                <?php } ?>
            }
        }

        <?php if($crop_enabled){?>
        var dataImg = "";
        var dataX = 0;
        var dataY = 0;
        var dataHeight = 0;
        var dataWidth = 0;
        var dataRotate = 0;
        var dataScaleX = 1;
        var dataScaleY = 1;

        var options = {
            aspectRatio: '<?php echo $crop_ratio?>',
            preview: '.img-preview',
            minContainerWidth: 300,
            minContainerHeight: 300,
            crop: function (e) {
                dataImg = $('#img_cropper').attr('src');
                dataX = Math.round(e.x);
                dataY = Math.round(e.y);
                dataHeight = Math.round(e.height);
                dataWidth = Math.round(e.width);
                dataRotate = e.rotate;
                dataScaleX = e.scaleX;
                dataScaleY = e.scaleY;
            }
        };

        function cropImageNow() {
            //console.log(dataX);
            $.post('/<?php echo BASE_ROUTE_CROP_IMAGE ?>', {
                data_img: dataImg,
                data_x: dataX,
                data_y: dataY,
                data_height: dataHeight,
                data_width: dataWidth,
                data_rotate: dataRotate,
                data_scale_x: dataScaleX,
                data_scale_y: dataScaleY
            }).done(function (data) {
                var obj = $.parseJSON(data);
                if (obj.error == 1) {
                    swal("Lỗi crop ảnh", obj.msg, "error");
                } else {
                    //console.log(obj.msg);
                    //update value (image cropped)
                    objImgUpdate.find('img').attr('src', obj.msg);
                    objImgUpdate.find('input').val(obj.msg);
                }
            });
        }
        <?php }?>
    </script>
<?php if ($crop_enabled) : ?>
    <div class="modal fade" id="modal_cropper" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary-dark">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Ảnh hiển thị phù hợp</h4>
                </div>
                <div class="modal-body">
                    <div class="block-content" align="center">
                        <p>
                            <img id="img_cropper"/>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Không thay đổi</button>
                    <button class="btn btn-sm btn-primary" type="button" onclick="cropImageNow()" data-dismiss="modal">
                        <i
                                class="fa fa-check"></i> Đồng ý
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- cropper image -->
    <link type="text/css" rel="stylesheet" href="/assets/base/lib/cropper/cropper.min.css">
    <script type="text/javascript" src="/assets/base/lib/cropper/cropper.min.js"></script>
<?php endif; ?>