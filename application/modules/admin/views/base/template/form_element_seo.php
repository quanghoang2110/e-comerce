<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */
//$form_element = new Form_element_object($obj);
//$grid_col = $form_element->grid_col ? $form_element->grid_col : 'col-xs-12';

if (Theme_object::$is_material_design): ?>
    <!-- meta title input -->
    <div class="form-group">
        <div class="form-material floating floating-label">
                <textarea id="input_meta_title" name="input_meta_title" maxlength="155"
                          class="form-control"><?php echo $data ? $data->meta_title : ''; ?></textarea>
            <label for="input_meta_title">SEO Title</label>
            <div class="help-block">Nội dung thẻ meta title (max 155 ký tự)</div>
        </div>
    </div>
    <!-- meta description input -->
    <div class="form-group">
        <div class="form-material floating floating-label">
                            <textarea id="input_meta_desc" name="input_meta_desc" maxlength="160"
                                      class="form-control"><?php echo $data ? $data->meta_desc : ''; ?></textarea>
            <label for="input_meta_desc">SEO Desc</label>
            <div class="help-block">Nội dung thẻ meta description (max 160 ký tự)</div>

        </div>
    </div>
<?php else: ?>
    <!-- meta title input -->
    <div class="form-group">
        <label for="input_meta_title">SEO Title</label>
        <textarea id="input_meta_title" name="input_meta_title" maxlength="155" placeholder="Nội dung thẻ meta title"
                  class="form-control"><?php echo $data ? $data->meta_title : ''; ?></textarea>
    </div>
    <!-- meta description input -->
    <div class="form-group">
        <label for="input_meta_desc">SEO Desc</label>
        <textarea id="input_meta_desc" name="input_meta_desc" maxlength="160"
                  placeholder="Nội dung thẻ meta description"
                  class="form-control"><?php echo $data ? $data->meta_desc : ''; ?></textarea>
    </div>
<?php endif; ?>