<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

$_SESSION['abc-test'] = "123123123";

if (isset($id_tinymce)): ?>
    <!-- tiny MCE -->
    <script type="text/javascript" src="<?php echo base_url() ?>tinymce/tinymce.min.js"></script>
<?php endif; ?>

<script type="text/javascript">

    //    urlDetail = "<?php //echo base_url($permission_url . '/' . BASE_ROUTE_ADMIN_XXX_DETAIL); ?>//";
    urlDelete = "<?php echo base_url($permission_url . '/' . BASE_ROUTE_ADMIN_XXX_DELETE); ?>";
    urlValid = "<?php echo base_url($permission_url . '/' . BASE_ROUTE_ADMIN_XXX_VALID); ?>";
    urlUpdate = "<?php echo base_url($permission_url . '/' . BASE_ROUTE_ADMIN_XXX_UPDATE); ?>";
    urlUpdatePosition = "<?php echo base_url($permission_url . '/' . BASE_ROUTE_ADMIN_XXX_UPDATE_POSITION); ?>";

    <?php if($them_contain_form): ?>
    var id_form = '<?php echo isset($id_for) ? $id_form : 'form_update' ?>';
    objFormSubmit = $('#' + id_form);

    $(document).ready(function () {

        <?php if(isset($id_tinymce) && $id_tinymce){?>
        tinyMCE.PluginManager.add('stylebuttons', function (editor, url) {
            ['pre', 'p', 'code', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'].forEach(function (name) {
                editor.addButton("style-" + name, {
                    tooltip: "Toggle " + name,
                    text: name.toUpperCase(),
                    onClick: function () {
                        editor.execCommand('mceToggleFormat', false, name);
                    },
                    onPostRender: function () {
                        var self = this, setup = function () {
                            editor.formatter.formatChanged(name, function (state) {
                                self.active(state);
                            });
                        };
                        editor.formatter ? setup() : editor.on('init', setup);
                    }
                })
            });
        });
        tinymce.init({
            keep_styles: true,
            elements: '<?php echo $id_tinymce ?>',
            mode: "exact",
            theme: "modern",
            width: '100%',
            height: 300,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code textcolor stylebuttons'
            ],
            invalid_styles: 'color font-size',
            block_formats: 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
            toolbar: 'undo redo | toolbar: "undo redo | style-p style-h1 style-h2 style-h3 style-pre style-code | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image | forecolor backcolor',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            external_filemanager_path: base_url + "filemanager/",
            filemanager_title: "Quản lý tệp tin",
            external_plugins: {"filemanager": base_url + "filemanager/plugin.min.js"},
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            valid_elements : '*[*]',
            valid_styles: {
                '*': 'border,font-size,color,font-family,width,height,text-align',
            },
            custom_colors: true
        });
        <?php } ?>

        if (objFormSubmit) {
            var options = {
                //target:        '#output2',   // target element(s) to be updated with server response
                beforeSubmit: showRequest,  // pre-submit callback
                success: showResponse  // post-submit callback

                // other available options:
                //url:       url         // override for form's 'action' attribute
                //type:      type        // 'get' or 'post', override for form's 'method' attribute
                //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
                //clearForm: true        // clear all form fields after successful submit
                //resetForm: true        // reset the form after successful submit

                // $.ajax options can be used here too, for example:
                //timeout:   3000
            };

            objFormSubmit.find('button[type=submit]').click(function () {
                <?php if(isset($id_tinymce) && $id_tinymce):?>
                <?php foreach(explode(',', $id_tinymce) as $id_x){ ?>
                // put it in the textarea
                $('#<?php echo trim($id_x) ?>').text(tinyMCE.get('<?php echo trim($id_x) ?>').getContent());
                <?php } ?>
                <?php endif; ?>
            });

            objFormSubmit.attr({'action': urlUpdate, 'method': 'post'});
            objFormSubmit.validate({
                ignore: [],
                submitHandler: function (form) {
                    $('#' + id_form).ajaxSubmit(options);
                    return false;
                },
                errorClass: 'help-block animated<?php echo !Theme_object::$is_material_design ? '' : ' text-right' ?> fadeInDown',
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    jQuery(e).closest('.form-group<?php echo !Theme_object::$is_material_design ? '' : ' div' ?>').append(error);

                    checkImageInputError();
                },
                highlight: function (e) {
                    var elem = jQuery(e);

                    elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                    elem.closest('.help-block').remove();
                },
                success: function (e) {
                    var elem = jQuery(e);

                    elem.closest('.form-group').removeClass('has-error');
                    elem.closest('.help-block').remove();
                },
                rules: {
                    'input_name': {
                        required: true,
                        minlength: 2
                    },
                    'input_career': {
                        required: true,
                        minlength: 2
                    },
                    'input_content': {
                        required: true,
                        minlength: 50
                    },
                    'input_meta_desc': {
                        maxlength: 160
                    },
                    'input_meta_title': {
                        maxlength: 155
                    },
                    'val-confirm-password2': {
                        required: true,
                        equalTo: '#val-password2'
                    },
                    'val-select22': {
                        required: true
                    },
                    'val-select2-multiple2': {
                        required: true,
                        minlength: 2
                    },
                    'val-suggestions2': {
                        required: true,
                        minlength: 5
                    },
                    'val-skill2': {
                        required: true
                    },
                    'val-currency2': {
                        required: true,
                        currency: ['$', true]
                    },
                    'val-website2': {
                        required: true,
                        url: true
                    },
                    'val-phoneus2': {
                        required: true,
                        phoneUS: true
                    },
                    'val-digits2': {
                        required: true,
                        digits: true
                    },
                    'val-number2': {
                        required: true,
                        number: true
                    },
                    'val-range2': {
                        required: true,
                        range: [1, 5]
                    },
                    'val-terms2': {
                        required: true
                    }
                },
                messages: {
                    'input_name': {
                        required: 'Vui lòng nhập ' + elementNameLabel,
                        minlength: 'Vui lòng nhập ' + elementNameLabel + ' ít nhất 2 ký tự'
                    },
                    'input_career': {
                        required: 'Vui lòng nhập Nghề nghiệp',
                        minlength: 'Vui lòng nhập Nghề nghiệp ít nhất 2 ký tự'
                    },
                    'input_content': {
                        required: 'Vui lòng nhập Nội dung',
                        minlength: 'Vui lòng nhập Nội dung ít nhất 50 ký tự'
                    },
                    'input_meta_desc': {
                        maxlength: 'Vui lòng nhập SEO description nhỏ hơn 160 ký tự'
                    },
                    'input_meta_title': {
                        maxlength: 'Vui lòng nhập SEO description nhỏ hơn 155 ký tự'
                    },
                    'val-email2': 'Please enter a valid email address',
                    'val-password2': {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long'
                    },
                    'val-confirm-password2': {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long',
                        equalTo: 'Please enter the same password as above'
                    },
                    'val-select22': 'Please select a value!',
                    'val-select2-multiple2': 'Please select at least 2 values!',
                    'val-suggestions2': 'What can we do to become better?',
                    'val-skill2': 'Please select a skill!',
                    'val-currency2': 'Please enter a price!',
                    'val-website2': 'Please enter your website!',
                    'val-phoneus2': 'Please enter a US phone!',
                    'val-digits2': 'Please enter only digits!',
                    'val-number2': 'Please enter a number!',
                    'val-range2': 'Please enter a number between 1 and 5!',
                    'val-terms2': 'You must agree to the service terms!'
                }
            });
        }
    });

    function checkImageInputError() {
//        console.log('test');
        var objImgErr = $('#img-update');
        if (objImgErr.length > 0) {
            if (objImgErr.hasClass('has-error')) objImgErr.find('img').addClass('img-error');
            else objImgErr.find('img').removeClass('img-error');
        }
    }

    // pre-submit callback
    function showRequest(formData, jqForm, options) {
        //show loading progress
        showLoading();
        return true;
    }

    // post-submit callback
    function showResponse(responseText, statusText, xhr, $form) {
        hideLoading();
        var obj = $.parseJSON(responseText);
//        console.log(obj);
        if (obj.error == 1) {
            swal(obj.msg, obj.error_detail, "error");
        } else {
            swal({
                title: "Thành công",
                text: obj.msg,
                confirmButtonText: "Đồng ý",
                type: "success"
            }, function () {
                if (backMainPage == true) backUrl();
                else if (backMainPage == "reload") window.location.reload();
            });
        }
    }

    function backUrl() {
        window.location.href = '<?php echo base_url($permission_url . '?per_page=' . $this->input->get('per_page')).'&cate='.$this->input->get('cate');?>';
    }
    <?php else: ?>
    function validAjax(id, cValid, type, str_btn_class, valid_icon = 'fa fa-check', invalid_icon = 'fa fa-ban', valid_txt = '', invalid_text = '') {
        var itemObj = $('#item-' + id);
        var itemObjFocus = itemObj.find("button." + str_btn_class);
        showSpinner(itemObjFocus);
//        showLoading();
        $.post(urlValid, {id: id, valid: cValid, type: type})
            .done(function (data) {
                    hideSpinner(itemObj, itemObjFocus);
//                    hideLoading();
                    var nValid = (cValid == 1) ? 0 : 1;
                    var onclick_str = 'validAjax(' + id + ',' + nValid + ', "' + type + '", "' + str_btn_class + '", "' + valid_icon + '","' + invalid_icon + '", "' + valid_txt + '","' + invalid_text + '")';
                    itemObj.find('button.' + str_btn_class).attr('onclick', onclick_str);

                    if (nValid == 1) {
                        itemObj.find('button.' + str_btn_class + ' i').removeClass().addClass(valid_icon);
                        if (valid_txt) {
                            itemObj.find('button.' + str_btn_class).attr("title", valid_txt);
                            itemObj.find('button.' + str_btn_class + ' span').html(valid_txt + ' &nbsp');
                        }
                    } else {
                        itemObj.find('button.' + str_btn_class + ' i').removeClass().addClass(invalid_icon);
                        if (invalid_text) {
                            itemObj.find('button.' + str_btn_class).attr("title", invalid_text);
                            itemObj.find('button.' + str_btn_class + ' span').html(invalid_text + ' &nbsp');
                        }
                    }
                }
            )
        ;
    }

    function updateAjax(id) {
        if (id) {
            window.location.href = urlUpdate + '?id=' + id + "&page=<?php echo $page?>&cate=<?php echo $this->input->get('cate') ?>";
        } else {
            window.location.href = urlUpdate + "?page=<?php echo $page ?>";
        }
    }

    function deleteAjax(id) {
        swal({
                title: "Bạn chắc chắc muốn xóa?",
                text: "Dữ liệu bị xóa sẽ không thể khôi phục lại!",
                type: "error",
                showCancelButton: true,
                cancelButtonText: "Hủy",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Đồng ý",
                showLoaderOnConfirm: true,
                closeOnConfirm: false
            },
            function () {
                $.post(urlDelete, {id: id})
                    .done(function (data) {
                        $('#item-' + id).remove();
                        //location.reload(true);
                        swal("Đã xóa!", "Dữ liệu đã được xóa.", "success");
                    });
            });
    }

    /**
     * change item's position by item_id
     * @param id
     * @param position
     */
    function changePosition(id, position) {
        // showLoading();
        var itemObj = $('#item-' + id);
        var itemObjFocus = itemObj.find("td div.position");
        showSpinner(itemObjFocus);
        $.post(urlUpdatePosition, {id: id, position: position})
            .done(function (data) {
                hideSpinner(itemObj, itemObjFocus);
                // hideLoading();
            });
    }

    function showSpinner(itemObjFocus) {
        var spinnerObj = $("#spinner").html();
        itemObjFocus.addClass("hidden").before(spinnerObj);
    }

    function hideSpinner(itemObj, itemObjFocus) {
        setTimeout(function () {
            itemObj.find("#objSpinner").remove();
            itemObjFocus.removeClass("hidden");
        }, 500);
    }

    //action update
    $('.action-update').click(function(){
        const id = $(this).attr('data-id');
        if(id) {
            window.location.href = urlUpdate + '?id=' + id + "&per_page=<?php echo $page?>&cate=<?php echo $this->input->get('cate') ?>";
            return false;
        }
        window.location.href = urlUpdate + "?per_page=<?php echo $page ?>";
    });
    <?php endif; ?>

</script>