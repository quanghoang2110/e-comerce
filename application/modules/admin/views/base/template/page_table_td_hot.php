<td align="center">
    <button class="item_hot btn-hot btn btn-flat"
            title="Trạng thái nổi bật" data-toggle="tooltip" data-placement="left"
	        <?php if ($permission_edit && $item->is_hot != -1){ ?>onclick="validAjax(<?php echo $item->id ?>, <?php echo $item->is_hot ?>, '<?php echo BASE_DATA_TYPE_HOT ?>', 'item_hot', 'fa fa-star', 'fa fa-star-o')"<?php } ?>>
        <i class="fa <?php if ($item->is_hot == 1) echo 'fa-star'; else echo 'fa-star-o'; ?>"></i>
    </button>
</td>