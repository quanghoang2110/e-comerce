<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */
$form_element = new Form_element_object($obj);

//$grid_col = $form_element->grid_col != DF_VALUE ? $form_element->grid_col : 'col-lg-3 col-md-4 col-xs-6';
$label = $form_element->label ? $form_element->label : 'Chuyên mục';
$field = $form_element->field != DF_VALUE ? $form_element->field : 'input_category';
$field_value = $form_element->field_value != DF_VALUE ? $form_element->field_value : '';
$attr = $form_element->attr != DF_VALUE ? $form_element->attr : "";
$help_block = $form_element->help_block != DF_VALUE ? $form_element->help_block : ''; ?>

<?php if (Theme_object::$is_material_design): ?>
    <div class="form-group">
        <div class="form-material floating inline-select2">
            <select id="<?php echo $field ?>" class="select2 form-control"
                    name="<?php echo $field ?>" <?php echo $attr ?>>
                <?php foreach ($form_element->select_options as $value => $name): ?>
                    <option value="<?php echo $value ?>"><?php echo $name ?></option>
                <?php endforeach; ?>
            </select>
            <label for="<?php echo $field ?>"><?php echo $label ?></label>
            <?php if ($help_block): ?>
                <div class="help-block"><?php echo $help_block ?></div>
            <?php endif; ?>
        </div>
    </div>
<?php else: ?>
    <div class="form-group">
        <label for="<?php echo $field ?>"><?php echo $label ?></label>
        <select id="<?php echo $field ?>" class="select2 form-control"
                name="<?php echo $field ?>" <?php echo $attr ?>>
            <?php get_select_option_multiple_level($category, 1, $field_value); ?>
        </select>
    </div>
    <?php if ($help_block): ?>
        <div class="help-block"><?php echo $help_block ?></div>
    <?php endif; ?>
<?php endif; ?>
