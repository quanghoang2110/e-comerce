<?php
//print_r($obj);
$form_element2 = new Form_element_object($obj);
//print_r($form_element2);
$field_value = $form_element2->field_value != DF_VALUE ? $form_element2->field_value : (isset($data) && $data ? $data->image_2 : '');
$label = $form_element2->label ? $form_element2->label : 'Ảnh đại diện';
$field = $form_element2->field != DF_VALUE ? $form_element2->field : 'image_2';
$attr = $form_element2->attr != DF_VALUE ? $form_element2->attr : false;
$grid_col = $form_element2->grid_col ? $form_element2->grid_col : 'col-sm-4 col-lg-2';
$help_block = $form_element2->help_block != DF_VALUE ? $form_element2->help_block : 'Tỉ lệ 300x300';

//array plus
//cropped

$crop_enabled = $form_element2->cropped ? $form_element2->cropped : false;
$crop_ratio = $form_element2->crop_ratio ? $form_element2->crop_ratio : 1;
?>
<style>
    .help-img-update2{
        margin-top: 0;
    }
</style>
<?php if ($crop_enabled && $crop_ratio != 1) { ?>
    <style>
        #img-update2 {
            height: 150px;
            width: <?php echo 150*$crop_ratio ?>px;
            /*background: #ccc;*/
        }
        .image-update2{
            width: auto;
            height: 150px;
            max-height: none;
            max-width: none;
        }
    </style>
<?php } ?>

<?php $iframe_src = base_url("filemanager/dialog.php?lang=vi&type=1&field_id={$field}"); ?>

    <div class="form-group" id="img-update2" onclick="showModalChooseImage2()">
        <?php echo get_img_tag($field_value, '', 'rfm-image image-update2 img-thumbnail is-link', BASE_IMG_DEFAULT); ?>
        <input type="text" id="<?php echo $field ?>" name="<?php echo $field ?>"
               class="rfm-link hidden form-control"
            <?php echo $attr ?> value="<?php echo $field_value; ?>"><br/>
        <label for="<?php echo $field ?>"><?php echo $label ?></label>
        <?php if ($help_block): ?>
            <div class="help-block help-img-update2"><?php echo $help_block ?></div>
        <?php endif; ?>
    </div>

    <!-- responsive file manager -->
    <div class="modal fade modal-filemanger modal-fullheight" id="modal_choose_img2" style="z-index: 9999;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary-dark">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Quản lý file</h4>
                </div>
                <div class="modal-body">
                    <iframe id="rfm-iframe2"
                            src=""
                            frameborder="0" height="100%" width="100%"></iframe>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.end responsive file manager -->
    <!--model file manager fullscreen-->
    <script>

        var objImgUpdate2 = $('#img-update2');

        function showModalChooseImage2() {
            $('#rfm-iframe2').attr('src', '<?php echo $iframe_src ?>');
            $('#modal_choose_img2').modal("show");
        }

        function responsive_filemanager_callback(field_id) {
            var url = $('#' + field_id).val();
            //console.log(url);
            if (typeof (url) !== 'undefined') {
                $('#' + field_id).parent().find("img").attr('src', url).removeClass('img-error');
                //remove error validation
                objImgUpdate2.removeClass('has-error');

                <?php if($crop_enabled){?>
                var imgChoosen = new Image();
                imgChoosen.src = url;
                imgChoosen.onload = function () {
                    //console.log(imgChoosen.width+'x'+imgChoosen.height);
                    var ratio = '<?php echo number_format($crop_ratio, 2)?>';
                    var imgChoosenRatio = (imgChoosen.width / imgChoosen.height).toFixed(2);
                    console.log(ratio + '|' + imgChoosenRatio);
                    if (imgChoosenRatio != ratio) {
                        if(field_id == 'image_2') {
                            $('#img_cropper2').attr('src', url);
                            $('#img_cropper2').cropper('destroy').attr('src', url).cropper(options2);
                            $('#modal_cropper2').modal('show');
                        }else{
                            $('#img_cropper').attr('src', url);
                            $('#img_cropper').cropper('destroy').attr('src', url).cropper(options);
                            $('#modal_cropper').modal('show');
                        }
                    }
                };
                <?php } ?>
            }
        }

        <?php if($crop_enabled){?>
        var dataImg2 = "";
        var dataX2 = 0;
        var dataY2 = 0;
        var dataHeight2 = 0;
        var dataWidth2 = 0;
        var dataRotate2 = 0;
        var dataScaleX2 = 1;
        var dataScaleY2 = 1;

        var options2 = {
            aspectRatio: '<?php echo $crop_ratio?>',
            preview: '.img-preview2',
            minContainerWidth: 300,
            minContainerHeight: 300,
            crop: function (e) {
                dataImg2 = $('#img_cropper2').attr('src');
                dataX2 = Math.round(e.x);
                dataY2 = Math.round(e.y);
                dataHeight2 = Math.round(e.height);
                dataWidth2 = Math.round(e.width);
                dataRotate2 = e.rotate;
                dataScaleX2 = e.scaleX;
                dataScaleY2 = e.scaleY;
            }
        };

        function cropImageNow2() {
            //console.log(dataX);
            $.post('/<?php echo BASE_ROUTE_CROP_IMAGE ?>', {
                data_img: dataImg2,
                data_x: dataX2,
                data_y: dataY2,
                data_height: dataHeight2,
                data_width: dataWidth2,
                data_rotate: dataRotate2,
                data_scale_x: dataScaleX2,
                data_scale_y: dataScaleY2
            }).done(function (data) {
                var obj = $.parseJSON(data);
                if (obj.error == 1) {
                    swal("Lỗi crop ảnh", obj.msg, "error");
                } else {
                    //console.log(obj.msg);
                    //update value (image cropped)
                    var field_id = '<?php echo $field; ?>';
                    if(field_id == 'image_2') {
                        objImgUpdate2.find('img').attr('src', obj.msg);
                        objImgUpdate2.find('input').val(obj.msg);
                    }else{
                        $('#img-update').find('img').attr('src', obj.msg);
                        $('#img-update').find('input').val(obj.msg);
                    }
                }
            });
        }
        <?php }?>
    </script>
<?php if ($crop_enabled) : ?>
    <div class="modal fade" id="modal_cropper2" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary-dark">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Ảnh hiển thị phù hợp</h4>
                </div>
                <div class="modal-body">
                    <div class="block-content" align="center">
                        <p>
                            <img id="img_cropper2"/>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Không thay đổi</button>
                    <button class="btn btn-sm btn-primary" type="button" onclick="cropImageNow2()" data-dismiss="modal">
                        <i
                                class="fa fa-check"></i> Đồng ý
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- cropper image -->
    <link type="text/css" rel="stylesheet" href="/assets/base/lib/cropper/cropper.min.css">
    <script type="text/javascript" src="/assets/base/lib/cropper/cropper.min.js"></script>
<?php endif; ?>