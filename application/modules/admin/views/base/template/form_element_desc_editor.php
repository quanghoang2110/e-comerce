<?php

$form_element = new Form_element_object($obj);

$field_value = $form_element->field_value != DF_VALUE ? $form_element->field_value : (isset($data) && $data ? $data->desc : '');
$label = $form_element->label ? $form_element->label : 'Mô tả';
$field = $form_element->field ? $form_element->field : 'input_desc';
$attr = $form_element->attr != DF_VALUE ? $form_element->attr : "maxlength='500'";
$grid_col = $form_element->grid_col ? $form_element->grid_col : 'col-xs-12';
$help_block = $form_element->help_block != DF_VALUE ? $form_element->help_block : '';
?>

<?php if (Theme_object::$is_material_design): ?>
	<?php if (Theme_object::$name != THEME_ONEUI): ?>
        <div class="form-group">
            <div class=""><label for="<?php echo $field ?>"><?php echo $label ?></label></div>
            <div class="">
                    <textarea id="<?php echo $field ?>" name="<?php echo $field ?>" <?php echo $attr ?>
                              placeholder="<?php echo $help_block ?>"
                              class="form-control"><?php echo $field_value; ?></textarea>
            </div>
        </div>
	<?php else: ?>
        <div class="form-group">
            <div class="row">
                <div class="col-xs-12">
                    <label for="<?php echo $field ?>"><?php echo $label ?></label>
                    <textarea id="<?php echo $field ?>" name="<?php echo $field ?>" <?php echo $attr ?>
                              placeholder="<?php echo $help_block ?>"
                              class="form-control"><?php echo $field_value; ?></textarea>
                </div>
            </div>
        </div>
	<?php endif; ?>
<?php else: ?>
    <div class="form-group">
        <label for="<?php echo $field ?>"><?php echo $label ?></label>
        <textarea id="<?php echo $field ?>" name="<?php echo $field ?>" <?php echo $attr ?>
                  placeholder="<?php echo $help_block ?>"
                  class="form-control"><?php echo $field_value; ?></textarea>
    </div>
<?php endif; ?>