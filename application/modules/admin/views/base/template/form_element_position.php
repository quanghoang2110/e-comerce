<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

$form_element = new Form_element_object($obj);

$field_value = $form_element->field_value != DF_VALUE ? $form_element->field_value : (isset($data) && $data ? $data->position : '');
$label = $form_element->label ? $form_element->label : 'Vị trí hiển thị';
$field = $form_element->field != DF_VALUE && $form_element->field ? $form_element->field : 'input_position';
$attr = $form_element->attr != DF_VALUE ? $form_element->attr : "maxlength='500'";
//$grid_col = $form_element->grid_col != DF_VALUE && $form_element->grid_col ? $form_element->grid_col : '';
$help_block = $form_element->help_block != DF_VALUE ? $form_element->help_block : ''; ?>

<?php if (Theme_object::$is_material_design): ?>
    <div class="form-group">
        <div class="form-material floating floating-label">
            <select id="<?php echo $field ?>" class="select2 form-control"
                    name="<?php echo $field ?>" <?php echo $attr ?>>
				<?php for ($i = 1; $i < 100; $i++) : ?>
                    <option value="<?php echo $i ?>" <?php echo $field_value == $i ? 'selected="selected"' : ''; ?>><?php echo $i ?></option>
				<?php endfor ?>
            </select>
            <label for="<?php echo $field ?>"><?php echo $label ?></label>
			<?php if ($help_block): ?>
                <div class="help-block"><?php echo $help_block ?></div>
			<?php endif; ?>
        </div>
    </div>
<?php else: ?>
    <div class="form-group">
        <label for="<?php echo $field ?>"><?php echo $label ?></label>
        <select id="<?php echo $field ?>" class="select2 form-control"
                name="<?php echo $field ?>" <?php echo $attr ?>>
			<?php for ($i = 1; $i < 100; $i++) : ?>
                <option value="<?php echo $i ?>" <?php echo $field_value == $i ? 'selected="selected"' : ''; ?>><?php echo $i ?></option>
			<?php endfor ?>
        </select>
		<?php if ($help_block): ?>
            <div class="help-block"><?php echo $help_block ?></div>
		<?php endif; ?>
    </div>
<?php endif; ?>
