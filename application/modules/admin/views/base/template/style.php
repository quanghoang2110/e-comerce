<style>
    .no-padding {
        padding: 0 !important;
    }

    body {
        font-family: "Segoe UI";
    }

    .bg-primary-dark {
        background-color: #ff5722;
    }

    .middle {
        vertical-align: middle !important;
    }

    .bg-primary-dark h4, .bg-primary-dark button span {
        color: white;
    }

    .header-navbar-fixed #header-navbar {
        /*border-bottom: 1px hidden #F25C27;*/
        /*background-color:#F25C27;*/
    }

    .skin-red .main-header .logo {
        background-color: #de5927;
    }

    .skin-red .main-header .navbar {
        background-color: #F25C27;
    }

    .navbar-nav > .user-menu > .dropdown-menu {
        width: 200px;
    }

    .navbar-nav > .user-menu > .dropdown-menu > li.user-header {
        height: auto;
    }

    .navbar-custom-menu > .navbar-nav > li > a {
        background-color: #de5927;
    }

    .dropdown-menu > li:hover {
        background-color: #e1e3e9;
    }

    .dropdown-menu > li > a:hover {
        background-color: transparent;
    }

    .navbar-custom-menu > .navbar-nav > li > .dropdown-menu {
        box-shadow: 0 1px 20px 0px #333333;
        border: 0;
    }

    .has-error .help-block {
        color: red;
    }

    .has-error div.mce-tinymce {
        border-color: red;
    }

    .clear {
        clear: both;
    }

    .other_field {
        width: 60%;
        height: 5px;
        border-bottom: 1px dotted #e6e6e6;
        margin-bottom: 30px;
    }

    .c_checked {
        /*padding-top: 10px;*/
    }

    .single-line {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .inline-select2 {
        /*margin-top: 17px;*/
    }

    .image-update {
        max-height: 100px;
        max-width: 100px;
    }

    .is-link {
        cursor: pointer;
    }

    .height_10 {
        height: 10px;
    }

    input.error, textarea.error {
        border: 1px dotted red !important;
    }

    img.img-error {
        border: 1px dotted red !important;
    }

    #img-update {
        text-align: center;
    }

    #img-update img {
        margin-bottom: 5px;
    }

    .div-material-checkbox {
        padding-top: 14px;
    }

    .form-group-x {
        margin-bottom: 15px;
    }

    .help-img-update {
        text-align: center;
        margin-top: -20px;
    }

    .page_filter {
        height: 34px;
    }

    .page_filter_label {
        float: left;
        line-height: 34px;
        margin-bottom: 20px
    }

    .width_30 {
        width: 30px;
    }

    .width_50 {
        width: 50px;
    }

    .width_60 {
        width: 60px;
    }

    .width_70 {
        width: 70px;
    }

    .width_80 {
        width: 80px;
    }

    .width_100 {
        width: 100px;
    }

    .width_110 {
        width: 110px;
    }

    .width_120 {
        width: 120px;
    }

    .width_130 {
        width: 130px;
    }

    .width_150 {
        width: 150px;
    }

    .width_180 {
        width: 150px;
    }

    .width_200 {
        width: 200px;
    }

    .width_300 {
        width: 300px;
    }

    .select2 {
        min-width: 60px;
    }

    .sweet-alert h2 {
        margin-bottom: 15px;
    }

    @media (min-width: 768px)
        .modal-dialog {
            width: 600px;
            margin: 30px auto;
        }

        .sweet-alert {
            top: 0 !important;
            margin-top: 40px !important;
        }
    }

    .sweet-alert {
        top: 0 !important;
        margin-top: 15px !important;
    }

    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6,
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th {
        font-family: "Segoe UI";
    }

    @-webkit-keyframes fadeInDown {
        0% {
            opacity: 0;
            -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
        }
        100% {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }

    @keyframes fadeInDown {
        0% {
            opacity: 0;
            -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
        }
        100% {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }

    .fadeInDown {
        -webkit-animation-name: fadeInDown;
        animation-name: fadeInDown;
    }

    #modalRealImg img {
        max-width: 100%;
        height: auto;
    }

    td .btn {
        background: transparent;
    }

    .form-material .select2-container--default .select2-selection--single {
        border: 1px solid #e9e9e9 !important;
        border-radius: 4px;
        background: #f9f9f9;
        text-align: center;
    }

    td.middle div.form-material {
        margin: 0
    }

    .no-margin {
        margin: 0 !important;
    }
</style>