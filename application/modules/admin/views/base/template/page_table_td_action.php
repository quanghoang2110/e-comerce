<td class="table_td_action" align="center">
	<?php if ($permission_edit): ?>
        <button class="btn-repair-question btn btn-icon-toggle" data-toggle="tooltip" data-placement="left" title="Sửa"
                onclick="updateAjax(<?php echo $item->id ?>, '<?php echo isset($item->name) ? $item->name : '' ?>')">
            <i class="fa fa-edit"></i>
        </button>
	<?php endif; ?>
	<?php if ($permission_delete) { ?>
        <button class="btn btn-icon-toggle" title="Xóa" data-toggle="tooltip" data-placement="left"
                onclick="deleteAjax(<?php echo $item->id ?>)">
            <i class="fa fa-close"></i>
        </button>
	<?php } ?>
</td>