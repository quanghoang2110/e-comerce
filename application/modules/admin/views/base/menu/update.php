<div class="form-group">
    <div class="col-xs-12">
        <div class="col-md-9">
            <?php
            $form_obj = new Form_element_object();
            $form_obj->label = "Tiêu đề";
            $form_obj->field = "input_name";
            $form_obj->field_value = $data ? $data->name : "";
            load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj);
            ?>

        </div>
        <div class="col-md-6">
            <?php
            $form_obj = new Form_element_object();
            $form_obj->label = "URL";
            $form_obj->field = "input_url";
            $form_obj->field_value = $data ? $data->url : "";
            load_form_element_object(BASE_ADMIN_FORM_ELEMENT_NAME, $form_obj);
            ?>
        </div>
        <div class="clear"></div>
        <div class="col-xs-12">
            <div class="form-group">
                <div class="col-md-4 col-lg-3">
                    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_POSITION) ?>
                </div>
                <div class="col-md-4 col-lg-3 custom-text-center">
                    <?php load_form_element(BASE_ADMIN_FORM_ELEMENT_VALID) ?>
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <div class="col-xs-12">
            <!--element action-->
            <?php load_form_elements(array(BASE_ADMIN_FORM_ELEMENT_ACTION), false, "col-md-11"); ?>
        </div>
    </div>
</div>
<?php load_view($theme_template . '/script'); ?>