<thead>
<tr>
    <th class="width_50">STT</th>
    <th>Tên</th>
    <th>Email</th>
    <th>Điện thoại</th>
    <th>Địa chỉ</th>
    <th>Thông tin</th>
    <th class="width_50">Action</th>
</tr>
</thead>
<tbody>
<?php if ($list) { ?>
	<?php foreach ($list as $index => $item) { ?>
		<?php
		$arr['index'] = $index;
		$arr['item'] = $item;
		$arr['permission_edit'] = false;
		$this->load->vars($arr);
		?>
        <tr id="item-<?php echo $item->id; ?>">
			<?php load_table_td(array(BASE_ADMIN_TABLE_TD_STT, BASE_ADMIN_TABLE_TD_NAME)) ?>
            <td><?php echo $item->email ?></td>
            <td><?php echo $item->phone ?></td>
            <td><?php echo $item->address ?></td>
            <td><?php echo $item->content ?></td>
			<?php load_table_td(BASE_ADMIN_TABLE_TD_ACTION) ?>
        </tr>
	<?php } ?>
<?php } ?>
</tbody>
<?php load_view($theme_template . '/script'); ?>

