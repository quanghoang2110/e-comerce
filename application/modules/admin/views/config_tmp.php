<link href="/assets/base/lib/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/assets/base/lib/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<script src="/assets/base/lib/jquery/jquery-3.1.1.min.js" type="application/javascript"></script>
<script src="/assets/base/lib/bootstrap-3.3.7/js/bootstrap.min.js" type="application/javascript"></script>
<style>
    body {
        background-color: #e9e9e9;
    }

    .bs-callout {
        padding: 20px;
        margin: 20px 0;
        border: 1px solid #eee;
        border-left-width: 5px;
        border-radius: 3px;
    }

    .bs-callout-warning h4 {
        color: #aa6708;
    }

    .bs-callout h4 {
        margin-top: 0;
        margin-bottom: 5px;
    }

    .bs-example {
        padding: 30px 15px 30px;
        margin: 0 -15px 15px;
        border-style: solid;

        margin-right: 0;
        margin-left: 0;
        background-color: #fff;
        border-color: #ddd;
        border-width: 1px;
        border-radius: 4px 4px 0 0;
        -webkit-box-shadow: none;
        box-shadow: none;
    }
</style>
<div style="height: 20px"></div>
<div class="container">
    <div class="bs-example bs-callout-warning">
        <div class="row">
            <div class="col-sm-6">
                <a class="btn btn-default" href="/<?php echo BASE_ROUTE_ADMIN_DASHBOARD ?>">&nbsp;<i
                            class="fa fa-home"></i>&nbsp;</a>
                <a class="btn btn-default" onclick="ajaxRun('/admin/config/admin_menu')">Update Admin
                    <strong>MENU</strong></a>
                <a class="btn btn-default" href="#" onclick="ajaxRun('/admin/config/admin_routes')">Update Admin
                    <strong>ROUTES</strong></a>
            </div>
            <div class="col-sm-6" align="right">
                <a class="btn btn-default" href="/<?php echo BASE_ROUTE_DEV_ALL_FORM_ELEMENT ?>"><strong>Form</strong>
                    Element</a>
                <a class="btn btn-default" href="#" data-toggle="modal"
                   data-target="#modalControllerRun"><strong>RUN</strong>
                    Controller</a>
            </div>
        </div>
    </div>
</div>

<div id="ajaxLoading">
    <?php include_once 'loading_new.php' ?>
</div>

<div class="modal fade" id="modalResp" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">RUN RESULT</h4>
            </div>
            <div class="modal-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalControllerRun" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">BACKEND <?php echo strtoupper($this->config->item('admin_version_folder')) ?>
                    CONTROLLER</h4>
            </div>
            <div class="modal-body">
                <div style="height: 30px"></div>
                <div class="input-group">
                    <span class="input-group-addon"
                          id="basic-addon3"><?php echo base_url(ADMIN_MODULE) ?>
                        /</span>
                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" onclick="goBackendController()">GO</button>
                      </span>
                </div>
                <div style="height: 50px"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->