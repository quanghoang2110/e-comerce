<script>
    function editModalLabel(id) {
        var obj = $('#modalLabel');
        var trObj = $('#tr-' + id);
        obj.find('#id').val(id);
        obj.find('#inputName').val(trObj.find('.td-name').text());
        obj.find('#inputPosition').val(trObj.find('.td-position').text());
        obj.find('#inputValid').val(trObj.find('.td-valid').text());
        obj.modal('show');
    }

    function addModalLabel() {
        var obj = $('#modalLabel');
        $('#formLabel')[0].reset();
        $('#formLabel').find('#opt').val('label');
        $('#formLabel').find('#id').val('');
        obj.modal('show');
    }

    function deleteLabel(id) {
        if (confirm("Bạn muốn xóa label này?")) {
//            console.log('tu xu ly...');
            location.href = "?type=config_menu&m=delete&t=label&i=" + id;
        }
    }
</script>

<?php include_once "config_tmp.php" ?>

<form class="form-horizontal" id="formLabel" method="post">

    <div class="modal fade" id="modalLabel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Menu Label</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">#name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="inputName" placeholder="Label name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">#position</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="position" id="inputPosition"
                                   placeholder="Position (ASC)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">#valid</label>
                        <div class="col-sm-10">
                            <select name="valid" class="form-control" id="inputValid">
                                <option value="1">Khả dụng</option>
                                <option value="0">Không khả dụng</option>
                            </select>
                        </div>
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="opt" id="opt" value="label">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Đồng ý</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</form>

<div class="container">
    <div class="bs-example bs-callout-warning">
        <div class="row">
            <div class="col-md-6">
                <h4>MENU</h4>
            </div>
            <div class="col-md-6" align="right">
                <button class="btn btn-default" onclick="addModalLabel()">Thêm mới Label</button>
                <button class="btn btn-default" onclick="addMenu()">Thêm mới Menu</button>
            </div>
        </div>
        <div style="height: 10px;"></div>
        <table class="table table-hover">
            <tr>
                <th>#name</th>
                <th>#menu</th>
                <th>#sub_menu</th>
                <th>#icon</th>
                <th>#position</th>
                <th>#valid</th>
                <th>Action</th>
            </tr>
            <?php $unused = [] ?>
            <?php foreach ($heads as $i1 => $lbl): ?>
                <tr id="tr-<?php echo $lbl->id ?>">
                    <td class="td-name" colspan="4"><?php echo $lbl->name ?></td>
                    <td class="td-position"><?php echo $lbl->position ?></td>
                    <td class="td-valid"><?php echo $lbl->valid ?></td>
                    <td>
                        <button class="btn btn-primary" onclick="editModalLabel(<?php echo $lbl->id ?>)">Sửa</button>
                        <button class="btn btn-danger" onclick="deleteLabel(<?php echo $lbl->id ?>);">Xóa</button>
                    </td>
                </tr>
                <!--                    group/menu -->
                <?php foreach ($menu as $jj => $mn): ?>
                    <?php if (!$mn->menuhead_id && !$mn->menugroup_id) $unused[$jj] = $mn; ?>
                    <?php if ($mn->menuhead_id == $lbl->id): ?>
                        <tr id="tr-m-<?php echo $mn->id ?>">
                            <td></td>
                            <td class="td-name" colspan="2"><?php echo $mn->name ?></td>
                            <td class="td-icon">
                                <i class="<?php echo $mn->icon ?>"></i>
                                <input type="hidden" value="<?php echo $mn->icon ?>"/>
                            </td>
                            <td class="td-position"><?php echo $mn->position ?></td>
                            <td class="td-valid"><?php echo $mn->valid ?></td>
                            <td>
                                <?php if (!$mn->is_group): ?>
                                    <button class="btn btn-primary"
                                            onclick="updateSubMenu(<?php echo $lbl->id ?>,'',<?php echo $mn->id ?>,<?php echo $mn->position ?>,<?php echo $mn->valid ?>,'<?php echo $mn->icon ?>')">
                                        Cập nhật
                                    </button>
                                <?php else: ?>
                                    <button class="btn btn-primary"
                                            onclick="editModalMenuGroup(<?php echo $lbl->id ?>,'<?php echo $mn->name ?>',<?php echo $mn->id ?>,<?php echo $mn->position ?>,<?php echo $mn->valid ?>,'<?php echo $mn->icon ?>')">
                                        Sửa
                                    </button>
                                    <button class="btn btn-danger"
                                            onclick="deleteMenuGroup(<?php echo $mn->id ?>);">Xóa
                                    </button>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php foreach ($mn->sub as $mns): ?>
                            <tr id="tr-s-<?php echo $mns->id ?>">
                                <td colspan="2"></td>
                                <td class="td-name"><?php echo $mns->name ?></td>
                                <td class="td-icon">
                                    <i class="<?php echo $mns->icon ?>"></i>
                                    <input type="hidden" value="<?php echo $mns->icon ?>"/>
                                </td>
                                <td class="td-position"><?php echo $mns->position ?></td>
                                <td class="td-valid"><?php echo $mns->valid ?></td>
                                <td>
                                    <button class="btn btn-primary"
                                            onclick="updateSubMenu(<?php echo $lbl->id ?>,<?php echo $mn->id ?>,<?php echo $mns->id ?>,<?php echo $mns->position ?>,<?php echo $mns->valid ?>,'<?php echo $mns->icon ?>')">
                                        Cập nhật
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
            <?php if ($unused): ?>
                <tr>
                    <td class="text-danger" colspan="10">Không được hiển thị</td>
                </tr>
                <?php foreach ($unused as $u_mn): ?>
                    <tr id="tr-s-<?php echo $u_mn->id ?>" class="text-danger">
                        <td colspan=""></td>
                        <td class="td-name" colspan="2"><?php echo $u_mn->name ?></td>
                        <td class="td-icon">
                            <i class="<?php echo $u_mn->icon ?>"></i>
                            <input type="hidden" value="<?php echo $u_mn->icon ?>"/>
                        </td>
                        <td class="td-position"><?php echo $u_mn->position ?></td>
                        <td class="td-valid"><?php echo $u_mn->valid ?></td>
                        <td>
                            <?php if (!$u_mn->is_group): ?>
                                <button class="btn btn-primary"
                                        onclick="updateSubMenu('<?php echo $u_mn->menuhead_id ?>','',<?php echo $u_mn->id ?>,<?php echo $u_mn->position ?>,<?php echo $u_mn->valid ?>,'<?php echo $u_mn->icon ?>')">
                                    Cập nhật
                                </button>
                            <?php else: ?>
                                <button class="btn btn-primary"
                                        onclick="editModalMenuGroup('<?php echo $u_mn->menuhead_id ?>','<?php echo $u_mn->name ?>',<?php echo $u_mn->id ?>,<?php echo $u_mn->position ?>,<?php echo $u_mn->valid ?>,'<?php echo $u_mn->icon ?>')">
                                    Sửa
                                </button>
                                <button class="btn btn-danger"
                                        onclick="deleteMenuGroup(<?php echo $u_mn->id ?>);">Xóa
                                </button>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>
    </div>

</div>
<script>

    function showLoading() {
        $('#ajaxLoading').show();
    }

    function hideLoading() {
        $('#ajaxLoading').hide();
    }

    function ajaxRun(url) {
        showLoading();
        $.get(url).done(function (data) {
            hideLoading();
            $('#modalResp .modal-body').html(data);
            $('#modalResp').modal('show');
        });
    }

    function goBackendController() {
        var obj = $('#basic-url');
//        console.log(controller);
        if (obj.val()) {
            window.open('<?php echo base_url(ADMIN_MODULE)?>/' + obj.val(), '_blank');
//            location.href = '<?php //echo base_url($theme_backend)?>///' + controller;
        } else {
            obj.focus();
        }
    }
</script>
<script>
    function editModalMenuGroup(head, name, id, position, valid, icon) {
        var obj = $('#modalMenu');
        obj.find('#inputHead').val(head);
        obj.find('#inputName').val(name);
        obj.find('#inputPosition').val(position);
        obj.find('#inputValid').val(valid);
        obj.find('#inputId').val(id);
        obj.find('#inputIcon').val(icon);
        obj.modal('show');
    }

    //add menuGroup
    function addMenu() {
        var obj = $('#modalMenu');
        $('#formMenu')[0].reset();
        $('#formMenu').find('#opt').val('menugroup');
        $('#formMenu').find('#id').val('');
        obj.modal('show');
    }

    function deleteMenuGroup(id) {
        if (confirm("Bạn muốn xóa menu này?")) {
//            console.log('tu xu ly...');
            location.href = "?type=config_menu&m=delete&t=group&i=" + id;
        }
    }
</script>
<form class="form-horizontal" id="formMenu" method="post">

    <div class="modal fade" id="modalMenu" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cập nhật MENU</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">#name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="inputName" placeholder="Menu name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputHead" class="col-sm-2 control-label">#label</label>
                        <div class="col-sm-10">
                            <select name="label" class="form-control" id="inputHead">
                                <?php foreach ($heads as $h1): ?>
                                    <option value="<?php echo $h1->id ?>"><?php echo $h1->name ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputIcon" class="col-sm-2 control-label">#icon</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="icon" id="inputIcon"
                                   placeholder="FontAwesomeIcons class">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPosition" class="col-sm-2 control-label">#position</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="position" id="inputPosition"
                                   placeholder="Position (ASC)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputValid" class="col-sm-2 control-label">#valid</label>
                        <div class="col-sm-10">
                            <select name="valid" class="form-control" id="inputValid">
                                <option value="1">Khả dụng</option>
                                <option value="0">Không khả dụng</option>
                            </select>
                        </div>
                        <input type="hidden" name="id" id="inputId" value="">
                        <input type="hidden" name="opt" id="opt" value="menugroup">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Đồng ý</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</form>


<script>
    function updateSubMenu(head, group, id, position, valid, icon) {
        var obj = $('#modalSubMenu');
        obj.find('#inputHead').val(head);
        obj.find('#inputGroup').val(group ? group : 0);
        obj.find('#inputPosition').val(position);
        obj.find('#inputValid').val(valid);
        obj.find('#inputId').val(id);
        obj.find('#inputIcon').val(icon);
        obj.modal('show');
    }
</script>
<form class="form-horizontal" id="formSubMenu" method="post">

    <div class="modal fade" id="modalSubMenu" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cập nhật MENU</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputHead" class="col-sm-2 control-label">#label</label>
                        <div class="col-sm-10">
                            <select name="label" class="form-control" id="inputHead">
                                <?php foreach ($heads as $h1): ?>
                                    <option value="<?php echo $h1->id ?>"><?php echo $h1->name ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputGroup" class="col-sm-2 control-label">#group</label>
                        <div class="col-sm-10">
                            <select name="group" class="form-control" id="inputGroup">
                                <option value="0">Không chọn</option>
                                <?php foreach ($menu as $m1): ?>
                                    <?php if ($m1->is_group): ?>
                                        <option value="<?php echo $m1->id ?>"><?php echo $m1->name ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputIcon" class="col-sm-2 control-label">#icon</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="icon" id="inputIcon"
                                   placeholder="FontAwesomeIcons class">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPosition" class="col-sm-2 control-label">#position</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="position" id="inputPosition"
                                   placeholder="Position (ASC)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputValid" class="col-sm-2 control-label">#valid</label>
                        <div class="col-sm-10">
                            <select name="valid" class="form-control" id="inputValid">
                                <option value="1">Khả dụng</option>
                                <option value="0">Không khả dụng</option>
                            </select>
                        </div>
                        <input type="hidden" name="id" id="inputId" value="">
                        <input type="hidden" name="opt" id="opt" value="menu">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Đồng ý</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</form>