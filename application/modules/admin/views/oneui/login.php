<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>CHEF STUDIO - Admin Dashboard</title>

    <meta name="description"
          content="CHEF STUDIO - Admin Dashboard">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="assets/img/favicons/favicon.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="<?php echo get_asset_url('/assets/base/lib/bootstrap-3.3.7/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" id="css-main" href="<?php echo get_asset_url('/assets/admin/oneui/css/oneui.css') ?>">

    <style>
        body {
            background-color: #f0f0f0 !important;
            background: url("/images/admin_bg.jpg") no-repeat center;
            background-size: 100%;
        }

        .xxx4 {
            background: #fff;
        }

        .content.content-boxed{
            opacity: .9;
        }

        @media screen and (max-height: 768px) {
            .pulldown {
                top: 70px;
            }
        }

        @media screen and (max-height: 500px) {
            .pulldown {
                top: 30px;
            }
        }
    </style>
    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>
<body>
<!-- Login Content -->
<div class="pulldown">
    <div class="content content-boxed overflow-hidden bg-white">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 xxx4">
                <div class="push-30-t push-50 animated fadeIn">
                    <!-- Login Title -->
                    <div class="text-center">
                        <i class="fa fa-2x fa-circle-o-notch text-primary"></i>
                        <?php if (isset($msg)) echo $msg; else { ?><p class="text-muted push-15-t">Đăng nhập hệ thống
                            quản trị</p><?php } ?>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Form -->
                    <!-- jQuery Validation (.js-validation-login class is initialized in js/pages/base_pages_login.js) -->
                    <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                    <form class="js-validation-login form-horizontal push-30-t" method="post">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="text" id="login-username" name="username">
                                    <label for="login-username">Tài khoản</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="password" id="login-password" name="password">
                                    <label for="login-password">Mật khẩu</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label class="css-input switch switch-sm switch-primary">
                                    <input type="checkbox" id="login-remember-me" name="login-remember-me"><span></span>
                                    Nhớ tài khoản?
                                </label>
                            </div>
                        </div>
                        <div class="form-group push-30-t">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                <button class="btn btn-sm btn-block btn-primary" type="submit">Log in</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Login Form -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Login Content -->

<!-- Login Footer -->
<div class="pulldown push-30-t text-center animated fadeInUp">
    <small class="text-muted"><span class="js-year-copy"></span> &copy; MCI</small>
</div>
<!-- END Login Footer -->

<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
<script src="<?php echo get_asset_url('/assets/base/lib/jquery/jquery-3.1.1.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/base/lib/bootstrap-3.3.7/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/admin/oneui/js/core/jquery.slimscroll.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/admin/oneui/js/core/jquery.scrollLock.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/admin/oneui/js/core/jquery.appear.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/admin/oneui/js/core/jquery.countTo.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/admin/oneui/js/core/jquery.placeholder.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/admin/oneui/js/core/js.cookie.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/admin/oneui/js/app.js') ?>"></script>

<!-- Page JS Plugins -->
<!--<script src="/assets/backend/oneui/js/plugins/jquery-validation/jquery.validate.min.js' ) ?>"></script>-->
<script src="<?php echo get_asset_url('/assets/base/lib/jquery-validate/jquery.validate.min.js') ?>"></script>
<script src="<?php echo get_asset_url('/assets/base/lib/jquery-validate/localization/messages_vi.min.js') ?>"></script>

<!-- Page JS Code -->
<script src="<?php echo get_asset_url('/assets/admin/oneui/js/pages/base_pages_login.js') ?>"></script>
</body>
</html>