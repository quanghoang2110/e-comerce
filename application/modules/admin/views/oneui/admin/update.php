<div class="modal-dialog modal-dialog-popin" id="fuadmin">
    <div class="modal-content">
        <form id="form_update" class="form-horizontal" method="post">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="fa fa-close"></i></span></button>
                    <h3 class="block-title"><?php echo $title; ?></h3>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="input_username">Tài khoản</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="input_username" name="username"
                                   placeholder="Tên tài khoản" required
                                   value="<?php echo $admin_data ? $admin_data->username : ''; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="input_fullname">Họ và tên</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control col-sm-9" id="input_fullname" name="fullname"
                                   placeholder="Họ tên"
                                   required
                                   value="<?php echo $admin_data ? $admin_data->fullname : ''; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="input_group">Nhóm quản trị</label>
                        <div class="col-md-8">
                            <select name="group" id="input_group" class="form-control select2">
								<?php foreach ($groups as $key => $value) { ?>
                                    <option value="<?php echo $value->id ?>" <?php echo $admin_data && $value->id == $admin_data->group_id ? 'selected' : ''; ?>><?php echo $value->name ?></option>
								<?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="input_valid">Khả dụng</label>
                        <div class="col-md-8">
                            <label class="css-input switch switch-primary">
                                <input id="input_valid" name="input_valid" type="checkbox" <?php echo ($admin_data && $admin_data->valid) || !$admin_data ? 'checked' : '' ?> ><span></span>
                            </label>
                        </div>
                    </div>
                    <input type="hidden" id="hdID" name="hdID"
                           value="<?php echo $admin_data ? $admin_data->id : ''; ?>"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
					<?php if ($permission_edit) { ?>
                        <button type="submit" class="btn btn-primary">Lưu</button>
					<?php } ?>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    objFormSubmit = $("#form_update");
    $(document).ready(function () {
        var options = {
            //target:        '#output2',   // target element(s) to be updated with server response
            beforeSubmit: showRequest,  // pre-submit callback
            success: showResponse  // post-submit callback
        };

        objFormSubmit.validate({
            ignore: "",
            submitHandler: function (form) {
                objFormSubmit.ajaxSubmit(options);

                // !!! Important !!! always return false to prevent standard browser submit and page navigation
                return false;
            }
        });
    });

    // pre-submit callback
    function showRequest(formData, jqForm, options) {
        //show loading progress
        showLoading();
        console.log("showLoading");
        return true;
    }
    // post-submit callback
    function showResponse(responseText, statusText, xhr, $form) {
        hideLoading();
        var obj = $.parseJSON(responseText);
        console.log(obj);
        $('#modal_update').modal('hide');
        if (obj.error == 1) {
            swal("Lỗi!", obj.msg, "error");
            swal({
                title: "Lỗi",
                text: obj.msg,
                confirmButtonText: "Đồng ý",
                type: "error"
            }, function () {
                $('#modal_update').modal('show');
            });
        } else {
            swal({
                title: "Thành công",
                text: obj.msg,
                confirmButtonText: "Đồng ý",
                type: "success"
            }, function () {
                window.location.reload();
            });
        }
    }
</script>