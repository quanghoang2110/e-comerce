<!--form jquery and form validate-->
<script src="<?php get_asset_url('/assets/base/lib/jquery-validate/jquery.validate.min.js') ?>"></script>
<script src="<?php get_asset_url('/assets/base/lib/jquery-validate/localization/messages_vi.min.js') ?>"></script>
<!--<script src="/assets/base/lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>-->

<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
<!--<script src="--><?php //echo get_admin_theme_asset() ?><!--js/core/jquery.min.js"></script>-->
<script src="<?php echo get_admin_theme_asset() ?>js/core/bootstrap.min.js"></script>
<script src="<?php echo get_admin_theme_asset() ?>js/core/jquery.slimscroll.min.js"></script>
<script src="<?php echo get_admin_theme_asset() ?>js/core/jquery.scrollLock.min.js"></script>
<script src="<?php echo get_admin_theme_asset() ?>js/core/jquery.appear.min.js"></script>
<script src="<?php echo get_admin_theme_asset() ?>js/core/jquery.countTo.min.js"></script>
<script src="<?php echo get_admin_theme_asset() ?>js/core/jquery.placeholder.min.js"></script>
<script src="<?php echo get_admin_theme_asset() ?>js/core/js.cookie.min.js"></script>
<script src="<?php echo get_admin_theme_asset() ?>js/app.js"></script>

<script src="<?php get_asset_url('/assets/base/lib/jquery/jquery.form.min.js" type="application/javascript') ?>"></script>
<!--<script src="--><?php //echo get_admin_theme_asset() ?><!--js/plugins/jquery-validation/jquery.validate.min.js"></script>-->
<!--<script src="--><?php //echo get_admin_theme_asset() ?><!--js/plugins/jquery-validation/additional-methods.min.js"></script>-->
<!--<script src="--><?php //echo get_admin_theme_asset() ?><!--js/pages/base_forms_validation.js"></script>-->

<!--sweet alert .js-->
<script src="<?php get_asset_url('/assets/base/lib/sweetalert/sweetalert.min.js" type="application/javascript') ?>"></script>

<!-- Select2 -->
<!--<script src="/assets/base/lib/select2/select2.full.min.js"></script>-->
<script src="<?php echo get_admin_theme_asset() ?>js/plugins/select2/select2.full.min.js"></script>
<script>
    $(function () {
        $(".select2").select2();
    });
    $(document).ready(function () {
        var docWidth = $(document).width();
        console.log(docWidth);
        if (docWidth < 1400) {
            $('#page-container').addClass('sidebar-mini');
        }
    })
</script>
<script>
    jQuery(function () {
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
//        App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
        App.initHelpers(['maxlength', 'select2', 'table-tools']);
    });
</script>