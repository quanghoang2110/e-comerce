<?php $admin_menu_file = get_admin_session(BASE_SESSION_ADMIN_IS_ROOT) ? BASE_CONF_FILE_MENU_ROOT : BASE_CONF_FILE_MENUGROUP_FOLDER . get_admin_session(BASE_SESSION_ADMIN_GROUPID); ?>
<?php $admin_menu = json_decode(file_get_contents($admin_menu_file)); ?>

<div class="side-content">
    <ul class="nav-main">
		<?php foreach ($admin_menu as $item): ?>
            <li class="nav-main-heading"><span class="sidebar-mini-hide"><?php echo $item->name ?></span></li>
			<?php foreach ($item->menu as $i_menu): ?>
				<?php if ($i_menu->is_group) : ?>
                    <li class="<?php echo in_array($role, explode(',', $i_menu->role)) ? 'open' : ''; ?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                    class="<?php echo $i_menu->icon ?> <?php echo in_array($role, explode(',', $i_menu->role)) ? 'active' : ''; ?>"></i><span
                                    class="sidebar-mini-hide"><?php echo ucfirst_utf8($i_menu->name) ?></span></a>
                        <ul>
							<?php foreach ($i_menu->menu as $ii_menu): ?>
                                <li class="">
                                    <a class="<?php echo $role == $ii_menu->category ? 'active' : ''; ?>" href="<?php echo base_url($ii_menu->url); ?>"><?php echo ucfirst_utf8($ii_menu->name) ?></a>
                                </li>
							<?php endforeach; ?>
                        </ul>
                    </li>
				<?php else: ?>
                    <li class="">
                        <a class="<?php echo $role == $i_menu->category ? 'active' : ''; ?>" href="<?php echo base_url($i_menu->url); ?>"><i class="<?php echo $i_menu->icon ?>"></i><span
                                    class="sidebar-mini-hide"><?php echo ucfirst_utf8($i_menu->name) ?></span></a>
                    </li>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endforeach; ?>
    </ul>
</div>
<style>
    .nav-main li>a.active{
        background-color: rgba(0, 0, 0, 0.2);
    }
    .nav-main li ul li a.active{
        background-color: transparent;
    }
</style>