<header id="header-navbar" class="content-mini content-mini-full">
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
        <li>
            <div class="btn-group">
                <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                    <?php echo get_img_tag($session_img, '', 'user-image', BASE_IMG_DEFAULT); ?>
                    <!--                    CMS-->
                    <label><?php echo $session_fullname ? $session_fullname : $session_username ?></label>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-header">Tài khoản</li>
                    <!--                    <li>-->
                    <!--                        <a tabindex="-1" href="base_pages_inbox.html">-->
                    <!--                            <i class="si si-envelope-open pull-right"></i>-->
                    <!--                            <span class="badge badge-primary pull-right">3</span>Inbox-->
                    <!--                        </a>-->
                    <!--                    </li>-->
                    <li>
                        <a tabindex="-1" href="/<?php echo BASE_ROUTE_ADMIN_PROFILE ?>">
                            <i class="si si-user pull-right"></i>
                            Thông tin cá nhân
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="#change-page" onclick="changePass()">
                            <i class="si si-key pull-right"></i>
                            Đổi mật khẩu
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="<?php echo base_url(BASE_ROUTE_ADMIN_LOGOUT) ?>">
                            <i class="si si-logout pull-right"></i>Thoát tài khoản
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Hệ thống</li>
                    <?php if ($session_is_root): ?>
                        <li>
                            <a tabindex="-1" href="/<?php echo BASE_ROUTE_FOR_DEV ?>">
                                <i class="si si-settings pull-right"></i>Cấu hình hệ thống
                            </a>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a tabindex="-1" href="/">
                            <i class="si si-lock pull-right"></i>Khóa màn hình
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <!--        <li>-->
        <!--            <button class="btn btn-default" data-toggle="layout" data-action="side_overlay_toggle" type="button">-->
        <!--                <i class="fa fa-tasks"></i>-->
        <!--            </button>-->
        <!--        </li>-->
    </ul>
    <!-- END Header Navigation Right -->

    <!-- Header Navigation Left -->
    <ul class="nav-header pull-left">
        <li class="hidden-md hidden-lg">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                <i class="fa fa-navicon"></i>
            </button>
        </li>
        <li class="hidden-xs hidden-sm">
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                <i class="fa fa-ellipsis-v"></i>
            </button>
            <button class="btn btn-primary" onclick="ajaxRun('/<?php echo MFW_ROUTE_UPDATE_FRONT_ROUTES ?>')"
                    type="button">
                <i class="fa fa-link"></i> Cập nhật short-link
            </button>
        </li>
        <!--        <li>-->
        <!--            <button class="btn btn-default pull-right" data-toggle="modal" data-target="#apps-modal" type="button">-->
        <!--                <i class="si si-grid"></i>-->
        <!--            </button>-->
        <!--        </li>-->
        <!--        <li class="visible-xs">-->
        <!--            <button class="btn btn-default" data-toggle="class-toggle" data-target=".js-header-search"-->
        <!--                    data-class="header-search-xs-visible" type="button">-->
        <!--                <i class="fa fa-search"></i>-->
        <!--            </button>-->
        <!--        </li>-->
        <!--        <li class="js-header-search header-search">-->
        <!--            <form class="form-horizontal" action="base_pages_search.html" method="post">-->
        <!--                <div class="form-material form-material-primary input-group remove-margin-t remove-margin-b">-->
        <!--                    <input class="form-control" type="text" id="base-material-text" name="base-material-text"-->
        <!--                           placeholder="Search..">-->
        <!--                    <span class="input-group-addon"><i class="si si-magnifier"></i></span>-->
        <!--                </div>-->
        <!--            </form>-->
        <!--        </li>-->
    </ul>
    <!-- END Header Navigation Left -->
</header>
<script>

    function ajaxRun(url) {
        showLoading();
        $.get(url).done(function (data) {
            hideLoading();
            $('#modalResp .modal-body').html(data);
            $('#modalResp').modal('show');
        });
    }
</script>
<div class="modal fade" id="modalResp" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">RUN RESULT</h4>
            </div>
            <div class="modal-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->