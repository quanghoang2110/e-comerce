<style>
    /*	custom style here*/
    .box-border {
        border: 1px solid #e0e0e0;
    }

    input#page_search_x:placeholder-shown {
        font-weight: normal;
    }

    .custom-text-center {
        text-align: center;
    }

    .custom-block-content {
        margin-left: 15px;
        margin-right: 15px;
    }

    .form-horizontal .form-group-material {
        /*margin-left: -30px!important;*/
        /*margin-right: -15px!important;*/
    }

    .form-material .select2-container--default .select2-selection--single {
        border-radius: 4px;
        background: #f9f9f9;
        border-color: #ccc;
        text-align: center;
    }
</style>