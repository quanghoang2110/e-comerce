<!--model view actual size image-->
<div id="modalRealImg" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialogx" role="document">
        <div class="modal-contentx">
            <div class="modal-body" align="center">
                <img src="" id="imgSrc"/>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    function viewRealImg(obj) {
        $('#imgSrc').attr('src', obj.find('img').attr('src'));
        return true;
    }
</script>
<style>
    .modal-dialogx {
        display: table;
        margin: 30px auto;
        padding: 0;
    }
</style>

<div class="modal" id="modal_msg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Nội dung thông báo</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-callback" data-dismiss="modal">Đồng ý</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    function showThongBao(text, callback) {
        $("#modal_msg .modal-body p").html(text);
        $("#modal_msg").modal("show");
        if (typeof (callback) !== 'undefined') {
            $("#modal_msg .btn-callback").click(function () {
                callback();
            });
        }
    }
    function showThongBaoReLoad(text, callback) {
        $("#modal_msg .modal-body p").html(text);
        $("#modal_msg").modal("show").on('hidden.bs.modal', function () {
            callback();
        });

        if (typeof (callback) !== 'undefined') {
            $("#modal_msg .btn-callback").click(function () {
                callback();
            });
        }
        setTimeout(function () {
            $("#modal_msg").modal("hide");
        }, 2500);
    }
</script>

<!-- change pass -->
<!-- BEGIN FORM MODAL MARKUP -->
<div class="modal fade" id="modal_change_pass" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <form onsubmit="return submitchangePass()" class="form-horizontal form-validate floating-label" role="form"
                  novalidate="novalidate" method="post" id="form_change_pass">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Thay đổi mật khẩu</h3>
                    </div>

                    <div class="modal-body">
                        <div class="alert alert-danger" id="err_msg_" role="alert">
                            <div id="_msg"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label for="cur_pass" class="control-label">Mật khẩu hiện tại</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="password" name="cur_pass" id="cur_pass" class="form-control"
                                       placeholder="Mật khẩu hiện tại" required aria-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label for="new_pass" class="control-label">Mật khẩu mới</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="password" name="new_pass" id="new_pass" class="form-control"
                                       placeholder="Mật khẩu mới" required data-rule-minlength="6">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label for="re_new_pass" class="control-label">Mật khẩu mới</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="password" name="re_new_pass" id="re_new_pass" class="form-control"
                                       placeholder="Nhập lại mật khẩu mới" required data-rule-equalto="#new_pass">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-primary">Đồng ý</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->
<script>
    function changePass() {
        document.getElementById("form_change_pass").reset();
        $('#form_change_pass #err_msg_').addClass('hide');
        $("#modal_change_pass").modal("show");
    }
    function submitchangePass() {
        $('#form_change_pass #err_msg_').addClass('hide');
        var status = 0;
        var msg = "";
        if ($('#cur_pass').val() == '') {
            msg = "<p>Vui lòng nhập mật khẩu hiện tại!</p>";
        } else if ($('#new_pass').val() == '') {
            msg = "<p>Vui lòng nhập mật khẩu mới.</p><p>Mật khẩu mới cần lớn hơn hoặc bằng 6 ký tự.</p>";
        } else if ($('#new_pass').val() != $('#re_new_pass').val()) {
            msg = "<p>Vui lòng nhập lại mật khẩu mới trùng với mật khẩu đã nhập.</p>";
        } else if ($('#new_pass').val().length < 6) {
            msg = "<p>Vui lòng nhập mật khẩu mới lớn hơn hoặc bằng 6 ký tự</p>";
        } else if ($('#cur_pass').val() == $('#new_pass').val()) {
            msg = "<p>Mật khẩu không thay đổi!</p>";
        } else {
            status = 1;
        }

        if (status == 0) {
            $('#form_change_pass #err_msg_').removeClass('hide');
            $("#_msg").html(msg);
            return false;
        }

        $('#form_change_pass #err_msg_').addClass('hide');

        $.post('/<?php echo BASE_ROUTE_ADMIN_CHANGE_PASS ?>', {
            cur_pass: $('#cur_pass').val(),
            new_pass: $('#new_pass').val()
        }).done(function (data) {
            var obj = $.parseJSON(data);
            if (obj.status == 0) {
                $('#modal_change_pass #err_msg_').removeClass('hide');
                $('#modal_change_pass #err_msg_ #_msg').text(obj.msg);

            } else {
                $(".modal").modal("hide");
                if (obj.status == -1) {
                    swal({
                        title: "Lỗi",
                        text: obj.msg,
                        confirmButtonText: "Đồng ý",
                        type: "error"
                    }, function () {
                        window.location.reload();
                    });
                } else swal("Thành công", obj.msg, "success");
            }
        });
        return false;
    }
</script>
<style>
    .error {
        color: red;
    }

    .margin-top-15 {
        margin-top: 15px;
    }

    .div_parent .name {
        color: #367FA9;
        font-size: 16px;
        font-weight: 700;

    }

    .div_child {
        font-size: 14px;
        font-weight: 500;
    }

    .name {
        border-bottom: 1px solid #DCEBF9;
    }
</style>

<div class="modal" id="modal_update"></div>