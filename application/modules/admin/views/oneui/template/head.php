<link rel="stylesheet" href="<?php get_asset_url('assets/base/lib/bootstrap-3.3.7/css/bootstrap.min.css') ?>">
<link rel="stylesheet" id="css-main" href="<?php get_asset_url('/assets/admin/oneui/css/themes/flat.min.css') ?>">

<!-- Select2 -->
<!--<link rel="stylesheet" href="/assets/base/lib/select2/select2.min.css">-->
<link rel="stylesheet" href="<?php echo get_admin_theme_asset() ?>js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo get_admin_theme_asset() ?>js/plugins/select2/select2-bootstrap.min.css">

<?php if ($is_theme_index): ?>
    <link rel="stylesheet" href="<?php echo get_admin_theme_asset() ?>js/plugins/datatables/jquery.dataTables.min.css">
<?php endif ?>

<!--sweet alert .css-->
<link href="<?php get_asset_url('/assets/base/lib/sweetalert/sweetalert.css') ?>" type="text/css" rel="stylesheet"/>

<link rel="stylesheet" id="css-main" href="<?php get_asset_url('/assets/admin/oneui/css/oneui.css') ?>">

<?php load_view($theme_template . 'style'); ?>

<script type="text/javascript">

    var size = '<?php echo $page_size ?>';
    var base_url = '<?php echo base_url(); ?>';
    var backMainPage = true;

    //url
    //    var urlDetail; //link detail
    var urlDelete; //link delete
    var urlUpdate; //link update data
    var urlUpdatePosition; //link update data
    //    var urlStatus;

    function pageFilter() {
        var objPageFilter = $('#page_filter').html();
        console.log(objPageFilter);
        if (typeof objPageFilter != "undefined") {
            $('#form_filter div#for-page-filter').html(objPageFilter);
        }
        $("#form_filter").submit();
    }

    //    $(document).ready(function () {
    //        $("body").addClass("hold-transition skin-red sidebar-mini");
    //Date picker
    //        $('.datepicker').datepicker({
    //            autoclose: true
    //        });
    //    });
</script>