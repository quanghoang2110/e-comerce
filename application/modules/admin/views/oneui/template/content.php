<div id="page-container" class="sidebar-l sidebar-o side-scroll">
    <nav id="sidebar">
        <!-- Sidebar Scroll Container -->
        <div id="sidebar-scroll">
            <!-- Sidebar Content -->
            <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
            <div class="sidebar-content">
                <!-- Side Header -->
                <div class="side-header side-content bg-white-op">
                    <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                    <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button"
                            data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times"></i>
                    </button>
                    <!-- Themes functionality initialized in App() -> uiHandleTheme() -->

                    <a class="h5 text-white" href="/<?php echo BASE_ROUTE_ADMIN_DASHBOARD ?>">
                        <i class="fa fa-circle-o-notch text-primary"></i> <span class="h4 font-w600 sidebar-mini-hide">cms</span>
                    </a>
                </div>
                <!-- END Side Header -->

                <!-- Side Content -->
				<?php include_once 'side_content.php' ?>
                <!-- END Side Content -->
            </div>
            <!-- Sidebar Content -->
        </div>
        <!-- END Sidebar Scroll Container -->
    </nav>
    <!-- END Sidebar -->

    <!-- Header -->
	<?php include "header.php"; ?>
    <!-- END Header -->

    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div class="content">
            <div class="block overflow-hidden">
                <div class="block-header">
                    <div class="pull-left">
                        <h1 class="page-heading">
							<?php echo $module_title ?>
                            <small><?php echo $module_desc ?></small>
                        </h1>
                    </div>
					<?php if ($page_action_enabled): ?>
						<?php load_page_action_btn($page_folder) ?>
					<?php endif; ?>
                </div>
				<?php if ($is_theme_index) { ?>
                    <div class="block-content">
                        <div id="DataTables_Table_2_wrapper"
                             class="dataTables_wrapper no-footer">
							<?php if ($paging_enable): ?>
								<?php //if (isset($paging) && $paging) : ?>
                                <form id="form_filter" class="form-inline dt-bootstrap">
                                    <div>
                                        <select data-toggle="tooltip" title="Hiển thị số lượng bản ghi trên 1 trang"
                                                name="per_size" onchange="pageFilter()"
                                                class="form-control pull-left">
                                            <option value="5" <?php echo $page_size == 5 ? 'selected="selected"' : '' ?>>
                                                5
                                            </option>
                                            <option value="10" <?php echo $page_size == 10 ? 'selected="selected"' : '' ?>>
                                                10
                                            </option>
                                            <option value="20" <?php echo $page_size == 20 ? 'selected="selected"' : '' ?>>
                                                20
                                            </option>
                                            <option value="50" <?php echo $page_size == 50 ? 'selected="selected"' : '' ?>>
                                                50
                                            </option>
                                        </select>

										<?php load_view($page_folder . '/index_page_filter'); ?>

                                        <div class="pull-right">
                                            <label>Tìm kiếm:
                                                <div class="input-group">
                                                    <input id="page_search_x"
                                                           name="search"
                                                           class="form-control"
                                                           value="<?php echo $keyword ?>"
                                                           placeholder="<?php echo $search_tip ?>">
                                                    <span class="input-group-btn"><button
                                                                class="btn btn-default"
                                                                type="button"
                                                                onclick="if($('#page_search_x').val()) {pageFilter()} else {$('#page_search_x').focus(); return false;}"><i
                                                                    class="fa fa-search"></i></button></span>
                                                </div>
                                        </div>
                                    </div>
                                </form>
								<?php //endif; ?>
							<?php endif; ?>
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-striped table-vcenter">
										<?php load_view($content) ?>
                                    </table>
                                </div>
                            </div>
							<?php if ($paging_enable): ?>
								<?php if (isset($paging) && $paging) : ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="dataTables_info" id="DataTables_Table_2_info" role="status"
                                                 aria-live="polite"><?php echo $paging_info ?></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="dataTables_paginate paging_full_numbers"
                                                 id="DataTables_Table_2_paginate">
												<?php echo $paging ?>
                                            </div>
                                        </div>
                                    </div>
								<?php endif ?>
							<?php endif ?>
                        </div>
                    </div>
				<?php } else if ($is_theme_custom) { ?>
					<?php load_view($content) ?>
				<?php } else { ?>
                    <div class="block-content block-content-full">
                        <form id="form_update" class="form-horizontal">
                            <div class="custom-block-content">
								<?php load_view($content) ?>
                            </div>
                        </form>
                    </div>
				<?php } ?>
            </div>
            <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
    <!-- Footer -->
    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
        <div class="pull-left">
            <a class="font-w600" href="#" target="_blank">MCI 1.0</a> &copy; <span
                    class="js-year-copy"></span>
        </div>
    </footer>
    <!-- END Footer -->
</div>

<?php include_once('tmp_modal.php'); ?>
