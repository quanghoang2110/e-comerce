<!-- Content Header (Page header) -->
<div class="col-sm-6 col-sm-offset-3">
    <!-- Error Titles -->
    <h1 class="font-s128 font-w300 text-city animated flipInX">404</h1>
    <h2 class="h3 font-w300 push-50 animated fadeInUp">Rất tiếc! Chúng tôi không tìm thấy trang mà bạn yêu cầu...</h2>
    <!-- END Error Titles -->

    <!-- Search Form -->
    <form class="form-horizontal push-50" action="" method="post">
        <div class="form-group">
            <div class="col-sm-6 col-sm-offset-3">
                <a href="/<?php echo BASE_ROUTE_ADMIN_DASHBOARD ?>" class="btn btn-primary">BACKEND</a>
                <a href="/" class="btn btn-info">FRONTEND</a>
                &nbsp;
                <a href="/<?php echo BASE_ROUTE_ADMIN_LOGOUT?>" class="btn btn-warning">Dùng tài khoản khác</a>
            </div>
        </div>
    </form>
    <!-- END Search Form -->
</div>