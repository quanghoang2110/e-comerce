<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 1/23/2017
 * Time: 10:35
 */
class Model_order extends MModel
{
    function get_order_detail($order_id)
    {
        $this->db->select('number,price_per_once,name');
        $this->db->from('order_detail');
        $this->db->join('product', 'order_detail.product_id=product.id', 'left');
        $this->db->where('order_detail.order_id', $order_id);
        return $this->db->get()->result();
    }
}