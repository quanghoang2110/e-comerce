<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 1/23/2017
 * Time: 10:35
 */
class Model_comment extends MModel
{
    function get_list_comment($page, $limit, $search = "")
    {
        $this->db->select("a.*, b.name as product_name, c.name as news_name, b.id as product_id, b.category_slug, c.id as news_id, d.name as news_category_name");
        $this->db->from('customer_comment a');
        $this->db->join('product b', 'a.category_id=b.id and a.type = 2', 'left');
        $this->db->join('news c', 'a.category_id=c.id and a.type = 1', 'left');
        $this->db->join('news_category d', 'c.category_id = d.id', 'left');
        if ($search) {
            $this->db->like("a.desc", $search);
        }
        $this->db->where("a.is_deleted", 0);
        $this->db->limit($limit, ($page - 1) * $limit);
        $this->db->order_by('id', 'desc');
        return $this->db->get()->result();
    }
}