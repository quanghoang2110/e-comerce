<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Agency_object extends Mfw_object
{
    public $name;
    public $address;
    public $email;
    public $phone;
    public $url;
    public $note;
    public $image;
    public $position;

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
    }
}