<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product_object
{
    public $title;
    public $id;
    public $num_posts = 5;
    public $same_id;

    /**
     * Product_object constructor.
     * @param null $data
     */
    public function __construct($data = null)
    {
        if ($data) {
            $this->title = $data->title;
            $this->id = $data->id;
            $this->num_posts = $data->num_posts;
            $this->same_id = $data->same_id;
        }
    }
}