<?php
/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 2/8/2017
 * Time: 10:41
 */
class Admin_object{
	public $id;
	public $group_id;
	public $img;
	public $username;
	public $fullname;
	public $is_root;
	public $valid;
	public $no_edit;
	public $password="";
}