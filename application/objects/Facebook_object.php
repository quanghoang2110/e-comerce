<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook_object extends Mfw_object
{
    public $data_layout = 'standard';
    public $data_size = 'small';
    public $method;
    public $url;
    public $show_face = true;
    public $share = true;
    public $num_posts = 5;
    public $app_id;

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
    }
}