<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_object extends Mfw_object
{
    public $id;

    //site config
    public $logo;
    public $favicon;
    public $cache_on;
    public $name;
    public $url;
    public $slogan;
    //seo
    public $meta_keyword;
    public $meta_title;
    public $meta_desc;
    public $meta_url;
    public $meta_image;
    public $admin_email;

    //social network
    public $style;
    public $fb_app_id;
    public $fb_fanpage;
    public $twitter;
    public $instagram;
    public $pinterest;
    public $gg_map;
    public $gg_plus_id;
    public $youtube;
    public $skype;

    //analytics
    public $gg_analytics_id;
    public $gg_tag_manager_id;

    //company info
    public $company_name;
    public $company_address;
    public $company_phone;
    public $company_phone_1;
    public $company_phone_2;
    public $company_phone_3;
    public $company_email;
    public $company_info;

    public $support_email;
    public $session_user;

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
    }
}