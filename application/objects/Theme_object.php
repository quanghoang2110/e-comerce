<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 2/8/2017
 * Time: 10:41
 */
define('THEME_ONEUI', 'oneui');
define('THEME_ONEUI2', 'oneui2');
define('THEME_ALT', 'alt');
define('THEME_MATERIAL', 'material');

class Theme_object
{
    static public $name;
    public static $is_material_design;
    public static $view_tmp;

    private $themes_admin = array(
        THEME_ONEUI => array(
            'theme' => THEME_ONEUI,
            'material' => TRUE
        ),
        THEME_ONEUI2 => array(
            'theme' => THEME_ONEUI,
            'material' => FALSE
        ),
        THEME_ALT => array(
            'theme' => THEME_ALT,
            'material' => FALSE
        ),
        THEME_MATERIAL => array(
            'theme' => THEME_MATERIAL,
            'material' => TRUE
        )
    );

    function __construct($type = "admin")
    {
        if ($type == "admin") {
            $theme = $this->themes_admin[THEME_ONEUI2];
            Theme_object::$name = $theme['theme'];
            Theme_object::$is_material_design = $theme['material'];
            Theme_object::$view_tmp = $theme['theme'] . '/template/';
        }
    }

    /**
     * full file path (in theme template folder)
     * @param $view_file
     * @return string
     */
    static function get_file_in_template($view_file)
    {
        return Theme_object::$name . '/template/' . $view_file;
    }

    /**
     * @return string
     */
    static function get_folder_admin_asset()
    {
        return get_asset_url('/assets/' . ADMIN_MODULE . "/" . Theme_object::$name . '/');
    }
}