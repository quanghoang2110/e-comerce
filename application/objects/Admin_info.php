<?php

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 2/8/2017
 * Time: 10:41
 */
class Admin_info
{
	public $id;
	public $group_id;
	public $img;
	public $username;
	public $fullname;
	public $is_root;
	public $valid;
	public $no_edit;
	public $password = "";

	/**
	 * Admin_object constructor.
	 * @param $data
	 */
	public function __construct($data)
	{
		if ($data) {
			$this->id = $data->id;
			$this->img = $data->img;
			$this->group_id = $data->group_id;
			$this->username = $data->username;
			$this->fullname = $data->fullname;
			$this->is_root = $data->is_root;
			$this->valid = $data->valid;
			$this->no_edit = $data->no_edit;
		}
	}


}