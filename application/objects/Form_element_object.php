<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 2/8/2017
 * Time: 10:41
 */
class Form_element_object extends Mfw_object
{
    public $view;
    public $id;
    public $label;
    public $grid_col = DF_VALUE;
    public $field = DF_VALUE;
    public $field_value = DF_VALUE;
    public $help_block = DF_VALUE;
    public $attr = DF_VALUE;
    public $label_class = "";

    public $select_options;

    //image
    public $cropped = BASE_ADMIN_IMG_CROPPED_DF;
    public $crop_ratio = 1;

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
    }
}