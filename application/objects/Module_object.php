<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Module_object extends Mfw_object
{
    public $name;
    public $method = "index";
    public $params;

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
    }
}