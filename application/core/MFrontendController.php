<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Frontend_Controller
 * base frontend controller
 * @author quanghv
 */
class MFrontendController extends MController
{
    protected $_current_menu, $_sort_by;

    protected $_logo, $_favicon;

    //seo
    protected $_site_name, $_seo_title, $_seo_desc, $seo_keyword, $_seo_url, $_seo_image;

    protected $_user_id, $_user_email, $_user_fullname, $_access_token, $_user_avatar, $_user_channel_registed;

    protected $_title, $_primary_name, $_default_name;

    protected $_top_slide_on_page = true;

    protected $_header_v = "", $_body_id = "index", $_body_class = "fixed-sn", $_main_class;
    protected $_facebook_sdk = false;

    protected $_module_view;

    function __construct()
    {
        // Construct our parent class
        parent::__construct();

        //set option config
        $this->_folder = "";//BASE_FOLDER_FRONTEND;

        $this->_theme = PUBLIC_THEME;

        if ($this->input->get('theme'))
            $this->_theme = $this->input->get('theme');

        $this->load->helper('mfw_url_helper');
        $this->load->helper('mfw_asset');
        $this->load->helper('mfw_module');

        $this->_theme_tmp = $this->_folder . '/' . $this->_theme . '/template/';
        $this->_theme_assets = "/assets/{$this->_folder}/{$this->_theme}/";
        $this->_theme_view = $this->_folder . '/' . $this->_theme;
    }

    private function _load_site_setting(): Setting_object
    {
        if (file_exists(BASE_CONF_FILE_SITE_SETTING)) {
            $str = file_get_contents(BASE_CONF_FILE_SITE_SETTING);
//            $setting = json_decode($str);
            $setting = new Setting_object(json_decode($str));
        } else {
            //load site info from data ------------------
            // Turn caching on
            $this->_model->enable_cache();

            $this->_load_data_from(BASE_TABLE_SITE_SETTING);
            $settings = $this->_model->_get_row();
            // Turn caching off
            $this->_model->disable_cache();
            $setting = new Setting_object($settings);
        }

        if ($this->_seo_title) {
            $setting->meta_title = $this->_seo_title;
        }
        if ($this->_seo_desc) {
            $setting->meta_desc = $this->_seo_desc;
        }
        if ($this->_seo_url) {
            $setting->meta_url = $this->_seo_url;
        }
        if ($this->_seo_image) {
            $setting->meta_image = $this->_seo_image;
        }

        return $setting;
    }


    function _init_page()
    {
        $this->_limit = BASE_CONF_LIMIT;
        $this->_base_init_page();
        $this->_sort_by = $this->input->get('sort_by', true);
    }

    /**
     * @param string $view
     */
    function _load_tmp($view = '')
    {
        $this->_module_vars['setting'] = $this->_load_site_setting();

        //filter
        $this->_module_vars['page'] = $this->_page;
        $this->_module_vars['page_size'] = $this->_limit;
        $this->_module_vars['page_keyword'] = $this->_search;
        $this->_module_vars['page_sort_by'] = $this->_sort_by;

//        $vars['style'] = $this->_style;
//        $this->_module_vars['site_name'] = $this->_site_name;
//        $this->_module_vars['seo_title'] = ellipsize($this->_seo_title, 65);
//        $this->_module_vars['logo'] = $this->_logo;
//        $this->_module_vars['favicon'] = $this->_favicon;
//        $this->_module_vars['seo_description'] = ellipsize($this->_seo_description, 165);
//        $this->_module_vars['seo_keyword'] = $this->_seo_keyword ? $this->_seo_keyword : "";
//        $this->_module_vars['seo_url'] = $this->_seo_url;
//        if (strpos($this->_seo_image, 'http') == false) {
//            $this->_seo_image = base_url($this->_seo_image);
//        }
//        $this->_module_vars['seo_image'] = $this->_seo_image;
//        $this->_module_vars['fb_fanpage'] = $this->_fb_fanpage;
//        $this->_module_vars['skype'] = $this->_skype;
//        $this->_module_vars['youtube'] = $this->_youtube;
//        $this->_module_vars['google_plus'] = $this->_google_plus;
//        $this->_module_vars['fb_app_id'] = $this->_fb_app_id;
//        $this->_module_vars['gg_analytics_id'] = $this->_gg_analytics_id;
//
//        $this->_module_vars['company_name'] = $this->_com_name;
//        $this->_module_vars['company_phone'] = $this->_com_phone;
//        $this->_module_vars['company_email'] = $this->_com_email;
//        $this->_module_vars['company_address'] = $this->_com_address;
//        $this->_module_vars['company_time'] = $this->_com_time;
//        $this->_module_vars['company_info'] = $this->_com_info;

//        $this->_module_vars['current_menu'] = $this->_current_menu;

//        $this->_module_vars['header'] = "header" . $this->_header_v;

//        $this->_module_vars['body_id'] = $this->_body_id;
        $this->_module_vars['body_class'] = $this->_body_class;
        $this->_module_vars['current_menu'] = $this->_current_menu;
//        $this->_module_vars['main_class'] = $this->_main_class;
        $this->_module_vars['module_view'] = $this->_module_view;
        $this->_module_vars['facebook_sdk'] = $this->_facebook_sdk;
//        $view = $view ? $view : 'base_tmp';
        $this->_base_load_tmp('base_tmp');
    }

    function _error_page()
    {

    }

    /**
     * base ajax change pass
     */
    function _ajax_change_pass()
    {
        $tbl_user = $this->config->item('tbl_user');
        if ($tbl_user) {

            $this->_load_data_from($tbl_user);
            $current_pass = $this->input->post("cur_pass");
            $status = -1;
            $msg = "Vui lòng đăng nhập";
            if ($this->_sess_userid) {
                if ($this->_model->_count(
                    array(
                        'id' => $this->_sess_userid,
                        'password' => encryption_password($current_pass)
                    )
                )
                ) {
                    $new_pass = $this->input->post("new_pass");
                    $this->_model->_update(
                        array('password' => encryption_password($new_pass)), array("id" => $this->_sess_userid)
                    );
                    $status = 1;
                    $msg = "Bạn đã thay đổi mật khẩu thành công!";
                } else {
                    $status = 0;
                    $msg = "Mật khẩu hiện tại không chính xác!";
                }
            }

            echo json_encode(array(
                'status' => $status,
                'msg' => $msg
            ));
        }
    }

    /**
     * subscribe
     * - receive notification
     */
    function ajax_subscribe()
    {
        $email = $this->input->post('input_email');
        $status = -1;
        //$msg = "Vui lòng nhập vào email của bạn";
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $this->_load_data_from('subscribe');
            $this->_model->_insert(array(
                'email' => $email,
                'status' => 1,
                'created_time' => date('Y-m-d H:i:s')
            ));
            $status = 1;
            $msg = "<p class='text-success'>Đăng ký nhận thông báo thành công!</p>";
        } else {
            $msg = "<p class='text-danger'>Email đăng ký không hợp lệ, vui lòng kiểm tra lại!</p>";
        }

        echo json_encode(array(
            'status' => $status,
            'msg' => $msg
        ));
    }

    /**
     * public account login
     */
    function ajax_login()
    {
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);

        $rules = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'password',
                'label' => 'Mật khẩu',
                'rules' => 'required'
            )
        );

        $response = $this->_run_form_validation($rules);
        if ($response === true) {
            $this->_load_data_from('account');
            $user = $this->_model->_get_row(array(
                'email' => $email,
                'password' => encryption_password($password)
            ));
            if (!$user) {
                $status = -1;
                $msg = "Tài khoản hoặc mật khẩu không hợp lệ!";
            } else {
                $status = 1;
                $msg = "Đăng nhập thành công";

                $this->set_usersession(array(
                    'user_id' => $user->id,
                    'user_fullname' => $user->fullname,
                    'user_email' => $user->email,
                    'user_avatar' => $user->avatar_link
                ));
            }

            echo $this->_ajax_response_msg($status, $msg);
        } else
            echo $response;
    }

    function ajax_logout()
    {
        $this->session->session_destroy();
//        $this->destroy_usersession();
        redirect("/");
    }

    /**
     * public account reg
     */
    function ajax_reg()
    {
        $this->_load_data_from('account');

        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $this->input->post('repassword', true);
        $phone = $this->input->post('phone', true);

        $rules = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|callback_is_exists_email[email]',
                'errors' => array(
                    'is_exists' => 'Email này đã được sử dụng.',
                ),
            ),
            array(
                'field' => 'password',
                'label' => 'Mật khẩu',
                'rules' => 'required'
            ),
            array(
                'field' => 'repassword',
                'label' => 'Nhập lại mật khẩu',
                'rules' => 'required|matches[password]',
                'errors' => array(
                    'matches' => 'Nhập lại mật khẩu không chính xác.',
                ),
            ),
            array(
                'field' => 'phone',
                'label' => 'Số điện thoại',
                'rules' => 'numeric|min_length[9]|max_length[12]'
            )
        );

        $response = $this->_run_form_validation($rules);
        if ($response === true) {
            $data = array(
                'email' => $email,
                'password' => encryption_password($password),
                'phone' => $phone,
                'create_time' => date('Y-m-d H:i:s')
            );
            $this->_model->_insert($data);
        } else
            echo $response;
    }

    function is_exists_email($str)
    {
        return $this->_model->_count(array('email' => $str)) <= 0;
    }

    function error_404()
    {
        echo $this->load->view('');
    }

    function _get_page_info($total)
    {
        $this->_base_get_page_info($total);
    }

//    function _check_token()
//    {
//        if ($this->_user_id) {
//            $check = $this->_rest(API_ACCESS_TOKEN_GET . '?id=' . $this->_user_id);
//            if ($check->status < 0 || ($check->status == 1 && $check->data->access_token != $this->_access_token)) {
//                $this->ajax_logout();
//            }
//        }
//    }

    function show_popup_crop_image()
    {
        $data['title'] = 'Upload ảnh';
        $data['div_data'] = $this->input->post('div_data');
        $data['input_data'] = $this->input->post('input_data');
        $data['preview_data'] = $this->input->post('preview_data');
        $data['ratio'] = ($this->input->post('ratio')) ? $this->input->post('ratio') : '1/1';
        $data['func'] = ($this->input->post('func')) ? $this->input->post('func') : '';
        $msg = $this->load->view($this->_folder . '/' . $this->_theme . '/template/popup_crop_image', $data, true);
        echo json_encode(array("status" => 1, "html" => $msg));
    }
}