<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Frontend_Controller
 * base frontend controller
 * @author quanghv
 */
class MFWController extends MController
{

    function __construct()
    {
        parent::__construct();
    }
}