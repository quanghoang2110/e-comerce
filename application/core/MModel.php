<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @copyright Go Solution JSC
 * Created by PhpStorm.
 * User: Hoang Quang
 * Date: 8/7/2015
 * Time: 10:35 AM
 */

require SYSDIR . "/core/Model.php";

class MModel extends CI_Model
{
    public $table_name;
    public $_query = "", $_search_keyword = "", $_search_row = "name";
    public $_where_row = "", $_where_in_array = array(), $_where_not_in_array = array();

    function enable_cache()
    {
        // Turn caching on
        $this->db->cache_on();
    }

    function disable_cache()
    {
        // Turn caching off
        $this->db->cache_off();
    }

    function reset_all()
    {
        $this->_query = "";
        $this->_search_keyword = "";
        $this->_search_row = "name";
        $this->_where_row = "";
        $this->_where_in_array = array();
        $this->_where_not_in_array = array();
    }

    private function search()
    {
        if ($this->_search_keyword) {
            $arr = explode(" ", $this->_search_keyword);
            $keyword = "";
            foreach ($arr as $index => $k) {
                $keyword .= $index == 0 ? $k : " " . $k;
                if (is_array($this->_search_row)) {
                    $querySearch = "";
                    foreach ($this->_search_row as $position => $searchRow) {
                        if ($position > 0) {
                            $querySearch .= " OR ";
                        }
                        $querySearch .= "$searchRow LIKE '%$keyword%'";
                    }
                    if($querySearch) {
                        $this->db->where("($querySearch)", null, false);
                    }
                } else {
                    $this->db->like($this->_search_row, $keyword);
                }
            }
        }
    }

    /**
     * count all result from db where
     * @param $where_arr
     * @return mixed
     */
    function _count($where_arr = "")
    {
        if ($where_arr)
            $this->db->where($where_arr);

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }

        //where in / where not in
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        //search query
        $this->search();

        $this->db->from($this->table_name);
        return $this->db->count_all_results();
    }

    /**
     * get data in table from database
     * @param $where_arr
     * @param $order_arr
     * @return mixed
     */
    function _get($where_arr = "", $order_arr = "", $select = "*")
    {

        $this->db->select($select);

        if ($where_arr) {
            $this->db->where($where_arr);
        }

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        return $this->db->get($this->table_name)->result();
    }

    function _get_array($where_arr = "", $order_arr = "", $select = "*")
    {

        $this->db->select($select);

        if ($where_arr) {
            $this->db->where($where_arr);
        }

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        return $this->db->get($this->table_name)->result_array();
    }

    /**
     * get limit result from query
     * @param $where_arr
     * @param string $order_arr
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    function _get_limit($where_arr, $order_arr = "", $page = 1, $limit = BASE_CONF_LIMIT, $select = "*")
    {
        $this->db->select($select);
        $offset = ($page - 1) * $limit;
        if ($where_arr) {
            $this->db->where($where_arr);
        }

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        if ($this->_search_keyword) {
            $this->db->like($this->_search_row, $this->_search_keyword);
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table_name)->result();
    }

    /**
     * get 1st row from query
     * @param $where_arr
     * @param string $order_arr
     * @param string $select
     * @return mixed
     */
    function _get_top_one($where_arr, $order_arr = array(), $select = "*")
    {
        $this->db->select($select);
        if ($where_arr) {
            $this->db->where($where_arr);
        }

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        if ($order_arr) {
            foreach ($order_arr as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        $this->db->limit(1);
        return $this->db->get($this->table_name)->row();
    }


    /**
     * return row data
     * @param $where_arr
     * @param $select
     * @return mixed
     */
    function _get_row($where_arr = "", $select = "*")
    {
        $this->db->select($select);
        if ($where_arr)
            $this->db->where($where_arr);

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }
        //where in
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        return $this->db->get($this->table_name)->row();
    }

    /**
     * return custom row object
     * @param string $where_arr
     * @param $object
     * @param string $select
     * @return mixed
     */
    function _get_custom_row_object($where_arr = "", $object, $select = "*")
    {
        $this->db->select($select);
        if ($where_arr)
            $this->db->where($where_arr);

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }
        //where in
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        return $this->db->get($this->table_name)->custom_row_object(0, $object);
    }

    /**
     * delete row in database
     * @param $where_arr
     */
    function _delete($where_arr = "")
    {
        if ($where_arr)
            $this->db->where($where_arr);

        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }
        //where in
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        $this->db->delete($this->table_name);
    }

    /**
     * insert data to database
     * @param $data_arr
     * @return mixed
     */
    function _insert($data_arr)
    {
        $this->db->insert($this->table_name, $data_arr);
        $err = $this->db->error();
//        print_r($err);
        if ($err && $err['message']) {
            return $err['message'];
        } else
            return $this->db->insert_id();
    }

    function _insert_duplicate($data_arr)
    {
        $updatestr = array();
        $keystr = array();
        $valstr = array();

        foreach ($data_arr as $key => $val) {
            $updatestr[] = "`{$key}` = '" . $val . "'";
            $keystr[] = "`{$key}`";
            $valstr[] = "'{$val}'";
        }

        $sql = "INSERT INTO " . $this->config->item('dbprefix') . $this->table_name . " (" . implode(', ', $keystr) . ") ";
        $sql .= "VALUES (" . implode(', ', $valstr) . ") ";
        $sql .= "ON DUPLICATE KEY UPDATE " . implode(', ', $updatestr);
        return $this->db->query($sql);
    }

    /**
     * insert_batch multi data to db
     * @param $data_arr
     * @return mixed
     */
    function _insert_batch($data_arr)
    {
//        print_r($data_arr);
        $this->db->insert_batch($this->table_name, $data_arr);
        $err = $this->db->error();
//        print_r($err);
        if ($err && $err['message']) {
            return $err['message'];
        } else
            return $this->db->insert_id();
    }

    /**
     * update data to database
     * @param $data_arr
     * @param $where_arr
     */
    function _update($data_arr, $where_arr = '')
    {
        //sql
        if ($this->_query) {
            $this->db->where($this->_query, null, false);
        }
        //where in
        if ($this->_where_row) {
            if ($this->_where_in_array) $this->db->where_in($this->_where_row, $this->_where_in_array);
            if ($this->_where_not_in_array) $this->db->where_not_in($this->_where_row, $this->_where_not_in_array);
        }

        if ($where_arr) {
            $this->db->where($where_arr);
        }

        $this->db->update($this->table_name, $data_arr);

//		$this->db->update($this->table_name, $data_arr, $where_arr);
        $err = $this->db->error();
        if ($err && $err['message']) {
            return $err['message'];
        } else
            return $this->db->affected_rows();
    }

    /**
     * update data to database
     * @param $data_arr
     * @param $where_arr
     * @param $where_query
     */
//	function _update_query($data_arr, $where_arr, $where_query)
//	{
//		$this->db->where($where_arr);
//		$this->db->where($where_query, null, false);
//		$this->db->update($this->table_name, $data_arr);
//		$err = $this->db->error();
//		if ($err && $err['message']) {
//			return $err['message'];
//		} else
//			return $this->db->affected_rows();
//	}

    /**
     * increase field
     * @param $total
     * @param $total_plus
     * @param $where_arr
     */
    function _increase_field($total, $total_plus, $where_arr)
    {
        $this->db->where($where_arr);
        $this->db->set($total, $total_plus, FALSE);
        $this->db->update($this->table_name);
    }

    function is_exists_check($where_arr, $array_id = '')
    {
        if ($array_id != '') {
            $this->db->where($array_id);
        }
        $this->db->select("*")
            ->from($this->table_name)
            ->where($where_arr);
        return $this->db->count_all_results() > 0;
    }


    /**
     * only for permission =============================================================================================
     */
//    private $_permission = $this->config->item('admin_permission');

    /**
     * @param $group_id
     * @return mixed
     */
    function get_user_permission($group_id)
    {
        $this->db->select("category,key");
        $this->db->from(BASE_TABLE_ADMIN_ROLE . " as a");
        $this->db->join(BASE_TABLE_ADMIN_ROLE_GROUP . " as b", 'a.id=b.permission_id', "left");
        $this->db->where('group_id', $group_id);
        return $this->db->get()->result();
    }

    /**
     * @return mixed
     */
    function get_permission_func()
    {
        $this->db->select('category_name');
        $this->db->where('valid', 1);
        $this->db->group_by('category,category_name');
        $this->db->order_by('category_name');
        return $this->db->get(BASE_TABLE_ADMIN_ROLE)->result();
    }

    function delete_invalid_admin_menu()
    {
        $query = "DELETE FROM " . BASE_TABLE_ADMIN_MENU . " WHERE category NOT IN (SELECT category FROM " . BASE_TABLE_ADMIN_ROLE . " GROUP BY category) and is_group=FALSE";
        $this->db->query($query);
    }
}