<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class MCartController
 * cart base controller
 * @author quanghv
 */
class MCartController extends MController
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper("cookie");
    }

    function _get_cart_cookie()
    {
        $cookie_get = get_cookie(MFW_CART_COOKIE);
        return unserialize($cookie_get) ? unserialize($cookie_get) : array();
    }

    function _set_cart_cookie($str)
    {
        delete_cookie(MFW_CART_COOKIE);
        $cookie = array(
            "name" => MFW_CART_COOKIE,
            "value" => $str,
            'expire' => 604800 //7 ngay
        );
        set_cookie($cookie);
    }

    function _clear_cart_cookie()
    {
        delete_cookie(MFW_CART_COOKIE);
    }

    /**
     * get product in cart
     *
     * @param array $products_in_cart
     * @return array|mixed
     */
    function _get_product_in_cart($products_in_cart = [])
    {
        $this->_load_data_from('product');

        if (!$products_in_cart) {
            $products_in_cart = $this->_get_cart_cookie();
        }

//        print_r($products_incart);

        $query = "";
        if ($products_in_cart) {
            $count = 0;
            foreach ($products_in_cart as $key => $value) {
                $query .= $count == 0 ? 'id in(' . $key : $key;
                $query .= ($count < count($products_in_cart) - 1) ? "," : ")";
                $count++;
            }
        }

        if ($query) {
            $query_arr = array(
                'query' => $query
            );
            $this->_set_query($query_arr);
            $products = $this->_model->_get('', '', 'product_type,id,name,image,category_slug,price_2,price_1');
        } else {
            $products = array();
        }
//        echo $this->_last_query();
        foreach ($products as $key => $value) {
            $products[$key]->soluong = $products_in_cart[$value->id];
        }

        return $products;
    }

//    function ajax_reload_cart()
//    {
//        $user = $this->get_usersession();
//        $emails = explode("@", $user['user_email']);
//        $vars['user_id'] = $user['user_id'];
//        $vars['user_fullname'] = $user['user_fullname'] ? $user['user_fullname'] : $emails[0];
//
//        //cart
//
//        $vars['products'] = $this->_get_product_in_cart();
//        //end cart
//
//        $this->load_site_setting();
//        $vars['logo'] = $this->_logo;
//
//        $this->load->vars($vars);
//        $this->load->view("frontend/{$this->_theme}/ajax/cart");
//    }

    /**
     * @return array|mixed
     */
    function add_to_cart()
    {
        $product_id = $this->_input_post("product_id");
        $number = $this->_input_post("number");

        $number = $number > 0 ? $number : 1;

        if ($product_id) {
            //set cookie
            $arr = $this->_get_cart_cookie();
//            print_r($arr);
            if ($arr && isset($arr[$product_id])) {
                // them san pham
                $arr[$product_id] += $number;
            } else {
                // 1 san pham
                $arr[$product_id] = $number;
            }

//            $arr[$product_id] = $arr[$product_id] > 20 ? 20 : $arr[$product_id];

            $this->_set_cart_cookie(serialize($arr));
//            echo 'ok';
            return $this->_get_product_in_cart($arr);
        } else {
            echo "invalid!";
            return false;
        }
    }

    function remove_from_cart($product_id)
    {
        if ($product_id) {
            //set cookie
            $arr = $this->_get_cart_cookie();
            if (key_exists($product_id, $arr)) {
                unset($arr[$product_id]);
            }
            $this->_set_cart_cookie(serialize($arr));
            if ($arr) {
                return $this->_get_product_in_cart($arr);
            } else return array();
        } else
            return false;
    }
}