<?php defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH."object/Theme_object.php";

class MBackendController extends MController
{
    protected $_title, $_primary_name, $_default_name;

    protected $_sess_required = true;

    //custom theme
    protected $_theme, $_theme_tmp, $_theme_ajax;

    //permission
    protected $_permission_controller_name;
    protected $_permission_view = 'read', $_permission_edit = 'edit', $_permission_delete = 'delete', $_permission_category, $_permission_url;

    protected $_module_title, $_module_desc;

    //after 17.2.2017 (apply for oneui)
    protected $_search_tip = 'Từ khóa..';
    protected $_paging_enable = true;
    protected $_page_filter = false;
    private $_is_theme_index = 1;
    private $_is_theme_custom = false;
    private $_is_theme_contain_form = false;
    private $_page_action_enabled = 1;

    function __construct()
    {
        // Construct our parent class
        parent::__construct();

        $this->load->helper('mfw_url');
        $this->load->helper('file');
        $this->load->helper('mfw_admin');

        $this->_folder = "";

        $this->db->cache_off();
        $this->load->library('session');

        new Theme_object();
        $this->_theme = Theme_object::$name;

        //config
        $this->_module_vars['seo_title'] = "Quản trị hệ thống";

        $this->_theme_tmp = $this->_folder . '/' . $this->_theme . '/template/';
        $this->_theme_ajax = $this->_folder . '/' . $this->_theme . '/ajax/';
        $this->_theme_view = $this->_folder . '/' . $this->_theme . '/';
        $this->_theme_assets = "/assets/admin/{$this->_theme}/";

        //session
//        $session_arr = $this->config->item('session');
        $this->_sess_userid = $this->session->userdata(BASE_SESSION_ADMIN_ID);
        $this->_sess_group_id = $this->session->userdata(BASE_SESSION_ADMIN_GROUPID);
        $this->_is_root = $this->session->userdata(BASE_SESSION_ADMIN_IS_ROOT);

        if ($this->_sess_required && !$this->_sess_userid && !$this->_sess_group_id) {
            $this->session->set_flashdata("back_url", $_SERVER['REDIRECT_URL']);
            $this->session->sess_destroy();
            redirect(BASE_ROUTE_ADMIN_LOGIN);
            exit();
        }

        $this->_permission_category = $this->_page_folder = $this->router->fetch_class();

        if ($this->_permission_controller_name) {
            $this->_permission_url = 'quan-tri/' . get_urltitle($this->_permission_controller_name);

            if (ENVIRONMENT == 'development') {
                //set access permission (access controller - ex: news, product)
                $this->_set_access_permission($this->_permission_category);
            }

            //check permission view
            if (!$this->check_permission($this->_permission_view, 'auto')) {
                //if no permission
                $this->no_permission();
            }
        }

        $this->load->helper('mfw_paging');

    }

    /**
     * show message no permission
     */
    private function no_permission()
    {
        //echo "";
        $data['msg'] = "<p style='color:red'>Bạn chưa được cấp quyền truy cập chức năng này!</p>";
        $data['url_redirect'] = base_url(BASE_ROUTE_ADMIN_LOGIN);
        echo $this->load->view("admin/base/thongbao", $data, true);
        exit();
    }

    /**
     * set access permission by category
     * @param $category
     */
    private function _set_access_permission($category)
    {

        //check permission from db
        $this->_load_data_from(BASE_TABLE_ADMIN_ROLE);
        $where = array('category' => $category);

        //check 3 role (read/edit/delete)
        if ($this->_model->_count($where) != -3) {

            $insert_read['name'] = "Xem";
            $insert_read['key'] = $this->_permission_view;
            $insert_read['category'] = $category;
            $insert_read['category_name'] = $this->_permission_controller_name;

            $this->_model->_insert_duplicate($insert_read);

            $insert_edit['name'] = "Sửa";
            $insert_edit['key'] = $this->_permission_edit;
            $insert_edit['category'] = $category;
            $insert_edit['category_name'] = $this->_permission_controller_name;
            $this->_model->_insert_duplicate($insert_edit);

            $insert_delete['name'] = "Xóa";
            $insert_delete['key'] = $this->_permission_delete;
            $insert_delete['category'] = $category;
            $insert_delete['category_name'] = $this->_permission_controller_name;
            $this->_model->_insert_duplicate($insert_delete);
        }

        //check admin menu
        $this->_load_data_from(BASE_TABLE_ADMIN_MENU);
        $menu_where = array(
            'category' => $category
        );

        $new_menu = array(
            'name' => $this->_permission_controller_name,
            //'icon' => 'fa fa-circle-o text-red',
            'url' => $this->_permission_url
        );

        if ($this->_model->_count($menu_where) != 1) {
            $this->_model->_delete($menu_where);
            $new_menu['category'] = $category;
            $this->_model->_insert($new_menu);
        } else {
            $this->_model->_update($new_menu, $menu_where);
        }

        $this->update_routes();
        $this->config_menu();
    }

    protected function update_routes()
    {
        $this->_load_data_from(BASE_TABLE_ADMIN_MENU);

        $this->_model->delete_invalid_admin_menu();

        $menu = $this->_model->_get(array('valid' => 1, 'is_group' => NULL));
        $modules = 'admin/';
        $routes = "<?php \n";
        foreach ($menu as $item) {
            $routes .= "\$route['" . $item->url . "']='{$modules}{$item->category}';\n";
            $routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_DETAIL . "']='{$modules}{$item->category}/detail';\n";
            $routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_UPDATE . "']='{$modules}{$item->category}/update';\n";
            $routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_UPDATE_POSITION . "']='{$modules}{$item->category}/ajax_position';\n";
            $routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_VALID . "']='{$modules}{$item->category}/ajax_valid';\n";
            $routes .= "\$route['" . $item->url . "/" . BASE_ROUTE_ADMIN_XXX_DELETE . "']='{$modules}{$item->category}/ajax_delete';\n";
        }
        if (!write_file(APPPATH . 'config/routes_mfw_db.php', $routes)) {
            $this->debug_inline("update_routes", "Unable to write file: routes_mfw_db");
        } else {
            $this->debug_inline("update_routes", "File is written: routes_mfw_db");
        }
    }

    /**
     * set session for permission check
     * @param Admin_object $admin_object
     */
    protected function _set_permission_session(Admin_object $admin_object)
    {
        $sess_arr = array(
            BASE_SESSION_ADMIN_ID => $admin_object->id,
            BASE_SESSION_ADMIN_GROUPID => $admin_object->group_id,
            BASE_SESSION_ADMIN_IS_ROOT => $admin_object->is_root,
            BASE_SESSION_ADMIN_USERNAME => $admin_object->username,
            BASE_SESSION_ADMIN_FULLNAME => $admin_object->fullname,
            BASE_SESSION_ADMIN_IMG => $admin_object->img
        );
        $this->session->set_userdata($sess_arr);

        $this->_sess_userid = $this->session->userdata(BASE_SESSION_ADMIN_ID);
        $this->_sess_group_id = $this->session->userdata(BASE_SESSION_ADMIN_GROUPID);
        $this->_is_root = $this->session->userdata(BASE_SESSION_ADMIN_IS_ROOT);
    }

    /**
     * check controller permission
     *
     * @param $permission_code
     * @param string $type
     * @return bool
     */
    protected function check_permission($permission_code, $type = '')
    {
        if (BASE_CONF_PERMISSION_REQUIRED && $this->_is_root != 1) {
            if ($permission_code && $this->_permission_controller_name) {
                //get user's permission
                if ($this->_sess_group_id) {
                    $permission_arr = $this->_model->get_user_permission($this->_sess_group_id);
                    //$this->debug_last_query() . '<br/>';
                    foreach ($permission_arr as $item) {
                        $permission[] = $item->category . '_' . $item->key;
                    }
                    return in_array($this->_permission_category . '_' . $permission_code, $permission);
                }
                return false;
            } else {
                if ($type == 'auto') {
                    $this->no_permission();
                }
                return false;
            }
        } else return true;
    }

    function _init_page()
    {
        $this->_limit = BASE_CONF_LIMIT_BACKEND;
        $this->_base_init_page();
        $this->_page_url = base_url($this->_permission_url) . "?per_size=" . $this->_limit;
    }

    function _get_page_info($total)
    {
        return $this->_base_get_page_info($total);
    }

    /**
     * load template
     * @param string $func_name file_view
     * @param string $view custom_view
     */
    function _load_tmp($func_name = '', $view = '')
    {
        $this->_module_vars['module_title'] = $this->_module_title ? $this->_module_title : ucfirst_utf8($this->_permission_controller_name);
        $this->_module_vars['module_desc'] = $this->_module_desc;

        //keyword search
        $this->_module_vars['keyword'] = $this->_search;
        //page
        $this->_module_vars['page'] = $this->_page;
        //limit per page
        $this->_module_vars['page_size'] = $this->_limit;
        //search
        $this->_module_vars['paging_enable'] = $this->_paging_enable;

        $this->_module_vars['role'] = $this->_permission_category;

        $this->_module_vars['permission_edit'] = $this->_permission_controller_name ? ($this->check_permission($this->_permission_edit) ? true : false) : true;
        $this->_module_vars['permission_delete'] = $this->_permission_controller_name ? ($this->check_permission($this->_permission_delete) ? true : false) : true;
        $this->_module_vars['permission_url'] = $this->_permission_url;

        $this->_module_vars['session_id'] = $this->_sess_userid;
        $this->_module_vars['session_group_id'] = $this->_sess_group_id;
        $this->_module_vars['session_is_root'] = $this->_is_root;
        $this->_module_vars['session_username'] = $this->session->userdata(BASE_SESSION_ADMIN_USERNAME);
        $this->_module_vars['session_fullname'] = $this->session->userdata(BASE_SESSION_ADMIN_FULLNAME);
        $this->_module_vars['session_img'] = $this->session->userdata(BASE_SESSION_ADMIN_IMG);

        //after 15.2.2017 - oneui

        $this->_content = $func_name ? $func_name : $this->_content;
        //default page_action_enabled for update page
        if ($func_name == 'update') {
            if ($this->_page_action_enabled === 1) $this->_page_action_enabled = false;
            if ($this->_is_theme_index === 1) {
                $this->_is_theme_contain_form = true;
                $this->_is_theme_index = false;
            }
        }

        $this->_module_vars['is_theme_index'] = $this->_is_theme_index;
        $this->_module_vars['is_theme_custom'] = $this->_is_theme_custom;
        $this->_module_vars['them_contain_form'] = $this->_is_theme_contain_form;
        $this->_module_vars['page_action_enabled'] = $this->_page_action_enabled;
        $this->_module_vars['search_tip'] = $this->_search_tip;

//		$this->_module_vars['page_action_btn'] = $this->_page_action_button == "page_action_btn" ? $this->_theme_tmp . "/" . $this->_page_action_button : $this->_page_action_button;

        //custom loc du lieu
//		if ($this->_page_filter) $this->_module_vars['page_filter'] = $this->_folder . '/' . $this->_theme . '/' . $this->_page_filter;

        $this->load->helper('mfw_template');

        $this->_base_load_tmp($view);
    }

    /**
     * @param $data
     * @param $where
     * @param string $db_table
     * @param boolean $json_resp
     * @return int|mixed
     */
    function update_to_db($data, $where = array(), $db_table = '', $json_resp = true)
    {
//        $id_for_update = 0;
        $json_msg = 'Dữ liệu không hợp lệ';
        //user has permission edit
        if ($this->check_permission($this->_permission_edit)) {
            if ($db_table || $this->_current_table) {
                $this->_current_table = $db_table ? $db_table : $this->_current_table;
                $this->_load_data_from($this->_current_table);
            }
            if ($where) {
                $id_for_update = $where['id'];
                $affected_row = $this->_model->_update($data, $where);
                if (!$affected_row) {
                    $affected_row = $this->_model->_count($where);
                } else {
                    $this->admin_log(BASE_ADMIN_ACTION_UPDATE, json_encode(array_merge($where, $data)), $where);
                }

                if ($affected_row) {
                    $json_msg = "Cập nhật dữ liệu thành công!";
                }
            } else {
                $affected_row = $this->_model->_insert($data);
                $id_for_update = $affected_row;
                if ($affected_row) {
                    $json_msg = "Thêm mới dữ liệu thành công!";
                    $this->admin_log(BASE_ADMIN_ACTION_INSERT, json_encode($data));
                } else $affected_row = 0;
            }


            $this->delete_cache();
            if ($json_resp) {
                $result = array(
                    'error' => $affected_row ? 0 : 1,
                    'msg' => $json_msg,
                    'id' => $id_for_update
                );
                return json_encode($result);
            } else return $affected_row;
        } else if ($json_resp) {
            $result = array(
                'error' => 0,
                'msg' => 'Lỗi không xác định!',
            );
            return json_encode($result);
        }
        return 0;
    }

    /**
     * @param $id
     * @param $data
     * @param $view
     * @return mixed
     */
    function _ajax_update($id, $data, $view = "", $data_more = "")
    {

        //user has permission edit
        if ($this->check_permission($this->_permission_edit)) {
            if ($id > 0) {
                $effected = $this->_model->_update($data, array('id' => $id));
                //$this->admin_log(BASE_ADMIN_ACTION_UPDATE);
            } else {
                $id = $this->_model->_insert($data);
                //$this->admin_log(BASE_ADMIN_ACTION_INSERT, json_encode($data));
            }

            $this->delete_cache();

            if ($view) {
                $ls = $this->_model->_get_row(array('id' => $id));
                foreach ($data_more as $key => $val) {
                    $ls->$key = $val;
                }
                //array_merge($ls, $data_more);
                $this->_load_updated_views($ls, $view);
            } else
                return $id;
        }
        return 0;
    }

    function _load_updated_views($ls, $view)
    {
        $vars['p'] = $this->input->post('in_p');
        $vars['ls'] = $ls;
        $this->load->vars($vars);
//        print_r($ls);
        $this->load->view($this->_theme_ajax . $view);
    }

    /**
     * xoa du lieu
     */
    function ajax_delete()
    {
        //user has permission delete
        if ($this->check_permission($this->_permission_delete)) {
            parent::ajax_delete();

            //log
            $this->admin_log(BASE_ADMIN_ACTION_DELETE);
        }
    }

    /**
     * kha dung
     */
    function ajax_valid()
    {
        if ($this->check_permission($this->_permission_edit)) {
            parent::ajax_valid();
        }
    }

    /**
     * thay doi vi tri
     */
    function ajax_position()
    {
        if ($this->check_permission($this->_permission_edit)) {
            parent::ajax_position();
        }
    }

    //=================== tmp display config =======================

    /**
     * using theme index tmp
     * @param bool $bool
     */
    function use_theme_index($bool = true)
    {
        $this->_is_theme_index = $bool;
        $this->_is_theme_contain_form = !$bool;
    }

    /**
     * using theme index tmp
     */
    function use_theme_custom()
    {
        $this->_is_theme_index = false;
        $this->_page_action_enabled = false;
        $this->_is_theme_custom = 1;
    }

    function them_contain_form()
    {
        $this->_is_theme_contain_form = true;
    }

    /**
     * hidden paging in page
     */
    function disable_paging()
    {
        $this->_paging_enable = false;
    }

    //hidden action button in page
    function disable_pagin_action_button()
    {
        $this->_page_action_enabled = false;
    }

    //show action button in page
    function enable_pagin_action_button()
    {
        $this->_page_action_enabled = true;
    }

    // ==================================================================

    /**
     * log admin action
     * @param $action_type
     * @param $content_new
     * @param $where
     */
    function admin_log($action_type, $content_new = "", $where = '')
    {
        if (BASE_CONF_LOG_ADMIN_ACTIVITY) {
            $content_raw = "";
            if ($where) {
                $content_raw = json_encode($this->_model->_get_row($where));
            }

            $data = array(
                'admin_id' => $this->_sess_userid,
                'action' => $action_type,
                'category' => $this->_permission_controller_name,
                'table' => $this->_model->table_name,
                'content' => $content_raw,
                'content_new' => $content_new
            );
            $this->_load_data_from(BASE_TABLE_ADMIN_LOG);
            $this->_model->_insert($data);
        }
    }

    function error($page = "404")
    {
        if ($page == "404") redirect(BASE_PAGE_NOT_FOUND);
    }

    /**
     * create file data (admin menu)
     */
    private function config_menu()
    {
        if ($this->_is_root) {//check: account is root
            if (!file_exists(APPPATH . BASE_CONF_FILE_MENU_ROOT)) {
                $this->_cf_admin_menu_root();
            }
        } else if (!file_exists(APPPATH . BASE_CONF_FILE_MENUGROUP_FOLDER . $this->_sess_group_id)) {
            //re-write file menugroup
            if ($this->_sess_group_id) $this->_cf_admin_menu_group($this->_sess_group_id);
        }
    }

    /**
     * config admin menu root
     */
    private function _cf_admin_menu_root()
    {
        $this->_load_data_from(BASE_TABLE_ADMIN_MENUHEAD);
        $head = $this->_model->_get(array('valid' => 1), array('position' => 'asc'));

        $head = $head ? $head : ["LABEL"];

        $this->_load_data_from(BASE_TABLE_ADMIN_MENU);
        foreach ($head as $index => $item) {
            $menu = $this->_model->_get(array('valid' => 1, 'menuhead_id' => $item->id), array('position' => 'asc'));
            if ($menu) {
                $group = $menu;
                foreach ($menu as $i => $v) {
                    $category_group = $v->category;
                    $group[$i]->is_group = $v->is_group == 1 ? true : false;
                    if ($group[$i]->is_group) {
                        $menux = $this->_model->_get(array('menugroup_id' => $v->id, 'valid' => 1), array('position' => 'asc'));
                        foreach ($menux as $a => $b) {
                            $category_group .= $b->category;
                            if ($a < count($menux) - 1) {
                                $category_group .= ",";
                            }
                        }
                        $group[$i]->menu = $menux;
                    }
                    $group[$i]->role = $category_group;
                }
                $head[$index]->menu = $group;
            }
        }

        $str = json_encode($head);

        if (!write_file(BASE_CONF_FILE_MENU_ROOT, $str)) {
            $this->debug_inline("_cf_admin_menu_root", "Unable to write the file: root menu");
        } else {
            $this->debug_inline("_cf_admin_menu_root", "File menu root is written");
        }
    }

    /**
     * config admin menu for group
     * @param $group_id
     */
    function _cf_admin_menu_group($group_id)
    {
        $tmp = json_decode(file_get_contents(BASE_CONF_FILE_MENU_ROOT));
//		echo $group_id;

        $group_permission = $this->group_permission($group_id);

//		print_r($group_permission);
        foreach ($tmp as $i0 => $adm) {
            $show_this = false;

            foreach ($adm->menu as $i1 => $adm_1) {

                if ($adm_1->is_group) {
                    $show_this_1 = false;
                    foreach ($adm_1->menu as $i2 => $adm_2) {
                        if (in_array($adm_2->id, $group_permission)) {
                            $show_this = true;
                            $show_this_1 = true;
                        } else {
                            unset($adm_1->menu[$i2]);
                        }
                    }
                    if (!$show_this_1) unset($adm->menu[$i1]);
                } else {
                    if (in_array($adm_1->id, $group_permission)) {
                        $show_this = true;
                    } else {
                        unset($adm->menu[$i1]);
                    }
                }
            }
            if (!$show_this) unset($tmp[$i0]);
        }

//		print_r($tmp);

        if (!write_file(BASE_CONF_FILE_MENUGROUP_FOLDER . $group_id, json_encode($tmp))) {
            $this->debug_inline("_cf_admin_menu_group", "Unable to write the file: " . $group_id);
        } else {
            $this->debug_inline("_cf_admin_menu_group", "File written: " . $group_id);
        }
    }

    function group_permission($group_id)
    {
        $count = 0;
        $this->load->model('model_permission_group');
        $model = new Model_permission_group();
        $query = "valid=1 and category in (";

        foreach ($model->get_permission_read($group_id) as $item) {
            $query .= "'" . $item->category . "',";
            $count++;
        }
        $str = [];
        if ($count > 0) {

            //permission cua group
            $this->_load_data_from(BASE_TABLE_ADMIN_MENU);
            $this->_model->_query = substr($query, 0, -1) . ")";

            $menu = $this->_model->_get(
                '',
                array('position' => 'asc', 'id' => 'asc'),
                'id'
            );

            foreach ($menu as $item) {
                $str[] = $item->id;
            }
        }
        return $str;
    }


    /**
     *
     */
    function update_frontend_routes()
    {
        $this->_load_data_from("slug");
        $routes = "<?php \n";
        foreach ($this->_model->_get() as $item) {
            if($item->type=='page') {
                $routes .= "\$route['" . strtolower($item->name) . "']='" . strtolower($item->route) . "';\n";
            }else {
                $routes .= "\$route['" . strtolower($item->name) . "']='" . strtolower($item->route) . "';\n";
                $routes .= "\$route['" . strtolower($item->name) . "/(:any)']='public/" . strtolower($item->type) . "/content/$1';\n";
            }
        }

        if (!write_file(APPPATH . 'config/routes_mfw_front_db.php', $routes)) {
//			echo 'Unable to write the file';
            echo '<p class="text-danger">Unable to write ROUTES file!</p>';
        } else {
//			echo 'File routes_go_db is written!';
            echo "<p class=\"text-success\">File ROUTES is written!</p>";
        }
    }
}