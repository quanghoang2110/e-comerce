<?php defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Class MController
 * base class for controller (include admin & frontend)
 * @author quanghv
 */
class MController extends MX_Controller
{
    protected $_sess_userid, $_sess_group_id, $_is_root;
    protected $_module_vars, $_content;//, $_module_sub;
    protected $_page_folder, $_page, $_limit, $_search, $_total, $_page_url;

    protected $_folder = "", $_folder_view = "";

    //custom theme
    protected $_theme, $_theme_tmp, $_theme_view;

    protected $session_conf;
    protected $_model;

    //
    protected $_current_table = "";

    //modules
    protected $_module, $_module_view;

    function __construct()
    {
        parent::__construct();
        if (BASE_AUTOLOAD_DATABASE) {
            $this->load->database();
        }
        $this->_model = new MModel();
        $this->load->helper('mfw_datetime');
        $this->load->helper('mfw_asset');
        $this->load->helper('mfw_module');
    }

    /*
     * load data from table
     */
    function _load_data_from($table_name)
    {
        $this->_current_table = $table_name;
        $this->_model->reset_all();
        $this->_model->table_name = $table_name;
    }

    /**
     * @param $query_arr
     */
    function _set_query($query_arr)
    {
        $this->_model->_query = isset($query_arr['query']) ? $query_arr['query'] : '';
        $this->_model->_search_keyword = isset($query_arr['keyword']) ? $query_arr['keyword'] : '';
        $this->_model->_search_row = isset($query_arr['search_row']) ? $query_arr['search_row'] : '';
    }

    /**
     * init page
     */
    function _base_init_page()
    {
        $this->_page = (int)$this->input->get("per_page", true);
        $limit = (int)$this->input->get("per_size", true);
        $this->_search = str_replace('[removed]', '', trim($this->input->get("search", true)));
        $this->_page = $this->_page > 0 ? $this->_page : 1;
        $this->_limit = $limit ? $limit : $this->_limit;

        $this->load->helper('mfw_paging');
    }

    /**
     * page info
     * @param $total
     * @return string
     */
    function _base_get_page_info($total)
    {
        $this->_page = $this->_page > 0 ? $this->_page : 1;
        $this->_total = $total;
        $item_from = (($this->_page - 1) * $this->_limit + 1) <= $total ? (($this->_page - 1) * $this->_limit + 1) : 0;
        $item_to = ($this->_page * $this->_limit) < $total ? ($this->_page * $this->_limit) : $total;
        return "Hiển thị từ {$item_from} tới {$item_to} trong {$total} kết quả";
    }

    /**
     * load tmp
     * @param string $view
     */
    function _base_load_tmp($view = '')
    {
        $vars['theme'] = $this->_theme;
        $vars['theme_template'] = $this->_theme_tmp;
        $vars['theme_view'] = $this->_theme_view;

        $page_folder = $this->_folder . '/' . $this->_theme . ($this->_page_folder ? '/' . $this->_page_folder : '');
        $vars['page_folder'] = $page_folder;

        $vars['module'] = $this->_module;
//        $vars['module_sub'] = $this->_module_sub;
        $vars["content"] = $page_folder . '/' . $this->_content;

        $vars['sess_userid'] = $this->_sess_userid;

        if ($this->_module_vars) {
            $vars = array_merge($vars, $this->_module_vars);
        }

        $this->load->vars($vars);
        $view = $view ? $view : $this->_folder . '/base/base_tmp';
        $this->load->view($view);
    }

    /**
     * ajax base =======================================================================================================
     */

    /**
     * update or insert if not exists
     * @param $id
     * @param $data
     * @return $id
     */
    function _ajax_update($id, $data)
    {
        if ($id > 0) {
            $this->_model->_update($data, array('id' => $id));
        } else {
            $id = $this->_model->_insert($data);
        }

        return $id;
    }

    /**
     * delete record by id
     * @return int
     */
    function ajax_delete()
    {
        if ($this->_model) {
//			sleep(3);
            $id = $this->input->post("id", true);
            if ($id) {
                $this->_model->_delete(array('id' => $id));
            }
            echo 1;
        }
    }

    /**
     * update valid by id
     * @return int
     */
    function ajax_valid()
    {
        $id = $this->input->post("id", true);
        $valid = $this->input->post("valid", true);
        $valid = $valid == 1 ? 0 : 1;

        switch ($this->input->post('type')) {
            case BASE_DATA_TYPE_HOME:
                $data = array('is_home' => $valid);
                break;
            case 'is_menu':
                $data = array('is_menu' => $valid);
                break;
            case BASE_DATA_TYPE_HOT:
                $data = array('is_hot' => $valid);
                break;
            default:
                $data = array('valid' => $valid);
                break;
        }

        if ($id > 0) {
            echo $this->_ajax_update($id, $data);
        }
    }

    /**
     * update position by id
     * @return int
     */
    function ajax_position()
    {
        $id = $this->input->post("id", true);
        $position = $this->input->post("position", true);

        if ($id > 0) {
            echo $this->_ajax_update($id, array('position' => $position));
        }
    }

    function ajax_crop_image()
    {
        $post = $this->input->post();
        $error = 1;
        $msg = "Thông tin truyền lên không hợp lệ!";

        if ($post) {
//			print_r($post);
            $config['image_library'] = 'gd2';
            $img = $post['data_img'];
            $config['source_image'] = "./" . $post['data_img'];
            $new_img = dirname($img) . "/" . time() . "_" . basename($img);
            $config['new_image'] = ".{$new_img}";
            $config['x_axis'] = $post['data_x'];
            $config['y_axis'] = $post['data_y'];
            $config['maintain_ratio'] = FALSE;
            $config['width'] = $post['data_width'];
            $config['height'] = $post['data_height'];

            $this->load->library('image_lib');
            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->crop()) {
                $msg = $this->image_lib->display_errors();
            } else {
                $error = 0;
                $msg = $new_img;
            }
        }
        echo json_encode(array('error' => $error, "msg" => $msg));
    }

    /**
     * form validation
     * @param array $config
     * @return mixed
     */
    function _validation($config = array())
    {
        $this->load->library('form_validation');

        foreach ($config as $item_config) {
            if (isset($item_config['errors'])) {
                $this->form_validation->set_rules(
                    $item_config["field"],
                    $item_config["label"],
                    $item_config["rules"],
                    $item_config["errors"]
                );
            } else $this->form_validation->set_rules(
                $item_config["field"],
                $item_config["label"],
                $item_config["rules"]
            );
        }

        return $this->form_validation->run();
    }

    function _last_query()
    {
        echo $this->db->last_query();
    }

    /**
     * @param $rules
     * @return bool|string
     */
    function _run_form_validation($rules, $delimiter_open = '', $delimiter_close = '')
    {
        $this->load->library('form_validation');
//        $this->form_validation->set_error_delimiters($delimiter_open, $delimiter_close);
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $status = -1;
            $msg = validation_errors($delimiter_open, $delimiter_close);
            return $this->_ajax_response_msg($status, $msg);
        } else
            return true;
    }

    /**
     * @param $status
     * @param $msg
     * @return string
     */
    function _ajax_response_msg($status, $msg)
    {
        return json_encode(array('status' => $status, 'msg' => $msg));
    }

    /**
     * input get clear xss
     * @param $key
     * @return mixed
     */
    function _input_get($key)
    {
        return $this->input->get($key, true);
    }

    /**
     * input post clear xss
     * @param $key
     * @param $is_number
     * @return mixed
     */
    function _input_post($key, $is_number = false)
    {
        if ($is_number) return (int)$this->input->post($key, true);
        else return $this->input->post($key, true);
    }

    /**
     * gui email xac nhan toi don hang
     *
     * @param $to_email
     * @param $message
     * @param string $code
     */
    function send_mail_confirm($to_email, $message, $code = 'LC')
    {
        $this->config->load('email');
        $from_email = $this->config->item('smtp_user');
        $user_agent = $this->config->item('useragent');

        $this->load->library("email");
        $this->email->from($from_email, $user_agent);
        $this->email->to($to_email);
        $this->email->subject('Xác nhận đơn hàng #' . $code);
//        $config = file_get_contents(BASE_CONF_FILE_SITE_SETTING);
//        if ($config) {
//            $setting = new Setting_object(json_decode($config));
//            $emails = explode(PHP_EOL, $setting->support_email);
//            $cc_mail = '';
//            foreach ($emails as $index => $item) {
//                $cc_mail .= $item;
//                if ($index < count($emails) - 1) {
//                    $cc_mail .= ",";
//                }
//            }
//            if ($cc_mail) {
//                $this->email->bcc($cc_mail);
//            }
//        }
        $this->email->message($message);
        $this->email->send();
//        echo $this->email->print_debugger();
    }

    /**
     * cart confirm to shop
     * @param $message
     * @param string $code
     */
    function send_mail_confirm_shop($message, $code = 'LC')
    {
        $this->config->load('email');
        $from_email = $this->config->item('smtp_user');
        $user_agent = $this->config->item('useragent');

        $this->load->library("email");
        $this->email->from($from_email, $user_agent);
        $this->email->subject('Có đơn hàng mới #' . $code);
        $config = file_get_contents(BASE_CONF_FILE_SITE_SETTING);
        if ($config) {
            $setting = new Setting_object(json_decode($config));
            $emails = explode(PHP_EOL, $setting->support_email);
            $shop_mail = '';
            foreach ($emails as $index => $item) {
                $shop_mail .= $item;
                if ($index < count($emails) - 1) {
                    $shop_mail .= ",";
                }
            }
            if ($shop_mail) {
                $this->email->to($shop_mail);
                $this->email->message($message);
                $this->email->send();
            }
        }
//        echo $this->email->print_debugger();
    }

    /**
     * gui email toi users
     *
     * @param $users
     * @param $subject
     * @param $message
     */
    function send_email_to($users, $subject, $message)
    {
        $this->config->load('email');
        $from_email = $this->config->item('smtp_user');
        $user_agent = $this->config->item('useragent');

        $this->load->library("email");
        $this->email->from($from_email, $user_agent);
        $this->email->to($users);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
//        echo $this->email->print_debugger();
    }



    /**
     * DEBUG
     */

    /**
     * @param bool $die
     */
    function debug_last_query($die = false)
    {
        echo $this->db->last_query();
        if ($die) die();
    }

    /**
     * @param $data
     * @param bool $die
     */
    function debug_data($data, $die = false)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        if ($die) die();
    }

    function delete_cache()
    {
        switch ($this->_model->table_name) {
            case 'product_type':
                $this->db->cache_delete(PUBLIC_ROUTE_AJAX_PRODUCT_TYPE, "index");
                break;
        }
    }

    function debug_inline($title, $msg)
    {
        if ($this->input->get('debug') !== null) {
            echo "<pre>";
            echo "<br/>---------------------------------------------<br/>";
            echo $title . "<br/>";
            if (is_array($msg) || is_object($msg)) {
                print_r($msg);
            } else {
                echo $msg;
            }
            echo "<br/>---------------------------------------------<br/>";
            echo "</pre>";
        }
    }
}