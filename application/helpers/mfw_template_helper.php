<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 2/23/2017
 * Time: 09:23
 */

/**
 * using for multiple level category
 *
 * @param $category
 * @param $level
 * @param $selected_ids
 */
function get_select_option_multiple_level($category, $level, $selected_ids)
{
	foreach ($category as $item):
		$str = "<option value='{$item->id}'";
		if (!is_array($selected_ids)) $selected_ids = array($selected_ids);
		$str .= in_array($item->id, $selected_ids) ? " selected" : "";
		$str .= ">";
		for ($i = 1; $i < $level; $i++)
			$str .= "....";
		$str .= $item->name;
		$str .= '</option >';
		echo $str;
		$level++;
		if (isset($item->sub)) get_select_option_multiple_level($item->sub, $level, $selected_ids);
		$level--;
	endforeach;
}

/**
 * convert array object to array(id=>names)
 *
 * @param $array
 * @return array
 */
function array_to_array_name($array)
{
	$arr_name = array();
	foreach ($array as $a) {
		$arr_name[$a->id] = $a->name;
	}
	return $arr_name;
}

