<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package CodeIgniter
 * @subpackage Helpers
 * @category Helpers
 * @author quanghv
 */

/**
 * ------------------------------------------------
 */

/**
 * Upload Image
 *
 * @access public
 * @param
 *            string
 * @param
 *            bool
 * @return string (image's name)
 */

if (!function_exists('upload_file')) {
    function upload_file($up_path, $type = '', $name = '')
    {
        $CI = &get_instance();
        $config ['upload_path'] = $up_path; /* NB! create this dir! */
        if (!$type)
            $type = 'doc|docm|DOCM|DOC|docx|DOCX|pdf|PDF|xlsx|XLSX|ppt|PPTX|gif|jpg|png|bmp|jpeg|GIF|JPG|PNG|BMP|JPEG|zip|rar|tar|ZIP|RAR|TAR';
        $config ['allowed_types'] = $type;
        $config ['max_size'] = '10000'; // max size 10MB
        /* Load the upload library */
        $CI->load->library('upload', $config);
        if (!$name)
            $name = 'attach';
        $upload = $CI->upload->do_upload($name);

        if (!$upload) {
            // echo "error";
            $array ['error'] = $CI->upload->display_errors();
            return $array;
        } else {
            // print_r($upload);
            $data = $CI->upload->data();
            // print_r($data);
            return $data ['file_name'];
        }
    }
}

if (!function_exists('upload_image')) {
    /**
     * upload image
     * @param $up_path
     * @param string $img_name
     * @param string $w_thumb : width thumb
     * @param string $h_thumb : height thumb
     * @param string $thumb : thumb name
     * @param bool|false $delete_raw : delete raw or not
     * @return mixed|string
     */
    function upload_image($up_path, $img_name = '', $w_thumb = '', $h_thumb = '', $thumb = "_thumb", $delete_raw = false)
    {
        $CI = &get_instance();
        $config ['upload_path'] = $up_path; /* NB! create this dir! */
        $config ['allowed_types'] = 'gif|jpg|png|bmp|jpeg|GIF|JPG|PNG|BMP|JPEG';
        $config ['max_size'] = '10000'; // 10MB
        $config ['max_width'] = '5000';
        $config ['max_height'] = '5000';
        $config ['encrypt_name'] = TRUE;
        /* Load the upload library */
        $CI->load->library('upload', $config);

        /* Create the config for image library */
        /* (pretty self-explanatory) */
        $configThumb = array();
        $configThumb ['image_library'] = 'gd2';
        $configThumb ['source_image'] = '';
        $configThumb ['create_thumb'] = TRUE;
        $configThumb ['thumb_marker'] = $thumb;
        $configThumb ['maintain_ratio'] = TRUE;

        /* Set the height and width or thumbs */
        /* Do not worry - CI is pretty smart in resizing */
        /* It will create the largest thumb that can fit in those dimensions */
        /* Thumbs will be saved in same upload dir but with a _thumb suffix */
        /* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
        if (!$w_thumb)
            $w_thumb = 100;
        $configThumb ['width'] = $w_thumb;
        if (!$h_thumb)
            $h_thumb = 100;
        $configThumb ['height'] = $h_thumb;
        /* Load the image library */
        $CI->load->library('image_lib');
        if (!$img_name)
            $img_name = 'image';
        /* Handle the file upload */
        $upload = $CI->upload->do_upload($img_name);
        if (!$upload) {
            $array['error'] = $CI->upload->display_errors();
            return $array;
        } else {
            $data = $CI->upload->data();
            //print_r ( $data );
            $uploadedFiles = $data;
            /* If the file is an image - create a thumbnail */
            $image = '';
            if ($data ['is_image'] == 1) {
                $configThumb ['source_image'] = $data ['full_path'];
                $CI->image_lib->initialize($configThumb);
                $CI->image_lib->resize();

                //		get image direct
                //		$image = $data ['file_name'];
                //		return $image;

                if ($delete_raw) {
                    if (file_exists($data ['full_path'])) {
                        unlink($data ['full_path']);
                    }
                }

                //get thumb image
                // store path to image from img folder to database
//                $raw_name = $data ['raw_name'];
//                $file_name = $data ['file_name'];
                $file_ext = $data ['file_ext'];
                $thumb_path = str_replace($file_ext, $thumb . $file_ext, $data ['file_name']);
                $image = $thumb_path;
            }
        }
        return $image;
    }
}

/**
 * Upload multiple Image
 *
 * @access public
 * @param
 *            string
 * @param
 *            bool
 * @return string (image's name)
 */
if (!function_exists('multiple_upload')) {
    /**
     * upload multiple image
     *
     * @param $up_path
     * @param string $img_name
     * @param string $prefix : prefix name follow after resize
     * @param $w_thumb : width thumb
     * @param $h_thumb : height thumb
     * @param $n_thumb : thumb name
     * @param bool|false $del_rawIMG : delete raw image or not
     * @param string $type : image type follow
     * @return array
     */
    function multiple_upload($up_path, $img_name = '', $prefix = "_w", $w_thumb, $h_thumb, $n_thumb, $del_rawIMG = false, $type = "")
    {
        $CI = &get_instance();

        $type = $type ? $type : 'gif|jpg|png|bmp|jpeg|GIF|JPG|PNG|BMP|JPEG';

        $config = array(
            'upload_path' => $up_path,
            'allowed_types' => $type,
            'max_size' => '10000', //10MB
            'max_width' => '5000',
            'max_height' => '5000',
            'encrypt_name' => TRUE
        );

        /* Load the upload library */
        $CI->load->library('upload');
        $CI->load->library('MyUpload', $config);

        /* Load the image library */
        $CI->load->library('image_lib');
        if (!$img_name)
            $img_name = 'image';
        /* Handle the file upload */
// 		echo $up_path;
        //print_r($_FILES[$img_name]);
        $uploaded = array();
        if (!$CI->myupload->do_multi_upload($img_name)) {
            $array ['error'] = $CI->myupload->display_errors();
            return $array;
        } else {
            $uploaded = $CI->myupload->get_multi_upload_data();
        }

        $images_uploaded = array();
        foreach ($uploaded as $data) {
            /* If the file is an image - create a thumbnail */
            $image = '';
            if ($data ['is_image'] == 1) {

                $configResize = array(
                    'image_library' => 'GD2',
                    'source_image' => $data ['full_path'],
                    'create_thumb' => TRUE,
                    'thumb_marker' => $prefix,
                    'maintain_ratio' => TRUE
                );

//                if ($data['image_width'] > IMG_WIDTH_MAX) {
//                    // resize image
//                    $configResize['width'] = IMG_WIDTH_MAX;
//                }
//                if ($data['image_height'] > IMG_HEIGHT_MAX) {
//                    $configResize['height'] = IMG_HEIGHT_MAX;
//                }
//                print_r($configResize);

                if ($data ['file_size'] > 100)
                    $configResize ['quality'] = '50';
                else
                    $configResize ['quality'] = '80';

                $CI->image_lib->initialize($configResize);
                if (!$CI->image_lib->resize()) {
                    echo $CI->image_lib->display_errors();
                }

                // create thumb
                $configThumb = array(
                    'image_library' => 'GD2',
                    'source_image' => $data ['full_path'],
                    'create_thumb' => TRUE,
                    'thumb_marker' => $n_thumb,
                    'maintain_ratio' => TRUE
                );

                if ($data ['image_width'] > $w_thumb) {
                    $configThumb['width'] = $w_thumb;
                }
                if ($data ['image_height'] > $h_thumb) {
                    $configThumb['height'] = $h_thumb;
                }

                $CI->image_lib->initialize($configThumb);
                $CI->image_lib->resize();

//                else
//                    $CI->image_lib->watermark();

                $raw_name = $data ['raw_name'];
                $file_name = $data ['file_name'];
                $file_ext = $data ['file_ext'];
                $thumb_path = $raw_name . $configThumb ['thumb_marker'] . $file_ext;
                $image = $up_path . $thumb_path;
                if ($del_rawIMG)
                    if (file_exists($data ['full_path']))
                        unlink($data ['full_path']);
            }
            $images_uploaded[] = $image;
        }
        return $images_uploaded;
    }

}