<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        quanghv
 */

/**
 * @param $url
 * @param $opt
 * @return string
 */
function get_asset_url($url, $opt = false)
{
    $fullUrl = $url;
    if (ASSET_FULL_URL) {
        $fullUrl = base_url($url);
    }

    if (!$opt) {
        echo $fullUrl;
    } else {
        return $fullUrl;
    }
}

/**
 * load asset css
 * @param $str
 */
function load_base_css($str)
{
    switch ($str) {
        case BASE_ASSET_ROBOTO_FONT:
            echo "<link rel=\"stylesheet\" href='" . get_asset_url('/assets/base/lib/roboto/font.roboto.css', true) . "'/>";
            break;
        case BASE_ASSET_BOOTSTRAP:
            break;
        case BASE_ASSET_MDB:
            echo "<link href='" . get_asset_url('/assets/base/lib/mdb-pro/css/bootstrap.min.css', true) . "' rel=\"stylesheet\">";
            echo "<link href='" . get_asset_url('/assets/base/lib/mdb-pro/css/mdb.min.css', 1) . "' rel=\"stylesheet\">";
            echo "<link href='" . get_asset_url('/assets/base/lib/font-awesome-4.6.3/css/font-awesome.min.css', 1) . "' rel=\"stylesheet\">";
            break;
    }
}

/**
 * @param $strs
 */
function load_css($strs)
{
    foreach ($strs as $css) {
        echo "<link rel='stylesheet' href='" . get_asset_url($css,1) . "' >";
    }
}

/**
 * @param $strs
 */
function load_js($strs)
{
    foreach ($strs as $js) {
        echo "<script type=\"text/javascript\" src='" . get_asset_url($js,1) . "'></script>";
    }
}

/**
 * load asset js
 * @param $str
 */
function load_base_js($str)
{
    switch ($str) {
        case BASE_ASSET_MDB:
            echo "<script type=\"text/javascript\" src='" . get_asset_url('/assets/base/lib/mdb-pro/js/jquery-2.2.3.min.js',1) . "'></script>";
            echo "<script type=\"text/javascript\" src='" . get_asset_url('/assets/base/lib/mdb-pro/js/tether.min.js',1) . "'></script>";
            echo "<script type=\"text/javascript\" src='" . get_asset_url('/assets/base/lib/mdb-pro/js/bootstrap.min.js',1) . "'></script>";
            echo "<script type=\"text/javascript\" src='" . get_asset_url('/assets/base/lib/mdb-pro/js/mdb.min.js',1) . "'></script>";
            break;
    }
}
