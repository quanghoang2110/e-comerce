<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        quanghv
 */
if (!function_exists('make_dirs')) {
    function make_dirs($path)
    {
        if (!is_dir($path)) {
            $str = explode("/", $path);
//            print_r($str);
            $ex_path = "";
            foreach ($str as $k => $p) {
                $ex_path .= $k == 0 ? $p : "/" . $p;
//                echo "<br/>";
                if (!is_dir($ex_path)) {
                    mkdir($ex_path);
                }
            }
        }
    }
}

if (!function_exists('remove_dirs')) {
    function remove_dirs($dir)
    {
        foreach (scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
    }
}