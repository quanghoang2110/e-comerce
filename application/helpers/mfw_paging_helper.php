<?php
if (!defined('BASEPATH'))
	exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        quanghv
 */
if (!function_exists('get_paging')) {
	function get_paging($url, $total, $limit)
	{
		$ci =& get_instance();
		$ci->load->library('pagination');
		$config['base_url'] = $url;
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;

		if (Theme_object::$name == THEME_ONEUI) {
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$page_tag_open = "<li class='paginate_button'>";
			$page_tag_close = "</li>";
			$config['num_tag_open'] = $page_tag_open;
			$config['num_tag_close'] = $page_tag_close;
			$config['cur_tag_open'] = '<li class="paginate_button active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
			$config['prev_tag_open'] = '<li class="paginate_button previous">';
			$config['prev_tag_close'] = $page_tag_close;
			$config['next_link'] = '<i class="fa fa-angle-right"></i>';
			$config['next_tag_open'] = '<li class="paginate_button next">';
			$config['next_tag_close'] = $page_tag_close;
			$config['num_links'] = 2;
			$config['use_page_numbers'] = TRUE;
			$config['page_query_string'] = TRUE;

			$config['first_link'] = 'Trang đầu';
			$config['first_tag_open'] = '<li class="paginate_button first">';
			$config['first_tag_close'] = $page_tag_close;
			$config['last_link'] = 'Trang cuối';
			$config['last_tag_open'] = '<li class="paginate_button last">';
			$config['last_tag_close'] = $page_tag_close;
		} else if (Theme_object::$name == THEME_ALT) {
			$config['full_tag_open'] = '<ul class="pagination float-sm-right">';
			$config['full_tag_close'] = '</ul>';
			$page_tag_open = "<li class='page-item'>";
			$page_tag_close = "</li>";
			$config['num_tag_open'] = $page_tag_open;
			$config['num_tag_close'] = $page_tag_close;
			$config['cur_tag_open'] = '<li class="active page-item"><a class="page-link" href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['prev_link'] = ' ‹‹ ';
			$config['prev_tag_open'] = '<li class="page-item prev">';
			$config['prev_tag_close'] = $page_tag_close;
			$config['next_link'] = ' ›› ';
			$config['next_tag_open'] = '<li class="page-item next">';
			$config['next_tag_close'] = $page_tag_close;
			$config['num_links'] = 2;
			$config['use_page_numbers'] = TRUE;
			$config['page_query_string'] = TRUE;

			$config['first_link'] = 'Trang đầu';
			$config['first_tag_open'] = '<li class="page-item prev"><a class="page-link">';
			$config['first_tag_close'] = $page_tag_close;
			$config['last_link'] = 'Trang cuối';
			$config['last_tag_open'] = '<li class="page-item prev"><a class="page-link">';
			$config['last_tag_close'] = $page_tag_close;
		} else if (Theme_object::$name == THEME_MATERIAL) {

			$config['full_tag_open'] = '<ul class="pagination float-sm-right">';
			$config['full_tag_close'] = '</ul>';
			$page_tag_open = "<li class='page-item'>";
			$page_tag_close = "</li>";
			$config['num_tag_open'] = $page_tag_open;
			$config['num_tag_close'] = $page_tag_close;
			$config['cur_tag_open'] = '<li class="active page-item"><a class="page-link" href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['prev_link'] = ' ‹‹ ';
			$config['prev_tag_open'] = '<li class="page-item prev">';
			$config['prev_tag_close'] = $page_tag_close;
			$config['next_link'] = ' ›› ';
			$config['next_tag_open'] = '<li class="page-item next">';
			$config['next_tag_close'] = $page_tag_close;
			$config['num_links'] = 2;
			$config['use_page_numbers'] = TRUE;
			$config['page_query_string'] = TRUE;

			$config['first_link'] = 'Trang đầu';
			$config['first_tag_open'] = '<li class="page-item prev"><a class="page-link">';
			$config['first_tag_close'] = $page_tag_close;
			$config['last_link'] = 'Trang cuối';
			$config['last_tag_open'] = '<li class="page-item prev"><a class="page-link">';
			$config['last_tag_close'] = $page_tag_close;
		}

		$ci->pagination->initialize($config);
		return $ci->pagination->create_links();
	}
}

if (!function_exists('get_public_paging')) {
	function get_public_paging($url, $total, $limit)
	{
		$ci =& get_instance();
		$ci->load->library('pagination');
		$config['base_url'] = $url;
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;

		$config['full_tag_open'] = '<ul class="pagination-list">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li><span class="page hover-focus-color">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li><a href="#" class="page current customBgColor">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_link'] = '← Trang trước';
		$config['prev_tag_open'] = '<li><span class="prev hover-focus-color">';
		$config['prev_tag_close'] = '</span></li>';
		$config['next_link'] = 'Trang sau →';
		$config['next_tag_open'] = '<li><span class="next hover-focus-color">';
		$config['next_tag_close'] = '</span></li>';
		$config['num_links'] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string'] = TRUE;

		$ci->pagination->initialize($config);
		return $ci->pagination->create_links();
	}
}

function get_mdb_paging($url, $total, $limit)
{

	$ci =& get_instance();
	$ci->load->library('pagination');
	$config['base_url'] = $url;
	$config['total_rows'] = $total;
	$config['per_page'] = $limit;
	$config['attributes'] = array('class' => 'page-link');

	$config['full_tag_open'] = '<nav><ul class="pagination">';

	$config['num_tag_open'] = '<li class="page-item">';
	$config['num_tag_close'] = '</li>';

	$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
	$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';


	$config['full_tag_close'] = '</ul></nav>';

	$config['prev_link'] = '<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>';
	$config['prev_tag_open'] = '<li class="page-item">';
	$config['prev_tag_close'] = '</li>';

	$config['next_link'] = '<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>';
	$config['next_tag_open'] = '<li class="page-item">';
	$config['next_tag_close'] = '</li>';

	$config['use_page_numbers'] = TRUE;
	$config['page_query_string'] = TRUE;

	$ci->pagination->initialize($config);
	return $ci->pagination->create_links();
}
