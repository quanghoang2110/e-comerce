<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * ss sendmail Helpers
 *
 * @package        application
 * @subpackage    Helpers
 * @category    Helpers
 * @link
 */
function send_email_common($subject, $view, $to_email, $data, $to_email_cc = null)
{
    $ci =& get_instance();

    $ci->config->load('email');
    $from_email = $ci->config->item('useragent');
    $user_agent = $ci->config->item('smtp_user');

    $ci->load->library("email");
    $ci->email->from($from_email, $user_agent);
    $ci->email->to($to_email);
    $ci->email->subject($subject);
    if ($to_email_cc) {
        $ci->email->cc($to_email_cc);
    }
    $message = $ci->load->view($view, $data, TRUE);
    $ci->email->message($message);

    if ($ci->email->send()) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * @param string $order_code
 * @param $data
 * @return bool
 */
function mail_confirm_order($data, $order_code = 'LC')
{
    $ci =& get_instance();

    $ci->config->load('email');
    $from_email = $ci->config->item('useragent');
    $user_agent = $ci->config->item('smtp_user');

    $ci->load->library("email");
    $ci->email->from($from_email, $user_agent);
    $ci->email->to('quanghoang2110@gmail.com');
    $ci->email->subject('Xác nhận đơn hàng ' . $order_code);
//    if ($to_email_cc) {
//        $ci->email->cc($to_email_cc);
//    }
    $message = $ci->load->view('mail_confirm', $data, TRUE);
    $ci->email->message($message);

    if ($ci->email->send()) {
        return TRUE;
    } else {
        return FALSE;
    }
}
/* End of file go2_sendemail_helper.php */
/* Location: ./application/helpers/ssacount_helper.php */