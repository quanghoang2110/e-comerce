<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

/**
 * for only datetime function!
 */

/**
 * tinh bai viet duoc tao cach hien tai bao lau
 * @author hungnm
 */
function calculate_time($time)
{

//	date_default_timezone_set('Asia/Ho_Chi_Minh');
	//echo $date= date('m-d-Y H:i:s') ;
	//echo '|'.date('m-d-Y H:i:s', strtotime($time)).'<br/>';

    $time_gmt7 = strtotime_gmt7($time);

    $ago = time() - $time_gmt7;

    $days = $ago / (60 * 60 * 24);
    $hours = $ago / (60 * 60);
    $mins = $ago / 60;

    if ($days < 1) {
        if ($hours >= 1) {
            $rs = (int)$hours . ' giờ trước';
        } else if ($mins >= 1) {
            $rs = (int)$mins . ' phút trước';
        } else {
            $rs = 'vài giây trước';
        }

    } else {

        if (date("Y", $time_gmt7) != date("Y")) {
            //ex: 20 tháng 11 năm 2015 lúc 12:01
//            $rs = date("d \\t\\h\\á\\n\\g m \\n\\ă\\m Y \\l\\ú\\c H:i", $time_gmt7);
            $rs = date("d \\t\\h\\á\\n\\g m \\n\\ă\\m Y", $time_gmt7);
        } else {
            //ex: 20 tháng 11 lúc 12:01
            $rs = date("d \\t\\h\\á\\n\\g m", $time_gmt7);
//            $rs = date("d \\t\\h\\á\\n\\g m \\l\\ú\\c H:i", $time_gmt7);
        }

//        if ($days >= 365) {
//            $rs = (int)($days / 365) . ' năm trước';
//        } else if ($days >= 30) {
//            $rs = (int)($days / 30) . ' tháng trước';
//        } else if ($days >= 7) {
//            $rs = (int)($days / 7) . ' tuần trước';
//        } else {
//            $rs = (int)$days . ' ngày trước';
//        }
//        $rs = "";
    }

    return $rs;
}

function strtotime_gmt7($time)
{
    return strtotime($time);
//    return strtotime($time) + 7 * 60 * 60;
}

function get_time_ago($time)
{
    $time_gmt7 = strtotime_gmt7($time);

    $ago = time() - $time_gmt7;

    $minute_time = 60;
    $hours_time = 60 * $minute_time;
    $day_time = 24 * $hours_time;
    $month_time = 30 * $day_time;
    $year_time = 365 * $day_time;

    $days = $ago / (60 * 60 * 24);
    $hours = $ago / (60 * 60);
    $mins = $ago / 60;

    if ($days < 1) {
        if ($hours >= 1) {
            $rs = (int)$hours . ' giờ trước';
        } else if ($mins >= 1) {
            $rs = (int)$mins . ' phút trước';
        } else {
            $rs = 'vài giây trước';
        }

    } else if ($ago / $month_time < 1) {
        $rs = ((int)$days) . " ngày trước";
    } else if ($ago / $year_time < 1) {
        $rs = ((int)($ago / $month_time)) . " tháng trước";
    } else
        $rs = ((int)($ago / $year_time)) . " năm trước";

    return $rs;
}

/**
 * get date from string (custom format)
 * @param $str
 * @param string $format
 * @return false|string
 */
function get_date_from_str($str, $format = "Y-m-d")
{
	$str = str_replace("/", "-", $str);
	return date($format, strtotime($str));
}