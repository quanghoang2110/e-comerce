<?php
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');
/**
 * String_standard_helper
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        quanghv
 */

/**
 * @param $object
 */
function load_object($object)
{
    include_once APPPATH . "objects/{$object}_object.php";
}

/**
 * @param $page_folder
 */
function load_page_action_btn($page_folder)
{
    $content = "{$page_folder}/page_action_btn";
    $path = APPPATH . "views/{$content}.php";
    if (!file_exists($path)) {
        $content = str_replace(Theme_object::$name, 'base', $content);
        $path = APPPATH . "views/{$content}.php";
        if (!file_exists($path)) {
            $content = ADMIN_MODULE . "/base/template/page_action_btn";
        }
    }
    $ci =& get_instance();
    $ci->load->view($content);
}

/**
 * @param string $opt
 * @return mixed
 */
function get_admin_session($opt)
{
    $ci =& get_instance();
    return $ci->session->userdata($opt);
}

function get_admin_theme_asset()
{
    return Theme_object::get_folder_admin_asset();
}

/**
 * @param $content
 * @param $data
 */
function load_view($content, $data = "")
{
    $ci = &get_instance();

    //check if file exists in THEME folder
    if (file_exists(MODULES_PATH_ADMIN_VIEWS . $content . ".php")) {
        $ci->load->view($content, $data);
    } else if (file_exists(MODULES_PATH_ADMIN_VIEWS . str_replace(Theme_object::$name, 'base', $content) . ".php")) {
        $ci->load->view(str_replace(Theme_object::$name, 'base', $content), $data);
    }
}

function load_td_name($value)
{
    load_view(Theme_object::get_file_in_template(BASE_ADMIN_TABLE_TD_NAME), ["value" => $value]);
}

/**
 * @param $td
 * @param string $index
 * @param string $item
 */
function load_table_td($td, $index = "", $item = "")
{
    $ci =& get_instance();
    if ($index) $arr['index'] = $index;
    if ($item) $arr['item'] = $item;
    if (isset($arr)) {
        $ci->load->vars($arr);
    }
    if (is_array($td)) {
        foreach ($td as $i) {
            load_view(Theme_object::get_file_in_template($i));
        }
    } else {
        load_view(Theme_object::get_file_in_template($td));
    }
}

/**
 * @param $element
 * @param string $label
 * @param string $field
 * @param string $field_value
 * @param string $help_block
 * @param string $attr
 */
function load_form_element($element, $label = '', $field = DF_VALUE, $field_value = DF_VALUE, $help_block = DF_VALUE, $attr = DF_VALUE)
{
    $obj = new Form_element_object();
    $obj->label = $label;
    $obj->field = $field;
    $obj->field_value = $field_value;
//	$obj->grid_col = $grid_col;
    $obj->attr = $attr;
    $obj->help_block = $help_block;

    load_view(Theme_object::get_file_in_template($element), array('obj' => $obj));
}

/**
 * @param $elements
 * @param bool $group
 * @param string $grid_col
 */
function load_form_elements($elements, $group = false, $grid_col = "")
{
    $obj = new Form_element_object();
    $obj->label = "";
    $obj->field = "";
    $obj->grid_col = $grid_col;


    echo $group ? "<div class='form-group'>" : '';
    foreach ($elements as $element) {
        load_view(Theme_object::get_file_in_template($element), array('obj' => $obj));
    }
    echo $group ? "</div>" : '';
}

/**
 * @param string $view
 * @param Form_element_object $obj
 * @internal param $Form_element_object $
 */
function load_form_element_object($view, Form_element_object $obj)
{
    load_view(Theme_object::get_file_in_template($view), array('obj' => $obj));
}

/**
 * @param $subfolder_str
 */
function filemanager_subfolder($subfolder_str)
{
    $ci = &get_instance();
    $ci->load->helper('cookie');
    delete_cookie(BASE_FILEMANAGER_SUBFOLDER_COOKIE);
    delete_cookie(BASE_FILEMANAGER_LATESTPOS_COOKIE);
    if (BASE_FILEMANAGER_SUBFOLDER) {
        $ci->load->helper('string');
        $base64 = base64_encode($subfolder_str);
        $base64 = substr_replace($base64, random_string("alnum", 1), BASE_FILEMANAGER_SUBFOLDER_POS1, 0);
        $base64 = substr_replace($base64, random_string("alnum", 1), BASE_FILEMANAGER_SUBFOLDER_POS2, 0);
        $base64 = base64_encode($base64);
        set_cookie(BASE_FILEMANAGER_SUBFOLDER_COOKIE, $base64, 7 * 24 * 3600);
    }
}