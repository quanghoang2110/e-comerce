<?php if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

/**
 * @param $module_name
 * @param $module_method
 * @param $params
 */
function load_mfw_module($module_name, $module_method = "index", ...$params)
{
    echo Modules::run("mfw/" . $module_name . "_controller/" . $module_method, ...$params);
}

function get_mfw_module($module_name, $module_method = "index", ...$params)
{
    return Modules::run("mfw/" . $module_name . "_controller/" . $module_method, ...$params);
}

function get_mfw_module_view($module_name, $module_method = "index", ...$params): View_object
{
    return Modules::run("mfw/" . $module_name . "_controller/" . $module_method, ...$params);
}