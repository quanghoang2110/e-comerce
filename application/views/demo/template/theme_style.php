<?php $this->load->helper('mfw_asset');?>
<?php load_base_css(BASE_ASSET_ROBOTO_FONT); ?>
<?php load_base_css(BASE_ASSET_MDB); ?>

<!-- Template styles -->
<style rel="stylesheet">
	/* TEMPLATE STYLES */

	main {
		margin-top: 3rem;
	}

	main .card {
		margin-bottom: 2rem;
	}

	@media only screen and (max-width: 768px) {
		.read-more {
			text-align: center;
		}
	}
    .slide a:hover{
        text-decoration: none;
    }
</style>