<script>
    /**
     * base scripts
     */
    <?php if(isset($modalMsg_show)):?>
    $("#modalMsg").modal("show");
    showThongBao("<?php echo $modalMsg_content ?>", 'home');
    <?php endif?>

    function showModal(idModal) {
        $('#' + idModal).modal('show');
        $('html').css('overflow', 'hidden');

        $('#' + idModal).on('hidden.bs.modal', function (e) {
            $('html').css('overflow', 'auto');
        });
    }
    function hideAllModal() {
        $('.modal').modal('hide');
    }
</script>

<!-- change pass -->
<!-- BEGIN FORM MODAL MARKUP -->
<div class="modal fade" id="modal_change_pass" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">Thay đổi mật khẩu</h4>
            </div>
            <form onsubmit="return submitchangePass()" class="form-horizontal form-validate floating-label" role="form"
                  novalidate="novalidate" method="post" id="form_change_pass">
                <div class="modal-body">
                    <div class="alert alert-danger hide" id="err_msg_" role="alert">
                        <strong>Oh snap!</strong>
                        <label id="_msg"></label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="cur_pass" class="control-label">Mật khẩu hiện tại</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="password" name="cur_pass" id="cur_pass" class="form-control"
                                   placeholder="Mật khẩu hiện tại" required aria-required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="new_pass" class="control-label">Mật khẩu mới</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="password" name="new_pass" id="new_pass" class="form-control"
                                   placeholder="Mật khẩu mới" required data-rule-minlength="6">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="re_new_pass" class="control-label">Mật khẩu mới</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="password" name="re_new_pass" id="re_new_pass" class="form-control"
                                   placeholder="Nhập lại mật khẩu mới" required data-rule-equalto="#new_pass">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->

<script>
    function changePass() {
        document.getElementById("form_change_pass").reset();
        $('#form_change_pass #err_msg_').addClass('hide');
        showModal('modal_change_pass');
    }
    function submitchangePass() {
        var status = 0;
        var msg = "";
        if ($('#cur_pass').val() == '') {
            msg = "<p>Vui lòng nhập mật khẩu hiện tại!</p>";
        } else if ($('#new_pass').val() == '') {
            msg = "<p>Vui lòng nhập mật khẩu mới.</p><p>Mật khẩu mới cần lớn hơn hoặc bằng 6 ký tự.</p>";
        } else if ($('#new_pass').val() != $('#re_new_pass').val()) {
            msg = "<p>Vui lòng nhập lại mật khẩu mới trùng với mật khẩu đã nhập.</p>";
        } else if ($('#new_pass').val().length < 6) {
            msg = "<p>Vui lòng nhập mật khẩu mới lớn hơn hoặc bằng 6 ký tự</p>";
        } else {
            status = 1;
        }

        if (status == 0) {
            showThongBao(msg, '', '#modal_change_pass');
            return false;
        }

        $('#form_change_pass #err_msg_').addClass('hide');
        $.post(urlChangePass, {
            cur_pass: $('#cur_pass').val(),
            new_pass: $('#new_pass').val()
        }).done(function (data) {
            var obj = $.parseJSON(data);
            if (obj.status == 0) {
                $('#modal_change_pass #err_msg_').removeClass('hide');
                $('#modal_change_pass #err_msg_ #_msg').text(obj.msg);

            } else {
                hideAllModal();
                if (obj.status == -1) {
                    showThongBao(obj.msg, 'reload');
                } else
                    showThongBao(obj.msg);
            }
        });
        return false;
    }
</script>

<!-- BEGIN FORM MODAL MARKUP -->
<div class="modal fade" id="modal_login" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content primary-info">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">Đăng nhập tài khoản</h4>
            </div>
            <form onsubmit="return submitchangePass()" class="form-horizontal form-validate floating-label" role="form"
                  novalidate="novalidate" method="post" id="form_change_pass">
                <div class="modal-body">
                    <div class="alert alert-danger hide" id="err_msg_" role="alert">
                        <strong>Oh snap!</strong>
                        <label id="_msg"></label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="cur_pass" class="control-label">Tài khoản</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="email" id="email" class="form-control" data-rule-email="true"
                                   placeholder="Email đăng nhập" required aria-required="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="new_pass" class="control-label">Mật khẩu</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="password" name="pass" id="pass" class="form-control"
                                   placeholder="Mật khẩu" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-primary">Đăng nhập</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->

<div class="modal" id="modal_msg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Nội dung thông báo</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="msgOK">Đồng ý</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    var afterThongBaoModal = "";
    function showThongBao(text, action='', onactionModal='') {
        $("#modal_msg .modal-body p").html(text);
        $("#modal_msg").modal("show");
        if (onactionModal) {
            afterThongBaoModal = onactionModal;
            $(onactionModal).modal('hide');
            afterThongBao();
        }
        if (action == 'home') {
            $('#modal_msg #msgOK').click(function () {
                location.href = '/';
            });
        }
        else if (action == 'reload') {
            $('#modal_msg #msgOK').click(function () {
                location.reload();
            });
        } else if (action == 'login') {
            $('#modal_msg #msgOK').click(function () {
//                location.href = '<?php //echo ROUTE_ADMIN_LOGIN ?>//';
            });
        }
    }
    function afterThongBao() {
        if (afterThongBaoModal) {
            $('#modal_msg').on('hidden.bs.modal', function (e) {
                $(afterThongBaoModal).modal("show");
                afterThongBaoModal = "";
            });
        }
    }
</script>