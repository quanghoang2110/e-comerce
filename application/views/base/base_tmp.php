<?php
$setting = new Setting_object($setting);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo get_text_not_empty($setting->meta_title, $setting->name) ?></title>
    <meta name="description"
          content="<?php echo $setting->meta_desc ?>"/>
    <meta name="keywords"
          content="<?php echo $setting->keyword ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--    facebook tag-->
    <meta property="fb:app_id" content="<?php echo $setting->fb_app_id ?>"/>
    <meta property="og:url"
          content="<?php echo get_text_not_empty($setting->meta_url, base_url()) ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="<?php echo $setting->meta_title ?>"/>
    <meta property="og:description" content="<?php echo $setting->meta_desc ?>"/>
    <meta property="og:image"
          content="<?php echo get_text_not_empty($setting->meta_image, $setting->logo) ?>"/>

    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:type" content="website"/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="<?php echo $setting->meta_title ?>"/>

    <link rel="shortcut icon" href="<?php echo $setting->favicon; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $setting->favicon; ?>" type="image/x-icon">

    <!-- load head tag -->
    <?php
    if (file_exists(APPPATH . "views/{$theme_template}/head.php"))
        $this->load->view("{$theme_template}/head");

    if (file_exists(APPPATH . "views/{$theme_template}/theme_style_min.php"))
        $this->load->view("{$theme_template}/theme_style_min");
    else
        if (file_exists(APPPATH . "views/{$theme_template}/theme_style.php"))
            $this->load->view("{$theme_template}/theme_style");
    ?>

</head>

<body>

<!--load content-->
<?php
//echo $theme_template;
$mfw_base_content = $theme_template . 'content';
if (file_exists(APPPATH . "views/{$mfw_base_content}.php"))
    $this->load->view($mfw_base_content) ?>

<?php include_once "base_js.php" ?>

<?php
if ($this->config->item('site_type') == 'ecomerce') {
    $this->load->view('public/base/ecomerce');
}
?>

<!--script-->
<?php
if (file_exists(APPPATH . "views/{$theme_template}/theme_js_min.php")) {
//    include_once "{$theme_template}/theme_js_min.php";
    $this->load->view("{$theme_template}/theme_js_min");
} else
    if (file_exists(APPPATH . "views/{$theme_template}/theme_js.php"))
        $this->load->view("{$theme_template}/theme_js");
?>

<div id="ajaxLoading">
    <?php include_once "loading_new.php" ?>
</div>

<?php if ($facebook_sdk): ?>
    <?php include_once "facebook_sdk.php" ?>
<?php endif; ?>

</body>

</html>