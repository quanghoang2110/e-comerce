<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 6/7/2017
 * Time: 10:44
 */
//filemanager subfolder
define('BASE_FILEMANAGER_SUBFOLDER', false);
define('BASE_FILEMANAGER_SUBFOLDER_COOKIE', "avaiable_folders");
define('BASE_FILEMANAGER_LATESTPOS_COOKIE', "last_position");
define('BASE_FILEMANAGER_SUBFOLDER_POS1', 3);
define('BASE_FILEMANAGER_SUBFOLDER_POS2', 5);
