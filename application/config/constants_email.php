<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 12:00
 */
/**
 * CONFIG PUBLIC ==================================================================================================
 */
define('MFW_MAIL_NAME', 'Chef Studio');
define('MFW_MAIL_DOMAIN', 'chefstudio.vn');
define('MFW_MAIL_URL', 'https://chefstudio.vn');