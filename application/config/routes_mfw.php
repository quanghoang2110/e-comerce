<?php

defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 12:00.
 */
require_once APPPATH . 'config/constants_mfw.php';

$route[BASE_ROUTE_FOR_DEV] = ADMIN_MODULE . '/config';
$route[BASE_ROUTE_DEV_ALL_FORM_ELEMENT] = ADMIN_MODULE . '/config/all_form_element';

$version = ADMIN_MODULE . '/';
$route[BASE_PAGE_NOT_FOUND] = $version . 'error/404';
$route[BASE_ROUTE_ADMIN_DASHBOARD] = $version . 'dashboard';
$route[BASE_ROUTE_ADMIN_LOGIN] = $version . 'login';
$route[BASE_ROUTE_ADMIN_LOGOUT] = $version . 'login/logout';
$route[BASE_ROUTE_ADMIN_PROFILE] = $version . 'profile';
$route[BASE_ROUTE_ADMIN_CHANGE_PASS] = $version . 'login/change_pass';
$route[BASE_ROUTE_ADMIN_RESET_PASS] = $version . 'admin/reset_pass';
$route[BASE_ROUTE_ADMIN_PERMISSION_VIEW_AJAX] = $version . 'admin_group/ajax_view_permission';
$route[BASE_ROUTE_ADMIN_PERMISSION_UPDATE_AJAX] = $version . 'admin_group/ajax_update_permission';

$route[BASE_ROUTE_CROP_IMAGE] = $version . 'admin/ajax_crop_image';

$route[MFW_ROUTE_UPDATE_FRONT_ROUTES] = $version . 'admin/update_frontend_routes';
$route[MFW_ROUTE_USER_SUBSCRIBE] = 'public/home/ajax_subscribe';

$route[MFW_ROUTE_UPLOAD_IMG] = '/uploads/r/';

$route[MFW_CART_ROUTE_ADD_COMMENT] = 'public/comment/add_comment';
$route[MFW_CART_ROUTE_REMOVER_COMMENT] = 'public/comment/remove_comment';
$route[MFW_CART_ROUTE_REFRESH_COMMENT] = 'public/comment/refresh_comment';
$route[MFW_CART_ROUTE_LOGOUT_COMMENT] = 'public/comment/logout_comment';
$route[MFW_CART_ROUTE_UPLOAD_IMAGE_COMMENT] = 'public/comment/upload';
//$route[MFW_CART_ROUTE_LOGOUT_COMMENT] = 'public/comment/remove_comment';
$route[MFW_NEXT_PAGE_COMMENT] = 'public/comment/load_page_comment';
$route[BASE_ROUTE_REPLY_COMMENT] = $version . 'comment/reply_comment_ajax';
$route[MFW_LOGIN] = 'public/login/index';
$route[MFW_REGISTER] = 'public/login/register';
$route[MFW_POST_REGISTER] = 'public/login/post_register';
$route[MFW_POST_LOGIN] = 'public/login/post_login';
$route[MFW_LOGOUT] = 'public/login/logout';
$route[MFW_USER_PROFILE] = 'public/user/user_profile';
$route[MFW_UPDATE_USER_PROFILE] = 'public/user/update_profile';
$route[MFW_UPDATE_USER_PASSWORD] = 'public/user/update_password';
$route[MFW_ORDER_FINISHED] = 'public/user/order_finished';
$route[MFW_ORDER_WAITTING] = 'public/user/order_wait';
$route[MFW_CART_PAYMENT_VNPAY] = 'public/cart/paymentVNPay';
$route[MFW_CART_PAYMENT_VNPAY_SUCCESS] = 'public/cart/payment_success';
$route[MFW_CART_PAYMENT_CALL_VNPAY_SUCCESS] = 'public/cart/callbackVNPay';

if (MFW_CART_ENABLED) {
    $route[MFW_CART_ROUTE_ADD_PRODUCT] = 'public/cart/add_product';
    $route[MFW_CART_ROUTE_REMOVE_PRODUCT] = 'public/cart/remove_product';
    $route[MFW_CART_ROUTE_REFRESH] = 'public/cart/cart_refresh';
    $route[MFW_CART_ROUTE_CART] = 'public/cart/index';
    $route[MFW_CART_ROUTE_INFO_SHIPPING] = 'public/cart/info_shipping';
    $route[MFW_CART_ROUTE_METHOD_PAYMENT] = 'public/cart/method_payment';
    $route[MFW_CART_ROUTE_UPDATE] = 'public/cart/update';
    $route[MFW_CART_ROUTE_SUCCESS] = 'public/cart/success';
}

$route[MFW_LIST_NEWS] = 'public/new/index';
//print_r($route);

/**
 * append routes from database -----------------------------------------------------------------------------------------.
 */
include_once APPPATH . 'config/routes_mfw_db.php';

