<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 12/16/2016
 * Time: 12:00
 */
/**
 * CONFIG PUBLIC ==================================================================================================
 */
define('PUBLIC_THEME', 'chef');
define('BASE_CONF_LIMIT', 12);
define('BASE_CONF_LIMIT_BACKEND', 10);
define('PUBLIC_ASSETS_FOLDER', "/assets/public/" . PUBLIC_THEME . "/");
/**
 * BASE CONFIG
 */
define('MFW_CART_ENABLED', TRUE);
//assets
define('ASSET_FULL_URL', true);
define('BASE_ASSET_ROBOTO_FONT', '/assets/base/lib/roboto/font.roboto.css');
define('BASE_CSS_FONT_ROBOTO', '/assets/base/lib/roboto/font.roboto.css');
define('BASE_CSS_FONT_AWESOME', '/assets/base/lib/font-awesome-4.7.0/css/font-awesome.min.css');
define('BASE_CSS_BOOTSTRAP', '/assets/base/lib/bootstrap-3.3.7/css/bootstrap.min.css');
define('BASE_JS_BOOTSTRAP', '/assets/base/lib/bootstrap-3.3.7/js/bootstrap.min.js');
define('BASE_ASSET_BOOTSTRAP', '/assets/base/lib/bootstrap-3.3.7/css/bootstrap.min.css');
define('BASE_ASSET_MDB', 'mdb_bootstrap');
//folder
define("ADMIN_MODULE", "admin");
define('NO_SUFFIX', 'NO_SUFFIX');
//img
define('BASE_IMG_DEFAULT', '/images/img_df.jpg');

/**
 * SITE DEFINED
 */

define('BASE_SITE_NAME', 'GO');


define('BASE_ADMIN_IMG_CROPPED_DF', FALSE);

///v1-alt admin
//controllers-oneui admin

/**
 * admin activity log
 */
define('BASE_CONF_LOG_ADMIN_ACTIVITY', TRUE);
define('BASE_ADMIN_ACTION_DELETE', 'DELETE');
define('BASE_ADMIN_ACTION_UPDATE', 'UPDATE');
define('BASE_ADMIN_ACTION_INSERT', 'INSERT');

/**
 * EXTRA FOLDER DATA
 */
define('MODULES_PATH', APPPATH . "modules/");
define('MODULES_PATH_ADMIN_CONTROLLER', APPPATH . "modules/" . ADMIN_MODULE . "/controllers/");
define('MODULES_PATH_ADMIN_VIEWS', APPPATH . "modules/" . ADMIN_MODULE . "/views/");
define('BASE_CONF_PERMISSION_REQUIRED', TRUE);
define('BASE_CONF_FILE_MENU_ROOT', APPPATH . 'm_cache/menu_root');
define('BASE_CONF_FILE_MENUGROUP_FOLDER', APPPATH . 'm_cache/menu_group/');

//site setting
define('BASE_CONF_FILE_SITE_SETTING', APPPATH . 'm_cache/site_setting');

/**
 * DB DEFINED
 */
define('BASE_AUTOLOAD_DATABASE', TRUE);
define('BASE_TABLE_ADMIN_LOG', 'admin_log');
define('BASE_TABLE_ADMIN', 'admin');
define('BASE_ADMIN_PASS_DF', '123456');
define('BASE_TABLE_ADMINGROUP', 'admin_group');
define('BASE_TABLE_ADMIN_MENU', 'admin_menu');
define('BASE_TABLE_ADMIN_MENUHEAD', 'admin_menuhead');
define('BASE_TABLE_ADMIN_ROLE', 'admin_permission'); //permission
define('BASE_TABLE_ADMIN_ROLE_GROUP', 'admin_permission_group'); //admin_group

define('BASE_DATA_TYPE_VALID', 'valid');
define('BASE_DATA_TYPE_HOT', 'hot');
define('BASE_DATA_TYPE_HOME', 'is-home');

define('BASE_TABLE_SITE_SETTING', 'site_setting'); //cau hinh thong tin website

/**
 * SESSION DEFINE
 */
define('BASE_SESSION_ADMIN_GROUPID', 'session_groupid_abcxyz');
define('BASE_SESSION_ADMIN_ID', 'session_id_abcxyz');
define('BASE_SESSION_ADMIN_IS_ROOT', 'session_id_is_root');
define('BASE_SESSION_ADMIN_USERNAME', 'session_username_abcxyz');
define('BASE_SESSION_ADMIN_FULLNAME', 'session_fullname_abcxyz');
define('BASE_SESSION_ADMIN_IMG', 'session_admin_img_abcxyz');

/**
 * PAGING DEFINE
 */
//define('SESSION_BASE_IS_ROOT', 'session_id_is_root');

/**
 * ROUTE
 */
define('BASE_ROUTE_FOR_DEV', 'for-dev');
define('BASE_ROUTE_DEV_ALL_FORM_ELEMENT', 'for-dev/all-element');

define('BASE_ROUTE_ADMIN_DASHBOARD', 'quan-tri');
define('BASE_PAGE_NOT_FOUND', BASE_ROUTE_ADMIN_DASHBOARD . '/page-not-found');
define('BASE_ROUTE_ADMIN_LOGIN', BASE_ROUTE_ADMIN_DASHBOARD . '/dang-nhap');
define('BASE_ROUTE_ADMIN_LOGOUT', BASE_ROUTE_ADMIN_DASHBOARD . '/dang-xuat');
define('BASE_ROUTE_ADMIN_PROFILE', BASE_ROUTE_ADMIN_DASHBOARD . '/profile');
define('BASE_ROUTE_ADMIN_CHANGE_PASS', BASE_ROUTE_ADMIN_DASHBOARD . '/doi-mat-khau');
define('BASE_ROUTE_ADMIN_RESET_PASS', BASE_ROUTE_ADMIN_DASHBOARD . '/reset-mat-khau');
define('BASE_ROUTE_ADMIN_PERMISSION_VIEW_AJAX', BASE_ROUTE_ADMIN_DASHBOARD . '/ajax/view-permission');
define('BASE_ROUTE_ADMIN_PERMISSION_UPDATE_AJAX', BASE_ROUTE_ADMIN_DASHBOARD . '/ajax/update-permission');
define('BASE_ROUTE_ADMIN_XXX_DETAIL', 'chi-tiet');
define('BASE_ROUTE_ADMIN_XXX_VALID', 'kha-dung');
define('BASE_ROUTE_ADMIN_XXX_UPDATE', 'cap-nhat');
define('BASE_ROUTE_ADMIN_XXX_UPDATE_POSITION', 'cap-nhat-vi-tri');
define('BASE_ROUTE_ADMIN_XXX_DELETE', 'xoa');
//
define('BASE_ROUTE_CROP_IMAGE', 'crop-image');


//=============== TEMPLATE =========================
define('DF_VALUE', 'ASDFWEREAFDSFSDA');
/**
 * TABLE TMP
 */
define('BASE_ADMIN_TABLE_TD_STT', '/page_table_td_stt');
define('BASE_ADMIN_TABLE_TD_IMG', '/page_table_td_image');
define('BASE_ADMIN_TABLE_TD_NAME', '/page_table_td_name');
define('BASE_ADMIN_TABLE_TD_POSITION', '/page_table_td_position');
define('BASE_ADMIN_TABLE_TD_VALID', '/page_table_td_valid');
define('BASE_ADMIN_TABLE_TD_HOT', '/page_table_td_hot');
define('BASE_ADMIN_TABLE_TD_ACTION', '/page_table_td_action');
/**
 * FORM ELEMENT TMP
 */
define('BASE_ADMIN_FORM_ELEMENT_NAME', '/form_element_name');
define('BASE_ADMIN_FORM_ELEMENT_CATEGORY', '/form_element_category');
define('BASE_ADMIN_FORM_ELEMENT_IMG', '/form_element_img');
define('BASE_ADMIN_FORM_ELEMENT_IMG2', '/form_element_img2');
define('BASE_ADMIN_FORM_ELEMENT_POSITION', '/form_element_position');
define('BASE_ADMIN_FORM_ELEMENT_VALID', '/form_element_valid');
define('BASE_ADMIN_FORM_ELEMENT_HOT', '/form_element_hot');
define('BASE_ADMIN_FORM_ELEMENT_DESC', '/form_element_desc');
define('BASE_ADMIN_FORM_ELEMENT_DESC_EDITOR', '/form_element_desc_editor');
define('BASE_ADMIN_FORM_ELEMENT_CONTENT', '/form_element_content');
define('BASE_ADMIN_FORM_ELEMENT_SEO', '/form_element_seo');
define('BASE_ADMIN_FORM_ELEMENT_ACTION', '/form_element_action');

/**
 * script
 */
define('BASE_ADMIN_FORM_ID_FORM', 'id_form');
define('BASE_ADMIN_FORM_ID_TINYMCE', 'id_tinymce');

define('MFW_ROUTE_UPDATE_FRONT_ROUTES', 'mfw-update-front-routes');
define('MFW_ROUTE_USER_SUBSCRIBE', 'nhan-thong-bao.html');

define("FACBOOK_ACTION_LIKE", "like");
define("FACBOOK_ACTION_COMMENT", "comment");

define("GOOGLE_CAPTCHA_SITE_KEY", "6Lch3ncUAAAAAArWSOSsg4bxw11pAsVfqoVqZT5n");
define("GOOGLE_CAPTCHA_SECRET_KEY", "6Lch3ncUAAAAAKIMTXLIg3bAO3dvXpqLqw3ttmMz");

define('NEWS_TYPE_CONTENT', 'CONTENT');
define('NEWS_TYPE_VIDEO', 'VIDEO');
define('NEWS_TYPE_MANUAL', 'MANUAL');
define('NEWS_TYPE_DISCOUNT', 'DISCOUNT');

define('PAGE_TYPE_VE_CHUNG_TOI', 'VE CHUNG TOI');
define('PAGE_TYPE_THANH_TOAN', 'THANH TOAN');
define('PAGE_TYPE_CHINH_SACH', 'CHINH SACH');
define('PAGE_TYPE_CAU_HOI', 'CAU HOI THUONG GAP');

define('MFW_ROUTE_UPLOAD_IMG', 'public-image');
define('MFW_COMMENT_COOKIE', 'mfw_cookie_comment');
define("MFW_CART_ROUTE_ADD_COMMENT", "add-comment");
define("MFW_CART_ROUTE_REFRESH_COMMENT", "comment-refresh");
define("MFW_CART_ROUTE_LOGOUT_COMMENT", "comment-logout");
define("MFW_CART_ROUTE_REMOVER_COMMENT", "remove-comment");
define("MFW_CART_ROUTE_UPLOAD_IMAGE_COMMENT", "upload-image");
define("MFW_NEXT_PAGE_COMMENT", "next-page-comment");
define("BASE_ROUTE_REPLY_COMMENT", BASE_ROUTE_ADMIN_DASHBOARD . "/reply-comment");
define("MFW_LIST_NEWS", "tin-tuc");

include_once 'constants_filemanager.php';

//if using card
if (MFW_CART_ENABLED) {
    include_once 'constants_cart.php';
}

include_once "constants_email.php";
