<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

require_once APPPATH.'config/routes_mfw.php';
require_once APPPATH.'config/routes_mfw_front_db.php';

$route['admin-config/routes'] = 'admin/config/admin_routes';
$route['admin-config/admin-menu'] = 'admin/config/admin_menu';
$route['admin-config/menu-sys'] = 'admin/config/menu_conf';
$route['admin-config/run/(:any)'] = 'admin/config/controller/$1';

$route['default_controller'] = 'public/home';
$route['translate_uri_dashes'] = false;

$route[ROUTE_HOME] = 'public/home';
$route[ROUTE_SEARCH] = 'public/product/search';
$route[ROUTE_CONTACT_US] = 'public/contact/index';
$route[ROUTE_CONTACT_US_SUCCESS] = 'public/contact/success';
$route[ROUTE_AGENCY] = 'public/agency';
$route[ROUTE_FAQ] = 'public/faq';
$route[ROUTE_FAQ.'/(:any)'] = 'public/faq/index/$1';
$route[ROUTE_DISCOUNT] = 'public/news/news_discount';
$route[ROUTE_FILTER] = 'public/product/filter_product';
//$route[ROUTE_TRADEMARK] = 'public/product/trademark';
//$route[ROUTE_CONTACT] = "public/contact";
//$route[ROUTE_SEARCH] = "public/search";
//$route[ROUTE_PRODUCT_QUICKVIEW] = "public/product/quick_view";
//$route[ROUTE_CART] = "public/cart";
//$route[ROUTE_CART_REFRESH] = "public/cart/refresh";
//$route[ROUTE_CART_ADD_PRODUCT] = "public/cart/add_product";
//$route[ROUTE_CART_ADD_PRODUCT_REVIEW] = "public/cart/add_product_review";

$uri = $this->uri->segment(1);
$slug_arr = explode('-', str_replace('.html', '', $uri));
$id = $slug_arr ? $slug_arr[count($slug_arr) - 1] : '';
if ($id > 0) {
    $route[$uri] = "public/page/content/{$id}";
}

$uri_segment_1 = $this->uri->segment(1);
$uri_segment_2 = $this->uri->segment(2);
$uri_segment_3 = $this->uri->segment(3);
if($uri_segment_2 == 'brands'){
    $route[$uri_segment_1.'/'.$uri_segment_2.'/'.$uri_segment_3] = $route[$uri_segment_1];
}

$theme = PUBLIC_THEME;
$route['demo'] = 'mfw/slider';
$route['404_override'] = 'public/error_page';
// print_r($route);
//$route['default_controller'] = "setup";
