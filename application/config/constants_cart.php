<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 6/7/2017
 * Time: 10:44.
 */
//cart
define('MFW_CART_COOKIE', 'mfw_cookie_cart');
define('MFW_CART_ROUTE_ADD_PRODUCT', 'add-product-to-cart');
define('MFW_CART_ROUTE_REMOVE_PRODUCT', 'remove-product-from-cart');
define('MFW_CART_ROUTE_REFRESH', 'cart-refresh.html');
define('MFW_CART_ROUTE_CART', 'gio-hang.html');
define('MFW_CART_ROUTE_UPDATE', 'dat-hang.html');
define('MFW_CART_ROUTE_INFO_SHIPPING', 'thong-tin-chuyen-hang.html');
define('MFW_CART_ROUTE_METHOD_PAYMENT', 'chon-phuong-thuc-thanh-toan.html');
define('MFW_CART_ROUTE_SUCCESS', 'dat-hang-thanh-cong.html');
define('MFW_LOGIN', 'dang-nhap.html');
define('MFW_REGISTER', 'dang-ky.html');
define('MFW_POST_REGISTER', 'luu-dang-ky.html');
define('MFW_POST_LOGIN', 'post-login.html');
define('MFW_LOGOUT', 'dang-xuat.html');
define('MFW_USER_PROFILE', 'thong-tin-tai-khoan.html');
define('MFW_UPDATE_USER_PROFILE', 'cap-nhat-thong-tin-tai-khoan.html');
define('MFW_UPDATE_USER_PASSWORD', 'cap-nhat-mat-khau-tai-khoan.html');
define('MFW_ORDER_FINISHED', 'don-hang-da-giao.html');
define('MFW_ORDER_WAITTING', 'don-hang-cho-giao.html');
define('MFW_CART_PAYMENT_VNPAY', 'thanh-toan-vnpay.html');
define('MFW_CART_PAYMENT_VNPAY_SUCCESS', 'thanh-toan-thanh-cong.html');
define('MFW_CART_PAYMENT_CALL_VNPAY_SUCCESS', 'callback-payment-vnpay.html');
