<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */


/**
 * Class Model_permission_group
 */
class Model_role extends MModel
{
	function __construct()
	{
		parent::__construct();
		$this->table_name = BASE_TABLE_ADMIN_ROLE;
	}

	function get_list()
	{
		$this->db->select('*');
		$this->db->order_by('category_name');
		$this->db->group_by('category,category_name');
		return $this->db->get($this->table_name)->result();
	}

	function count_list()
	{
		$this->db->from($this->table_name);
		$this->db->group_by('category,category_name');
		$this->db->count_all_results();
	}

	function get_permission($group_id)
	{
		$this->db->select('a.*');
		$this->db->from(BASE_TABLE_ADMIN_ROLE . " as a");
		$this->db->join(BASE_TABLE_ADMIN_ROLE_GROUP . " as b", 'a.id=b.permission_id', 'left');
		$this->db->where('b.group_id', $group_id);
		$this->db->order_by('category', 'asc');
		$this->db->order_by('id', 'asc');
		return $this->db->get()->result();
	}

	function get_permission_read($group_id)
	{
		$this->db->where('a.key', 'read');
		return $this->get_permission($group_id);
	}

	function get_group_permission($group_id)
	{
		$query = "(case when (SELECT COUNT(*) FROM " . BASE_TABLE_ADMIN_ROLE_GROUP . " b WHERE b.group_id='{$group_id}' AND b.permission_id=a.id)=1 then 'checked' else '' end) checked";
		$this->db->select("a.*");
		$this->db->select($query, false);
		$this->db->from(BASE_TABLE_ADMIN_ROLE . " as a");
		$this->db->order_by('category', 'asc');
		$this->db->order_by('id', 'asc');
		return $this->db->get()->result();
	}
}