<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 1/23/2017
 * Time: 10:35
 */
class Model_product extends MModel
{
	function get_list($page, $limit, $search="", $category = "")
	{
		$this->db->select("a.*, b.name as category");
		$this->db->from('product a');
		$this->db->join('product_category b', 'a.category_id=b.id', 'left');
		if($category){
			$this->db->where_in('a.category_id', $category);
		}
		if($search){
            $this->db->group_start();
			$this->db->like("a.name", $search);
			$this->db->or_like("a.code", $search);
            $this->db->group_end();
		}
//		$this->db->where('')
		$this->db->limit($limit, ($page - 1) * $limit);
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
	}

    public function _get_category_children($cate_id)
    {
        $this->db->select('id');
        $this->db->from('product_category');
        $this->db->where('parent_id', $cate_id);
        $this->db->where('valid', 1);
        $childrens = $this->db->get()->result();
        $__array_id[] = $cate_id;
        if (!empty($childrens)) {
            foreach ($childrens as $item) {
                $__array_id[] = $item->id;
                $this->db->select('id');
                $this->db->from('product_category');
                $this->db->where('parent_id', $item->id);
                $this->db->where('valid', 1);
                $children_1 = $this->db->get()->result();
                if (!empty($children_1)) {
                    foreach ($children_1 as $item_1) {
                        $__array_id[] = $item_1->id;
                        $this->db->select('id');
                        $this->db->from('product_category');
                        $this->db->where('parent_id', $item_1->id);
                        $this->db->where('valid', 1);
                        $children_2 = $this->db->get()->result();
                        if (!empty($children_2)) {
                            foreach ($children_2 as $item_2) {
                                $__array_id[] = $item_2->id;
                                $this->db->select('id');
                                $this->db->from('product_category');
                                $this->db->where('parent_id', $item_2->id);
                                $this->db->where('valid', 1);
                                $children_3 = $this->db->get()->result();
                                if (!empty($children_3)) {
                                    foreach ($children_3 as $item_3) {
                                        $__array_id[] = $item_3->id;
                                    }
                                };
                            }
                        }
                    }
                }
            }
        }
        return $__array_id;
    }
}