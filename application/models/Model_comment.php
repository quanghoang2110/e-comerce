<?php
/**
 * Copyright (c) $today.year.Go Solutions Jsc.
 */

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 1/23/2017
 * Time: 10:35
 */
class Model_comment extends MModel
{
    /**
     * @param $page
     * @param $limit
     * @param $id
     * @param $email
     * @param $type
     * @return mixed
     */
    function getList($page, $limit, $id, $type, $email)
    {
        $this->db->from('customer_comment');
        if ($email != NULL) {
            $this->db->where('(parent_id IS NULL AND type=' . $type . ' AND valid = 1 AND is_deleted = 0 AND category_id =' . $id . ')');
            $this->db->or_where('(parent_id IS NULL  AND type=' . $type . ' AND valid = 0 AND is_deleted = 0 AND category_id =' . $id . ' AND email="' . $email . '")');
        } else {
            $this->db->where('parent_id', null);
            $this->db->where('type', $type);
            $this->db->where('valid', 1);
            $this->db->where('is_deleted', 0);
            $this->db->where('category_id', $id);
        }
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, ($page - 1) * $limit);
        $comment_parent = $this->db->get()->result();

        if ($comment_parent) {
            foreach ($comment_parent as $item) {
                $this->db->select("a.*");
                $this->db->from('customer_comment a');
                if ($email != NULL) {
                    $this->db->where('(a.parent_id=' . $item->id . '  AND a.valid = 1 AND a.is_deleted = 0 AND a.category_id =' . $id . ')');
                    $this->db->or_where('(a.parent_id=' . $item->id . ' AND a.valid = 0 AND a.is_deleted = 0 AND a.category_id =' . $id . ' AND a.email="' . $email . '")');
                } else {
                    $this->db->where('(a.parent_id=' . $item->id . ' AND a.valid = 1 AND a.is_deleted = 0 AND a.category_id =' . $id . ')');
                }
                $this->db->order_by('a.id', 'asc');
                $comment_children = $this->db->get()->result();
                $item->children = $comment_children;
            }
        }

        return $comment_parent;
    }
}