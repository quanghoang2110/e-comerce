<?php

/**
 * Created by PhpStorm.
 * User: Hoang Van Quang
 * Date: 1/23/2017
 * Time: 10:35
 */
class Model_news_discount extends MModel
{
	function get_list($page, $limit, $search="", $category = "")
	{
		$this->db->select("a.*, b.name as category");
		$this->db->from('news a');
		$this->db->join('news_category b', 'a.category_id=b.id', 'left');
		if($category){
			$this->db->where('a.category_id', $category);
		}
		if($search){
			$this->db->like("a.name", $search);
		}
		$this->db->where('type', NEWS_TYPE_DISCOUNT);
		$this->db->limit($limit, ($page - 1) * $limit);
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
	}
}