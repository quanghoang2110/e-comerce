<main>
    <div class="container mfw-container">

        <?php load_mfw_module('breadcrumb', 'index', $category, $data->name) ?>

        <div class="row">
            <div class="d-none d-lg-block d-xl-block col-lg-3 mfw-side-left">
                <?php load_mfw_module("product", "best_seller", 5); ?>
            </div>
            <div class="col-lg-9">
                <section class="my-4">
                    <!-- Grid row -->
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="sp-loading"><img src="/images/sp-loading.gif" alt=""><br>LOADING</div>
                            <div class="sp-wrap">
                                <?php $imgs = json_decode($data->image); ?>
                                <?php foreach ($imgs as $img): ?>
                                    <a href="<?php echo get_img_url($img) ?>">
                                        <?php echo get_img_tag($img, $data->name); ?>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="col-lg-6 mfw-product-content">
                            <div class="mfw-title">
                                <h1><?php echo $data->name ?></h1>
                            </div>
                            <div class="mfw-price">
                                <?php
                                $price_1 = $data->price_1;
                                $price_2 = $data->price_2;
                                ?>
                                <?php if ($price_1 && $price_2): ?>
                                    <span class="mfw-sale"><?php echo get_price($price_2) ?></span>
                                    <span class="mfw-raw"><?php echo get_price($price_1) ?></span>
                                    <span class="mfw-discount"><?php echo get_discount_price($price_1, $price_2) ?></span>
                                <?php else: ?>
                                    <span class="mfw-sale"><?php echo get_price($price_2) ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="product-info mt-3">
                                <?php
                                $pice = $data->pice;
                                $code = $data->code;
                                ?>
                                <?php if ($pice): ?>
                                    <div>Đơn vị: <?php echo $pice ?></div>
                                <?php endif ?>

                                <?php if ($code): ?>
                                    <div>Mã sản phẩm: <?php echo $code ?></div>
                                <?php endif ?>
                                <div>Tình
                                    trạng: <?php echo $data->is_empty ? "<label style='color:red'>Hết hàng</label>" : 'Còn hàng' ?></div>
                            </div>
                            <div class="product-desc mt-3 py-3">
                                <span><?php echo $data->desc ?></span>
                            </div>
                            <?php if (!$data->is_empty): ?>
                                <div class="product-add-cart mt-3">
                                    <div class="item-num mr-3">
                                        <div class="quantity"><input id="productQuanlity" type="text" name="num" min="1"
                                                                     value="1"
                                                                     class="input-number"/></div>
                                        <div class="btn-plus">
                                            <a href="#add" class="btn-plus-up">
                                                <i class="fa fa-caret-up"></i>
                                            </a>
                                            <a href="#minus" class="btn-plus-down">
                                                <i class="fa fa-caret-down"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <button type="button"
                                            onclick="addToCart(<?php echo $data->id ?>, $('#productQuanlity').val())"
                                            class="btn mfw-btn btn-add-cart m-0"
                                            product-id="<?php echo $data->id ?>">
                                        <i class="fa fa-cart-plus"
                                           aria-hidden="true"></i> &nbsp;Thêm vào giỏ hàng
                                    </button>
                                </div>
                            <?php endif ?>
                            <div class="mt-3">
                                <?php
                                $fb_obj = new Facebook_object();
                                $fb_obj->url = base_url(get_detail_url($data->category_slug, $data->name, $data->id));
                                $fb_obj->data_size = "large";
                                $fb_obj->data_layout = "button";
                                $fb_obj->method = FACBOOK_ACTION_LIKE;
                                ?>
                                <?php load_mfw_module('facebook', 'index', $fb_obj) ?>
                            </div>
                        </div>

                        <?php if ($data->content): ?>
                            <div class="col-12 mt-3 product-info">
                                <div class="mfw-news-title mfw-header-title">
                                    <label>Thông tin sản phẩm</label>
                                </div>
                                <div class="product-content">
                                    <?php echo $data->content ?>
                                    <div class="">&nbsp;</div>
                                </div>
                                <div class="read-more">
                                    <a href="#read-more">Xem thêm</a>
                                </div>
                            </div>
                        <?php endif ?>

                        <div class="mt-3">
                            <?php
                            $fb_obj = new Facebook_object();
                            $fb_obj->url = base_url(get_detail_url($data->category_slug, $data->name, $data->id));
                            $fb_obj->method = FACBOOK_ACTION_COMMENT;
                            ?>
                            <?php load_mfw_module('facebook', 'index', $fb_obj) ?>
                        </div>
                        <div class="col-12">
                            <div class="mfw-news-title mfw-header-title">
                                <h3>Sản phẩm tương tự</h3>
                            </div>
                            <?php load_mfw_module("product", "same", $data->id, 5); ?>
                        </div>
                        <!--                        <div class="col-12 my-5">-->
                        <!--                            <div class="mfw-news-title mfw-header-title">-->
                        <!--                                <h3>Sản phẩm đã xem</h3>-->
                        <!--                            </div>-->
                        <!--                            --><?php //load_mfw_module("product", "same", $data->id, 5); ?>
                        <!--                        </div>-->
                    </div>
                    <!-- Grid row -->
                </section>
            </div>
        </div>
    </div>
</main>