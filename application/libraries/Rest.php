<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . "/third_party/Requests.php";

class Rest
{
    function Rest()
    {
        Requests::register_autoloader();
    }

    function post($url, $data)
    {
        $response = Requests::post($url, array(), $data);
        if ($response) {
            return json_decode($response->body);
        }
        return null;
    }

    function get($url)
    {
        $response = Requests::get($url);
        if ($response) {
            return json_decode($response->body);
        }
        return null;
    }
}